/* Variables */

float4x4 WorldViewProjection;

texture TextureMap;
sampler2D textureMapSampler = sampler_state
{
	Texture = (TextureMap);
	MagFilter = Point;
	MinFilter = Point;
	AddressU = Clamp;
	AddressV = Clamp;
};

float4x4[] SkinTransforms;

/* Vertex shader input structures */

struct VertexShaderInput
{
	float4 Position : position0;
	float2 TexCoord : texcoord0;
	float4 BlendIndices : blendindices0;
	float4 BlendWeights : blendweights0;
};

/* Vertex shader output structures */

struct VertexShaderOutput
{
	float4 Position : position0;
	float2 TexCoord : texcoord0;
};

/* Vertex shaders */

VertexShaderOutput AmbientVertexShader(VertexShaderInput pVertexInput)
{
	VertexShaderOutput vertexOutput;

	float4x4 transform = SkinTransfirms[pVertexInput.BlendIndices.x] * pVertexInput.BlendWeight.x;
	transform += SkinTransfirms[pVertexInput.BlendIndices.y] * pVertexInput.BlendWeight.y;
	transform += SkinTransfirms[pVertexInput.BlendIndices.z] * pVertexInput.BlendWeight.z;
	transform += SkinTransfirms[pVertexInput.BlendIndices.w] * pVertexInput.BlendWeight.w;
	vertexOutput.Position = mul(mul(viewPosition, transform), WorldViewProjection);

	vertexOutput.TexCoord = pVertexInput.TexCoord;

	return vertexOutput;
}

/* Pixel shaders */

float4 AmbientPixelShader(VertexShaderOutput pVertexOutput) : color0
{
	float4 texColor = tex2D(textureMapSampler, pVertexOutput.TexCoord);
	texColor.a = 1;

	return texColor;
}

/* Techniques */

technique Ambient
{
	pass Pass1
	{
		VertexShader = compile vs_2_0 AmbientVertexShader();
		PixelShader = compile ps_2_0 AmbientPixelShader();
	}
}