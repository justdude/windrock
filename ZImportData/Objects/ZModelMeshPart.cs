﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace ZImportData.Objects
{
    public class ZModelMeshPart
    {
        /* Fields */

        private int verticesCount;
        private int trianglesCount;
        private VertexBuffer vertexBuffer;
        private IndexBuffer indexBuffer;
        private Effect effect;

        /* Properties */

        public int VerticesCount
        {
            get
            {
                return this.verticesCount;
            }
            set
            {
                this.verticesCount = value;
            }
        }
        public int TrianglesCount
        {
            get
            {
                return this.trianglesCount;
            }
            set
            {
                this.trianglesCount = value;
            }
        }
        public VertexBuffer VertexBuffer
        {
            get
            {
                return this.vertexBuffer;
            }
            set
            {
                this.vertexBuffer = value;
            }
        }
        public IndexBuffer IndexBuffer
        {
            get
            {
                return this.indexBuffer;
            }
            set
            {
                this.indexBuffer = value;
            }
        }
        [ContentSerializer(SharedResource = true)]
        public Effect Effect
        {
            get
            {
                return this.effect;
            }
            set
            {
                this.effect = value;
            }
        }

        /* Constructors */

        public ZModelMeshPart()
        {
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}