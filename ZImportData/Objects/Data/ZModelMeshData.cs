﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ZImportData.Objects.Data
{
    public class ZModelMeshData
    {
        /* Fields */

        private Vector3[] positions;
        protected ZModelMeshGeometryData[] geometries;

        /* Properties */

        public Vector3[] Positions
        {
            get
            {
                return this.positions;
            }
        }
        public ZModelMeshGeometryData[] Geometries
        {
            get
            {
                return this.geometries;
            }
        }

        /* Constructors */

        public ZModelMeshData(Vector3[] pPositions, ZModelMeshGeometryData[] pGeometries)
        {
            this.positions = pPositions;
            this.geometries = pGeometries;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}