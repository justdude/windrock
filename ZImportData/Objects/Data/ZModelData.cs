﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ZImportData.Objects.Data
{
    public class ZModelData
    {
        /* Fields */

        protected ZModelMeshData[] meshes;
        protected Dictionary<string, int> meshIndices;

        /* Properties */

        [ContentSerializer]
        public ZModelMeshData[] Meshes
        {
            get
            {
                return this.meshes;
            }
        }
        public Dictionary<string, int> MeshIndices
        {
            get
            {
                return this.meshIndices;
            }
        }
        public int MeshCount
        {
            get
            {
                return this.meshes.Length;
            }
        }

        /* Constructors */

        public ZModelData( ZModelMeshData[] pMeshes, Dictionary<string, int> pMeshIndices)
        {
            this.meshes = pMeshes;
            this.meshIndices = pMeshIndices;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}