﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ZImportData.Objects.Data
{
    public class ZModelMeshGeometryData
    {
        /* Fields */

        private int[] vertexIndices;
        private ZChannel[] channels;
        private Dictionary<string, int> channelIndices;

        /* Properties */

        public int[] VertexIndices
        {
            get
            {
                return this.vertexIndices;
            }
        }
        public ZChannel[] Channels
        {
            get
            {
                return this.channels;
            }
        }
        public Dictionary<string, int> ChannelIndices
        {
            get
            {
                return this.channelIndices;
            }
        }
        public int VertexIndicesCount
        {
            get
            {
                return this.vertexIndices.Length;
            }
        }

        /* Constructors */

        public ZModelMeshGeometryData(int[] pVertexIndices, ZChannel[] pChannels, Dictionary<string, int> pChannelIndices)
        {
            this.vertexIndices = pVertexIndices;
            this.channels = pChannels;
            this.channelIndices = pChannelIndices;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public ZChannel GetChannel(string pName)
        {
            ZChannel channel = null;

            int channelIndex = -1;
            if (this.channelIndices.TryGetValue(pName, out channelIndex))
            {
                channel = this.channels[channelIndex];
            }

            return channel;
        }

    }
}