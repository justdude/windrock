﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZImportData.Objects.Data
{
    public class ZChannel
    {
        /* Fields */

        //private Type valueType;
        private object[] values;

        /* Properties */

        public object[] Values
        {
            get
            {
                return this.values;
            }
        }

        /* Constructors */

        public ZChannel(object[] pValues)
        {
            //this.valueType = pValueType;
            this.values = pValues;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}