﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ZImportData.Objects
{
    public class ZModelSkeletonAnimation
    {
        /* Fields */

        private TimeSpan duration;
        private int length;
        private Dictionary<int, Matrix[]> boneAnimations;

        private float keyframesPerSecond;

        /* Properties */

        public TimeSpan Duration
        {
            get
            {
                return this.duration;
            }
            set
            {
                this.duration = value;
                this.keyframesPerSecond = (float) this.boneAnimations.Count / (float) this.duration.TotalSeconds;
            }
        }
        public int Length
        {
            get
            {
                return this.length;
            }
        }
        public Dictionary<int, Matrix[]> BoneAnimations
        {
            get
            {
                return this.boneAnimations;
            }
            set
            {
                this.boneAnimations = value;
            }
        }

        /* Constructors */

        public ZModelSkeletonAnimation()
        {
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public Matrix GetTransform(int pBoneIndex, int pKeyframe)
        {
            return this.boneAnimations[pBoneIndex][pKeyframe];
        }
        public Matrix GetTransform(int pBoneIndex, TimeSpan pAnimationTime)
        {
            float time = ((float) pAnimationTime.TotalSeconds * this.keyframesPerSecond) % (float) this.duration.TotalSeconds;
            int currentKeyframe = (int) Math.Max(0, (int) Math.Floor(time));
            int nextKeyframe = (int) Math.Min(this.length - 1, (int) Math.Ceiling(time));
            float factor = time % 1.0f;

            Matrix[] keyframes = null;
            keyframes = this.boneAnimations[pBoneIndex];

            return Matrix.Lerp(keyframes[currentKeyframe], keyframes[nextKeyframe], factor);
        }
        public Matrix[] GetTransforms(TimeSpan pAnimationTime)
        {
            Matrix[] transforms = new Matrix[this.boneAnimations.Count];

            float time = ((float) pAnimationTime.TotalSeconds * this.keyframesPerSecond) % (float) this.duration.TotalSeconds;
            int currentKeyframe = (int) Math.Max(0, (int) Math.Floor(time));
            int nextKeyframe = (int) Math.Min(this.length - 1, (int) Math.Ceiling(time));
            float factor = time % 1.0f;

            Matrix[] keyframes = null;

            for (int boneIndex = 0; boneIndex < this.boneAnimations.Count; boneIndex++)
            {
                keyframes = this.boneAnimations[boneIndex];
                transforms[boneIndex] = Matrix.Lerp(keyframes[currentKeyframe], keyframes[nextKeyframe], factor);
            }

            return transforms;
        }

    }
}