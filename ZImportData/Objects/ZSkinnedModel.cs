﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ZImportData.Objects
{
    public class ZSkinnedModel : ZModel
    {
        /* Fields */

        private ZModelSkeleton skeleton;
        private Matrix[] absoluteTransforms;
        private Matrix[] skinTransforms;

        /* Properties */

        public ZModelSkeleton Skeleton
        {
            get
            {
                return this.skeleton;
            }
        }

        /* Constructors */

        public ZSkinnedModel(ZModelMesh[] pMeshes, ZModelSkeleton pSkeleton)
            : base(pMeshes, null)
        {
            this.skeleton = pSkeleton;

            this.InitializeTransforms();
        }

        /* Private methods */

        private void InitializeTransforms()
        {
            this.absoluteTransforms = new Matrix[this.skeleton.BoneCount];
            this.skinTransforms = new Matrix[this.skeleton.BoneCount];

            for (int boneIndex = 0; boneIndex < this.skeleton.BoneCount; boneIndex++)
            {
                skinTransforms[boneIndex] = Matrix.Identity;
            }
        }
        private void UpdateTransforms()
        {
            Matrix[] transforms = this.skeleton.GetTransforms();
            this.absoluteTransforms[0] = transforms[0] * this.skeleton.RelativeBindPoses[0] * this.transform;
            int parentBoneIndex = -1;
            for (int boneIndex = 1; boneIndex < this.skeleton.BoneCount; boneIndex++)
            {
                parentBoneIndex = this.skeleton.ParentBoneIndices[boneIndex];
                this.absoluteTransforms[boneIndex] = transforms[boneIndex] * this.skeleton.RelativeBindPoses[boneIndex] * this.absoluteTransforms[parentBoneIndex];
            }

            for (int boneIndex = 0; boneIndex < this.skeleton.BoneCount; boneIndex++)
            {
                this.skinTransforms[boneIndex] = this.skeleton.InverseAbsoluteBindPoses[boneIndex] * this.absoluteTransforms[boneIndex];
            }

            #region Переписка с интерпритатором
            // MatrixToString(this.skeleton.RelativeBindPoses[this.skeleton.BoneIndices["Head"]]);
            // "[-11.9997; -0.0014; 0.0876] (0.0251; 0.0114; -0.0258) <1.0000; 1.0000; 1.0000>"
            // MatrixToString(this.skeleton.GetTransform("Head"));
            // "[0.0000; 0.0000; 0.0000] (0.1960; 0.3265; -0.0694) <1.0000; 1.0000; 1.0000>"
            // MatrixToString(this.skeleton.GetTransform("Head") * this.skeleton.RelativeBindPoses[this.skeleton.BoneIndices["Head"]]);
            // "[-11.9997; -0.0014; 0.0876] (0.2266; 0.3335; -0.0871) <1.0000; 1.0000; 1.0000>"
            // MatrixToString(this.absoluteTransforms[this.skeleton.BoneIndices["Head"]]);
            // "[-0.0876; 12.0876; -11.9994] (-0.1339; -0.4431; -0.5520) <1.0000; 1.0000; 1.0000>"
            // MatrixToString(this.absoluteTransforms[this.skeleton.ParentBoneIndices[this.skeleton.BoneIndices["Head"]]]);
            // "[0.0000; 12.0000; 0.0000] (-0.5018; -0.4982; -0.4982) <1.0000; 1.0000; 1.0000>"
            // MatrixToString(Matrix.Invert(this.absoluteTransforms[this.skeleton.ParentBoneIndices[this.skeleton.BoneIndices["Head"]]]));
            // "[0.0000; 0.0000; -12.0000] (0.5018; 0.4982; 0.4982) <1.0000; 1.0000; 1.0000>"
            // MatrixToString(this.absoluteTransforms[this.skeleton.ParentBoneIndices[this.skeleton.BoneIndices["Head"]]] * Matrix.Invert(this.absoluteTransforms[this.skeleton.ParentBoneIndices[this.skeleton.BoneIndices["Head"]]]));
            // "[0.0000; 0.0000; 0.0000] (0.0000; 0.0000; 0.0000) <1.0000; 1.0000; 1.0000>"
            // MatrixToString(this.absoluteTransforms[this.skeleton.BoneIndices["Head"]] * Matrix.Invert(this.absoluteTransforms[this.skeleton.ParentBoneIndices[this.skeleton.BoneIndices["Head"]]]));
            // "[-11.9997; -0.0014; 0.0876] (0.2266; 0.3335; -0.0871) <1.0000; 1.0000; 1.0000>"
            // MatrixToString(this.skeleton.RelativeBindPoses[this.skeleton.BoneIndices["Head"]]);
            // "[-11.9997; -0.0014; 0.0876] (0.0251; 0.0114; -0.0258) <1.0000; 1.0000; 1.0000>"
            #endregion
        }

        private static string MatrixToString(Matrix pMatrix)
        {
            Vector3 scale = Vector3.Zero;
            Quaternion rotation = Quaternion.Identity;
            Vector3 translation = Vector3.Zero;
            pMatrix.Decompose(out scale, out rotation, out translation);
            return string.Format(
                "[{0:F4}; {1:F4}; {2:F4}] ({3:F4}; {4:F4}; {5:F4}) <{6:F4}; {7:F4}; {8:F4}>",
                translation.X,
                translation.Y,
                translation.Z,
                rotation.X,
                rotation.Y,
                rotation.Z,
                scale.X,
                scale.Y,
                scale.Z);

        }
        private static string MatricesToString(Matrix[] pMatrices)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine();
            for (int i = 0; i < pMatrices.Length; i++)
            {
                builder.AppendFormat(
                    "{0:D2}| {1}\n",
                    i,
                    MatrixToString(pMatrices[i]));
            }

            return builder.ToString();
        }

        /* Protected methods */



        /* Public methods */

        public void Update(GameTime pGameTime)
        {
            this.skeleton.Update(pGameTime);
            this.UpdateTransforms();
        }
        public override void Draw(Matrix pWorld, Matrix pView, Matrix pProjection)
        {
            throw new NotSupportedException();

            //Effect effect = null;
            //GraphicsDevice graphicsDevice = null;

            //foreach (ZModelMesh mesh in this.meshes)
            //{
            //    foreach (ZModelMeshPart part in mesh.Parts)
            //    {
            //        effect = part.Effect;
            //        effect.Parameters["World"].SetValue(pWorld);
            //        effect.Parameters["WorldViewProj"].SetValue(pWorld * pView * pProjection);
            //        effect.Parameters["WorldViewProj"].SetValue(pWorld * pView * pProjection);
            //        if (effect.Parameters.Any(x => x.Name == "SkinTransforms"))
            //        {
            //            effect.Parameters["SkinTransforms"].SetValue(this.skinTransforms);
            //        }

            //        graphicsDevice = effect.GraphicsDevice;
            //        graphicsDevice.SetVertexBuffer(part.VertexBuffer);
            //        graphicsDevice.Indices = part.IndexBuffer;

            //        foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            //        {
            //            pass.Apply();
            //            graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, part.VerticesCount, 0, part.TrianglesCount);
            //        }

            //    }
            //}
        }
        public override void Draw(Effect pEffect)
        {
            GraphicsDevice graphicsDevice = pEffect.GraphicsDevice;
            pEffect.Parameters["SkinTransforms"].SetValue(this.skinTransforms);

            foreach (ZModelMesh mesh in this.meshes)
            {
                foreach (ZModelMeshPart part in mesh.Parts)
                {
                    graphicsDevice.SetVertexBuffer(part.VertexBuffer);
                    graphicsDevice.Indices = part.IndexBuffer;

                    foreach (EffectPass pass in pEffect.CurrentTechnique.Passes)
                    {
                        pass.Apply();
                        graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, part.VerticesCount, 0, part.TrianglesCount);
                    }
                }
            }
        }

    }
}