﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace ZImportData.Objects
{
    public class ZModelMesh
    {
        /* Fields */

        private Matrix transform;
        private ZModelMeshPart[] parts;

        /* Properties */

        public Matrix Transform
        {
            get
            {
                return this.transform;
            }
            set
            {
                this.transform = value;
            }
        }
        [ContentSerializer]
        public ZModelMeshPart[] Parts
        {
            get
            {
                return this.parts;
            }
            set
            {
                this.parts = value;
            }
        }

        /* Constructors */

        public ZModelMesh()
        {
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}