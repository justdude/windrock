﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ZImportData.Objects
{
    public class ZModelSkeleton
    {
        /* Fields */

        private Dictionary<string, int> boneIndices;
        private int[] parentBoneIndices;
        private int boneCount;
        private Matrix[] relativeBindPoses;
        private Matrix[] inverseAbsoluteBindPoses;
        private Matrix[] boneTransforms;
        private Dictionary<string, ZModelSkeletonAnimation> animations;
        private string mainAnimation;
        private Dictionary<int, string> boneAnimations;

        private TimeSpan animationTime;
        private bool isPaused;

        private bool isBoneTransformsEnabled;
        private bool isBoneAnimationsEnabled;

        /* Properties */

        public Dictionary<string, int> BoneIndices
        {
            get
            {
                return this.boneIndices;
            }
        }
        public int[] ParentBoneIndices
        {
            get
            {
                return this.parentBoneIndices;
            }
        }
        public int BoneCount
        {
            get
            {
                return this.boneCount;
            }
            set
            {
                this.boneCount = value;
            }
        }
        public Matrix[] RelativeBindPoses
        {
            get
            {
                return this.relativeBindPoses;
            }
        }
        public Matrix[] InverseAbsoluteBindPoses
        {
            get
            {
                return this.inverseAbsoluteBindPoses;
            }
        }
        //public Matrix[] RelativeTransforms
        //{
        //    get
        //    {
        //        return this.boneTransforms;
        //    }
        //}
        public bool IsBoneTransformsEnabled
        {
            get
            {
                return this.isBoneTransformsEnabled;
            }
        }
        public bool IsBoneAnimationsEnabled
        {
            get
            {
                return this.isBoneAnimationsEnabled;
            }
        }

        /* Constructors */

        public ZModelSkeleton(int pBoneCount, Dictionary<string, int> pBoneIndices, int[] pParentBoneIndices, Matrix[] pRelativeBindPoses, Matrix[] pInverseAbsoluteBindPoses)
        {
            this.boneCount = pBoneCount;

            if (pBoneIndices == null || pBoneIndices.Count != this.boneCount
                || pParentBoneIndices == null || pParentBoneIndices.Length != this.boneCount
                || pRelativeBindPoses == null || pRelativeBindPoses.Length != this.boneCount
                || pInverseAbsoluteBindPoses == null || pInverseAbsoluteBindPoses.Length != this.boneCount)
            {
                throw new Exception();
            }

            this.boneIndices = pBoneIndices;
            this.parentBoneIndices = pParentBoneIndices;
            this.relativeBindPoses = pRelativeBindPoses;
            this.inverseAbsoluteBindPoses = pInverseAbsoluteBindPoses;

            this.boneTransforms = new Matrix[this.boneCount];
            for (int i = 0; i < this.boneCount; i++)
            {
                this.boneTransforms[i] = Matrix.Identity;
            }

            this.boneAnimations = new Dictionary<int, string>();

            this.isBoneTransformsEnabled = true;
            this.isBoneAnimationsEnabled = true;
        }

        /* Private methods */

        private bool IsAnimationNameValid(string pAnimationName)
        {
            return !string.IsNullOrEmpty(pAnimationName) && this.animations.ContainsKey(pAnimationName);
        }

        /* Protected methods */



        /* Public methods */

        public void Update(GameTime pGameTime)
        {
            if (!this.isPaused)
            {
                this.animationTime += pGameTime.ElapsedGameTime;
            }
        }

        public void SetMainAnimation(string pAnimationName)
        {
            this.mainAnimation = pAnimationName;
        }
        public void ResetMainAnimation()
        {
            //for (int boneIndex = 0; boneIndex < this.boneCount; boneIndex++)
            //{
            //    this.boneAnimationNames[boneIndex] = null;
            //}
            this.mainAnimation = null;
            this.animationTime = TimeSpan.Zero;
            this.isPaused = true;
        }

        public void SetBoneAnimation(int pBoneIndex, string pAnimationName)
        {
            if (!this.boneIndices.Values.Contains(pBoneIndex))
            {
                throw new Exception(
                    string.Format(
                        "There are no bone with index {0} in this skeleton, dude.",
                        pBoneIndex));
            }
            if (!this.animations.ContainsKey(pAnimationName))
            {
                throw new Exception(
                    string.Format(
                        "There are no animation with name \"{0}\" for this skeleton, dude.",
                        pAnimationName));
            }

            if (this.boneAnimations.ContainsKey(pBoneIndex))
            {
                this.boneAnimations[pBoneIndex] = pAnimationName;
            }
            else
            {
                this.boneAnimations.Add(pBoneIndex, pAnimationName);
            }
        }
        public void ResetBoneAnimation(int pBoneIndex)
        {
            this.boneAnimations.Remove(pBoneIndex);
        }
        public void ResetBoneAnimations()
        {
            this.boneAnimations.Clear();
        }

        public void SetAnimation(string pAnimationName)
        {
            this.mainAnimation = pAnimationName;
            this.ResetBoneAnimations();
        }
        public void ResetAnimation()
        {
            this.mainAnimation = null;
            this.animationTime = TimeSpan.Zero;
            this.isPaused = true;
        }

        public void StartAnimation(string pAnimationName)
        {
            if (!this.IsAnimationNameValid(pAnimationName))
            {
                this.ResetMainAnimation();
            }
            else
            {
                for (int boneIndex = 0; boneIndex < this.boneCount; boneIndex++)
                {
                    this.boneAnimations[boneIndex] = pAnimationName;
                }
                this.animationTime = TimeSpan.Zero;
                this.isPaused = false;
            }
        }
        public void PauseAnimation()
        {
            this.isPaused = true;
        }
        public void ResumeAnimation()
        {
            this.isPaused = false;
        }
        public void StopAnimation()
        {
            this.animationTime = TimeSpan.Zero;
            this.isPaused = true;
        }

        public void SetBoneTransform(int pBoneIndex, float pYaw, float pPinch, float pRoll)
        {
            this.boneTransforms[pBoneIndex] = Matrix.CreateFromYawPitchRoll(pYaw, pPinch, pRoll);
        }
        public void SetBoneTransform(string pBoneName, float pYaw, float pPinch, float pRoll)
        {
            int boneIndex = -1;
            if (this.boneIndices.TryGetValue(pBoneName, out boneIndex))
            {
                this.SetBoneTransform(boneIndex, pYaw, pPinch, pRoll);
            }
        }
        public void SetBoneTransform(int pBoneIndex, Quaternion pQuaternion)
        {
            this.boneTransforms[pBoneIndex] = Matrix.CreateFromQuaternion(pQuaternion);
        }
        public void SetBoneTransform(string pBoneName, Quaternion pQuaternion)
        {
            int boneIndex = -1;
            if (this.boneIndices.TryGetValue(pBoneName, out boneIndex))
            {
                this.SetBoneTransform(boneIndex, pQuaternion);
            }
        }
        public void ResetBoneTransform(int pBoneIndex)
        {
            this.boneTransforms[pBoneIndex] = Matrix.Identity;
        }
        public void ResetBoneTransforms()
        {
            for (int boneIndex = 0; boneIndex < this.boneCount; boneIndex++)
            {
                this.boneTransforms[boneIndex] = Matrix.Identity;
            }
        }

        public Matrix[] GetTransforms()
        {
            Matrix[] transforms = new Matrix[this.boneCount];

            for (int boneIndex = 0; boneIndex < this.boneCount; boneIndex++)
            {

                transforms[boneIndex] = this.boneTransforms[boneIndex];
                continue;

                ZModelSkeletonAnimation animation = null;
                if (this.isBoneTransformsEnabled)
                {
                    if (this.isBoneAnimationsEnabled && this.animations != null)
                    {
                        if (this.animations.TryGetValue(this.boneAnimations[boneIndex], out animation))
                        {
                            transforms[boneIndex] = this.boneTransforms[boneIndex] * animation.GetTransform(boneIndex, this.animationTime);
                        }
                        else
                        {
                            transforms[boneIndex] = this.boneTransforms[boneIndex];
                        }
                    }
                    else
                    {
                        transforms[boneIndex] = this.boneTransforms[boneIndex];
                    }
                }
                else
                {
                    if (this.animations.TryGetValue(this.boneAnimations[boneIndex], out animation))
                    {
                        transforms[boneIndex] = animation.GetTransform(boneIndex, this.animationTime);
                    }
                    else
                    {
                        transforms[boneIndex] = Matrix.Identity;
                    }
                }
            }

            return transforms;
        }
        public Matrix GetTransform(int pBoneIndex)
        {
            Matrix transform = Matrix.Identity;

            if (this.isBoneTransformsEnabled)
            {
                if (this.isBoneAnimationsEnabled && this.animations != null)
                {
                    ZModelSkeletonAnimation animation = null;
                    if (this.animations.TryGetValue(this.boneAnimations[pBoneIndex], out animation))
                    {
                        transform = this.boneTransforms[pBoneIndex] * animation.GetTransform(pBoneIndex, this.animationTime);
                    }
                    else
                    {
                        transform = this.boneTransforms[pBoneIndex];
                    }
                }
                else
                {
                    transform = this.boneTransforms[pBoneIndex];
                }
            }
            else
            {
                ZModelSkeletonAnimation animation = null;
                if (this.animations.TryGetValue(this.boneAnimations[pBoneIndex], out animation))
                {
                    transform = animation.GetTransform(pBoneIndex, this.animationTime);
                }
                else
                {
                    transform = Matrix.Identity;
                }
            }


            return transform;
        }
        public Matrix GetTransform(string pBoneName)
        {
            Matrix transform = Matrix.Identity;

            int boneIndex = -1;
            if (this.boneIndices.TryGetValue(pBoneName, out boneIndex))
            {
                transform = this.GetTransform(boneIndex);
            }

            return transform;
        }

    }
}