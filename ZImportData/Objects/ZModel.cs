﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ZImportData.Objects
{
    public class ZModel
    {
        /* Fields */

        protected Matrix transform;
        protected ZModelMesh[] meshes;
        protected Dictionary<string, int> meshIndices;

        /* Properties */

        public Matrix Transform
        {
            get
            {
                return this.transform;
            }
            set
            {
                this.transform = value;
            }
        }
        [ContentSerializer]
        public ZModelMesh[] Meshes
        {
            get
            {
                return this.meshes;
            }
        }
        public int MeshCount
        {
            get
            {
                return this.meshes.Length;
            }
        }
        public Dictionary<string, int> MeshIndices
        {
            get
            {
                return this.meshIndices;
            }
            set
            {
                this.meshIndices = value;
            }
        }

        /* Constructors */

        public ZModel(ZModelMesh[] pMeshes, Dictionary<string, int> pMeshNames)
        {
            this.meshes = pMeshes;
            this.meshIndices = pMeshNames;

            this.transform = Matrix.Identity;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        /// <summary>
        /// Отрисовывает модель с использованием её собственных шейдеров.
        /// </summary>
        public virtual void Draw(Matrix pWorld, Matrix pView, Matrix pProjection)
        {
            Effect effect = null;
            GraphicsDevice graphicsDevice = null;
            Matrix world = this.transform * pWorld;

            foreach (ZModelMesh mesh in this.meshes)
            {
                foreach (ZModelMeshPart part in mesh.Parts)
                {
                    effect = part.Effect;
                    effect.Parameters["World"].SetValue(world);
                    effect.Parameters["WorldViewProj"].SetValue(world * pView * pProjection);

                    graphicsDevice = effect.GraphicsDevice;
                    graphicsDevice.SetVertexBuffer(part.VertexBuffer);
                    graphicsDevice.Indices = part.IndexBuffer;

                    foreach (EffectPass pass in effect.CurrentTechnique.Passes)
                    {
                        pass.Apply();
                        graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, part.VerticesCount, 0, part.TrianglesCount);
                    }

                }
            }
        }
        /// <summary>
        /// Отрисовывает модель с использованием заданого шейдера.
        /// </summary>
        public virtual void Draw(Effect pEffect)
        {
            GraphicsDevice graphicsDevice = pEffect.GraphicsDevice;

            foreach (ZModelMesh mesh in this.meshes)
            {
                foreach (ZModelMeshPart part in mesh.Parts)
                {
                    graphicsDevice.SetVertexBuffer(part.VertexBuffer);
                    graphicsDevice.Indices = part.IndexBuffer;

                    foreach (EffectPass pass in pEffect.CurrentTechnique.Passes)
                    {
                        pass.Apply();
                        graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, part.VerticesCount, 0, part.TrianglesCount);
                    }
                }
            }
        }
        /// <summary>
        /// Отрисовывает заданый меш модели с использованием заданого шейдера.
        /// </summary>
        public virtual void DrawMesh(int pMeshIndex, Effect pEffect)
        {
            ZModelMesh mesh = this.meshes[pMeshIndex];
            GraphicsDevice graphicsDevice = pEffect.GraphicsDevice;

            foreach (ZModelMeshPart part in mesh.Parts)
            {
                graphicsDevice.SetVertexBuffer(part.VertexBuffer);
                graphicsDevice.Indices = part.IndexBuffer;

                foreach (EffectPass pass in pEffect.CurrentTechnique.Passes)
                {
                    pass.Apply();
                    graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, part.VerticesCount, 0, part.TrianglesCount);
                }
            }
        }
        /// <summary>
        /// Отрисовывает заданую часть заданого меша  с использованием заданого шейдера.
        /// </summary>
        public virtual void DrawPart(int pMeshIndex, int pPartIndex, Effect pEffect)
        {
            ZModelMeshPart part = this.meshes[pMeshIndex].Parts[pPartIndex];
            GraphicsDevice graphicsDevice = pEffect.GraphicsDevice;
            graphicsDevice.SetVertexBuffer(part.VertexBuffer);
            graphicsDevice.Indices = part.IndexBuffer;

            foreach (EffectPass pass in pEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, part.VerticesCount, 0, part.TrianglesCount);
            }
        }

    }
}