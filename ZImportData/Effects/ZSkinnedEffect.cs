﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ZImportData.Effects
{
    public class ZSkinnedEffect : Effect
    {
        /* Fields */

        //private Matrix world;
        //private Matrix view;
        //private Matrix projection;
        private Matrix worldViewProjection;
        private Vector3 ambientLigthColor;
        private float ambientIntensity;
        private Matrix[] skinTransforms;

        //private EffectParameter worldParameter;
        //private EffectParameter viewParameter;
        //private EffectParameter projectionParameter;
        private EffectParameter worldViewProjectionParameter;
        private EffectParameter ambientLigthColorParameter;
        private EffectParameter ambientIntensityParameter;
        private EffectParameter skinTransformsParameter;

        /* Properties */

        //public Matrix World
        //{
        //    get
        //    {
        //        return this.world;
        //    }
        //    set
        //    {
        //        this.world = value;
        //        this.worldParameter.SetValue(this.world);
        //    }
        //}
        //public Matrix View
        //{
        //    get
        //    {
        //        return this.view;
        //    }
        //    set
        //    {
        //        this.view = value;
        //        this.viewParameter.SetValue(this.view);
        //    }
        //}
        //public Matrix Projection
        //{
        //    get
        //    {
        //        return this.projection;
        //    }
        //    set
        //    {
        //        this.projection = value;
        //        this.projectionParameter.SetValue(this.projection);
        //    }
        //}
        public Matrix WorldViewProjection
        {
            get
            {
                return this.worldViewProjection;
            }
            set
            {
                this.worldViewProjection = value;
                this.worldViewProjectionParameter.SetValue(this.worldViewProjection);
            }
        }
        public Vector3 AmbientLigthColor
        {
            get
            {
                return this.ambientLigthColor;
            }
            set
            {
                this.ambientLigthColor = value;
                this.ambientLigthColorParameter.SetValue(this.ambientLigthColor);
            }
        }
        public float AmbientIntensity
        {
            get
            {
                return this.ambientIntensity;
            }
            set
            {
                this.ambientIntensity = value;
                this.ambientIntensityParameter.SetValue(this.ambientIntensity);
            }
        }
        public Matrix[] SkinTransforms
        {
            get
            {
                return this.skinTransforms;
            }
            set
            {
                this.skinTransforms = value;
                this.skinTransformsParameter.SetValue(this.skinTransforms);
            }
        }

        /* Constructors */

        public ZSkinnedEffect(GraphicsDevice pGraphicsDevice)
            : base(pGraphicsDevice, ZSkinnedEffectCode.Instance.Code)
        {
            this.InitializeParameters();
        }

        /* Private methods */

        private void InitializeParameters()
        {
            //this.worldParameter = this.Parameters["World"];
            //this.viewParameter = this.Parameters["View"];
            //this.projectionParameter = this.Parameters["Projection"];
            this.worldViewProjectionParameter = this.Parameters["WorldViewProjection"];
            this.ambientLigthColorParameter = this.Parameters["AmbientColor"];
            this.ambientIntensityParameter = this.Parameters["AmbientIntensity"];
            this.skinTransformsParameter = this.Parameters["SkinTransforms"];
        }

        /* Protected methods */



        /* Public methods */



        /* Classes */

        private class ZSkinnedEffectCode : ZEffectCode
        {
            private static ZSkinnedEffectCode instance;

            public static ZSkinnedEffectCode Instance
            {
                get
                {
                    return instance ?? (instance = new ZSkinnedEffectCode(@"~/Resources/Effects/Skinned.xnb"));
                }
            }

            private ZSkinnedEffectCode(string pFileName)
            {
                this.LoadCode(pFileName);
            }
        }

    }
}