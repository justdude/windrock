﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ZImportData.Effects
{
    public class ZEffectCode
    {
        /* Fields */

        protected byte[] code;

        /* Properties */

        public byte[] Code
        {
            get
            {
                return this.code;
            }
        }

        /* Constructors */

        

        /* Private methods */

        

        /* Protected methods */

        protected void LoadCode(string pFileName)
        {
            Stream stream = null;
            try
            {
                stream = File.OpenRead(pFileName);
                BinaryReader reader = new BinaryReader(stream);
                code = new byte[reader.BaseStream.Length];
                int bufferSize = 80;
                int readedBytesCount = 0;
                int index = 0;
                byte[] buffer = new byte[bufferSize];
                while ((readedBytesCount = reader.Read(buffer, index, bufferSize)) > 0)
                {
                    Array.Copy(buffer, 0, this.code, index, readedBytesCount);
                    index += readedBytesCount;
                }
            }
            finally
            {
                stream.Close();
            }
        }

        /* Public methods */



    }
}