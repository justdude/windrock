﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ZImportData.Objects.Data;

namespace ZImportData.Readers.Data
{
    public class ZModelMeshDataReader : ContentTypeReader<ZModelMeshData>
    {
        protected override ZModelMeshData Read(ContentReader pReader, ZModelMeshData pInstance)
        {
            Vector3[] positions = pReader.ReadObject<List<Vector3>>().ToArray<Vector3>();
            ZModelMeshGeometryData[] geometries = pReader.ReadObject<List<ZModelMeshGeometryData>>().ToArray<ZModelMeshGeometryData>();

            return new ZModelMeshData(positions, geometries);
        }
    }
}