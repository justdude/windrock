﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ZImportData.Objects.Data;

namespace ZImportData.Readers.Data
{
    public class ZChannelReader : ContentTypeReader<ZChannel>
    {
        protected override ZChannel Read(ContentReader pReader, ZChannel pInstance)
        {
            //Type valueType = pReader.ReadObject<Type>();
            object[] values = pReader.ReadObject<List<object>>().ToArray<object>();

            return new ZChannel(values);
        }
    }
}