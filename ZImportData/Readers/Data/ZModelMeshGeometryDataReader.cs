﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ZImportData.Objects.Data;

namespace ZImportData.Readers.Data
{
    public class ZModelMeshGeometryDataReader : ContentTypeReader<ZModelMeshGeometryData>
    {
        protected override ZModelMeshGeometryData Read(ContentReader pReader, ZModelMeshGeometryData pInstance)
        {
            int[] positionIndices = pReader.ReadObject<List<int>>().ToArray<int>();
            ZChannel[] channels = pReader.ReadObject<List<ZChannel>>().ToArray<ZChannel>();
            Dictionary<string, int> channelIndices = pReader.ReadObject<Dictionary<string, int>>();

            return new ZModelMeshGeometryData(positionIndices, channels, channelIndices);
        }
    }
}