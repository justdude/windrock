﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ZImportData.Objects.Data;

namespace ZImportData.Readers.Data
{
    public class ZModelDataReader : ContentTypeReader<ZModelData>
    {
        protected override ZModelData Read(ContentReader pReader, ZModelData pInstance)
        {
            ZModelMeshData[] meshes = pReader.ReadObject<List<ZModelMeshData>>().ToArray<ZModelMeshData>();
            Dictionary<string, int> meshIndices = pReader.ReadObject<Dictionary<string, int>>();

            return new ZModelData(meshes, meshIndices);
        }
    }
}