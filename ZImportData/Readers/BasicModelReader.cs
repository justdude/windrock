﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ImportData.Objects;

namespace ImportData.Readers
{
    public class BasicModelReader : ContentTypeReader<SimpleModel>
    {
        protected override SimpleModel Read(ContentReader pReader, SimpleModel pBasicModel)
        {
            List<SimpleModelPart> parts = pReader.ReadObject<SimpleModelPart[]>();

            SimpleModel model = new SimpleModel();
            model.Parts = parts;

            return new SimpleModel();
        }
    }
}