﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ZImportData.Objects;

namespace ZImportData.Readers
{
    public class ZModelReader : ContentTypeReader<ZModel>
    {
        protected override ZModel Read(ContentReader pReader, ZModel pInstance)
        {
            ZModelMesh[] meshes = pReader.ReadObject<List<ZModelMesh>>().ToArray<ZModelMesh>();
            Dictionary<string, int> meshIndices = pReader.ReadObject<Dictionary<string, int>>();

            return new ZModel(meshes, meshIndices);
        }
    }
}