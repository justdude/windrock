﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ZImportData.Objects;

namespace ZImportData.Readers
{
    public class ZSkinnedModelReader : ContentTypeReader<ZSkinnedModel>
    {
        protected override ZSkinnedModel Read(ContentReader pReader, ZSkinnedModel pInstance)
        {
            ZModelMesh[] meshes = pReader.ReadObject<List<ZModelMesh>>().ToArray<ZModelMesh>();
            ZModelSkeleton skeleton = pReader.ReadObject<ZModelSkeleton>();

            return new ZSkinnedModel(meshes, skeleton);
        }
    }
}