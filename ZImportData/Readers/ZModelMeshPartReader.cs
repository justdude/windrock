﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ZImportData.Objects;

namespace ZImportData.Readers
{
    public class ZModelMeshPartReader : ContentTypeReader<ZModelMeshPart>
    {
        protected override ZModelMeshPart Read(ContentReader pReader, ZModelMeshPart pInstance)
        {
            int verticesCount = pReader.ReadInt32();
            int trianglesCount = pReader.ReadInt32();
            VertexBuffer vertexBuffer = pReader.ReadObject<VertexBuffer>();
            IndexBuffer indexBuffer = pReader.ReadObject<IndexBuffer>();
            Effect effect = pReader.ReadObject<Effect>();

            return new ZModelMeshPart()
            {
                VerticesCount = verticesCount,
                TrianglesCount = trianglesCount,
                VertexBuffer = vertexBuffer,
                IndexBuffer = indexBuffer,
                Effect = effect
            };
        }
    }
}