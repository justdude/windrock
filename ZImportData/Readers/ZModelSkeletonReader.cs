﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ZImportData.Objects;

namespace ZImportData.Readers
{
    public class ZModelSkeletonReader : ContentTypeReader<ZModelSkeleton>
    {
        protected override ZModelSkeleton Read(ContentReader pReader, ZModelSkeleton pInstance)
        {
            int boneCount = pReader.ReadInt32();
            Dictionary<string, int> boneIndices = pReader.ReadObject<Dictionary<string, int>>();
            int[] parentBoneIndices = pReader.ReadObject<int[]>();
            Matrix[] relativeBindPoses = pReader.ReadObject<Matrix[]>();
            Matrix[] inverseAbsoluteBindPoses = pReader.ReadObject<Matrix[]>();

            return new ZModelSkeleton(
                boneCount,
                boneIndices,
                parentBoneIndices,
                relativeBindPoses,
                inverseAbsoluteBindPoses);
        }
    }
}