﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ZImportData.Objects;

namespace ZImportData.Readers
{
    public class ZModelMeshReader : ContentTypeReader<ZModelMesh>
    {
        protected override ZModelMesh Read(ContentReader pReader, ZModelMesh pInstance)
        {
            Matrix transform = pReader.ReadMatrix();
            List<ZModelMeshPart> parts = pReader.ReadObject<List<ZModelMeshPart>>();

            return new ZModelMesh()
            {
                Transform = transform,
                Parts = parts.ToArray<ZModelMeshPart>()
            };
        }
    }
}