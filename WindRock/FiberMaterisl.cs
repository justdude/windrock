﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpoonCraft.Damaging;

namespace SpoonCraft.Materials
{
    public class FiberMaterial : Material
    {
        /* Fields */



        /* Properties */



        /* Constructors */

        public FiberMaterial(string pName, Damage pResistance)
            : base(pName, pResistance)
        {
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}