﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using WindRock.Common;
using GameHelper.Data;
using System.IO;

namespace WindRock
{
    public static class Core
    {
        /* Enums */

        public enum CompasRose : byte
        {
            FourPoint = 4,
            EightPoint = 8,
            SixteenPoint = 16
        }
        public enum CardinalDirection : byte
        {
            None = 0,
            North = 1,
            NorthNorthEast = 2,
            NorthEast = 3,
            EastNorthEast = 4,
            East = 5,
            EastSouthEast = 6,
            SouthEast = 7,
            SouthSouthEast = 8,
            South = 9,
            SouthSouthWest = 10,
            SouthWest = 11,
            WestSouthWest = 12,
            West = 13,
            WestNorthWest = 14,
            NorthWest = 15,
            NorthNorthWest = 16
        }

        /* Fields & Constants */

        public static readonly Area3I WorldArea = new Area3I(0, 0, 0, 1, 0, 0);
        public static readonly Vector3I ChunkSizes = new Vector3I(16, 16, 16);
        public static readonly Vector3 OneDivChunkSizes = new Vector3(1f / ChunkSizes.X, 1f / ChunkSizes.Y, 1f / ChunkSizes.Z);
        public static readonly Vector2I BlocksTexturesCount = new Vector2I(16, 16);
        public static readonly Vector3I ActiveAreaRadiuses = new Vector3I(2, 2, 2);
        public static readonly Vector3I VisibleAreaRadiuses = new Vector3I(5, 5, 5);
        public const int Seed = 0;
        public static readonly Vector2 BlockTextureProportionalSize = new Vector2(
            1.0f / (float) BlocksTexturesCount.X,
            1.0f / (float) BlocksTexturesCount.Y);
        public const float Pixel = 0.0625f; // 1 / 16
        public static readonly Color DebugTextColor = Color.Black;
        public const float OneDivTwoPi = 1f / MathHelper.TwoPi;
        //public const int VertexBufferSegmentSize = 256;
        public static TimeSpan LeftButtonTimeout = TimeSpan.FromMilliseconds(250);
        public static TimeSpan RightButtonTimeout = TimeSpan.FromMilliseconds(250);
        public const int TicksPerSecond = 20;
        public static TimeSpan TickDuration = TimeSpan.FromSeconds(1f / (float) TicksPerSecond);
        public static TimeSpan WaterUpdateTimeout = FromTics(5);
        public const byte DefaultWaterAmount = 10;
        public const byte MaxLiquidAmount = 16;
        public const short CompressedWaterAmount = 1;
        public const float OneDivMaxWaterAmount = 1f / MaxLiquidAmount;
        public const short MaxWaterTransferAmount = short.MaxValue;
        public const int ChunkPerIterationUpdateCount = 5;
        public const int RandomChunkUpdateBlocksCount = 5;
        public static float ParticleScaleExponente = 0.92f;
        public const PlayerCameraMode DefaultPlayerCameraMode = PlayerCameraMode.ThirdPerson;

        public const bool IsUseAlterntiveBlockRenderSystem = true;

        public static string BlockModelDirectory = @"Models/Blocks";

        private static readonly Dictionary<CardinalDirection, string> fullCardinalDirectionNames = new Dictionary<CardinalDirection, string>
        {
            {CardinalDirection.North, "North"},
            {CardinalDirection.NorthNorthEast, "North-North-East"},
            {CardinalDirection.NorthEast, "North-East"},
            {CardinalDirection.EastNorthEast, "East-North-East"},
            {CardinalDirection.East, "East"},
            {CardinalDirection.EastSouthEast, "East-South-East"},
            {CardinalDirection.SouthEast, "South-East"},
            {CardinalDirection.SouthSouthEast, "South-South-East"},
            {CardinalDirection.South, "South"},
            {CardinalDirection.SouthSouthWest, "South-South-West"},
            {CardinalDirection.SouthWest, "South-West"},
            {CardinalDirection.WestSouthWest, "West-South-West"},
            {CardinalDirection.West, "West"},
            {CardinalDirection.WestNorthWest, "West-North-West"},
            {CardinalDirection.NorthWest, "North-West"},
            {CardinalDirection.NorthNorthWest, "North-North-West"}
        };
        private static readonly Dictionary<CardinalDirection, string> shortCardinalDirectionNames = new Dictionary<CardinalDirection, string>
        {
            {CardinalDirection.North, "N"},
            {CardinalDirection.NorthNorthEast, "NNE"},
            {CardinalDirection.NorthEast, "NE"},
            {CardinalDirection.EastNorthEast, "ENE"},
            {CardinalDirection.East, "E"},
            {CardinalDirection.EastSouthEast, "ESE"},
            {CardinalDirection.SouthEast, "SE"},
            {CardinalDirection.SouthSouthEast, "SSE"},
            {CardinalDirection.South, "S"},
            {CardinalDirection.SouthSouthWest, "SSW"},
            {CardinalDirection.SouthWest, "SW"},
            {CardinalDirection.WestSouthWest, "WSW"},
            {CardinalDirection.West, "W"},
            {CardinalDirection.WestNorthWest, "WNW"},
            {CardinalDirection.NorthWest, "NW"},
            {CardinalDirection.NorthNorthWest, "NNW"}
        };
        public static readonly string ScreenshotsFolder =
            string.Format(
                "{0}/{1}",
                Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                "Spooncraft/Screenshots");

        // Properties



        // Constructors



        // Private methods



        // Protected methods



        // Public methods

        public static float ToPixels(float pCount)
        {
            return Core.Pixel * pCount;
        }
        public static CardinalDirection GetCardinalDirection(float pAzimuth, CompasRose pCompasRose)
        {
            CardinalDirection cardinalDirection = CardinalDirection.None;

            float azimuthFactor = -pAzimuth * OneDivTwoPi + 1f / (float) pCompasRose * 0.5f;
            if (azimuthFactor < 0)
            {
                azimuthFactor = 1 + azimuthFactor;
            }
            int step = (int) (16 / (int) pCompasRose);
            cardinalDirection = (CardinalDirection) (Math.Floor(azimuthFactor % 1 * (float) pCompasRose) * step + 1);

            if ((int) cardinalDirection > 16)
            {
                while (false)
                    ;
            }

            return cardinalDirection;
        }
        public static string GetCardinalDirectionName(CardinalDirection pCardinalDirection, bool pIsFullName)
        {
            return pIsFullName ? fullCardinalDirectionNames[pCardinalDirection] : shortCardinalDirectionNames[pCardinalDirection];
        }
        public static int ToTicks(TimeSpan pElapsedTime)
        {
            return (int) Math.Floor(pElapsedTime.TotalSeconds / TickDuration.TotalSeconds);
        }
        public static TimeSpan FromTics(int pTicks)
        {
            return TimeSpan.FromSeconds(TickDuration.TotalSeconds * pTicks);
        }

        // // //

        public static string GetBlockModelPath(string pModelName)
        {
            return Path.Combine(Core.BlockModelDirectory, pModelName);
        }

    }
}