﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpoonCraft.Common;
using SpoonCraft.Damaging;

namespace SpoonCraft.Materials
{
    public class Material
    {
        /* Fields */

        protected string name;
        protected Damage resistance;

        /* Properties */

        public string Name
        {
            get
            {
                return this.name;
            }
        }
        public Damage Resistance
        {
            get
            {
                return this.resistance;
            }
        }
        //public int RotResistance
        //{
        //    get
        //    {
        //        return this.rotResistance;
        //    }
        //}

        /* Constructors */

        public Material(string pName, Damage pResistance)
        {
            this.name = pName;
            this.resistance = pResistance;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public Damage ReduceDamage(Damage pDamage)
        {
            return pDamage - this.resistance;
        }

    }
}