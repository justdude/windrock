﻿using GameHelper.Cameras;
using GameHelper.Data;
using GameHelper.GUI.General.Elements;
using GameHelper.Input;
using GameHelper.Models;
using GUITest.GUI.Standard.TextureManagers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using WindRock.Blocks;
using WindRock.Common;
using WindRock.Entities;
using WindRock.Registers;
using WindRock.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.GUI.Standard;
using GameHelper;
using WindRock.Extentions;

namespace WindRock
{
    public class Player
    {
        /* Constants */

        private static float YawSpeed = MathHelper.PiOver4 * 0.25f;
        private static float PinchSpeed = MathHelper.PiOver4 * 0.25f;
        private static Vector3 Camera2Offset = new Vector3(0, 0, 10);
        private static Vector3 Camera3Offset = new Vector3(0, 0, -10);

        /* Fields */

        private string id;
        private WindRockGame game;
        private World world;
        private SpriteBatch spriteBatch;
        private SpriteFont debugFont;
        private Humanoid avatar;
        private Camera camera;
        private Vector2 centericMousePosition;
        private PlayerCameraMode cameraMode;
        private FlatArrow bodyDirectionArrow;
        //private FlatArrow lookDirectionArrow;
        //private Vector3I targetBlockCoord;
        private Box targetBlockBox;
        private Target target;
        private Dictionary<Vector3I, Box> blockSelection;
        //private BlockPreview blockPreview;
        //private RasterizerState defaultRasterizerState;
        private MouseInputManager avatarMouseInputManager;
        private MouseInputManager guiMouseInputManager;
        private KeyInputManager guiKeyInputManager;
        private CustomKeyInputManager customKeyInputManager;

        private int selectedBlockIndex;
        private short selectedBlockID;
        private short[] blockIdentifiers;

        private bool isShowInterface;
        private Texture2D aimTexture;
        private Vector2 aimPosition;

        private UIForm mainMenuForm;
        private UIForm activeForm;

        /* Properties */

        public string ID
        {
            get
            {
                return this.id;
            }
        }
        public Camera Camera
        {
            get
            {
                return this.camera;
            }
        }
        public Vector3 Position
        {
            get
            {
                return this.avatar.Position;
            }
        }

        /* Constructors */

        public Player(WindRockGame pGame, World pWorld, Humanoid pAvatar)
        {
            this.game = pGame;
            this.world = pWorld;
            this.avatar = pAvatar;

            this.id = Guid.NewGuid().ToString();
        }

        /* Private methods */

        private void UpdateAvatarMouseInput(GameTime pGameTime)
        {
            this.avatarMouseInputManager.Update(pGameTime);

            if (this.avatarMouseInputManager.PressedButtons.HasFlag(MouseButtons.LeftButton))
            {
                this.avatarMouseInputManager.LeftMouseButton.SetTimeout(Core.LeftButtonTimeout);
                if (this.target != null)
                {
                    this.world.SetAirToBufferByPlayer(this.target.Coords);
                    this.CleareTarget();
                }
            }
            else if (this.avatarMouseInputManager.RightMouseButton.IsPressedAndNotHundled)
            {
                this.avatarMouseInputManager.PressedButtons.HasFlag(MouseButtons.RightButton);
                this.avatarMouseInputManager.RightMouseButton.SetHandledWithTimeout(Core.RightButtonTimeout);
                if (this.target != null)
                {
                    int x = 0;
                    int y = 0;
                    int z = 0;

                    if (this.target.Block.IsHard)
                    {
                        switch (this.target.Face)
                        {
                            case Direction.IncreaseY:
                                {
                                    x = this.target.Coords.X;
                                    y = this.target.Coords.Y + 1;
                                    z = this.target.Coords.Z;
                                }
                                break;
                            case Direction.DecreaseY:
                                {
                                    x = this.target.Coords.X;
                                    y = this.target.Coords.Y - 1;
                                    z = this.target.Coords.Z;
                                }
                                break;
                            case Direction.DecreaseX:
                                {
                                    x = this.target.Coords.X - 1;
                                    y = this.target.Coords.Y;
                                    z = this.target.Coords.Z;
                                }
                                break;
                            case Direction.IncreaseX:
                                {
                                    x = this.target.Coords.X + 1;
                                    y = this.target.Coords.Y;
                                    z = this.target.Coords.Z;
                                }
                                break;
                            case Direction.DecreaseZ:
                                {
                                    x = this.target.Coords.X;
                                    y = this.target.Coords.Y;
                                    z = this.target.Coords.Z - 1;
                                }
                                break;
                            case Direction.IncreaseZ:
                                {
                                    x = this.target.Coords.X;
                                    y = this.target.Coords.Y;
                                    z = this.target.Coords.Z + 1;
                                }
                                break;
                        }
                    }
                    else
                    {
                        x = this.target.Coords.X;
                        y = this.target.Coords.Y;
                        z = this.target.Coords.Z;
                    }

                    BlockTemplate block = BlockRegister.Instance.Get(this.selectedBlockID);
                    //if (block is SlabBlockTemplate)
                    //{
                    //    if (this.target.BlockID == this.selectedBlockID)
                    //    {
                    //        SlabBlockData targetSlabBlockData = this.world.GetBlockData(this.target.Coords) as SlabBlockData;
                    //        targetSlabBlockData.Layout = SlabLayout.Top | SlabLayout.Bottom;
                    //        targetSlabBlockData.IsNatural = false;
                    //    }
                    //    else
                    //    {
                    //        SlabBlockData slabBlockData = block.CreateDefaultMetadata(false) as SlabBlockData;
                    //        slabBlockData.Layout = SlabLayout.Top;
                    //        this.world.SetBlockToBufferByPlayer(x, y, z, this.selectedBlockID, slabBlockData);
                    //    }
                    //}
                    //else
                    {
                        this.world.SetBlockToBufferByPlayer(x, y, z, this.selectedBlockID, block.CreateDefaultMetadata());
                    }

                    this.CleareTarget();
                }
            }
            else if (this.avatarMouseInputManager.MiddleMouseButton.IsPressedAndNotHundled)
            {
                this.avatarMouseInputManager.MiddleMouseButton.SetHandled();
                if (this.target != null)
                {
                    this.selectedBlockID = this.world.GetBlockID(this.target.Coords);
                    for (int i = 0; i < this.blockIdentifiers.Length; i++)
                    {
                        if (this.blockIdentifiers[i] == this.selectedBlockID)
                        {
                            this.selectedBlockIndex = i;
                            break;
                        }
                    }
                }
            }

            float wheelNotches = (float) Math.Floor(this.avatarMouseInputManager.GetWheelNotches());
            if (wheelNotches != 0)
            {
                int newSelectedBlockIndex = (int) (this.selectedBlockIndex + wheelNotches) % this.blockIdentifiers.Length;
                this.selectedBlockIndex = newSelectedBlockIndex >= 0 ? newSelectedBlockIndex : this.blockIdentifiers.Length + newSelectedBlockIndex;
                this.selectedBlockID = this.blockIdentifiers[this.selectedBlockIndex];
                this.avatarMouseInputManager.SetWheelHandled(wheelNotches);
            }
        }

        private void CenterMousePosition()
        {
            this.centericMousePosition = new Vector2(this.game.GraphicsDevice.Viewport.Width * 0.5f, this.game.GraphicsDevice.Viewport.Height * 0.5f);
            Mouse.SetPosition((int) this.centericMousePosition.X, (int) this.centericMousePosition.Y);
        }
        private Vector3 GetCameraPosition()
        {
            Vector3 cameraPosition = Vector3.Zero;

            switch (this.cameraMode)
            {
                case PlayerCameraMode.FirstPerson:
                    cameraPosition = this.avatar.Position + this.avatar.GetEyesOffset();
                    break;
                case PlayerCameraMode.ThirdPerson:
                    cameraPosition = this.avatar.Position + this.avatar.GetEyesOffset() + Vector3.Transform(Player.Camera2Offset, Matrix.CreateFromYawPitchRoll(this.avatar.LookYaw, this.avatar.LookPinch, 0));
                    break;
                case PlayerCameraMode.ThirdPersonOnFace:
                    cameraPosition = this.avatar.Position + this.avatar.GetEyesOffset() + Vector3.Transform(Player.Camera3Offset, Matrix.CreateFromYawPitchRoll(this.avatar.LookYaw, this.avatar.LookPinch, 0));
                    break;
            }

            return cameraPosition;
        }
        private Vector3 GetCameraDirection()
        {
            Vector3 cameraDirection = Vector3.Zero;

            switch (this.cameraMode)
            {
                case PlayerCameraMode.FirstPerson:
                //cameraDirection = this.humanoid.LookDirection;
                //break;
                case PlayerCameraMode.ThirdPerson:
                    cameraDirection = this.avatar.LookDirection;
                    break;
                case PlayerCameraMode.ThirdPersonOnFace:
                    cameraDirection = -this.avatar.LookDirection;
                    break;
            }

            return cameraDirection;
        }
        private void DetermineTargetBlock()
        {
            Ray aimRay = new Ray(this.avatar.Position + this.avatar.GetEyesOffset(), this.avatar.LookDirection);

            //this.blockSelection.Clear();
            //foreach (Vector3I blockCoord in this.GetBlocksByRay(aimRay, 5))
            //{
            //    short blockID = this.world.GetBlockID(blockCoord.X, blockCoord.Y, blockCoord.Z);
            //    Block block = BlockRegister.Instance.GetBlock(blockID);
            //    if (!this.blockSelection.ContainsKey(blockCoord))
            //    {
            //        this.blockSelection.Add(
            //        blockCoord,
            //        new Box(
            //            this.game,
            //            Color.White,
            //            new Vector3(0, 0, 0),
            //            new Vector3(1, 1, 1),
            //            new Vector3(blockCoord.X, blockCoord.Y, blockCoord.Z)));
            //    }
            //}

            //this.targetBlock = null;
            //this.targetBlockCoord = Vector3I.Zero;
            //this.targetBlockBox = null;

            this.CleareTarget();

            foreach (RayTracingData rayTracingData in this.GetBlocksByRay(aimRay, 10))
            {
                if (Core.WorldArea.Contains(World.GetCoordOfChunk(rayTracingData.Coord)))
                {
                    short blockID = this.world.GetBlockID(rayTracingData.Coord);
                    if (blockID != -1 && blockID != BlockRegister.AirBlockID)
                    {
                        BlockTemplate block = BlockRegister.Instance.Get(blockID);
                        this.SetTarget(rayTracingData.Coord, blockID, block, rayTracingData.Face);
                        break;
                    }
                }
            }
        }
        private IEnumerable<RayTracingData> GetBlocksByRay(Ray pRay, float pAxisDistance)
        {
            List<RayTracingData> rayTracingDataList = null;
            if (pRay.Direction != Vector3.Zero)
            {
                rayTracingDataList = new List<RayTracingData>();

                // Ячейка.
                Vector3I cellCoord = new Vector3I(
                    (int) Math.Floor(pRay.Position.X),
                    (int) Math.Floor(pRay.Position.Y),
                    (int) Math.Floor(pRay.Position.Z));

                // Направление премещения по осям.
                Vector3I directionSign = new Vector3I(
                    Math.Sign(pRay.Direction.X),
                    Math.Sign(pRay.Direction.Y),
                    Math.Sign(pRay.Direction.Z));

                // Показывает дискретные координаты следующей грани.
                Vector3I nextPlaneCoords = new Vector3I(
                    cellCoord.X + (directionSign.X > 0 ? 1 : 0),
                    cellCoord.Y + (directionSign.Y > 0 ? 1 : 0),
                    cellCoord.Z + (directionSign.Z > 0 ? 1 : 0));

                // ...
                Vector3 T = new Vector3(
                    (nextPlaneCoords.X - pRay.Position.X) / pRay.Direction.X,
                    (nextPlaneCoords.Y - pRay.Position.Y) / pRay.Direction.Y,
                    (nextPlaneCoords.Z - pRay.Position.Z) / pRay.Direction.Z);

                if (float.IsNaN(T.X))
                {
                    T.X = float.PositiveInfinity;
                }
                if (float.IsNaN(T.Y))
                {
                    T.Y = float.PositiveInfinity;
                }
                if (float.IsNaN(T.Z))
                {
                    T.Z = float.PositiveInfinity;
                }

                // ...
                Vector3 D = new Vector3(
                    pRay.Direction.X == 0 ? float.PositiveInfinity : directionSign.X / pRay.Direction.X,
                    pRay.Direction.Y == 0 ? float.PositiveInfinity : directionSign.Y / pRay.Direction.Y,
                    pRay.Direction.Z == 0 ? float.PositiveInfinity : directionSign.Z / pRay.Direction.Z);

                if (float.IsNaN(D.X))
                {
                    D.X = float.PositiveInfinity;
                }
                if (float.IsNaN(D.Y))
                {
                    D.Y = float.PositiveInfinity;
                }
                if (float.IsNaN(D.Z))
                {
                    D.Z = float.PositiveInfinity;
                }

                // А теперь цикл.

                Direction face = Direction.None;
                Vector3 contactPosition = Vector3.Zero;

                Vector3 maxDistanceByAxises = pRay.Direction * pAxisDistance;
                Vector3 distanceByAxises = Vector3.Zero;

                int iterationsCount = 0;
                while (true)
                {
                    yield return new RayTracingData(cellCoord, face, contactPosition);

                    if (T.X < T.Y)
                    {
                        if (T.X < T.Z)
                        {
                            cellCoord.X += directionSign.X;
                            T.X += D.X;

                            distanceByAxises.X += D.X;
                            if (Math.Abs(cellCoord.X - pRay.Position.X) > pAxisDistance)
                            {
                                break;
                            }

                            face =
                                directionSign.X == 1 ? Direction.DecreaseX : // Наоборот.
                                directionSign.X == -1 ? Direction.IncreaseX : // Наоборот.
                                Direction.None;

                        }
                        else //if (T.Z <= T.X)
                        {
                            cellCoord.Z += directionSign.Z;
                            T.Z += D.Z;

                            distanceByAxises.Z += D.Z;
                            if (Math.Abs(cellCoord.Z - pRay.Position.Z) > pAxisDistance)
                            {
                                break;
                            }

                            face =
                                directionSign.Z == 1 ? Direction.DecreaseZ : // Наоборот.
                                directionSign.Z == -1 ? Direction.IncreaseZ : // Наоборот.
                                Direction.None;
                        }
                    }
                    else // (T.Y <= T.X)
                    {
                        if (T.Y < T.Z)
                        {
                            cellCoord.Y += directionSign.Y;
                            T.Y += D.Y;

                            distanceByAxises.Y += D.Y;
                            if (Math.Abs(cellCoord.Y - pRay.Position.Y) > pAxisDistance)
                            {
                                break;
                            }

                            face =
                                directionSign.Y == 1 ? Direction.DecreaseY : // Наоборот.
                                directionSign.Y == -1 ? Direction.IncreaseY : // Наоборот.
                                Direction.None;
                        }
                        else //if (T.Z <= T.Y)
                        {
                            cellCoord.Z += directionSign.Z;
                            T.Z += D.Z;

                            distanceByAxises.Z += D.Z;
                            if (Math.Abs(cellCoord.Z - pRay.Position.Z) > pAxisDistance)
                            {
                                break;
                            }

                            face =
                                directionSign.Z == 1 ? Direction.DecreaseZ : // Наоборот.
                                directionSign.Z == -1 ? Direction.IncreaseZ : // Наоборот.
                                Direction.None;
                        }
                    }

                    //if (cellCoord.Y < 0 || cellCoord.Y > Core.ChunkSizes.Y)
                    //{
                    //    break;
                    //}

                    iterationsCount++;
                    if (iterationsCount > pAxisDistance * 3)
                    {
                        // HACK: Даное решение не является идеальным, но позволяет избежать вылета.
                        System.Diagnostics.Debug.WriteLine("Количество итераций трейсера привысило допустимый лимит.");
                        break;
                    }

                } // while (true)
            } // if (pRay.Direction != Vector3.Zero)
        }
        //private List<RayTracingData> GetBlocksByRay(Ray pRay, float pAxisDistance)
        //{
        //    List<RayTracingData> rayTracingDataList = null;
        //    if (pRay.Direction != Vector3.Zero)
        //    {
        //        rayTracingDataList = new List<RayTracingData>();

        //        // Ячейка.
        //        Vector3I cell = new Vector3I(
        //            (int) Math.Floor(pRay.Position.X),
        //            (int) Math.Floor(pRay.Position.Y),
        //            (int) Math.Floor(pRay.Position.Z));

        //        // Направление премещения по осям.
        //        Vector3I directionSign = new Vector3I(
        //            Math.Sign(pRay.Direction.X),
        //            Math.Sign(pRay.Direction.Y),
        //            Math.Sign(pRay.Direction.Z));

        //        // Показывает дискретные координаты следующей грани.
        //        Vector3I nextPlaneCoords = new Vector3I(
        //            cell.X + (directionSign.X > 0 ? 1 : 0),
        //            cell.Y + (directionSign.Y > 0 ? 1 : 0),
        //            cell.Z + (directionSign.Z > 0 ? 1 : 0));

        //        // ...
        //        Vector3 T = new Vector3(
        //            (nextPlaneCoords.X - pRay.Position.X) / pRay.Direction.X,
        //            (nextPlaneCoords.Y - pRay.Position.Y) / pRay.Direction.Y,
        //            (nextPlaneCoords.Z - pRay.Position.Z) / pRay.Direction.Z);

        //        if (float.IsNaN(T.X))
        //        {
        //            T.X = float.PositiveInfinity;
        //        }
        //        if (float.IsNaN(T.Y))
        //        {
        //            T.Y = float.PositiveInfinity;
        //        }
        //        if (float.IsNaN(T.Z))
        //        {
        //            T.Z = float.PositiveInfinity;
        //        }

        //        // ...
        //        Vector3 D = new Vector3(
        //            pRay.Direction.X == 0 ? float.PositiveInfinity : directionSign.X / pRay.Direction.X,
        //            pRay.Direction.Y == 0 ? float.PositiveInfinity : directionSign.Y / pRay.Direction.Y,
        //            pRay.Direction.Z == 0 ? float.PositiveInfinity : directionSign.Z / pRay.Direction.Z);

        //        if (float.IsNaN(D.X))
        //        {
        //            D.X = float.PositiveInfinity;
        //        }
        //        if (float.IsNaN(D.Y))
        //        {
        //            D.Y = float.PositiveInfinity;
        //        }
        //        if (float.IsNaN(D.Z))
        //        {
        //            D.Z = float.PositiveInfinity;
        //        }

        //        // А теперь цикл.

        //        BlockSide face = BlockSide.None;
        //        Vector3 contactPosition = Vector3.Zero;

        //        Vector3 maxDistanceByAxises = pRay.Direction * pAxisDistance;
        //        Vector3 distanceByAxises = Vector3.Zero;

        //        while (true)
        //        {
        //            //yield return C;

        //            if (cell.Y >= 0 && cell.Y < Core.ChunkSize.Y)
        //            {
        //                rayTracingDataList.Add(new RayTracingData(cell, face, contactPosition));
        //            }

        //            if (T.X < T.Y)
        //            {
        //                if (T.X < T.Z)
        //                {
        //                    cell.X += directionSign.X;
        //                    T.X += D.X;

        //                    distanceByAxises.X += D.X;
        //                    if (Math.Abs(cell.X - pRay.Position.X) > pAxisDistance)
        //                    {
        //                        break;
        //                    }

        //                    face =
        //                        directionSign.X == 1 ? BlockSide.DecreaseX : // Наоборот.
        //                        directionSign.X == -1 ? BlockSide.IncreaseX : // Наоборот.
        //                        BlockSide.None;

        //                }
        //                else //if (T.Z <= T.X)
        //                {
        //                    cell.Z += directionSign.Z;
        //                    T.Z += D.Z;

        //                    distanceByAxises.Z += D.Z;
        //                    if (Math.Abs(cell.Z - pRay.Position.Z) > pAxisDistance)
        //                    {
        //                        break;
        //                    }

        //                    face =
        //                        directionSign.Z == 1 ? BlockSide.DecreaseZ : // Наоборот.
        //                        directionSign.Z == -1 ? BlockSide.IncreaseZ : // Наоборот.
        //                        BlockSide.None;
        //                }
        //            }
        //            else // (T.Y <= T.X)
        //            {
        //                if (T.Y < T.Z)
        //                {
        //                    cell.Y += directionSign.Y;
        //                    T.Y += D.Y;

        //                    distanceByAxises.Y += D.Y;
        //                    if (Math.Abs(cell.Y - pRay.Position.Y) > pAxisDistance)
        //                    {
        //                        break;
        //                    }

        //                    face =
        //                        directionSign.Y == 1 ? BlockSide.DecreaseY : // Наоборот.
        //                        directionSign.Y == -1 ? BlockSide.IncreaseY : // Наоборот.
        //                        BlockSide.None;
        //                }
        //                else //if (T.Z <= T.Y)
        //                {
        //                    cell.Z += directionSign.Z;
        //                    T.Z += D.Z;

        //                    distanceByAxises.Z += D.Z;
        //                    if (Math.Abs(cell.Z - pRay.Position.Z) > pAxisDistance)
        //                    {
        //                        break;
        //                    }

        //                    face =
        //                        directionSign.Z == 1 ? BlockSide.DecreaseZ : // Наоборот.
        //                        directionSign.Z == -1 ? BlockSide.IncreaseZ : // Наоборот.
        //                        BlockSide.None;
        //                }
        //            }
        //        }
        //    }

        //    return rayTracingDataList;
        //}

        private void SetTarget(Vector3I pCoord, short pBlockID, BlockTemplate pBlock, Direction pFace)
        {
            this.target = new Target(pCoord, pBlockID, pBlock, pFace);
            BoundingBox blockBounds = pBlock.GetBounds(pCoord);
            this.targetBlockBox = new Box(this.game, Color.Black, blockBounds.Min - new Vector3(0.01f), blockBounds.Max + new Vector3(0.01f));
        }
        private void CleareTarget()
        {
            this.target = null;
            this.targetBlockBox = null;
        }

        // // //

        private void ProcessInput(GameTime pGameTime)
        {
            this.ProcessCustomKeyInput(pGameTime);

            if (this.activeForm != null)
            {
                if (this.guiKeyInputManager == null)
                {
                    this.guiKeyInputManager = new KeyInputManager(TimeSpan.FromSeconds(0.5f), TimeSpan.FromSeconds(0.05f));
                }
                if (this.guiMouseInputManager == null)
                {
                    this.guiMouseInputManager = new MouseInputManager();
                }

                this.ProcessGUIInput(pGameTime);
                this.game.IsMouseVisible = true;
            }
            else
            {
                this.ProcessPlayerInput(pGameTime);
                this.game.IsMouseVisible = false;
            }
        }

        private void ProcessCustomKeyInput(GameTime pGameTime)
        {
            this.customKeyInputManager.Update(pGameTime);
            if (this.customKeyInputManager.GetKeyInfo("Menu").IsPressedAndNotHundled)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))
                {
                    this.game.Exit();
                }

                if (this.activeForm == null)
                {
                    if (this.mainMenuForm == null)
                    {
                        this.InitializeMainMenuForm();
                    }

                    this.activeForm = this.mainMenuForm;
                }
                else
                {
                    this.activeForm = null;
                }

                this.customKeyInputManager.GetKeyInfo("Menu").SetHandled();
            }
            if (this.customKeyInputManager.GetKeyInfo("ChangeCameraMode").IsPressedAndNotHundled)
            {
                this.customKeyInputManager.GetKeyInfo("ChangeCameraMode").SetHandled();
                switch (this.cameraMode)
                {
                    case PlayerCameraMode.FirstPerson:
                        this.cameraMode = PlayerCameraMode.ThirdPerson;
                        break;
                    case PlayerCameraMode.ThirdPerson:
                        this.cameraMode = PlayerCameraMode.ThirdPersonOnFace;
                        break;
                    case PlayerCameraMode.ThirdPersonOnFace:
                        this.cameraMode = PlayerCameraMode.FirstPerson;
                        break;
                }
            }
            if (this.customKeyInputManager.GetKeyInfo("ToggleInterface").IsPressedAndNotHundled)
            {
                this.customKeyInputManager.GetKeyInfo("ToggleInterface").SetHandled();
                this.isShowInterface = !this.isShowInterface;
            }
            if (this.customKeyInputManager.GetKeyInfo("FullScreenToggle").IsPressedAndNotHundled)
            {
                this.customKeyInputManager.GetKeyInfo("FullScreenToggle").SetHandled();
                this.game.ToggleFullScreen();
            }
            if (this.customKeyInputManager.GetKeyInfo("Pause").IsPressedAndNotHundled)
            {
                this.customKeyInputManager.GetKeyInfo("Pause").SetHandled();
                this.world.IsPaused = !this.world.IsPaused;
            }
        }
        private void ProcessPlayerInput(GameTime pGameTime)
        {
            MouseState mouseState = Mouse.GetState();
            float yaw = (this.centericMousePosition.X - mouseState.X) * YawSpeed;
            float pinch = (this.centericMousePosition.Y - mouseState.Y) * PinchSpeed;
            this.CenterMousePosition();

            this.DetermineTargetBlock();
            this.UpdateAvatarMouseInput(pGameTime);

            this.customKeyInputManager.Update(pGameTime);

            this.avatar.ProcessInput(pGameTime, this.customKeyInputManager, yaw, pinch);
        }
        private void ProcessGUIInput(GameTime pGameTime)
        {
            this.guiKeyInputManager.Update(pGameTime);
            this.guiMouseInputManager.Update(pGameTime);
            this.activeForm.ProcessInput(this.guiMouseInputManager, this.guiKeyInputManager);
            this.activeForm.Update(pGameTime);
        }

        private void DrawDebugText(GameTime pGameTime)
        {
            this.spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);

            #region Чанк

            Chunk centerChunk = null;
            if (this.world.TryGetBaseChunk(out centerChunk)
                && centerChunk != null)
            {
                this.spriteBatch.DrawString(
                    this.debugFont,
                    string.Format(
                        "Chunk {0}: | Mobs: {1}",
                        centerChunk.Coord,
                        centerChunk.MobManager.MobsCount),
                    new Vector2(10, 40),
                    Core.DebugTextColor);
            }

            //Chunk centerChunk = null;
            //if (this.world.TryGetBaseChunk(out centerChunk)
            //    && centerChunk != null)
            //{
            //    this.spriteBatch.DrawString(
            //        this.debugFont,
            //        string.Format(
            //            "Chunk {0}:\nOpaque buffer: {1}\nNot opaque buffer: {2}\nParticles: {3}\nMobs: {4}",
            //            centerChunk.Coord,
            //            centerChunk.OpaqueBlocksVertexBuffer == null ?
            //                "-" :
            //                string.Format(
            //                    "{0}/{1} vertices | {2} | {3}/{4} bytes",
            //                    centerChunk.OpaqueBlocksVerticesCount,
            //                    centerChunk.OpaqueBlocksVertexBuffer.VertexCount,
            //                    Math.Floor((float) centerChunk.OpaqueBlocksVertexBuffer.VertexCount / Core.VertexBufferSegmentSize),
            //                    centerChunk.OpaqueBlocksVerticesCount * centerChunk.OpaqueBlocksVertexBuffer.VertexDeclaration.VertexStride,
            //                    centerChunk.OpaqueBlocksVertexBuffer.VertexCount * centerChunk.OpaqueBlocksVertexBuffer.VertexDeclaration.VertexStride),
            //            centerChunk.TransparentBlocksVertexBuffer == null ?
            //                "-" :
            //                string.Format(
            //                    "{0}/{1} vertices | {2} | {3}/{4} bytes",
            //                    centerChunk.TransparentBlocksVerticesCount,
            //                    centerChunk.TransparentBlocksVertexBuffer.VertexCount,
            //                    Math.Floor((float) centerChunk.TransparentBlocksVertexBuffer.VertexCount / Core.VertexBufferSegmentSize),
            //                    centerChunk.TransparentBlocksVerticesCount * centerChunk.TransparentBlocksVertexBuffer.VertexDeclaration.VertexStride,
            //                    centerChunk.TransparentBlocksVertexBuffer.VertexCount * centerChunk.TransparentBlocksVertexBuffer.VertexDeclaration.VertexStride),
            //            centerChunk.ParticleManager.ParticlesCount,
            //            centerChunk.MobManager.MobsCount),
            //        new Vector2(10, 40),
            //        Core.DebugTextColor);
            //}

            #endregion

            this.spriteBatch.DrawString(
                this.debugFont,
                string.Format(
                    "Camera mode: {0}",
                    this.cameraMode),
                new Vector2(10, 100),
                Core.DebugTextColor);

            this.spriteBatch.DrawString(
                this.debugFont,
                string.Format(
                    "Position: (x:{0:F2}; y:{1:F2}; z:{2:F2})",
                    this.avatar.Position.X,
                    this.avatar.Position.Y,
                    this.avatar.Position.Z),
                new Vector2(10, 110),
                Core.DebugTextColor);

            this.spriteBatch.DrawString(
                this.debugFont,
                string.Format(
                    "Look direction: x:{0:F2}; y:{1:F2}; z:{2:F2}",
                    this.avatar.LookDirection.X,
                    this.avatar.LookDirection.Y,
                    this.avatar.LookDirection.Z,
                    this.avatar.LookYaw,
                    this.avatar.LookPinch),
                new Vector2(10, 120),
                Core.DebugTextColor);

            this.spriteBatch.DrawString(
                this.debugFont,
                string.Format(
                    "yaw:{0:F4}π/{1:F1}°; pinch:{2:F4}π/{3:F1}°; {4}",
                    this.avatar.LookYaw,
                    MathHelper.ToDegrees(this.avatar.LookYaw),
                    this.avatar.LookPinch,
                    MathHelper.ToDegrees(this.avatar.LookPinch),
                    Core.GetCardinalDirectionName(Core.GetCardinalDirection(this.avatar.BodyYaw, Core.CompasRose.SixteenPoint), false)),
                new Vector2(10, 130),
                Core.DebugTextColor);

            this.spriteBatch.DrawString(
                this.debugFont,
                string.Format(
                    "Body direction: {0:F2}; {1:F2}; {2:F2}",
                    this.avatar.BodyDirection.X,
                    this.avatar.BodyDirection.Y,
                    this.avatar.BodyDirection.Z),
                new Vector2(10, 140),
                Core.DebugTextColor);

            string selectedBlockName = BlockRegister.Instance.Get(this.selectedBlockID).Name;
            this.spriteBatch.DrawString(
                this.debugFont,
                string.Format(
                    "Selected block: ({0}/{1}) #{2} {3}",
                    this.selectedBlockIndex + 1,
                    this.blockIdentifiers.Length,
                    this.selectedBlockID,
                    selectedBlockName),
                new Vector2(10, 150),
                Core.DebugTextColor);

            if (this.world.IsPaused)
            {
                string str = "PAUSE";
                this.spriteBatch.DrawString(
                    this.debugFont,
                    str,
                    new Vector2(
                        (this.game.GraphicsDevice.Viewport.Width - this.debugFont.MeasureString(str).X) * 0.5f,
                        (this.game.GraphicsDevice.Viewport.Height - this.debugFont.MeasureString(str).Y) * 0.5f - 20),
                    new Color(Core.DebugTextColor.ToVector4() * new Vector4(1, 1, 1, 0.25f + (float) Math.Abs(Math.Cos(pGameTime.TotalGameTime.TotalSeconds * 5.0f) * 0.75f))));
            }

            {
                string str = string.Format(
                    "PSE: {0:F2}",
                    Core.ParticleScaleExponente);
                this.spriteBatch.DrawString(
                    this.debugFont,
                    str,
                    new Vector2(
                        (this.game.GraphicsDevice.Viewport.Width - this.debugFont.MeasureString(str).X) - 10,
                        60),
                    Color.Black);
            }

            this.spriteBatch.End();

            this.world.DrawDebugText();
            this.game.DrawDebugText(pGameTime);

            //this.spriteBatch.Begin();
            //this.game.TextConsole.Draw(this.spriteBatch);
            //this.spriteBatch.End();
        }
        private void DrawCursor()
        {
            //this.spriteBatch.Begin();

            // Указатель.
            this.spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.PointWrap, DepthStencilState.DepthRead, RasterizerState.CullNone);
            this.spriteBatch.Draw(
                this.aimTexture,
                this.aimPosition,
                Color.White);
            this.spriteBatch.End();

            //this.spriteBatch.End();
        }

        //private void InitializeGUI()
        //{
        //}
        private void InitializeMainMenuForm()
        {
            Texture2D guiTexture = this.game.Content.Load<Texture2D>(@"Textures/GUI/gray");
            SpriteFont font = this.game.Content.Load<SpriteFont>(@"Fonts/GUI/default");

            // Main menu form
            //StretchedTextureManager formTextureManager = new StretchedTextureManager(
            //    guiTexture,
            //    new Rectangle(0, 0, 18, 18),
            //    4, 4, 4, 4);
            //Vector2 formSize = new Vector2(400, 300);
            //Vector2 formPosition = new Vector2(
            //    (this.game.GraphicsDevice.Viewport.Width - formSize.X) * 0.5f,
            //    (this.game.GraphicsDevice.Viewport.Height - formSize.Y) * 0.5f);
            //this.mainMenuForm = new Form(formTextureManager, formPosition, formSize);

            Texture2D formTexture = this.game.Content.Load<Texture2D>(@"Textures/GUI/menu");
            Vector2 formSize = new Vector2(this.game.GraphicsDevice.Viewport.Width, this.game.GraphicsDevice.Viewport.Height);
            this.mainMenuForm = new GameHelper.GUI.Tiled.Form(formTexture, Vector2.Zero, formSize);

            // "Exit" button
            StretchedButtonTextureManager buttonTextureManager = new StretchedButtonTextureManager(
               guiTexture,
               new Rectangle(18, 0, 18, 18),
               new Rectangle(36, 0, 18, 18),
               new Rectangle(18, 18, 18, 18),
               new Rectangle(36, 18, 18, 18),
               4, 4, 4, 4);
            Vector2 exitButtonSize = new Vector2(160, 30);
            Vector2 exitButtonPosition = new Vector2(
                50, //(this.mainMenuForm.InnerSize.X - exitButtonSize.X) * 0.5f,
                this.mainMenuForm.InnerSize.Y - exitButtonSize.Y - 50);
            Button exitButton = new Button(buttonTextureManager, exitButtonPosition, exitButtonSize, "EXIT", font, Color.Black, Color.White);
            exitButton.Pressed += new GameHelper.GUI.General.Events.UIEventHandler(exitButton_Pressed);
            this.mainMenuForm.Add(exitButton);
        }

        void exitButton_Pressed(GameHelper.GUI.General.Elements.UIElement pSender)
        {
            this.game.Exit();
        }

        /* Protected methods */



        /* Public methods */

        public void Initialize()
        {
            this.avatarMouseInputManager = new MouseInputManager();

            this.spriteBatch = new SpriteBatch(this.game.GraphicsDevice);
            this.customKeyInputManager = new CustomKeyInputManager();
            this.customKeyInputManager.RegisterKey("Menu", Keys.Escape);
            this.customKeyInputManager.RegisterKey("MoveForward", Keys.W);
            this.customKeyInputManager.RegisterKey("MoveBackward", Keys.S);
            this.customKeyInputManager.RegisterKey("MoveLeft", Keys.A);
            this.customKeyInputManager.RegisterKey("MoveRight", Keys.D);
            this.customKeyInputManager.RegisterKey("MoveUp", Keys.Space);
            this.customKeyInputManager.RegisterKey("MoveDown", Keys.LeftShift);
            this.customKeyInputManager.RegisterKey("ToggleInterface", Keys.F1);
            this.customKeyInputManager.RegisterKey("ChangeCameraMode", Keys.F5);
            this.customKeyInputManager.RegisterKey("FullScreenToggle", Keys.F11);
            this.customKeyInputManager.RegisterKey("LogScrollUp", Keys.PageUp);
            this.customKeyInputManager.RegisterKey("LogScrollDown", Keys.PageDown);
            this.customKeyInputManager.RegisterKey("Pause", Keys.P);
            this.customKeyInputManager.RegisterKey("Console", Keys.Enter);

            float aspectRatio = this.game.GraphicsDevice.GetAspectRation();
            this.camera = new Camera(this.avatar.Position, new Vector3(0, -5, 1), Vector3.Up, MathHelper.PiOver4, aspectRatio, 0.01f, 1000);
            this.camera.Initialize();
            this.cameraMode = Core.DefaultPlayerCameraMode;

            this.bodyDirectionArrow = new FlatArrow(this.game, this.avatar.Position, Color.SkyBlue, false, Core.ToPixels(4), Core.ToPixels(2), 0, Core.ToPixels(4), 2, Core.ToPixels(4));

            this.blockSelection = new Dictionary<Vector3I, Box>();

            this.blockIdentifiers = BlockRegister.Instance.GetKeys();
            this.selectedBlockIndex = 0;
            this.selectedBlockID = this.blockIdentifiers[this.selectedBlockIndex];

            this.isShowInterface = true;

            this.CenterMousePosition();
        }
        public void LoadContent()
        {
            this.debugFont = this.game.Content.Load<SpriteFont>(@"Fonts\SegoeUIMono8");

            this.aimTexture = this.game.Content.Load<Texture2D>(@"Textures\aim");
            this.aimPosition = new Vector2(
                (this.game.GraphicsDevice.Viewport.Width - this.aimTexture.Bounds.Width) / 2,
                (this.game.GraphicsDevice.Viewport.Height - this.aimTexture.Bounds.Height) / 2);
        }
        public void Update(GameTime pGameTime)
        {
            if (this.game.IsActive)
            {
                this.ProcessInput(pGameTime);
            }

            this.avatar.Update(pGameTime, this.world);

            this.camera.SetLayout(this.GetCameraPosition(), this.GetCameraDirection());
            //this.camera.Update(pGameTime);

            this.bodyDirectionArrow.Update(this.avatar.Position, this.avatar.BodyYaw, 0, 0);
            //this.lookDirectionArrow.Update(this.humanoid.Position + this.humanoid.GetEyesOffset(), 4, this.humanoid.LookYaw, this.humanoid.LookPinch, 0);
        }
        public void Draw(GameTime pGameTime)
        {
            if (this.activeForm != null)
            {
                this.activeForm.Refresh(this.spriteBatch);
            }

            this.game.GraphicsDevice.Clear(Color.CornflowerBlue);

            this.world.Draw(pGameTime, this.camera);

            this.game.GraphicsDevice.SamplerStates[0] = SamplerState.PointWrap;
            this.game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            if (this.cameraMode == PlayerCameraMode.ThirdPerson
                || this.cameraMode == PlayerCameraMode.ThirdPersonOnFace)
            {
                this.avatar.Draw(this.camera);
                //this.lookDirectionArrow.Draw(this.camera);
            }
            this.bodyDirectionArrow.Draw(this.camera);

            if (this.targetBlockBox != null)
            {
                this.targetBlockBox.Draw(this.camera);
            }

            if (this.activeForm != null)
            {
                //SpriteBatch formSpriteBatch = new SpriteBatch(this.game.GraphicsDevice);
                //this.activeForm.Refresh(formSpriteBatch);
                //this.activeForm.Draw(formSpriteBatch);

                this.activeForm.Refresh(this.spriteBatch);
                this.activeForm.Draw(this.spriteBatch);
            }
            else
            {
                if (this.isShowInterface)
                {
                    this.DrawCursor();
                    this.DrawDebugText(pGameTime);
                }
            }
        }

        // // //

        public BoundingBox GetBounds()
        {
            return this.avatar.Bounds;
        }

    }
}