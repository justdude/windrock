﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Threading;
using WindRock.Managers;
using WindRock.Entities;
using WindRock.Generators;
using WindRock.Common;
using GameHelper.Models;
using GameHelper.Cameras;
using GameHelper.Data;
using WindRock.Blocks;
using WindRock.Registers;
using WindRock.ParticleSystem;
using System.Diagnostics;
using WindRock.Metadatas;

namespace WindRock.Common
{
    public class World
    {
        // Fields

        private bool isPaused;

        private float gravity;

        private Random random;
        private WindRockGame game;
        private SpriteBatch spriteBatch;
        private SpriteFont SegoeUIMono8;
        private Vector2 chunksInfoPosition;

        private Vector3I baseChunkCoord;
        private Area3I activeArea;
        private Area3I visibleArea;
        private Dictionary<Vector3I, Chunk> chunks;
        private WorldGenerator generator;

        private Texture2D blocksTexture;
        private Effect blockEffect;
        private AlphaTestEffect alphaTestEffect;
        private List<Vector3I> chunksToLoadOrCreate;
        private Thread chunkGenerationThread;
        private Thread processBlockUpdatesPackagesThread;
        private MobSpawner mobSpawner;
        private WorldGeneratorSettings defaultGeneratorSettings;
        private WorldGeneratorSettings hellGeneratorSettings;

        private object chunksVertexBuffersLock;
        private ParticleManager particleManager;
        private List<BlockUpdatesPackage> blockUpdatesPackages;
        private BlockUpdatesPackage currentBlockUpdatesPackage;

        private Dictionary<Vector3I, StructureStampCollection> structureStamps;

        private Texture2D defaultBlockTexture;
        private Effect defaultBlockEffect;

        /* Properties */

        public bool IsPaused
        {
            get
            {
                return this.isPaused;
            }
            set
            {
                this.isPaused = value;
            }
        }
        public WindRockGame Game
        {
            get
            {
                return this.game;
            }
        }
        public Random Random
        {
            get
            {
                return this.random;
            }
        }
        public Vector3I BaseChunkCoord
        {
            set
            {
                if (this.baseChunkCoord != value)
                {
                    this.baseChunkCoord = value;

                    this.activeArea = new Area3I(
                        this.baseChunkCoord.X - Core.ActiveAreaRadiuses.X,
                        this.baseChunkCoord.X + Core.ActiveAreaRadiuses.X,
                        this.baseChunkCoord.Y - Core.ActiveAreaRadiuses.Y,
                        this.baseChunkCoord.Y + Core.ActiveAreaRadiuses.Y,
                        this.baseChunkCoord.Z - Core.ActiveAreaRadiuses.Z,
                        this.baseChunkCoord.Z + Core.ActiveAreaRadiuses.Z);

                    this.visibleArea = new Area3I(
                        this.baseChunkCoord.X - Core.VisibleAreaRadiuses.X,
                        this.baseChunkCoord.X + Core.VisibleAreaRadiuses.X,
                        this.baseChunkCoord.Y - Core.VisibleAreaRadiuses.Y,
                        this.baseChunkCoord.Y + Core.VisibleAreaRadiuses.Y,
                        this.baseChunkCoord.Z - Core.VisibleAreaRadiuses.Z,
                        this.baseChunkCoord.Z + Core.VisibleAreaRadiuses.Z);
                }
            }
            get
            {
                return this.baseChunkCoord;
            }
        }
        public Area3I ActiveArea
        {
            get
            {
                return this.activeArea;
            }
        }
        public Area3I VisibleArea
        {
            get
            {
                return this.visibleArea;
            }
        }
        public float Gravity
        {
            get
            {
                return this.gravity;
            }
        }
        public ParticleManager ParticleManager
        {
            get
            {
                return this.particleManager;
            }
        }

        /* Constructors */

        public World(WindRockGame pGame, float pGravity)
        {
            this.game = pGame;
            this.gravity = pGravity;

            this.spriteBatch = new SpriteBatch(this.game.GraphicsDevice);
            this.chunksInfoPosition = new Vector2(10, 200);
            this.blockEffect = this.game.Content.Load<Effect>(@"Effects\CustomEffect");
            this.SegoeUIMono8 = this.Game.Content.Load<SpriteFont>(@"Fonts\SegoeUIMono8");

            this.alphaTestEffect = new AlphaTestEffect(this.game.GraphicsDevice)
            {
                AlphaFunction = CompareFunction.Greater,
                ReferenceAlpha = 0
            };

            this.defaultGeneratorSettings = new WorldGeneratorSettings(
                0, 5,
                BlockRegister.StoneBlockID,
                BlockRegister.DirtBlockID, BlockRegister.Undefined,//BlockRegister.StonyDirtBlockID,
                0.1f, 1, 5,
                BlockRegister.SodBlockID, BlockRegister.Undefined,//BlockRegister.StonySodBlockID, 
                0.1f);

            this.hellGeneratorSettings = new WorldGeneratorSettings(
                0, 25,
                BlockRegister.HellStoneBlockID,
                BlockRegister.NotBurningFirestoneBlockID, BlockRegister.BurnedOutFirestoneBlockID, 0.2f, 1, 5,
                BlockRegister.NotBurningFirestoneBlockID, BlockRegister.HellQuartzBlockID, 0.025f);

            this.chunksVertexBuffersLock = new object();
        }

        /* Private methods */

        /// <summary>
        /// Тело потока chunkGenerationThread.
        /// По мере надобности создает или загружает сохраненные чанки.
        /// </summary>
        private void LoadOrCreateChunks()
        {
            while (true)
            {
                bool isNeedToLoadOrCreateChunks = false;

                lock (this.chunksToLoadOrCreate)
                {
                    isNeedToLoadOrCreateChunks = this.chunksToLoadOrCreate.Count > 0;
                }

                if (isNeedToLoadOrCreateChunks)
                {
                    try
                    {
                        Vector3I chunkCoord = Vector3I.Zero;
                        lock (this.chunksToLoadOrCreate)
                        {
                            chunkCoord = Vector3I.Zero;
                            float minDistance = float.PositiveInfinity;
                            for (int i = 0; i < this.chunksToLoadOrCreate.Count; i++)
                            {
                                float distance = (this.baseChunkCoord - this.chunksToLoadOrCreate[i]).Length();
                                if (distance < minDistance)
                                {
                                    minDistance = distance;
                                    chunkCoord = this.chunksToLoadOrCreate[i];
                                }
                            }

                            this.chunksToLoadOrCreate.Remove(chunkCoord);
                        }

                        Chunk chunk = this.generator.CreateChunk(chunkCoord, this.defaultGeneratorSettings);
                        //chunk.InitializeBlockRenderManager(this.game.GraphicsDevice, this.defaultBlockTexture, this.defaultBlockEffect);

                        lock (this.chunks)
                        {
                            this.chunks[chunkCoord] = chunk;
                        }

                        BlockUpdatesPackage blockUpdatesPackage = new BlockUpdatesPackage();

                        blockUpdatesPackage.AddChunk(chunkCoord);

                        Vector3I nearbyChunkCoord = chunkCoord - Vector3I.UnitX;
                        if (this.IsChunkExists(nearbyChunkCoord))
                        {
                            blockUpdatesPackage.AddChunk(nearbyChunkCoord);
                        }

                        nearbyChunkCoord = chunkCoord + Vector3I.UnitX;
                        if (this.IsChunkExists(nearbyChunkCoord))
                        {
                            blockUpdatesPackage.AddChunk(nearbyChunkCoord);
                        }

                        nearbyChunkCoord = chunkCoord - Vector3I.UnitY;
                        if (this.IsChunkExists(nearbyChunkCoord))
                        {
                            blockUpdatesPackage.AddChunk(nearbyChunkCoord);
                        }

                        nearbyChunkCoord = chunkCoord + Vector3I.UnitY;
                        if (this.IsChunkExists(nearbyChunkCoord))
                        {
                            blockUpdatesPackage.AddChunk(nearbyChunkCoord);
                        }

                        nearbyChunkCoord = chunkCoord - Vector3I.UnitZ;
                        if (this.IsChunkExists(nearbyChunkCoord))
                        {
                            blockUpdatesPackage.AddChunk(nearbyChunkCoord);
                        }

                        nearbyChunkCoord = chunkCoord + Vector3I.UnitZ;
                        if (this.IsChunkExists(nearbyChunkCoord))
                        {
                            blockUpdatesPackage.AddChunk(nearbyChunkCoord);
                        }

                        this.EnqueueBlockUpdatesPackage(blockUpdatesPackage);

                        StructureStampCollection stamps = null;
                        if (this.structureStamps.TryGetValue(chunk.Coord, out stamps))
                        {
                            this.ApplyStamps(chunk, stamps, true);
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex.Message);
                    }
                }
                else
                {
                    Thread.Sleep(TimeSpan.FromSeconds(1));
                }
            }
        }
        private void ProcessBlockUpdatesPackages()
        {
            BlockUpdatesPackage blockUpdatesPackage = null;

            while (true)
            {
                try
                {
                    if (this.TryDequeueBlockUpdatesPackage(out blockUpdatesPackage))
                    {
                        this.ApplyBlockUpdatesPackage(blockUpdatesPackage);
                    }
                    else
                    {
                        Thread.Sleep(50);
                    }
                }
                catch (ThreadInterruptedException)
                {
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }

                blockUpdatesPackage = null;
            }
        }
        private void UpdateActiveArea(Vector3 pPosition)
        {
            Vector3I newActiveAreaCenter = new Vector3I(
                (int) Math.Floor(pPosition.X / Core.ChunkSizes.X),
                (int) Math.Floor(pPosition.Y / Core.ChunkSizes.Y),
                (int) Math.Floor(pPosition.Z / Core.ChunkSizes.Z));

            if (newActiveAreaCenter != this.baseChunkCoord)
            {
                this.BaseChunkCoord = newActiveAreaCenter;
                CheckForLoadOrCreateChunksRequired();
            }
        }
        private void EnqueueBlockUpdatesPackage(BlockUpdatesPackage pBlockUpdatesPackage)
        {
            lock (this.blockUpdatesPackages)
            {
                this.blockUpdatesPackages.Add(pBlockUpdatesPackage);
            }
        }
        private bool TryDequeueBlockUpdatesPackage(out BlockUpdatesPackage pBlockUpdatesPackage)
        {
            bool result = false;

            lock (this.blockUpdatesPackages)
            {
                if (this.blockUpdatesPackages.Count > 0)
                {
                    pBlockUpdatesPackage = this.blockUpdatesPackages[0];
                    this.blockUpdatesPackages.RemoveAt(0);
                    result = true;
                }
                else
                {
                    pBlockUpdatesPackage = null;
                }
            }

            return result;
        }

        private void ApplyBlockUpdatesPackage(BlockUpdatesPackage pBlockUpdatesPackage)
        {
            List<Chunk> chunksToUpdateBlockMap = pBlockUpdatesPackage.GetChunks(this);
            foreach (Chunk chunkItem in chunksToUpdateBlockMap)
            {
                chunkItem.ApplyBlockBuffer(pBlockUpdatesPackage.GetBlocksBuffer(chunkItem.Coord));
            }

            List<Chunk> chunksToPrepareNewVertexBuffers = pBlockUpdatesPackage.GetChunksToUpdateVertexBuffers(this);
            Queue<Chunk> chunksToUseNewVertexBuffers = new Queue<Chunk>(chunksToPrepareNewVertexBuffers);

            Chunk chunk = null;
            int chunkIndex = chunksToPrepareNewVertexBuffers.Count - 1;
            while (chunksToPrepareNewVertexBuffers.Count > 0)
            {
                chunk = chunksToPrepareNewVertexBuffers[chunkIndex];
                if (Monitor.TryEnter(chunk.BlockMapLock))
                {
                    try
                    {
                        chunk.PrepareNewVertexBuffers(this.defaultBlockTexture, this.defaultBlockEffect);
                        chunksToPrepareNewVertexBuffers.RemoveAt(chunkIndex);
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex.Message);
                    }
                    finally
                    {
                        Monitor.Exit(chunk.BlockMapLock);
                    }
                }

                chunkIndex--;
                if (chunkIndex < 0)
                {
                    chunkIndex = chunksToPrepareNewVertexBuffers.Count - 1;
                }
            }

            Monitor.Enter(this.chunksVertexBuffersLock);
            try
            {
                while (chunksToUseNewVertexBuffers.Count > 0)
                {
                    chunksToUseNewVertexBuffers.Dequeue().UseNewVertexBuffers();
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            finally
            {
                Monitor.Exit(this.chunksVertexBuffersLock);
            }
        }

        /// <summary>
        /// Use for store updates making by player.
        /// </summary>
        private BlockUpdatesPackage playerBlockUpdatePackage;

        /* Protected methods */



        /* Public methods */

        public void Initialize()
        {
            this.random = new Random();

            this.chunks = new Dictionary<Vector3I, Chunk>();
            this.generator = new WorldGenerator(Core.Seed, this);

            this.blocksTexture = this.game.Content.Load<Texture2D>(@"Textures\blocks");
            //this.blockEffect.Texture = thisAtlasTexture;
            //this.blockEffect.TextureEnabled = true;

            //this.blockEffect.LightingEnabled = true;
            //this.blockEffect.DirectionalLight0.Enabled = true;
            //this.blockEffect.DirectionalLight0.Direction = new Vector3( -1, -2, -3 );
            //this.blockEffect.DirectionalLight0.DiffuseColor = new Vector3( 1, 1, 1 );
            //this.blockEffect.AmbientLightColor = new Vector3( 0.5f, 0.5f, 0.5f );

            this.chunksToLoadOrCreate = new List<Vector3I>();

            this.mobSpawner = new MobSpawner(this, 0, TimeSpan.FromSeconds(1000));
            this.mobSpawner.Initialize();

            this.particleManager = new ParticleManager(this);
            this.particleManager.Initialize();

            this.blockUpdatesPackages = new List<BlockUpdatesPackage>();
            this.currentBlockUpdatesPackage = new BlockUpdatesPackage();

            this.structureStamps = new Dictionary<Vector3I, StructureStampCollection>();

            // // //

            this.CheckForLoadOrCreateChunksRequired();

            this.chunkGenerationThread = new Thread(this.LoadOrCreateChunks)
            {
                IsBackground = true,
                Priority = ThreadPriority.Normal
            };
            this.chunkGenerationThread.Start();

            this.processBlockUpdatesPackagesThread = new Thread(this.ProcessBlockUpdatesPackages)
            {
                IsBackground = true,
                Priority = ThreadPriority.Normal
            };
            this.processBlockUpdatesPackagesThread.Start();

            //this.playerBlockUpdatePackage = new BlockUpdatesPackage();
        }
        public void Update(GameTime pGameTime, bool pIsTick, Vector3 pPlayerPosition)
        {
            if (this.playerBlockUpdatePackage != null)
            {
                this.ApplyBlockUpdatesPackage(this.playerBlockUpdatePackage);
                this.playerBlockUpdatePackage = null;
            }

            //Stopwatch stopwatch = new Stopwatch();
            //stopwatch.Start();

            UpdateActiveArea(pPlayerPosition);

            List<Chunk> activeChunks = this.GetActiveChunks();

            if (!this.isPaused)
            {
                foreach (Chunk chunk in activeChunks)
                {
                    chunk.Update(pGameTime, pIsTick);
                }

                if (!this.currentBlockUpdatesPackage.IsEmpty)
                {
                    this.EnqueueBlockUpdatesPackage(this.currentBlockUpdatesPackage);
                    this.currentBlockUpdatesPackage = new BlockUpdatesPackage();
                }
            }

            if (!this.isPaused)
            {
                this.mobSpawner.Update(pGameTime);
                this.particleManager.Update(pGameTime);
            }

            //stopwatch.Stop();
            //System.Diagnostics.Debug.WriteLine(
            //    "Word.Update() выполнен за {0}",
            //    stopwatch.Elapsed);
        }
        public void Draw(GameTime pGameTime, Camera pCamera)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            //try
            //{

            BoundingFrustum frustrum = new BoundingFrustum(pCamera.View * pCamera.Projection);
            this.game.GraphicsDevice.RasterizerState = RasterizerState.CullClockwise;

            List<Chunk> visibleChunks = this.GetVisibleChunks(frustrum);

            if (Core.IsUseAlterntiveBlockRenderSystem)
            {
                this.game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                lock (this.chunksVertexBuffersLock)
                {
                    foreach (Chunk chunk in visibleChunks)
                    {
                        chunk.Draw(pCamera);
                    }
                }
            }
            else
            {
                // Эффект для непрозрачных болков.
                this.blockEffect.Parameters["WorldViewProjection"].SetValue(pCamera.View * pCamera.Projection);
                this.blockEffect.Parameters["Texture"].SetValue(this.blocksTexture);

                // TODO: Изменить эффект для блоков с прозрачностью так, чобы он использовал матрицу WorldViewProjection вместо трех матриц World, View и Projection.
                // Эффект для блоков с прозрачностью.
                this.alphaTestEffect.World = Matrix.Identity;
                this.alphaTestEffect.View = pCamera.View;
                this.alphaTestEffect.Projection = pCamera.Projection;
                this.alphaTestEffect.Texture = this.blocksTexture;

                #region Старая логика
                //// Нерозрачные блоки.
                //#region
                //{
                //    this.game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                //    this.game.GraphicsDevice.BlendState = BlendState.Opaque;

                //    foreach (EffectPass pass in this.blockEffect.CurrentTechnique.Passes)
                //    {
                //        pass.Apply();
                //        foreach (Chunk chunk in visibleChunksInFrustrum)
                //        {
                //            if (chunk.OpaqueBlocksVertexBuffer != null)
                //            {
                //                this.game.GraphicsDevice.SetVertexBuffer(chunk.OpaqueBlocksVertexBuffer);
                //                this.game.GraphicsDevice.DrawPrimitives(
                //                    PrimitiveType.TriangleList,
                //                    0,
                //                    chunk.OpaqueBlocksVerticesCount / 3);
                //            }
                //        }
                //    }
                //}
                //#endregion

                //// Блоки с прозрачностью.
                //#region
                //{
                //    this.game.GraphicsDevice.BlendState = BlendState.AlphaBlend;

                //    foreach (EffectPass pass in this.alphaTestEffect.CurrentTechnique.Passes)
                //    {
                //        pass.Apply();
                //        foreach (Chunk chunk in visibleChunksInFrustrum)
                //        {
                //            if (chunk != null
                //                && chunk.TransparentBlocksVertexBuffer != null)
                //            {
                //                this.game.GraphicsDevice.SetVertexBuffer(chunk.TransparentBlocksVertexBuffer);
                //                this.game.GraphicsDevice.DrawPrimitives(
                //                    PrimitiveType.TriangleList,
                //                    0,
                //                    chunk.TransparentBlocksVerticesCount / 3);
                //            }
                //        }

                //    }
                //    this.game.GraphicsDevice.BlendState = BlendState.Opaque;
                //}
                //#endregion
                #endregion

                this.game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;

                Monitor.Enter(this.chunksVertexBuffersLock);
                try
                {
                    foreach (Chunk chunk in visibleChunks)
                    {
                        // Непрозрачные блоки.
                        if (chunk.OpaqueBlocksVertexBuffer != null)
                        {
                            this.blockEffect.CurrentTechnique.Passes[0].Apply();
                            this.game.GraphicsDevice.BlendState = BlendState.Opaque;
                            this.game.GraphicsDevice.SetVertexBuffer(chunk.OpaqueBlocksVertexBuffer);
                            this.game.GraphicsDevice.DrawPrimitives(
                                PrimitiveType.TriangleList,
                                0,
                                chunk.OpaqueBlocksVertexBuffer.VertexCount / 3);
                        }

                        // Прозрачные блоки.
                        if (chunk.TransparentBlocksVertexBuffer != null)
                        {
                            this.alphaTestEffect.CurrentTechnique.Passes[0].Apply();
                            this.game.GraphicsDevice.BlendState = BlendState.AlphaBlend;
                            this.game.GraphicsDevice.SetVertexBuffer(chunk.TransparentBlocksVertexBuffer);
                            this.game.GraphicsDevice.DrawPrimitives(
                                PrimitiveType.TriangleList,
                                0,
                                chunk.TransparentBlocksVertexBuffer.VertexCount / 3);
                        }
                    }
                }
                finally
                {
                    Monitor.Exit(this.chunksVertexBuffersLock);
                }

                this.game.GraphicsDevice.BlendState = BlendState.Opaque;
            }

            // Мобы.
            // Не обрабатываются в паралельных потоках.
            foreach (Chunk chunk in visibleChunks)
            {
                chunk.MobManager.Draw(pCamera);
            }

            // Система частиц.
            // Не обрабатывае в паралельных потоках.
            this.spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.PointWrap, DepthStencilState.Default, RasterizerState.CullCounterClockwise);
            this.ParticleManager.Draw(pCamera, this.spriteBatch, frustrum);
            this.spriteBatch.End();
            //}
            //catch(Exception ex)
            //{
            //    System.Diagnostics.Debug.WriteLine(ex.Message);
            //}

            stopwatch.Stop();
            System.Diagnostics.Debug.WriteLine("World.Draw: {0}", stopwatch.Elapsed.ToString());
        }

        // // //

        public void DrawDebugText()
        {
            // Информация о чанках.
            lock (this.chunks)
            {
                this.spriteBatch.Begin();
                this.spriteBatch.DrawString(
                    this.SegoeUIMono8,
                    string.Format(
                        "Cache: {0} chunks;\nTo load or create: {1}",
                        this.chunks.Count,
                        this.chunksToLoadOrCreate.Count),
                    this.chunksInfoPosition,
                    Core.DebugTextColor);
                this.spriteBatch.End();
            }
        }

        // // //

        public void CheckForLoadOrCreateChunksRequired()
        {
            for (int x = this.ActiveArea.MinX; x <= this.ActiveArea.MaxX; x++)
            {
                for (int y = this.ActiveArea.MinY; y <= this.ActiveArea.MaxY; y++)
                {
                    for (int z = this.ActiveArea.MinZ; z <= this.ActiveArea.MaxZ; z++)
                    {
                        lock (this.chunks)
                        {
                            Vector3I chunkCoord = new Vector3I(x, y, z);
                            if (Core.WorldArea.Contains(chunkCoord)
                                && !this.chunks.ContainsKey(chunkCoord))
                            {
                                this.chunks.Add(chunkCoord, null);
                                //this.chunksToLoadOrCreate.Enqueue(chunkCoord);
                                this.chunksToLoadOrCreate.Add(chunkCoord);
                                //this.chunkGenerationThread.Interrupt();
                            }
                        }
                    }
                }
            }
        }
        public bool TryGetChunk(Vector3I pChunkCoords, out Chunk pChunk)
        {
            lock (this.chunks)
            {
                return this.chunks.TryGetValue(pChunkCoords, out pChunk);
            }
        }
        public Chunk GetChunk(Vector3I pChunkCoords)
        {
            lock (this.chunks)
            {
                return this.chunks.FirstOrDefault(pair => pair.Key == pChunkCoords).Value;
            }
        }

        /// <summary>
        /// Возвращает ID блока по указаным мировым координатам.
        /// </summary>
        public short GetBlockID(int pX, int pY, int pZ)
        {
            short blockID = -1;

            Vector3I chunkCoord = new Vector3I(
                (int) Math.Floor((float) pX / Core.ChunkSizes.X),
                (int) Math.Floor((float) pY / Core.ChunkSizes.Y),
                (int) Math.Floor((float) pZ / Core.ChunkSizes.Z));

            Chunk chunk = null;
            if (this.chunks.TryGetValue(chunkCoord, out chunk) && chunk != null)
            {
                int inChunkX = pX % Core.ChunkSizes.X;
                int inChunkY = pY % Core.ChunkSizes.Y;
                int inChunkZ = pZ % Core.ChunkSizes.Z;
                blockID = chunk.GetBlockID(
                    inChunkX >= 0 ? inChunkX : Core.ChunkSizes.X + inChunkX,
                    inChunkY >= 0 ? inChunkY : Core.ChunkSizes.Y + inChunkY,
                    inChunkZ >= 0 ? inChunkZ : Core.ChunkSizes.Z + inChunkZ);
            }

            return blockID;
        }
        /// <summary>
        /// Возвращает ID блока по указаным мировым координатам.
        /// </summary>
        public short GetBlockID(Vector3I pCoord)
        {
            short blockID = -1;

            Vector3I chunkCoord = new Vector3I(
                (int) Math.Floor((float) pCoord.X / Core.ChunkSizes.X),
                (int) Math.Floor((float) pCoord.Y / Core.ChunkSizes.Y),
                (int) Math.Floor((float) pCoord.Z / Core.ChunkSizes.Z));

            Chunk chunk = null;
            if (this.chunks.TryGetValue(chunkCoord, out chunk) && chunk != null)
            {
                int inChunkX = pCoord.X % Core.ChunkSizes.X;
                int inChunkY = pCoord.Y % Core.ChunkSizes.Y;
                int inChunkZ = pCoord.Z % Core.ChunkSizes.Z;
                blockID = chunk.GetBlockID(
                    inChunkX >= 0 ? inChunkX : Core.ChunkSizes.X + inChunkX,
                    inChunkY >= 0 ? inChunkY : Core.ChunkSizes.Y + inChunkY,
                    inChunkZ >= 0 ? inChunkZ : Core.ChunkSizes.Z + inChunkZ);
            }

            return blockID;
        }

        // // //

        /// <summary>
        /// Устанавливает блок по указаным мировым координатам в буфер отложеной инициализации.
        /// </summary>
        public void SetBlockToBuffer(int pX, int pY, int pZ, short pID, BlockMetadata pData)
        {
            this.currentBlockUpdatesPackage.SetBlock(pX, pY, pZ, pID, pData);
        }
        /// <summary>
        /// Устанавливает блок по указаным мировым координатам в буфер отложеной инициализации.
        /// </summary>
        public void SetBlockToBuffer(Vector3I pCoord, short pID, BlockMetadata pData)
        {
            this.currentBlockUpdatesPackage.SetBlock(pCoord.X, pCoord.Y, pCoord.Z, pID, pData);
        }

        public void SetAirToBuffer(int pX, int pY, int pZ)
        {
            this.currentBlockUpdatesPackage.SetAir(pX, pY, pZ);
        }
        public void SetAirToBuffer(Vector3I pCoord)
        {
            this.currentBlockUpdatesPackage.SetAir(pCoord.X, pCoord.Y, pCoord.Z);
        }

        public void SetToUpdate(int pX, int pY, int pZ)
        {
            this.currentBlockUpdatesPackage.SetToUpdate(pX, pY, pZ);
        }
        public void SetToUpdate(Vector3I pCoord)
        {
            this.currentBlockUpdatesPackage.SetToUpdate(pCoord.X, pCoord.Y, pCoord.Z);
        }

        // // //

        /// <summary>
        /// Устанавливает блок по указаным мировым координатам в буфер отложеной инициализации.
        /// </summary>
        public void SetBlockToBufferByPlayer(int pX, int pY, int pZ, short pID, BlockMetadata pData)
        {
            if (this.playerBlockUpdatePackage == null)
            {
                this.playerBlockUpdatePackage = new BlockUpdatesPackage();
            }

            this.playerBlockUpdatePackage.SetBlock(pX, pY, pZ, pID, pData);
        }
        public void SetAirToBufferByPlayer(int pX, int pY, int pZ)
        {
            if (this.playerBlockUpdatePackage == null)
            {
                this.playerBlockUpdatePackage = new BlockUpdatesPackage();
            }

            this.playerBlockUpdatePackage.SetAir(pX, pY, pZ);
        }
        public void SetAirToBufferByPlayer(Vector3I pCoord)
        {
            if (this.playerBlockUpdatePackage == null)
            {
                this.playerBlockUpdatePackage = new BlockUpdatesPackage();
            }

            this.playerBlockUpdatePackage.SetAir(pCoord.X, pCoord.Y, pCoord.Z);
        }
        public void SetToBufferByPlayer(int pX, int pY, int pZ)
        {
            if (this.playerBlockUpdatePackage == null)
            {
                this.playerBlockUpdatePackage = new BlockUpdatesPackage();
            }

            this.playerBlockUpdatePackage.SetToUpdate(pX, pY, pZ);
        }

        // // //

        public void LoadContent()
        {
            this.defaultBlockEffect = this.game.Content.Load<Effect>(@"Effects/Blocks/default");
            this.defaultBlockTexture = this.game.Content.Load<Texture2D>(@"Textures/Blocks/default");
        }

        /// <summary>
        /// Получает данный блока по указаным мировым координатам.
        /// </summary>
        public BlockMetadata GetBlockData(int pX, int pY, int pZ)
        {
            BlockMetadata BlockData = null;

            Vector3I chunkCoords = new Vector3I(
                (int) Math.Floor(pX * Core.OneDivChunkSizes.X),
                (int) Math.Floor(pY * Core.OneDivChunkSizes.Y),
                (int) Math.Floor(pZ * Core.OneDivChunkSizes.Z));
            Chunk chunk = this.chunks[chunkCoords];

            if (chunk != null)
            {
                int inChunkX = pX % Core.ChunkSizes.X;
                int inChunkY = pY % Core.ChunkSizes.Y;
                int inChunkZ = pZ % Core.ChunkSizes.Z;
                Vector3I blockCoordInChunk = new Vector3I(
                    inChunkX >= 0 ? inChunkX : Core.ChunkSizes.X + inChunkX,
                    inChunkY >= 0 ? inChunkY : Core.ChunkSizes.Y + inChunkY,
                    inChunkZ >= 0 ? inChunkZ : Core.ChunkSizes.Z + inChunkZ);
                BlockData = chunk.GetBlockData(blockCoordInChunk);
            }

            return BlockData;
        }
        /// <summary>
        /// Получает данный блока по указаным мировым координатам.
        /// </summary>
        public BlockMetadata GetBlockData(Vector3I pCoord)
        {
            BlockMetadata BlockData = null;

            Vector3I chunkCoords = new Vector3I(
                (int) Math.Floor(pCoord.X * Core.OneDivChunkSizes.X),
                (int) Math.Floor(pCoord.Y * Core.OneDivChunkSizes.Y),
                (int) Math.Floor(pCoord.Z * Core.OneDivChunkSizes.Z));
            Chunk chunk = this.chunks[chunkCoords];

            if (chunk != null)
            {
                int inChunkX = pCoord.X % Core.ChunkSizes.X;
                int inChunkY = pCoord.Y % Core.ChunkSizes.Y;
                int inChunkZ = pCoord.Z % Core.ChunkSizes.Z;
                Vector3I blockCoordInChunk = new Vector3I(
                    inChunkX >= 0 ? inChunkX : Core.ChunkSizes.X + inChunkX,
                    inChunkY >= 0 ? inChunkY : Core.ChunkSizes.Y + inChunkY,
                    inChunkZ >= 0 ? inChunkZ : Core.ChunkSizes.Z + inChunkZ);
                BlockData = chunk.GetBlockData(blockCoordInChunk);
            }

            return BlockData;
        }

        /// <summary>
        /// Выполняет попытку получить данный блока по указаным мировым координатам.
        /// </summary>
        public bool TryGetBlockData(int pX, int pY, int pZ, out BlockMetadata pData)
        {
            bool result = false;

            Vector3I chunkCoords = new Vector3I(
                (int) Math.Floor(pX * Core.OneDivChunkSizes.X),
                (int) Math.Floor(pY * Core.OneDivChunkSizes.Y),
                (int) Math.Floor(pZ * Core.OneDivChunkSizes.Z));
            Chunk chunk = this.chunks[chunkCoords];

            if (chunk != null)
            {
                int inChunkX = pX % Core.ChunkSizes.X;
                int inChunkY = pY % Core.ChunkSizes.Y;
                int inChunkZ = pZ % Core.ChunkSizes.Z;
                Vector3I blockCoordInChunk = new Vector3I(
                    inChunkX >= 0 ? inChunkX : Core.ChunkSizes.X + inChunkX,
                    inChunkY >= 0 ? inChunkY : Core.ChunkSizes.Y + inChunkY,
                    inChunkZ >= 0 ? inChunkZ : Core.ChunkSizes.Z + inChunkZ);
                result = chunk.TryGetBlockData(blockCoordInChunk, out pData);
            }
            else
            {
                pData = null;
            }

            return result;
        }
        /// <summary>
        /// Выполняет попытку получить данный блока по указаным мировым координатам.
        /// </summary>
        public bool TryGetBlockData(Vector3I pCoord, out BlockMetadata pData)
        {
            bool result = false;

            Vector3I chunkCoords = new Vector3I(
                (int) Math.Floor(pCoord.X * Core.OneDivChunkSizes.X),
                (int) Math.Floor(pCoord.Y * Core.OneDivChunkSizes.Y),
                (int) Math.Floor(pCoord.Z * Core.OneDivChunkSizes.Z));
            Chunk chunk = this.chunks[chunkCoords];

            if (chunk != null)
            {
                int inChunkX = pCoord.X % Core.ChunkSizes.X;
                int inChunkY = pCoord.Y % Core.ChunkSizes.Y;
                int inChunkZ = pCoord.Z % Core.ChunkSizes.Z;
                Vector3I blockCoordInChunk = new Vector3I(
                    inChunkX >= 0 ? inChunkX : Core.ChunkSizes.X + inChunkX,
                    inChunkY >= 0 ? inChunkY : Core.ChunkSizes.Y + inChunkY,
                    inChunkZ >= 0 ? inChunkZ : Core.ChunkSizes.Z + inChunkZ);
                result = chunk.TryGetBlockData(blockCoordInChunk, out pData);
            }
            else
            {
                pData = null;
            }

            return result;
        }

        //public void SetChunkUpdateVerticesRequired(Vector2I pChunkCoords)
        //{
        //    Chunk chunk = this.GetChunk(pChunkCoords);
        //    if (chunk != null)
        //    {
        //        chunk.SetUpdateVerticesRequired();
        //    }
        //}
        /// <summary>
        /// Возвращает координату чанка по мировым кординатам блока.
        /// </summary>
        public static Vector3I GetCoordOfChunk(int pX, int pY, int pZ)
        {
            return new Vector3I(
                (int) Math.Floor(pX * Core.OneDivChunkSizes.X),
                (int) Math.Floor(pY * Core.OneDivChunkSizes.Y),
                (int) Math.Floor(pZ * Core.OneDivChunkSizes.Z));
        }
        /// <summary>
        /// Возвращает координату чанка по мировым кординатам блока.
        /// </summary>
        public static Vector3I GetCoordOfChunk(Vector3I pCoordOfBlock)
        {
            return new Vector3I(
                (int) Math.Floor(pCoordOfBlock.X * Core.OneDivChunkSizes.X),
                (int) Math.Floor(pCoordOfBlock.Y * Core.OneDivChunkSizes.Y),
                (int) Math.Floor(pCoordOfBlock.Z * Core.OneDivChunkSizes.Z));
        }
        public static void SplitCoords(Vector3I pBlockCoordInWorld, out Vector3I pBlockCoordInChunk, out Vector3I pChunkCoord)
        {
            int inChunkX = pBlockCoordInWorld.X % Core.ChunkSizes.X;
            int inChunkY = pBlockCoordInWorld.Y % Core.ChunkSizes.Y;
            int inChunkZ = pBlockCoordInWorld.Z % Core.ChunkSizes.Z;
            pBlockCoordInChunk = new Vector3I(
                inChunkX >= 0 ? inChunkX : Core.ChunkSizes.X + inChunkX,
                inChunkY >= 0 ? inChunkY : Core.ChunkSizes.Y + inChunkY,
                inChunkZ >= 0 ? inChunkZ : Core.ChunkSizes.Z + inChunkZ);
            pChunkCoord = new Vector3I(
                (int) Math.Floor(pBlockCoordInWorld.X * Core.OneDivChunkSizes.X),
                (int) Math.Floor(pBlockCoordInWorld.Y * Core.OneDivChunkSizes.Y),
                (int) Math.Floor(pBlockCoordInWorld.Z * Core.OneDivChunkSizes.Z));
        }
        public static void SplitCoords(int pX, int pY, int pZ, out Vector3I pBlockCoordInChunk, out Vector3I pChunkCoord)
        {
            int inChunkX = pX % Core.ChunkSizes.X;
            int inChunkY = pY % Core.ChunkSizes.Y;
            int inChunkZ = pZ % Core.ChunkSizes.Z;
            pBlockCoordInChunk = new Vector3I(
                inChunkX >= 0 ? inChunkX : Core.ChunkSizes.X + inChunkX,
                inChunkY >= 0 ? inChunkY : Core.ChunkSizes.Y + inChunkY,
                inChunkZ >= 0 ? inChunkZ : Core.ChunkSizes.Z + inChunkZ);
            pChunkCoord = new Vector3I(
                (int) Math.Floor(pX * Core.OneDivChunkSizes.X),
                (int) Math.Floor(pY * Core.OneDivChunkSizes.Y),
                (int) Math.Floor(pZ * Core.OneDivChunkSizes.Z));
        }

        /// <summary>
        /// Возвращает список непустых чанков, находащихся в активной области.
        /// </summary>
        public List<Chunk> GetActiveChunks()
        {
            lock (this.chunks)
            {
                return this.chunks.Values.Where(
                    chunk =>
                        chunk != null
                        && this.IsChunkInActiveArea(chunk.Coord)).ToList<Chunk>();
            }
        }
        /// <summary>
        /// Возвращает список непустых чанков, находящихся в видимой области.
        /// </summary>
        public List<Chunk> GetVisibleChunks()
        {
            lock (this.chunks)
            {
                return this.chunks.Values.Where(
                    chunk =>
                        chunk != null
                        && this.IsChunkInVisibleArea(chunk.Coord)).ToList<Chunk>();
            }
        }
        /// <summary>
        /// Возвращает список непустых чанков, находящихся в видимой области и обьектом BoundingFrustum.
        /// </summary>
        public List<Chunk> GetVisibleChunks(BoundingFrustum pFrustrum)
        {
            lock (this.chunks)
            {
                return this.chunks.Values.Where(
                    chunk =>
                        chunk != null
                        && this.IsChunkInVisibleArea(chunk.Coord)
                        && pFrustrum.Intersects(chunk.BoundingBox)).ToList<Chunk>();
            }
        }
        public bool IsChunkExists(Vector3I pCoord)
        {
            bool result = false;

            Chunk chunk = null;
            lock (this.chunks)
            {
                result =
                    this.chunks.TryGetValue(pCoord, out chunk)
                    && chunk != null;
            }

            return result;
        }

        /// <summary>
        /// Выполняет попытку получить базовый чанк.
        /// </summary>
        public bool TryGetBaseChunk(out Chunk pChunk)
        {
            lock (this.chunks)
            {
                return this.chunks.TryGetValue(this.baseChunkCoord, out pChunk);
            }
        }
        /// <summary>
        /// Выполняет проверку на вхождение чанка в активную область.
        /// </summary>
        public bool IsChunkInActiveArea(Vector3I pCoord)
        {
            return
                pCoord.X >= this.activeArea.MinX && pCoord.X <= this.activeArea.MaxX
                && pCoord.Y >= this.activeArea.MinY && pCoord.Y <= this.activeArea.MaxY
                && pCoord.Z >= this.activeArea.MinZ && pCoord.Z <= this.activeArea.MaxZ;
        }
        /// <summary>
        /// Выполняет проверку на вхождение чанка в видимую область.
        /// </summary>
        public bool IsChunkInVisibleArea(Vector3I pCoord)
        {
            return
                pCoord.X >= this.visibleArea.MinX && pCoord.X <= this.visibleArea.MaxX
                && pCoord.Y >= this.visibleArea.MinY && pCoord.Y <= this.visibleArea.MaxY
                && pCoord.Z >= this.visibleArea.MinZ && pCoord.Z <= this.visibleArea.MaxZ;
        }

        public void AddParticle(IParticleData pParticle)
        {
            this.ParticleManager.AddParticle(pParticle);
        }
        public void AddParticleEmitter(ParticleEmitter pParticleEmitter)
        {
            this.ParticleManager.AddParticleSpawner(pParticleEmitter);
        }
        public void RemoveParticleEmitter(ParticleEmitter pParticleEmitter)
        {
            this.ParticleManager.RemoveParticleSpawner(pParticleEmitter);
        }
        ///// <summary>
        ///// Устанавливает в очередь на обновление вершин чанк и, при необходимости, его соседей.
        ///// </summary>
        ///// <remarks>
        ///// Раньше метод назывался CheckChunksForVertexBuffersUpdateRequired.
        ///// Но это пи***ц как не доставляло...
        ///// </remarks>
        //public void SetVerticesUpdateRequired(Vector3I pChunkCoord, Vector3I pBlockCoordInChunk)
        //{
        //    lock (this.chunksToUpdateVertexBuffers)
        //    {
        //        this.chunksToUpdateVertexBuffers.Add(pChunkCoord);

        //        Vector3I nearbyChunkCoord = Vector3I.Zero;

        //        if (pBlockCoordInChunk.X == 0)
        //        {
        //            nearbyChunkCoord = pChunkCoord - Vector3I.UnitX;
        //            if (this.IsChunkExists(nearbyChunkCoord)
        //                && !this.chunksToUpdateVertexBuffers.Contains(nearbyChunkCoord))
        //            {
        //                this.chunksToUpdateVertexBuffers.Add(nearbyChunkCoord);
        //            }
        //        }
        //        else if (pBlockCoordInChunk.X == Core.ChunkSizes.X - 1)
        //        {
        //            nearbyChunkCoord = pChunkCoord + Vector3I.UnitX;
        //            if (this.IsChunkExists(nearbyChunkCoord)
        //                && !this.chunksToUpdateVertexBuffers.Contains(nearbyChunkCoord))
        //            {
        //                this.chunksToUpdateVertexBuffers.Add(nearbyChunkCoord);
        //            }
        //        }

        //        if (pBlockCoordInChunk.Y == 0)
        //        {
        //            nearbyChunkCoord = pChunkCoord - Vector3I.UnitY;
        //            if (this.IsChunkExists(nearbyChunkCoord)
        //                && !this.chunksToUpdateVertexBuffers.Contains(nearbyChunkCoord))
        //            {
        //                this.chunksToUpdateVertexBuffers.Add(nearbyChunkCoord);
        //            }
        //        }
        //        else if (pBlockCoordInChunk.Y == Core.ChunkSizes.Y - 1)
        //        {
        //            nearbyChunkCoord = pChunkCoord + Vector3I.UnitY;
        //            if (this.IsChunkExists(nearbyChunkCoord)
        //                && !this.chunksToUpdateVertexBuffers.Contains(nearbyChunkCoord))
        //            {
        //                this.chunksToUpdateVertexBuffers.Add(nearbyChunkCoord);
        //            }
        //        }

        //        if (pBlockCoordInChunk.Z == 0)
        //        {
        //            nearbyChunkCoord = pChunkCoord - Vector3I.UnitZ;
        //            if (this.IsChunkExists(nearbyChunkCoord)
        //                && !this.chunksToUpdateVertexBuffers.Contains(nearbyChunkCoord))
        //            {
        //                this.chunksToUpdateVertexBuffers.Add(nearbyChunkCoord);
        //            }
        //        }
        //        else if (pBlockCoordInChunk.Z == Core.ChunkSizes.Z - 1)
        //        {
        //            nearbyChunkCoord = pChunkCoord + Vector3I.UnitZ;
        //            if (this.IsChunkExists(nearbyChunkCoord)
        //                && !this.chunksToUpdateVertexBuffers.Contains(nearbyChunkCoord))
        //            {
        //                this.chunksToUpdateVertexBuffers.Add(nearbyChunkCoord);
        //            }
        //        }
        //    }
        //}

        /// <summary>
        /// Добавляет моба в мир.
        /// </summary>
        public void AddMob(Mob pMob)
        {
            Vector3I coordOfBlock = new Vector3I(
                (int) Math.Floor(pMob.Position.X),
                (int) Math.Floor(pMob.Position.Y),
                (int) Math.Floor(pMob.Position.Z));
            Chunk chunk = this.GetChunk(World.GetCoordOfChunk(coordOfBlock));
            chunk.MobManager.AddEntity(pMob);
        }

        public void AddStamps(StructurePattern pPatter, Vector3I pCoord)
        {
            Area3I patternArea = pPatter.GetArea(pCoord);
            Vector3I chunkAreaMin = World.GetCoordOfChunk(patternArea.Min);
            Vector3I chunkAreaMax = World.GetCoordOfChunk(patternArea.Max);

            //if (chunkAreaMin != chunkAreaMax)
            //{
            StructureStamp stamp = null;
            Vector3I chunkCoord = Vector3I.Zero;

            for (int x = chunkAreaMin.X; x <= chunkAreaMax.X; x++)
            {
                for (int y = chunkAreaMin.Y; y <= chunkAreaMax.Y; y++)
                {
                    for (int z = chunkAreaMin.Z; z <= chunkAreaMax.Z; z++)
                    {
                        try
                        {
                            chunkCoord = new Vector3I(x, y, z);
                            stamp = StructureStamp.Create(pPatter, pCoord, chunkCoord);

                            if (stamp != null)
                            {
                                StructureStampCollection stamps = null;
                                if (!this.structureStamps.TryGetValue(chunkCoord, out stamps))
                                {
                                    stamps = new StructureStampCollection();
                                    this.structureStamps.Add(chunkCoord, stamps);
                                }

                                stamps.Add(stamp);
                            }
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debug.WriteLine(ex.Message);
                        }
                    }
                }
            }
        }

        public bool TryGetStamps(Vector3I pChunkCoord, out StructureStampCollection pStamps)
        {
            return this.structureStamps.TryGetValue(pChunkCoord, out pStamps);
        }
        public void RemoveStamps(Vector3I pChunkCoord)
        {
            this.structureStamps.Remove(pChunkCoord);
        }
        public void ApplyStamps(Chunk pChunk, StructureStampCollection pStamps, bool pIsUseBuffer)
        {
            short[, ,] blockMap = Chunk.CreateBlockMap();

            for (int x = 0; x < Core.ChunkSizes.X; x++)
            {
                for (int z = 0; z < Core.ChunkSizes.Z; z++)
                {
                    for (int y = 0; y < Core.ChunkSizes.Y; y++)
                    {
                        blockMap[x, y, z] = -1;
                    }
                }
            }

            foreach (StructureStamp stamp in pStamps)
            {
                stamp.PressOn(ref blockMap);
            }

            pStamps.Clear();
            this.structureStamps.Remove(pChunk.Coord);

            short blockID = BlockRegister.AirBlockID;
            BlockTemplate block = null;
            BlockMetadata blockData = null;

            if (pIsUseBuffer)
            {
                for (int x = 0; x < Core.ChunkSizes.X; x++)
                {
                    for (int y = 0; y < Core.ChunkSizes.Y; y++)
                    {
                        for (int z = 0; z < Core.ChunkSizes.Z; z++)
                        {
                            blockID = blockMap[x, y, z];

                            if (blockID >= 0)
                            {
                                block = BlockRegister.Instance.Get(blockID);
                                blockData = block.CreateDefaultMetadata();
                                pChunk.SetBlock(x, y, z, blockID, blockData);
                            }
                        }
                    }
                }
            }
            else
            {
                BlockBuffer blockBuffer = new BlockBuffer();
                for (int x = 0; x < Core.ChunkSizes.X; x++)
                {
                    for (int y = 0; y < Core.ChunkSizes.Y; y++)
                    {
                        for (int z = 0; z < Core.ChunkSizes.Z; z++)
                        {
                            blockID = blockMap[x, y, z];

                            if (blockID >= 0)
                            {
                                if (blockID == BlockRegister.AirBlockID)
                                {
                                    blockBuffer.Add(
                                        new Vector3I(x, y, z),
                                        new BlockBufferItem(BlockRegister.AirBlockID, null));
                                }
                                else
                                {
                                    block = BlockRegister.Instance.Get(blockID);
                                    blockData = block.CreateDefaultMetadata();
                                    blockBuffer.Add(
                                        new Vector3I(x, y, z),
                                        new BlockBufferItem(blockID, blockData));
                                }
                            }
                        }
                    }
                }

                BlockUpdatesPackage blockUpdatesPackage = new BlockUpdatesPackage(pChunk.Coord, blockBuffer);
                this.EnqueueBlockUpdatesPackage(blockUpdatesPackage);
            }
        }

    }
}