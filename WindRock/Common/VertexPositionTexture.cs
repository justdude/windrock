﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace SpoonCraft.Common
{
    public struct VertexPositionTexture : IVertexType
    {
        // Fields

        private Vector3 position;
        private Vector2 textureCoords;
        private float shade;

        public static readonly VertexElement[] VertexElements =
        {
            new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
            new VertexElement(sizeof(float)*3, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0),
            new VertexElement(sizeof(float)*5, VertexElementFormat.Single, VertexElementUsage.TextureCoordinate, 1)
        };
        public static readonly VertexDeclaration VertexDeclaration = new VertexDeclaration( VertexElements );

        // Properties

        VertexDeclaration IVertexType.VertexDeclaration
        {
            get
            {
                return VertexDeclaration;
            }
        }
        public Vector3 Position
        {
            set
            {
                this.position = value;
            }
            get
            {
                return this.position;
            }
        }
        public Vector2 TextureCoords
        {
            set
            {
                this.textureCoords = value;
            }
            get
            {
                return this.textureCoords;
            }
        }

        public static int SizeInBytes
        {
            get
            {
                return sizeof( float ) * 6;
            }
        }

        // Constructors

        public VertexPositionTexture(Vector3 pPosition, Vector2 pTextureCoords)
        {
            this.position = pPosition;
            this.textureCoords = pTextureCoords;
            this.shade = 0.5f;
        }

        // Private methods



        // Protected methods



        // Public methods



    }
}