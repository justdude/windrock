﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.Data;
using WindRock.Blocks;
using WindRock.Registers;
using WindRock.Metadatas;

namespace WindRock.Common
{
    public class BlockUpdatesPackage
    {
        /* Fields */

        private Dictionary<Vector3I, BlockBuffer> blocksBuffers;

        /* Properties */

        public bool IsEmpty
        {
            get
            {
                return this.blocksBuffers.Count == 0;
            }
        }

        /* Constructors */

        public BlockUpdatesPackage()
        {
            this.blocksBuffers = new Dictionary<Vector3I, BlockBuffer>();
        }
        public BlockUpdatesPackage(Vector3I pChunkCoord, BlockBuffer pBlocksBuffer)
        {
            this.blocksBuffers = new Dictionary<Vector3I, BlockBuffer>();
            this.blocksBuffers.Add(pChunkCoord, pBlocksBuffer);
        }
        public BlockUpdatesPackage(Dictionary<Vector3I, BlockBuffer> pBlocksBuffers)
        {
            this.blocksBuffers = pBlocksBuffers;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public void SetBlock(int pX, int pY, int pZ, short pID, BlockMetadata pData)
        {
            try
            {
                Vector3I blockCoordInChunk = Vector3I.Zero;
                Vector3I chunkCoord = Vector3I.Zero;

                World.SplitCoords(pX, pY, pZ, out blockCoordInChunk, out chunkCoord);

                BlockBuffer blockBuffer = null;
                if (!this.blocksBuffers.TryGetValue(chunkCoord, out blockBuffer))
                {
                    blockBuffer = new BlockBuffer();
                    this.blocksBuffers.Add(chunkCoord, blockBuffer);
                }

                BlockBufferItem blockBufferItem = null;

                if (blockBuffer.TryGetValue(blockCoordInChunk, out blockBufferItem)
                    && blockBufferItem != null)
                {
                    blockBufferItem.BlockID = pID;
                    blockBufferItem.BlockData = pData;
                }
                else
                {
                    blockBufferItem = new BlockBufferItem(pID, pData);
                    blockBuffer.Add(blockCoordInChunk, blockBufferItem);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }
        public void SetAir(int pX, int pY, int pZ)
        {
            this.SetBlock(pX, pY, pZ, BlockRegister.AirBlockID, null);
        }
        public void SetToUpdate(int pX, int pY, int pZ)
        {
            Vector3I blockCoordInChunk = Vector3I.Zero;
            Vector3I chunkCoord = Vector3I.Zero;

            World.SplitCoords(pX, pY, pZ, out blockCoordInChunk, out chunkCoord);

            BlockBuffer blockBuffer = null;
            if (!this.blocksBuffers.TryGetValue(chunkCoord, out blockBuffer))
            {
                blockBuffer = new BlockBuffer();
                this.blocksBuffers.Add(chunkCoord, blockBuffer);
            }

            if (!blockBuffer.ContainsKey(blockCoordInChunk))
            {
                blockBuffer.Add(blockCoordInChunk, null);
            }
        }
        public short GetBlockID(Vector3I pBlockCoordInWorld, out BlockMetadata pData)
        {
            short blockID = BlockRegister.AirBlockID;

            Vector3I chunkCoord = World.GetCoordOfChunk(pBlockCoordInWorld);
            BlockBuffer blockBuffer = null;
            if (this.blocksBuffers.TryGetValue(chunkCoord, out blockBuffer))
            {
                BlockBufferItem blockBufferItem = null;
                Vector3I blockCoordInChunk = Chunk.GetBlockCoordInChunk(pBlockCoordInWorld);
                if (blockBuffer.TryGetValue(pBlockCoordInWorld, out blockBufferItem)
                    && blockBufferItem != null)
                {
                    blockID = blockBufferItem.BlockID;
                    pData = blockBufferItem.BlockData;
                }
                else
                {
                    pData = null;
                }
            }
            else
            {
                pData = null;
            }

            return blockID;
        }
        public short GetBlockID(Vector3I pBlockCoordInWorld)
        {
            short blockID = BlockRegister.AirBlockID;

            Vector3I chunkCoord = World.GetCoordOfChunk(pBlockCoordInWorld);
            BlockBuffer blockBuffer = null;
            if (this.blocksBuffers.TryGetValue(chunkCoord, out blockBuffer))
            {
                BlockBufferItem blockBufferItem = null;
                Vector3I blockCoordInChunk = Chunk.GetBlockCoordInChunk(pBlockCoordInWorld);
                if (blockBuffer.TryGetValue(pBlockCoordInWorld, out blockBufferItem)
                    && blockBufferItem != null)
                {
                    blockID = blockBufferItem.BlockID;
                }
            }

            return blockID;
        }
        public bool IsAir(Vector3I pCoord)
        {
            return this.GetBlockID(pCoord) == BlockRegister.AirBlockID;
        }
        public void AddChunk(Vector3I pCoord)
        {
            if (!this.blocksBuffers.ContainsKey(pCoord))
            {
                this.blocksBuffers.Add(pCoord, new BlockBuffer());
            }
        }

        public List<Chunk> GetChunks(World pWorld)
        {
            List<Chunk> chunks = new List<Chunk>(this.blocksBuffers.Count);

            foreach (Vector3I chunkCoord in this.blocksBuffers.Keys)
            {
                chunks.Add(pWorld.GetChunk(chunkCoord));
            }

            return chunks;
        }
        public List<Chunk> GetChunksToUpdateVertexBuffers(World pWorld)
        {
            List<Vector3I> chunkCoords = new List<Vector3I>();

            foreach (Vector3I chunkCoord in this.blocksBuffers.Keys)
            {
                if (!chunkCoords.Contains(chunkCoord))
                {
                    chunkCoords.Add(chunkCoord);
                }

                BlockBuffer blockBuffer = this.blocksBuffers[chunkCoord];
                foreach (Vector3I blockCoordInChunk in this.blocksBuffers.Keys)
                {
                    Vector3I nearbyChunkCoord = Vector3I.Zero;

                    if (blockCoordInChunk.X == 0)
                    {
                        nearbyChunkCoord = chunkCoord - Vector3I.UnitX;
                        if (pWorld.IsChunkExists(nearbyChunkCoord)
                            && !chunkCoords.Contains(nearbyChunkCoord))
                        {
                            chunkCoords.Add(nearbyChunkCoord);
                        }
                    }
                    else if (blockCoordInChunk.X == Core.ChunkSizes.X - 1)
                    {
                        nearbyChunkCoord = chunkCoord + Vector3I.UnitX;
                        if (pWorld.IsChunkExists(nearbyChunkCoord)
                            && !chunkCoords.Contains(nearbyChunkCoord))
                        {
                            chunkCoords.Add(nearbyChunkCoord);
                        }
                    }

                    if (blockCoordInChunk.Y == 0)
                    {
                        nearbyChunkCoord = chunkCoord - Vector3I.UnitY;
                        if (pWorld.IsChunkExists(nearbyChunkCoord)
                            && !chunkCoords.Contains(nearbyChunkCoord))
                        {
                            chunkCoords.Add(nearbyChunkCoord);
                        }
                    }
                    else if (blockCoordInChunk.Y == Core.ChunkSizes.Y - 1)
                    {
                        nearbyChunkCoord = chunkCoord + Vector3I.UnitY;
                        if (pWorld.IsChunkExists(nearbyChunkCoord)
                            && !chunkCoords.Contains(nearbyChunkCoord))
                        {
                            chunkCoords.Add(nearbyChunkCoord);
                        }
                    }

                    if (blockCoordInChunk.Z == 0)
                    {
                        nearbyChunkCoord = chunkCoord - Vector3I.UnitZ;
                        if (pWorld.IsChunkExists(nearbyChunkCoord)
                            && !chunkCoords.Contains(nearbyChunkCoord))
                        {
                            chunkCoords.Add(nearbyChunkCoord);
                        }
                    }
                    else if (blockCoordInChunk.Z == Core.ChunkSizes.Z - 1)
                    {
                        nearbyChunkCoord = chunkCoord + Vector3I.UnitZ;
                        if (pWorld.IsChunkExists(nearbyChunkCoord)
                            && !chunkCoords.Contains(nearbyChunkCoord))
                        {
                            chunkCoords.Add(nearbyChunkCoord);
                        }
                    }
                }
            }

            List<Chunk> chunks = new List<Chunk>();
            Chunk chunk = null;
            for (int i = 0; i < chunkCoords.Count; i++)
            {
                if (pWorld.TryGetChunk(chunkCoords[i], out chunk))
                {
                    chunks.Add(chunk);
                }
            }

            return chunks;
        }
        public BlockBuffer GetBlocksBuffer(Vector3I pChunkCoord)
        {
            return this.blocksBuffers[pChunkCoord];
        }

    }
}