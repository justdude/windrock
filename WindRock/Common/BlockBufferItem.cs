﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindRock.Blocks;
using WindRock.Metadatas;

namespace WindRock.Common
{
    public class BlockBufferItem
    {
        /* Fields */

        private short blockID;
        private BlockMetadata blockData;

        /* Properties */

        public short BlockID
        {
            get
            {
                return this.blockID;
            }
            set
            {
                this.blockID = value;
            }
        }
        public BlockMetadata BlockData
        {
            get
            {
                return this.blockData;
            }
            set
            {
                this.blockData = value;
            }
        }

        /* Constructors */

        public BlockBufferItem(short pBlockID, BlockMetadata pBlockData)
        {
            this.blockID = pBlockID;
            this.blockData = pBlockData;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}