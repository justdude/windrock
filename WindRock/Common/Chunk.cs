﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using WindRock.Common;
using WindRock.Entities;
using WindRock.Managers;
using GameHelper.Models;
using GameHelper.Data;
using WindRock.Blocks;
using WindRock.Registers;
using WindRock.ParticleSystem;
using System.Diagnostics;
using WindRock.Metadatas;
using WindRock.Blocks.Drawing;
using GameHelper.Cameras;

namespace WindRock.Common
{
    public class Chunk
    {
        /* Fields */

        private World world;
        private Vector3I coord;
        private short[, ,] blockMap;
        private BoundingBox boundingBox;
        private VertexBuffer opaqueBlocksVertexBuffer;
        private VertexBuffer newOpaqueBlocksVertexBuffer;
        private VertexBuffer transparentBlocksVertexBuffer;
        private VertexBuffer newTransparentBlocksVertexBuffer;
        //private VertexPositionTexture[] opaqueBlocksVertices;
        //private VertexPositionTexture[] transparentBlocksVertices;

        //private int opaqueBlocksVertexIndex;
        //private int transparentBlocksVertexIndex;

        private MobManager entityManager;

        private Dictionary<Vector3I, BlockMetadata> metadatas;

        //private int opaqueVertexIndex;
        //private int transparentVertexIndex;

        private Dictionary<Vector3I, Text> texts;
        private SpriteFont font;
        private Queue<Vector3I> blocksToUpdate;

        private int ticks;

        private object blockMapLock;

        private BlockRenderer blockRenderer;
        private BlockRenderer newBlockRenderer;

        /* Properties */

        public World World
        {
            get
            {
                return this.world;
            }
        }
        public Vector3I Coord
        {
            get
            {
                return this.coord;
            }
        }
        public BoundingBox BoundingBox
        {
            get
            {
                return this.boundingBox;
            }
        }
        public MobManager MobManager
        {
            get
            {
                return this.entityManager;
            }
        }

        //public int OpaqueBlocksVerticesCount
        //{
        //    get
        //    {
        //        return this.opaqueBlocksVertexIndex + 1;
        //    }
        //}
        //public int TransparentBlocksVerticesCount
        //{
        //    get
        //    {
        //        return this.transparentBlocksVertexIndex + 1;
        //    }
        //}
        public int Ticks
        {
            get
            {
                return this.ticks;
            }
        }
        // Обьекты блокировки.
        public object BlockMapLock
        {
            get
            {
                return this.blockMapLock;
            }
        }
        public VertexBuffer OpaqueBlocksVertexBuffer
        {
            get
            {
                return this.opaqueBlocksVertexBuffer;
            }
        }
        public VertexBuffer TransparentBlocksVertexBuffer
        {
            get
            {
                return this.transparentBlocksVertexBuffer;
            }
        }

        /* Constructors */

        public Chunk(World pWorld, Vector3I pCoord)
        {
            this.world = pWorld;
            this.coord = pCoord;
            this.blockMap = new short[Core.ChunkSizes.X, Core.ChunkSizes.Y, Core.ChunkSizes.Z];
            this.boundingBox = new BoundingBox(
                new Vector3(
                    this.coord.X * Core.ChunkSizes.X,
                    this.coord.Y * Core.ChunkSizes.Y,
                    this.coord.Z * Core.ChunkSizes.Z),
                new Vector3(
                    (this.coord.X + 1) * Core.ChunkSizes.X,
                    (this.coord.Y + 1) * Core.ChunkSizes.Y,
                    (this.coord.Z + 1) * Core.ChunkSizes.Z));

            this.entityManager = new MobManager(this.world.Game, this.world);
            this.entityManager.Initialize();

            this.metadatas = new Dictionary<Vector3I, BlockMetadata>();

            this.texts = new Dictionary<Vector3I, Text>();
            this.font = this.world.Game.Content.Load<SpriteFont>(@"Fonts\SegoeUIMono8");

            this.blocksToUpdate = new Queue<Vector3I>();

            this.blockMapLock = new object();
        }

        /* Private methods */

        private int GetVerticesCount(int pStartX, int pStartY, int pStartZ, bool pIsOpaque)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            int verticesCount = 0;

            BlockMetadata metadata = null;
            BlockTemplate block = null;

            for (int x = pStartX; x < Core.ChunkSizes.X; x++)
            {
                for (int y = pStartY; y < Core.ChunkSizes.Y; y++)
                {
                    for (int z = pStartZ; z < Core.ChunkSizes.Z; z++)
                    {
                        block = BlockRegister.Instance.Get(this.blockMap[x, y, z]);
                        if (block != null)
                        {
                            metadata = this.metadatas[new Vector3I(x, y, z)];
                            verticesCount += block.GetVerticesCount(metadata, pIsOpaque);
                        }
                    }
                }
            }

            stopwatch.Stop();
            System.Diagnostics.Debug.WriteLine(
                "Чанк {0}: Вершины посчитаны за {1}",
                this.coord,
                stopwatch.Elapsed);

            return verticesCount;
        }
        /// <summary>
        /// Добавляет блок в очередь на обязательное обновление.
        /// </summary>
        private void AddBlockToUpdateQueie(Vector3I pCoord)
        {
            if (!this.blocksToUpdate.Contains(pCoord))
            {
                this.blocksToUpdate.Enqueue(pCoord);
            }
        }
        /// <summary>
        /// Обновляет блоки, поставленый в очередь на обновление.
        /// </summary>
        private void UpdateBlocksInQueie()
        {
            if (this.blocksToUpdate.Count > 0)
            {
                Queue<Vector3I> newBlocksToUpdate = new Queue<Vector3I>();
                while (this.blocksToUpdate.Count > 0)
                {
                    Vector3I blockCoord = this.blocksToUpdate.Dequeue();
                    short blockID = this.blockMap[blockCoord.X, blockCoord.Y, blockCoord.Z];
                    if (blockID != BlockRegister.AirBlockID)
                    {
                        BlockTemplate block = BlockRegister.Instance.Get(blockID);
                        block.UpdateData(this.world, this.GetBlockCoordInWorld(blockCoord), ref newBlocksToUpdate);
                    }
                }

                this.blocksToUpdate = newBlocksToUpdate;
            }
        }
        /// <summary>
        /// Обновляет несколько случайных блоки в чанке.
        /// </summary>
        private void UpdateRandomBlocks()
        {
            for (int i = 0; i < Core.RandomChunkUpdateBlocksCount; i++)
            {
                Vector3I blockCoord = new Vector3I(
                    this.world.Random.Next(Core.ChunkSizes.X),
                    this.world.Random.Next(Core.ChunkSizes.Y),
                    this.world.Random.Next(Core.ChunkSizes.Z));
                short blockID = this.GetBlockID(blockCoord);
                if (blockID != BlockRegister.AirBlockID)
                {
                    BlockTemplate block = BlockRegister.Instance.Get(blockID);
                    block.UpdateData(this.world, this.GetBlockCoordInWorld(blockCoord), ref this.blocksToUpdate);
                }
            }
        }

        /* Protected methods */



        /* Public methods */

        public void InitializeBlockRenderManager(GraphicsDevice pGraphicsDevice, Texture2D pDefaultTexture, Effect pDefaultEffect)
        {
            this.blockRenderer = new BlockRenderer(pGraphicsDevice, pDefaultTexture, pDefaultEffect);
        }
        public void Update(GameTime pGameTime, bool pIsTick)
        {
            if (pIsTick)
            {
                this.ticks++;
                this.UpdateBlocksInQueie();
                this.UpdateRandomBlocks();
            }

            this.entityManager.Update(pGameTime, pIsTick);
        }

        // // //

        /// <summary>
        /// Возвращает ID блока из карты блоков.
        /// </summary>
        public short GetBlockID(int pX, int pY, int pZ)
        {
            return this.blockMap[pX, pY, pZ];
        }
        /// <summary>
        /// Возвращает ID блока из карты блоков.
        /// </summary>
        public short GetBlockID(Vector3I pCoord)
        {
            try
            {
                return this.blockMap[pCoord.X, pCoord.Y, pCoord.Z];
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return -1;
            }
        }

        /// <summary>
        /// Устанавливает блок напрямую в карту блоков.
        /// </summary>
        public void SetBlock(int pX, int pY, int pZ, short pID, BlockMetadata pData)
        {
            if (pData == null)
            {
                throw new NullReferenceException();
            }

            if (this.IsAir(pX, pY, pZ))
            {
                if (pID != BlockRegister.AirBlockID)
                {
                    this.blockMap[pX, pY, pZ] = pID;
                    Vector3I blockCoord = new Vector3I(pX, pY, pZ);
                    this.metadatas.Add(blockCoord, pData);
                }
            }
            else
            {
                if (pID == BlockRegister.AirBlockID)
                {
                    this.blockMap[pX, pY, pZ] = pID;
                    Vector3I blockCoord = new Vector3I(pX, pY, pZ);
                    this.metadatas.Remove(blockCoord);
                }
                else
                {
                    this.blockMap[pX, pY, pZ] = pID;
                    Vector3I blockCoord = new Vector3I(pX, pY, pZ);
                    this.metadatas[blockCoord] = pData;
                }
            }
        }
        /// <summary>
        /// Устанавливает блок напрямую в карту блоков.
        /// </summary>
        public void SetBlock(Vector3I pCoord, short pID, BlockMetadata pData)
        {
            if (pData == null)
            {
                throw new NullReferenceException();
            }

            if (this.IsAir(pCoord))
            {
                if (pID != BlockRegister.AirBlockID)
                {
                    this.blockMap[pCoord.X, pCoord.Y, pCoord.Z] = pID;
                    this.metadatas.Add(pCoord, pData);
                }
            }
            else
            {
                if (pID == BlockRegister.AirBlockID)
                {
                    this.blockMap[pCoord.X, pCoord.Y, pCoord.Z] = pID;
                    this.metadatas.Remove(pCoord);
                }
                else
                {
                    this.blockMap[pCoord.X, pCoord.Y, pCoord.Z] = pID;
                    this.metadatas[pCoord] = pData;
                }
            }
        }

        /// <summary>
        /// Устанавливает пустой блок в карту блоков.
        /// </summary>
        public void SetAir(int pX, int pY, int pZ)
        {
            Vector3I blockCoord = new Vector3I(pX, pY, pZ);
            this.blockMap[pX, pY, pZ] = BlockRegister.AirBlockID;
            this.metadatas.Remove(blockCoord);
        }
        /// <summary>
        /// Устанавливает пустой блок в карту блоков.
        /// </summary>
        public void SetAir(Vector3I pCoord)
        {
            this.blockMap[pCoord.X, pCoord.Y, pCoord.Z] = BlockRegister.AirBlockID;
            this.metadatas.Remove(pCoord);
        }

        /// <summary>
        /// Проверяет, является ли указаый блок пустым.
        /// </summary>
        public bool IsAir(int pX, int pY, int pZ)
        {
            return this.GetBlockID(pX, pY, pZ) == BlockRegister.AirBlockID;
        }
        /// <summary>
        /// Проверяет, является ли указаый блок пустым.
        /// </summary>
        public bool IsAir(Vector3I pCoord)
        {
            return this.GetBlockID(pCoord) == BlockRegister.AirBlockID;
        }

        /// <summary>
        /// Устанавливает блок через буффер отложеной инициализации блоков.
        /// Для внесения изменений в карту блоков следует вызвать метод AcceptBlockBuffer().
        /// </summary>
        public void ApplyBlockBuffer(BlockBuffer pBlockBuffer)
        {
            foreach (Vector3I blockCoord in pBlockBuffer.Keys)
            {
                BlockBufferItem blockBufferItem = pBlockBuffer[blockCoord];
                if (blockBufferItem != null)
                {
                    if (blockBufferItem.BlockID == BlockRegister.AirBlockID)
                    {
                        this.SetAir(blockCoord);
                    }
                    else
                    {
                        this.SetBlock(blockCoord, blockBufferItem.BlockID, blockBufferItem.BlockData);
                    }
                }
            }
        }

        /// <summary>
        /// Возвращает данные блока.
        /// </summary>
        public BlockMetadata GetBlockData(Vector3I pCoord)
        {
            return this.metadatas[pCoord];
        }
        /// <summary>
        /// Выполняет попытку получить данные блока.
        /// </summary>
        public bool TryGetBlockData(Vector3I pCoord, out BlockMetadata pBlockData)
        {
            return this.metadatas.TryGetValue(pCoord, out pBlockData);
        }

        // // //

        ///// <summary>
        ///// Обновляет вершини мэша блоков.
        ///// </summary>
        //public void UpdateVertices()
        //{
        //    // Определить видимые стороны блоков.
        //    this.UpdateBlocksVisibleSides();

        //    // Обновить массивы вершин.
        //    this.UpdateBlocksVertices();

        //    // Обновить буфера вершин.
        //    this.UpdateVertexBuffers();

        //    this.isUpdateVerticesRequired = false;
        //}

        /// <summary>
        /// Обновляет информацию о видимых стоонах блоков.
        /// </summary>
        public void UpdateBlocksVisibleSides()
        {
            BlockContext blockContext = new BlockContext(
                this,
                this.world.GetChunk(this.coord + Vector3I.UnitX),
                this.world.GetChunk(this.coord - Vector3I.UnitX),
                this.world.GetChunk(this.coord + Vector3I.UnitY),
                this.world.GetChunk(this.coord - Vector3I.UnitY),
                this.world.GetChunk(this.coord + Vector3I.UnitZ),
                this.world.GetChunk(this.coord - Vector3I.UnitZ));

            short blockID;
            BlockTemplate block = null;
            try
            {
                for (int x = 0; x < Core.ChunkSizes.X; x++)
                {
                    for (int y = 0; y < Core.ChunkSizes.Y; y++)
                    {
                        for (int z = 0; z < Core.ChunkSizes.Z; z++)
                        {
                            blockID = this.blockMap[x, y, z];
                            if (blockID != BlockRegister.AirBlockID)
                            {
                                block = BlockRegister.Instance.Get(blockID);
                                this.metadatas[new Vector3I(x, y, z)].VisibleSides = block.GetBlockSides(x, y, z, blockContext);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }
        /// <summary>
        /// Преобразует координаты блока внутри чанка в мировые координаты.
        /// </summary>
        public Vector3I GetBlockCoordInWorld(int pX, int pY, int pZ)
        {
            return new Vector3I(
                pX + this.coord.X * Core.ChunkSizes.X,
                pY + this.coord.Y * Core.ChunkSizes.Y,
                pZ + this.coord.Z * Core.ChunkSizes.Z);
        }
        /// <summary>
        /// Преобразует координаты блока внутри чанка в мировые координаты.
        /// </summary>
        public Vector3I GetBlockCoordInWorld(Vector3I pCoordInChunk)
        {
            return new Vector3I(
                pCoordInChunk.X + this.coord.X * Core.ChunkSizes.X,
                pCoordInChunk.Y + this.coord.Y * Core.ChunkSizes.Y,
                pCoordInChunk.Z + this.coord.Z * Core.ChunkSizes.Z);
        }
        /// <summary>
        /// Преобразует мировые координаты блока в координаты внутри чанка.
        /// </summary>
        public static Vector3I GetBlockCoordInChunk(Vector3I pCoordInWorld)
        {
            int inChunkX = pCoordInWorld.X % Core.ChunkSizes.X;
            int inChunkY = pCoordInWorld.Y % Core.ChunkSizes.Y;
            int inChunkZ = pCoordInWorld.Z % Core.ChunkSizes.Z;
            Vector3I coordInChunk = new Vector3I(
                inChunkX >= 0 ? inChunkX : Core.ChunkSizes.X + inChunkX,
                inChunkY >= 0 ? inChunkY : Core.ChunkSizes.Y + inChunkY,
                inChunkZ >= 0 ? inChunkZ : Core.ChunkSizes.Z + inChunkZ);

            return coordInChunk;
        }
        /// <summary>
        /// Преобразует мировую высоту в высоту внутри чанка.
        /// </summary>
        public static int GetHeightInChunk(int pHeightInWorld)
        {
            int heightInChunk = pHeightInWorld % Core.ChunkSizes.Y;
            return
                heightInChunk >= 0 ?
                heightInChunk :
                Core.ChunkSizes.Y + heightInChunk;
        }
        /// <summary>
        /// Преобразует высоту внутри чанка в мировую высоту.
        /// </summary>
        public int GetHeightInWorld(int pHeightInChunk)
        {
            return pHeightInChunk + this.coord.Y * Core.ChunkSizes.Y;
        }

        public void PrepareNewVertexBuffers(Texture2D pDefaultTexture, Effect pDefaultEffect)
        {
            if (Core.IsUseAlterntiveBlockRenderSystem)
            {
                this.UpdateBlocksVisibleSides();

                short blockID = -1;
                BlockMetadata BlockData = null;
                Vector3I blockPosition = Vector3I.Zero;
                BlockTemplate block = null;

                this.newBlockRenderer = new BlockRenderer(this.World.Game.GraphicsDevice, pDefaultTexture, pDefaultEffect);

                // Определить вершины.
                for (int x = 0; x < Core.ChunkSizes.X; x++)
                {
                    for (int y = 0; y < Core.ChunkSizes.Y; y++)
                    {
                        for (int z = 0; z < Core.ChunkSizes.Z; z++)
                        {
                            blockID = this.blockMap[x, y, z];
                            block = BlockRegister.Instance.Get(blockID);
                            if (block != null)
                            {
                                blockPosition = new Vector3I(
                                    this.coord.X * Core.ChunkSizes.X + x,
                                    this.coord.Y * Core.ChunkSizes.Y + y,
                                    this.coord.Z * Core.ChunkSizes.Z + z);
                                BlockData = this.metadatas[new Vector3I(x, y, z)];

                                try
                                {
                                    block.BuildVertices2(blockPosition, blockID, BlockData, ref this.newBlockRenderer);
                                }
                                catch (Exception ex)
                                {
                                    System.Diagnostics.Debug.WriteLine(ex.Message);
                                }
                            }
                        }
                    }
                }

                this.newBlockRenderer.GenerateVertexBuffers();
            }
            else
            {
                this.UpdateBlocksVisibleSides();

                BlockMetadata BlockData = null;
                Vector3I blockPosition = Vector3I.Zero;
                BlockTemplate block = null;

                VertexPositionTexture[] opaqueBlocksVertices = null;
                VertexPositionTexture[] transparentBlocksVertices = null;

                int opaqueBlocksVertexIndex = -1;
                int transparentBlocksVertexIndex = -1;

                int opaqueVertexCount = -1;
                int transparentVertexCount = -1;

                try
                {
                    // Буффер вершин непрозрачных блоков.
                    opaqueVertexCount = this.GetVerticesCount(0, 0, 0, true);
                    if (opaqueVertexCount > 0)
                    {
                        opaqueBlocksVertices = new VertexPositionTexture[opaqueVertexCount];
                        System.Diagnostics.Debug.WriteLine(
                            "Чанк {0}: Непрозрачные блоки: Определено {1} вершин.",
                            this.coord.ToString(),
                            opaqueVertexCount);
                    }

                    // Буффер вершин прозрачных блоков.
                    transparentVertexCount = this.GetVerticesCount(0, 0, 0, false);
                    if (transparentVertexCount > 0)
                    {
                        transparentBlocksVertices = new VertexPositionTexture[transparentVertexCount];
                        System.Diagnostics.Debug.WriteLine(
                            "Чанк {0}: Прозрачные блоки: Определено {1} вершин.",
                            this.coord.ToString(),
                            transparentVertexCount);
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }

                // Определить вершины.
                for (int x = 0; x < Core.ChunkSizes.X; x++)
                {
                    for (int y = 0; y < Core.ChunkSizes.Y; y++)
                    {
                        for (int z = 0; z < Core.ChunkSizes.Z; z++)
                        {
                            block = BlockRegister.Instance.Get(this.blockMap[x, y, z]);
                            if (block != null)
                            {
                                blockPosition = new Vector3I(
                                    this.coord.X * Core.ChunkSizes.X + x,
                                    this.coord.Y * Core.ChunkSizes.Y + y,
                                    this.coord.Z * Core.ChunkSizes.Z + z);
                                BlockData = this.metadatas[new Vector3I(x, y, z)];

                                try
                                {
                                    block.BuildVertices(
                                        blockPosition,
                                        BlockData,
                                        ref opaqueBlocksVertices, ref opaqueBlocksVertexIndex,
                                        ref transparentBlocksVertices, ref transparentBlocksVertexIndex);
                                }
                                catch (Exception ex)
                                {
                                    System.Diagnostics.Debug.WriteLine(ex.Message);
                                }
                            }
                        }
                    }
                }

                this.newOpaqueBlocksVertexBuffer = null;
                this.newTransparentBlocksVertexBuffer = null;

                // Обновить буффер непрозрачных вершин.
                if (opaqueVertexCount > 0)
                {
                    this.newOpaqueBlocksVertexBuffer = new VertexBuffer(
                        this.world.Game.GraphicsDevice,
                        typeof(VertexPositionTexture),
                        opaqueVertexCount,
                        BufferUsage.WriteOnly);
                    this.newOpaqueBlocksVertexBuffer.SetData<VertexPositionTexture>(opaqueBlocksVertices, 0, opaqueVertexCount);
                }

                // Обновить буффер прозрачных вершин.
                if (transparentVertexCount > 0)
                {
                    this.newTransparentBlocksVertexBuffer = new VertexBuffer(
                        this.world.Game.GraphicsDevice,
                        typeof(VertexPositionTexture),
                        transparentVertexCount,
                        BufferUsage.WriteOnly);
                    this.newTransparentBlocksVertexBuffer.SetData<VertexPositionTexture>(transparentBlocksVertices, 0, transparentVertexCount);
                }
            }
        }
        public void UseNewVertexBuffers()
        {
            if (Core.IsUseAlterntiveBlockRenderSystem)
            {
                if (this.blockRenderer != null)
                {
                    this.blockRenderer.Dispose();
                }
                this.blockRenderer = this.newBlockRenderer;
                this.newBlockRenderer = null;
            }
            else
            {
                this.opaqueBlocksVertexBuffer = this.newOpaqueBlocksVertexBuffer;
                this.transparentBlocksVertexBuffer = this.newTransparentBlocksVertexBuffer;

                this.newOpaqueBlocksVertexBuffer = null;
                this.newTransparentBlocksVertexBuffer = null;
            }
        }

        public static short[, ,] CreateBlockMap()
        {
            return new short[Core.ChunkSizes.X, Core.ChunkSizes.Y, Core.ChunkSizes.Z];
        }

        public void Draw(Camera pCamera, Matrix pWorld)
        {
            //this.blockRenderManager.Draw(pCamera, pWorld);
        }
        public void Draw(Camera pCamera)
        {
            if (this.blockRenderer != null)
            {
                this.blockRenderer.Draw(pCamera);
            }
        }

    }
}