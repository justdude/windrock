﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace SpoonCraft.Common
{
    public class TickGameTime
    {
        /* Fields */

        private GameTime gameTime;
        private int totalTicks;
        private int elapsedTicks;

        /* Properties */

        public GameTime GameTime
        {
            get
            {
                return this.gameTime;
            }
        }
        public TimeSpan TotalGameTime
        {
            get
            {
                return this.gameTime.TotalGameTime;
            }
        }
        public TimeSpan ElapsedGameTime
        {
            get
            {
                return this.gameTime.ElapsedGameTime;
            }
        }
        public int TotalTicks
        {
            get
            {
                return this.totalTicks;
            }
        }
        public int ElapsedTicks
        {
            get
            {
                return this.elapsedTicks;
            }
        }

        /* Constructors */

        public TickGameTime(GameTime pGameTime, int pTotalTicks, int pElapsedTicks)
        {
            this.gameTime = pGameTime;
            this.totalTicks = pTotalTicks;
            this.elapsedTicks = pElapsedTicks;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}