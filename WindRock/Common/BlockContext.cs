﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindRock.Common
{
    public class BlockContext
    {
        /* Fields */

        private Chunk chunk;
        private Chunk chunkIncreaseX;
        private Chunk chunkDecreaseX;
        private Chunk chunkIncreaseY;
        private Chunk chunkDecreaseY;
        private Chunk chunkIncreaseZ;
        private Chunk chunkDecreaseZ;

        /* Properties */

        public Chunk Chunk
        {
            get
            {
                return this.chunk;
            }
        }
        public Chunk ChunkIncreaseX
        {
            get
            {
                return this.chunkIncreaseX;
            }
        }
        public Chunk ChunkDecreaseX
        {
            get
            {
                return this.chunkDecreaseX;
            }
        }
        public Chunk ChunkIncreaseY
        {
            get
            {
                return this.chunkIncreaseY;
            }
        }
        public Chunk ChunkDecreaseY
        {
            get
            {
                return this.chunkDecreaseY;
            }
        }
        public Chunk ChunkIncreaseZ
        {
            get
            {
                return this.chunkIncreaseZ;
            }
        }
        public Chunk ChunkDecreaseZ
        {
            get
            {
                return this.chunkDecreaseZ;
            }
        }

        /* Constructors */

        public BlockContext(Chunk pChunk, Chunk pChunkIncreaseX, Chunk pChunkDecreaseX, Chunk pChunkIncreaseY, Chunk pChunkDecreaseY, Chunk pChunkIncreaseZ, Chunk pChunkDecreaseZ)
        {
            this.chunk = pChunk;
            this.chunkIncreaseX = pChunkIncreaseX;
            this.chunkDecreaseX = pChunkDecreaseX;
            this.chunkIncreaseY = pChunkIncreaseY;
            this.chunkDecreaseY = pChunkDecreaseY;
            this.chunkIncreaseZ = pChunkIncreaseZ;
            this.chunkDecreaseZ = pChunkDecreaseZ;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}