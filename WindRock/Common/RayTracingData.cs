﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using GameHelper.Data;
using WindRock.Blocks;
using GameHelper;

namespace WindRock.Common
{
    public class RayTracingData
    {
        /* Fields */

        private Vector3I coord;
        private Direction face;
        private Vector3 contancPosition;

        /* Properties */

        public Vector3I Coord
        {
            get
            {
                return this.coord;
            }
        }
        public Direction Face
        {
            get
            {
                return this.face;
            }
        }
        public Vector3 ContancPosition
        {
            get
            {
                return this.contancPosition;
            }
        }

        /* Constructors */

        public RayTracingData(Vector3I pCoord, Direction pFace, Vector3 pContancPosition)
        {
            this.coord = pCoord;
            this.face = pFace;
            this.contancPosition = pContancPosition;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}