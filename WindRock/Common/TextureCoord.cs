﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindRock.Common
{
    public struct TextureCoord
    {
        /* Fields */

        public byte Row;
        public byte Col;

        /* Properties */

        public static TextureCoord Zero
        {
            get
            {
                return new TextureCoord(0, 0);
            }
        }

        /* Constructors */

        public TextureCoord(byte pRow, byte pCol)
        {
            this.Row = pRow;
            this.Col = pCol;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}