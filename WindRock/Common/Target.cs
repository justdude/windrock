﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindRock.Common;
using GameHelper.Data;
using WindRock.Blocks;
using GameHelper;

namespace WindRock.Common
{
    public class Target
    {
        /* Fields */

        private Vector3I coord;
        private short blockID;
        private BlockTemplate block;
        private Direction face;

        /* Properties */

        public Vector3I Coords
        {
            get
            {
                return this.coord;
            }
        }
        public short BlockID
        {
            get
            {
                return this.blockID;
            }
        }
        public BlockTemplate Block
        {
            get
            {
                return this.block;
            }
        }
        public Direction Face
        {
            get
            {
                return this.face;
            }
        }

        /* Constructors */

        public Target(Vector3I pCoord, short pBlockID, BlockTemplate pBlock, Direction pFace)
        {
            this.coord = pCoord;
            this.blockID = pBlockID;
            this.block = pBlock;
            this.face = pFace;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}