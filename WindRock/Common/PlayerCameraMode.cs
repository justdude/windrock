﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindRock.Common
{
    public enum PlayerCameraMode : byte
    {
        FirstPerson = 0,
        ThirdPerson = 1,
        ThirdPersonOnFace = 2
    }
}