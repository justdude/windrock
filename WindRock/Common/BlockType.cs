﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpoonCraft.Engine
{
    public enum BlockType : byte
    {
        None = 0,
        Dirt = 1,
        Grass = 2,
        StonyDirt = 3,
        StonyGrass = 4,
        Cobblestone = 5,
        BigCobblestone = 6,
        IronOre = 7,
        GoldOre = 8,
        Max = 255
    }
}