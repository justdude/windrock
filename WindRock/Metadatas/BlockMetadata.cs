﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindRock.Common;
using GameHelper;

namespace WindRock.Metadatas
{
    public class BlockMetadata : Metadata
    {
        /* Fields */

        protected Direction visibleSides;

        /* Properties */

        public Direction VisibleSides
        {
            get
            {
                return this.visibleSides;
            }
            set
            {
                this.visibleSides = value;
            }
        }

        /* Constructors */

        public BlockMetadata()
            : base()
        {
            this.visibleSides = Direction.All;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}