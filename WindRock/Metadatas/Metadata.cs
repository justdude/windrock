﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindRock.Metadatas
{
    public class Metadata
    {
        /* Fields */

        protected Dictionary<string, object> data;

        /* Properties */



        /* Constructors */

        public Metadata()
        {
            this.data = new Dictionary<string, object>();
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public bool Has(string pKey)
        {
            return this.data.ContainsKey(pKey);
        }
        public T Get<T>(string pKey)
        {
            return (T) this.data[pKey];
        }
        public T GetOrDefault<T>(string pKey)
        {
            return this.data.ContainsKey(pKey) ? (T) this.data[pKey] : default(T);
        }
        public bool TryGet<T>(string pKey, out T pValue)
        {
            bool isContainsKey = this.data.ContainsKey(pKey);
            pValue = isContainsKey ? (T) this.data[pKey] : default(T);
            return isContainsKey;
        }
        public void Set(string pKey, object pValue)
        {
            if (this.data.ContainsKey(pKey))
            {
                this.data[pKey] = pValue;
            }
            else
            {
                this.data.Add(pKey, pValue);
            }
        }
        public void Drop(string pKey)
        {
            this.data.Remove(pKey);
        }

    }
}