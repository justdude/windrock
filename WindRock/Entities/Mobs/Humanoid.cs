﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using GameHelper.Input;

namespace WindRock.Entities
{
    public abstract class Humanoid : Mob
    {
        /* Fields */

        protected float lookYaw;
        protected float lookPinch;

        /* Properties */

        public float LookYaw
        {
            get
            {
                return this.lookYaw;
            }
        }
        public float LookPinch
        {
            get
            {
                return this.lookPinch;
            }
        }

        /* Constructors */

        public Humanoid(WindRockGame pGame, Vector3 pPosition, float pYaw, BoundingBox pBounds)
            : base(pGame, pPosition, pYaw, pBounds)
        {
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        ///// <summary>
        ///// Возвращает сдвиг головы относительно позиции моба.
        ///// </summary>
        //public abstract Vector3 GetHeadOffset();
        /// <summary>
        /// Возвращает сдвиг глаз относительно позиции головы.
        /// </summary>
        public abstract Vector3 GetEyesOffset();
        ///// <summary>
        ///// Возвращает сдвиг глаз относительно позиции головы с учетом дополнительных сдвига и поворота.
        ///// </summary>
        //public abstract Vector3 GetEyesOffset(Vector3 pExtraOffset);
        /// <summary>
        /// Выполняет обработку пользовательского ввода.
        /// </summary>
        public abstract void ProcessInput(GameTime pGameTime, CustomKeyInputManager pCustomKeyInputManager, float pYaw, float pPinch);

    }
}