﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindRock.Entities
{
    public enum MobState : byte
    {
        Stand,
        Walk,
        Attack
    }
}