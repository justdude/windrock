﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using GameHelper.Input;
using Microsoft.Xna.Framework.Input;
using WindRock.Common;

namespace WindRock.Entities
{
    public class Human : Humanoid
    {
        /* Constants */

        private static Vector3 HeadOffset = new Vector3(0, 1.5f, 0);
        private static Vector3 EyesOffset = new Vector3(0, 0.25f, 0.25f);

        /* Fields */

        private float speed;
        private float maxSpeed;
        private float acceleration;
        private int headBoneIndex;

        private bool isAjustByLookDirection;

        /* Properties */

        protected Vector3 MoveDirection
        {
            get
            {
                return this.bodyDirection;
            }
        }

        /* Constructors */

        public Human(WindRockGame pGame, Vector3 pPosition, float pYaw)
            : base(pGame, pPosition, pYaw, new BoundingBox())
        {
            this.bounds = this.GetBounds(pPosition);
            this.lookYaw = pYaw;
            this.lookPinch = 0;
            this.isFlying = true;
        }

        /* Private methods */

        private void UpdateLookDirection(GameTime pGameTime, float pYaw, float pPinch)
        {
            float time = (float) pGameTime.ElapsedGameTime.TotalSeconds;

            // Yaw.
            if (Math.Abs(pYaw) > 0.1f)
            {
                this.lookYaw = MathHelper.WrapAngle(this.lookYaw + pYaw * time);
            }

            // Pinch.
            if (Math.Abs(pPinch) > 0.1f)
            {
                this.lookPinch = MathHelper.Clamp(this.lookPinch + pPinch * time, -(MathHelper.PiOver2 * 0.999f), MathHelper.PiOver2 * 0.999f);
            }

            this.lookDirection = Vector3.Transform(Vector3.Forward, Matrix.CreateFromYawPitchRoll(this.lookYaw, this.lookPinch, 0));
        }
        private void UpdateBodyDirection(GameTime pGameTime)
        {
            this.bodyYaw = this.lookYaw;

            this.bodyDirection = Vector3.Transform(
                Vector3.Forward,
                Matrix.CreateFromYawPitchRoll(this.bodyYaw, 0, 0));

            //this.transformableModel.SetBoneTransform(
            //    this.transformableModel.GetBoneIndex("Body"),
            //    Matrix.CreateFromYawPitchRoll(this.bodyYaw, 0, 0));
        }
        private void UpdateVelocity(GameTime pGameTime, CustomKeyInputManager pCustomKeyInputManager)
        {
            float time = (float) pGameTime.ElapsedGameTime.TotalSeconds;
            this.speed = this.maxSpeed; //Math.Min(this.speed + this.acceleration * time, this.maxSpeed);
            bool isMoving = false;

            // Движение вперед или назад.
            if (pCustomKeyInputManager["MoveForward"].IsPressed)
            {
                //this.position += this.MoveDirection * this.speed * time;
                this.velocity = this.MoveDirection * this.speed * time;
                isMoving = true;
            }
            else if (pCustomKeyInputManager["MoveBackward"].IsPressed)
            {
                //this.position -= this.MoveDirection * this.speed * time;
                this.velocity = -this.MoveDirection * this.speed * time;
                isMoving = true;
            }

            // Движение вправо или влево.
            if (pCustomKeyInputManager["MoveRight"].IsPressed)
            {
                //this.position += Vector3.Cross(this.MoveDirection, Vector3.Up) * this.speed * time;
                this.velocity = Vector3.Cross(new Vector3(this.MoveDirection.X, 0, this.MoveDirection.Z), Vector3.Up) * this.speed * time;
                isMoving = true;
            }
            else if (pCustomKeyInputManager["MoveLeft"].IsPressed)
            {
                //this.position -= Vector3.Cross(this.MoveDirection, Vector3.Up) * this.speed * time;
                this.velocity = -Vector3.Cross(new Vector3(this.MoveDirection.X, 0, this.MoveDirection.Z), Vector3.Up) * this.speed * time;
                isMoving = true;
            }

            // Движение вверх или вниз.
            if (pCustomKeyInputManager["MoveUp"].IsPressed)
            {
                //this.position += Vector3.Up * this.speed * time;
                this.velocity = Vector3.Up * this.speed * time;
                isMoving = true;
            }
            else if (pCustomKeyInputManager["MoveDown"].IsPressed)
            {
                //this.position -= Vector3.Up * this.speed * time;
                this.velocity = -Vector3.Up * this.speed * time;
                isMoving = true;
            }

            if (!isMoving)
            {
                this.speed = 0;
                this.velocity = Vector3.Zero;
            }

        }

        /* Protected methods */



        /* Public methods */

        public override void Initialize()
        {
            base.Initialize();

            this.maxSpeed = 16.0f;
            this.acceleration = 16.0f;
        }
        public override void LoadContent()
        {
            //this.model = this.game.Content.Load<Model>(@"Models\Human\human");
            //this.headBoneIndex = this.model.Bones.IndexOf(this.model.Bones.FirstOrDefault(x => x.Name.Equals("Head"))); //GetBoneIndex("Head");

        }
        public override void Update(GameTime pGameTime, World pWorld)
        {
            //this.transformableModel.SetCustomTransform(this.headBoneIndex, Matrix.CreateFromYawPitchRoll(0, this.lookPinch, 0));
            base.Update(pGameTime, pWorld);

            //this.UpdatePosition(pGameTime, pWorld);
            Matrix rootTransform = Matrix.CreateFromYawPitchRoll(0, MathHelper.PiOver4, 0) * Matrix.CreateTranslation(this.position);
            //this.model.Update(rootTransform);
        }

        // // //

        public override string GetDefaultName()
        {
            return "Human";
        }
        public override void ProcessAI(GameTime pGameTime)
        {
            throw new NotImplementedException();
        }
        public override void ProcessInput(GameTime pGameTime, CustomKeyInputManager pCustomKeyInputManager, float pYaw, float pPinch)
        {
            this.UpdateLookDirection(pGameTime, pYaw, pPinch);
            this.UpdateBodyDirection(pGameTime);
            this.UpdateVelocity(pGameTime, pCustomKeyInputManager);
        }
        //public override Vector3 GetHeadOffset()
        //{
        //    return Vector3.Transform(HeadOffset, Matrix.CreateFromYawPitchRoll(this.bodyYaw, 0, 0));
        //}
        public override Vector3 GetEyesOffset()
        {
            return new Vector3(0, 1.8f, 0);
        }
        //public override Vector3 GetEyesOffset(Vector3 pExtraOffset)
        //{
        //    return Vector3.Transform(EyesOffset + pExtraOffset, Matrix.CreateFromYawPitchRoll(this.lookYaw, this.lookPinch, 0));
        //}
        public override BoundingBox GetBounds(Vector3 pPosition)
        {
            return new BoundingBox(
                pPosition - new Vector3(0.4f, 0, 0.4f),
                pPosition + new Vector3(0.4f, 1.8f, 0.4f));
        }
        public static bool TryAdjustSpawnCoord(ref int pX, ref int pY, ref int pZ, World pWorld)
        {
            return TryAdjustLandMobSpawnCoord(ref pX, ref pY, ref pZ, pWorld, 16, 64);
        }

    }
}