﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using GameHelper.Input;
using WindRock.Common;
using GameHelper.Models;
using GameHelper.Cameras;
using WindRock.Blocks;
using WindRock.Registers;
using GameHelper.Data;
using Microsoft.Xna.Framework.Graphics;

namespace WindRock.Entities
{
    public abstract class Mob : Entity
    {
        /* Fields */

        protected Vector3 lookDirection;
        protected float maxLookDeviation;
        protected string name;
        protected MobState state;
        protected Model model;
        protected bool isOnGround;
        protected bool isFlying;
        protected Vector3 velocity;

        /* Properties */

        public string Name
        {
            set
            {
                this.name = value;
            }
            get
            {
                return string.IsNullOrEmpty(this.name) ? this.GetDefaultName() : this.name;
            }
        }
        public Vector3 LookDirection
        {
            get
            {
                return this.lookDirection;
            }
        }
        public Model Model
        {
            get
            {
                return this.model;
            }
        }
        public MobState State
        {
            get
            {
                return this.state;
            }
        }
        public BoundingBox Bounds
        {
            get
            {
                return this.bounds;
            }
        }

        /* Constructors */

        public Mob(WindRockGame pGame, Vector3 pPosition, float pYaw, BoundingBox pBounds)
            : base(pGame, pPosition, pYaw, pBounds)
        {
            this.lookDirection = Vector3.Transform(Vector3.Forward, Matrix.CreateFromYawPitchRoll(this.bodyYaw, 0, 0));
            //this.game.TextConsole.WriteLine(string.Format("LookDirection created as {0}", this.lookDirection.ToString()));
        }

        /* Private methods */

        private static void Swap(ref int pValue1, ref int pValue2)
        {
            pValue1 = pValue1 + pValue2;
            pValue2 = pValue1 - pValue2;
            pValue1 = pValue1 - pValue2;
        }

        /* Protected methods */

        protected static bool TryAdjustLandMobSpawnCoord(ref int pX, ref int pY, ref int pZ, World pWorld, int pMinAltitude, int pMaxAltitude)
        {
            if (pMinAltitude > pMaxAltitude)
            {
                Swap(ref pMinAltitude, ref pMaxAltitude);
            }

            bool isSeccess = false;

            int y = pY;

            // Тип: наземное существо.
            // Альтитуда: 24 до 64.
            // Условия поиска:
            // - если твердый блок - подняться на 1 вверх;
            // - если вода - вернуть false;
            // - если воздух - вернуть true;
            // - - если под воздухом твердый блок - вернуть true;
            // - - если под воздухом воздух блок - опуститься на 1 вниз;
            // - - если под воздухом вода - вернуть false;
            short blockID = 0;
            while (y >= pMinAltitude && y <= pMaxAltitude)
            {
                blockID = pWorld.GetBlockID(pX, y, pZ);
                if (blockID == BlockRegister.AirBlockID
                    || !BlockRegister.Instance.Get(blockID).IsHard)
                {
                    return true;

                    short underBlockID = pWorld.GetBlockID(pX, y - 1, pZ);
                    if (underBlockID == BlockRegister.AirBlockID
                        || !BlockRegister.Instance.Get(underBlockID).IsHard)
                    {
                        y--;
                    }
                    else if (BlockRegister.Instance.Get(underBlockID).IsHard)
                    {
                        isSeccess = true;
                        pY = y;
                        break;
                    }
                }
                else if (BlockRegister.Instance.Get(blockID).IsHard)
                {
                    y++;
                }
                else
                {
                    break;
                }
            }

            return isSeccess;
        }
        protected void UpdatePosition(GameTime pGameTime, World pWorld)
        {
            Vector3 newPosition = this.position;
            Vector3 newVelocity = this.velocity;

            int minCoordX = (int) Math.Floor(this.bounds.Min.X + 0.5);
            int maxCoordX = (int) Math.Floor(this.bounds.Max.X + 0.5);
            int minCoordY = (int) Math.Floor(this.bounds.Min.Y + 0.5);
            int maxCoordY = (int) Math.Floor(this.bounds.Max.Y + 0.5);
            int minCoordZ = (int) Math.Floor(this.bounds.Min.Z + 0.5);
            int maxCoordZ = (int) Math.Floor(this.bounds.Max.Z + 0.5);

            // Это не особо грузи - можно не париться.
            for (int x = minCoordX; x < maxCoordX; x++)
            {
                for (int y = minCoordY; y < maxCoordY; y++)
                {
                    for (int z = minCoordZ; z < maxCoordZ; z++)
                    {
                        Vector3I blockCoord = new Vector3I(x, y, z);
                        short blockID = pWorld.GetBlockID(blockCoord);
                        BlockTemplate block = BlockRegister.Instance.Get(blockID);
                        if (block != null)
                        {
                            block.Touch(this, pWorld, blockCoord);
                        }

                    }
                }
            }

            if (!this.isFlying)
            {
                // А вот тут полная каша. Полюбасику нужно переделывать!
                // Блин, 4 часа...  хочется спать и жрать одновременно... Брбрбрбрбрбрбрбрбрбрбр!
                {
                    this.isOnGround = false;

                    float stepByY = 0.5f;

                    // Определяем область блоков для поиска коллизий путем округления крайних точек.
                    // Если текущий обект bounds B имеет интервал, например, по X is {-0.5; 0.5}, а координата P = {10; 15; -20},
                    // то интервал по X позиционированого bounds B' будет равен X is {9.5; 10.5}.
                    // Тогда блоки, которые могут пересекаться с B' следует проверять в интервале {Floor(B'.MinX + 0.5), Floor(B'.MaxX + 0.5)}.
                    // 0.5 - это сдвиг к центру блока.

                    for (float offsetY = 0; offsetY <= this.velocity.Y; offsetY += stepByY)
                    {
                        float y = this.bounds.Min.Y - offsetY;
                        int coordY = (int) Math.Floor(y);

                        if (coordY < 0 || coordY >= Core.ChunkSizes.Y)
                        {
                            // Моб выпал под мир или удрыстал искать планетян.
                            break;
                        }

                        // else

                        BoundingBox underMobBounds = new BoundingBox(
                            new Vector3(this.bounds.Min.X, y - offsetY - stepByY, this.bounds.Min.Z),
                            new Vector3(this.bounds.Max.X, y - offsetY, this.bounds.Max.Z));

                        for (int coordX = minCoordX; coordX <= maxCoordX; coordX++)
                        {
                            for (int coordZ = minCoordZ; coordZ <= maxCoordZ; coordZ++)
                            {
                                BlockTemplate block = BlockRegister.Instance.Get(pWorld.GetBlockID(coordX, coordY, coordZ));
                                if (block != null && block.IsHard)
                                {
                                    BoundingBox blockBounds = block.GetBounds(coordX, coordY, coordZ);
                                    if (underMobBounds.Intersects(blockBounds))
                                    {
                                        this.isOnGround = true;
                                        newPosition.Y = blockBounds.Max.Y + this.bounds.Min.Y - newPosition.Y;
                                        newVelocity.Y = 0;
                                        //break;
                                    }
                                }
                            }
                        }
                    }
                }

                if (!this.isOnGround)
                {
                    newPosition.Y -= newVelocity.Y;
                    newVelocity.Y += pWorld.Gravity * (float) pGameTime.ElapsedGameTime.TotalSeconds;
                }
            }

            newPosition += newVelocity;

            //// Движение вперед и всторону.
            //{
            //    this.bounds = this.GetBounds(this.position);
            //    Vector3 unitVelosity = Vector3.Normalize(this.velocity);
            //    float velocityLength = this.velocity.Length();

            //    Vector3 newPosition = this.position;

            //    for (float distance = Math.Max(1, velocityLength); distance < velocityLength; distance = Math.Max(distance++, velocityLength))
            //    {
            //        //Vector3 newPosition
            //        newPosition = this.position + unitVelosity * distance;
            //        BoundingBox newBounds = this.GetBounds(this.position);

            //        int minCoordX = (int) Math.Floor(newBounds.Min.X + 0.5);
            //        int maxCoordX = (int) Math.Floor(newBounds.Max.X + 0.5);
            //        int minCoordY = (int) Math.Floor(newBounds.Min.Y + 0.5);
            //        int maxCoordY = (int) Math.Floor(newBounds.Max.Y + 0.5);
            //        int minCoordZ = (int) Math.Floor(newBounds.Min.Z + 0.5);
            //        int maxCoordZ = (int) Math.Floor(newBounds.Max.Z + 0.5);

            //        for (int coordX = minCoordX; coordX <= maxCoordX; coordX++)
            //        {
            //            for (int coordY = minCoordY; coordY <= maxCoordY; coordY++)
            //            {
            //                for (int coordZ = minCoordZ; coordZ <= maxCoordZ; coordZ++)
            //                {
            //                    Block block = BlockRegister.Instance.GetBlock(pWorld.GetBlockID(coordX, coordY, coordZ));
            //                    if (block != null && block.IsHard)
            //                    {
            //                        Vector3 blockPosition = new Vector3(coordX + 0.5f, coordY + 0.5f, coordZ + 0.5f);
            //                        BoundingBox blockBounds = block.GetBounds(coordX, coordY, coordZ);
            //                        if (newBounds.Intersects(blockBounds))
            //                        {
            //                            float penitrationByX =
            //                                newPosition.X < blockPosition.X ?
            //                                newBounds.Max.X - blockBounds.Min.X :
            //                                blockBounds.Max.X - newBounds.Min.X;
            //                            float penitrationByY =
            //                                newPosition.Y < blockPosition.Y ?
            //                                newBounds.Max.Y - blockBounds.Min.Y :
            //                                blockBounds.Max.Y - newBounds.Min.Y;
            //                            float penitrationByZ =
            //                                newPosition.Z < blockPosition.Z ?
            //                                newBounds.Max.Z - blockBounds.Min.Z :
            //                                blockBounds.Max.Z - newBounds.Min.Z;
            //                            float maxPenitration = Math.Max(Math.Max(penitrationByX, penitrationByY), penitrationByZ);

            //                            if (penitrationByX == maxPenitration)
            //                            {
            //                                newPosition.X -= penitrationByX;
            //                                this.velocity.X = 0;
            //                            }

            //                            if (penitrationByY == maxPenitration)
            //                            {
            //                                newPosition.Y -= penitrationByY;
            //                                this.velocity.Y = 0;
            //                            }

            //                            if (penitrationByZ == maxPenitration)
            //                            {
            //                                newPosition.Z -= penitrationByZ;
            //                                this.velocity.Z = 0;
            //                            }

            //                            unitVelosity = Vector3.Normalize(this.velocity);
            //                            if (this.velocity.X == 0 && this.velocity.Y == 0 && this.velocity.Z == 0)
            //                            {
            //                                break;
            //                            }
            //                        }
            //                    }
            //                }
            //            }
            //        }

            //    }

            //    this.position = newPosition;
            //}

            this.position = newPosition;
            this.velocity = newVelocity;
        }

        /* Public methods */

        public override void Initialize()
        {
            this.state = MobState.Stand;
            this.box = new Box(this.game, Color.White, this.bounds.Min, this.bounds.Max);
        }
        public override void Update(GameTime pGameTime, World pWorld)
        {
            this.UpdatePosition(pGameTime, pWorld);
            this.bounds = this.GetBounds(this.position);
            this.box.SetBounds(this.bounds);
        }
        public override void Draw(Camera pCamera)
        {
            this.box.Draw(pCamera);
            //this.transformableModel.Draw(pCamera);
        }

        // // //

        //public void SetLayout(Vector3 pPosition, Vector3 pBodyDirection, Vector3 pLookDirection)
        //{
        //    this.position = pPosition;
        //    this.bodyDirection = Vector3.Normalize(pBodyDirection);
        //    this.lookDirection = Vector3.Normalize(pLookDirection);

        //    //float lookDeviation = (float) Math.Acos(Vector2.Dot(
        //    //    new Vector2(pDirection.X, pDirection.Z),
        //    //    new Vector2(pLookDirection.X, pLookDirection.Z)));

        //    //if (lookDeviation > this.maxLookDeviation)
        //    //{
        //    //    this.direction = pLookDirection;
        //    //}
        //    //else
        //    //{
        //    //    this.direction = pDirection;
        //    //}
        //}

        //public void SetPosition(Vector3 pPosition)
        //{
        //    this.position = pPosition;
        //}
        //public void SetMoveDirection(Vector3 pMoveDirection)
        //{
        //    this.moveDirection = pMoveDirection;
        //}
        //public void SetLookDirection(Vector3 pLookDirection)
        //{
        //    this.lookDirection = pLookDirection;
        //}

        /// <summary>
        /// Возвращает имя моба по умолчанию.
        /// </summary>
        public abstract string GetDefaultName();
        public abstract void ProcessAI(GameTime pGameTime);

    }
}