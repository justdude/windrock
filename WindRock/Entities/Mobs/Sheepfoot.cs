﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using SpoonCraft.Common;

namespace SpoonCraft.Entities
{
    public class Sheepfoot : Mob
    {
        /* Fields */

        private float speed;
        private float maxSpeed;
        private float acceleration;

        /* Properties */



        /* Constructors */

        public Sheepfoot(SpoonCraftGame pGame, Vector3 pPosition, float pYaw)
            : base(pGame, pPosition, pYaw, new BoundingBox())
        {
            this.bounds = this.GetBounds(pPosition);
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public override void Initialize()
        {
            base.Initialize();

            this.maxSpeed = 16.0f;
            this.acceleration = 16.0f;
        }
        public override void LoadContent()
        {
            this.model = new TransformableModel(this.game);
            this.model.LoadContent(@"Models\sheepfoot");
        }
        public override string GetDefaultName()
        {
            return "Human";
        }
        public override void ProcessAI(GameTime pGameTime)
        {
            throw new NotImplementedException();
        }
        public override BoundingBox GetBounds(Vector3 pPosition)
        {
            return new BoundingBox(
                pPosition - new Vector3(0.25f, 0, 0.25f),
                pPosition + new Vector3(0.25f, 1.125f, 0.25f));
        }
        public static bool TryAdjustSpawnCoord(ref int pX, ref int pY, ref int pZ, World pWorld)
        {
            return TryAdjustLandMobSpawnCoord(ref pX, ref pY, ref pZ, pWorld, 16, 64);
        }

    }
}