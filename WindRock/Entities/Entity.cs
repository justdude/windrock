﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using WindRock.Common;
using GameHelper.Models;
using GameHelper.Cameras;

namespace WindRock.Entities
{
    public abstract class Entity
    {
        /* Fields */

        protected WindRockGame game;
        protected Vector3 position;
        protected float bodyYaw;
        protected Vector3 bodyDirection;
        protected BoundingBox bounds;
        protected Box box;

        /* Properties */

        public Game Game
        {
            get
            {
                return this.game;
            }
        }
        public Vector3 Position
        {
            get
            {
                return this.position;
            }
        }
        public Vector3 BodyDirection
        {
            get
            {
                return this.bodyDirection;
            }
        }
        public float BodyYaw
        {
            get
            {
                return this.bodyYaw;
            }
        }

        /* Constructors */

        public Entity(WindRockGame pGame, Vector3 pPosition, float pYaw, BoundingBox pBounds)
        {
            this.game = pGame;
            this.position = pPosition;
            this.bodyYaw = pYaw;
            this.bounds = pBounds;
        }

        /* Private methods */

        //private void UpdateTransform()
        //{
        //}

        /* Protected methods */



        /* Public methods */

        public abstract void Initialize();
        public abstract void LoadContent();
        public abstract void Update(GameTime pGameTime, World pWorld);
        public abstract void Draw(Camera pCamera);

        // // //

        public abstract BoundingBox GetBounds(Vector3 pPosition);
    }
}