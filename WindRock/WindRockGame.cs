﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using WindRock.States;
using WindRock.Common;
using WindRock.Managers;
using WindRock.Entities;
using GameHelper.GUI;
using GameHelper.Data;
using GameHelper.Scripting;
using WindRock.Scripting;
using GameHelper;
using WindRock.Blocks;
using WindRock.Registers;
using WindRock.ParticleSystem;
using WindRock.Generators;

namespace WindRock
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class WindRockGame : Game
    {
        /* Fields */

        private static Vector2 gameInfoScreenPosition = new Vector2(10, 10);

        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private StateManager stateManager;
        private PerformanceDispecher performanceDispecher;
        //private CommandConsole<GameCommandContext, GameCommand> commandConsole;
        private GameCommandContext commandContext;
        private GameCommandRegister commandRegister;

        private SpriteFont segoeUIMono8;

        private Player player;
        private World world;

        private int totalTicks;
        private TimeSpan tickTimeout;
        private TimeSpan totalTickTime;
        private bool isTick;

        /* Properties */

        public StateManager StateManager
        {
            get
            {
                return this.stateManager;
            }
        }
        //public TextConsole TextConsole
        //{
        //    get
        //    {
        //        return this.commandConsole;
        //    }
        //}
        public Player Player
        {
            get
            {
                return this.player;
            }
        }
        public World World
        {
            get
            {
                return this.world;
            }
        }

        /* Constructor */

        public WindRockGame()
        {
            graphics = new GraphicsDeviceManager(this);

            int width = 1000;
            int height = (int) ((float) width * 9.0f / 16.0f);
            graphics.PreferredBackBufferWidth = width;
            graphics.PreferredBackBufferHeight = height;
            //graphics.IsFullScreen = true;
            Content.RootDirectory = "Content";
            IsMouseVisible = false;
            //IsFixedTimeStep = true;
            //TargetElapsedTime = TimeSpan.FromSeconds(1f / 2f);
        }

        /* Private methors */

        private void InitializeBlocks()
        {
            // Core block
            BlockRegister.Instance.Add(
                BlockRegister.BedrockBlockID,
                new SideBlockTemplate("Коренная порода", true, new TextureCoord(0, 0)));

            // Dirt block
            BlockRegister.Instance.Add(
                BlockRegister.DirtBlockID,
                new SideBlockTemplate("Земля", true, new TextureCoord(2, 1)));

            // Stony dirt block
            BlockRegister.Instance.Add(
                BlockRegister.StonyDirtBlockID,
                new SideBlockTemplate("Каменистая земля", true, new TextureCoord(2, 2)));

            // Sod block
            BlockRegister.Instance.Add(
                BlockRegister.SodBlockID,
                new SodBlockTemplate("Газон", true, new TextureCoord(1, 1), new TextureCoord(0, 1), new TextureCoord(2, 1)));

            // Stony sod block
            BlockRegister.Instance.Add(
                BlockRegister.StonySodBlockID,
                new SodBlockTemplate("Каменистый газон", true, new TextureCoord(1, 2), new TextureCoord(0, 2), new TextureCoord(2, 2)));

            // Stone block
            BlockRegister.Instance.Add(
                BlockRegister.StoneBlockID,
                new SideBlockTemplate("Камень", true, new TextureCoord(0, 3)));

            // Cobblestone block
            BlockRegister.Instance.Add(
                BlockRegister.CobblestoneBlockID,
                new SideBlockTemplate("Булыжник", true, new TextureCoord(1, 3)));

            // Granite
            BlockRegister.Instance.Add(
                BlockRegister.GraniteBlockID,
                new SideBlockTemplate("Гранит", true, new TextureCoord(0, 4)));

            // Iron ore block
            BlockRegister.Instance.Add(
                BlockRegister.IronOreBlockID,
                new SideBlockTemplate("Железная руда", true, new TextureCoord(2, 4)));

            //// Gold ore block
            //BlockRegister.Instance.Add(
            //    BlockRegister.GoldOreBlockID,
            //    new SolidBlockTemplate(true, new TextureCoord(2, 4)));

            // Glass block
            BlockRegister.Instance.Add(
                BlockRegister.GlassDirtBlockID,
                new SideBlockTemplate("Стекло", false, new TextureCoord(3, 0)));

            // Wood log block
            BlockRegister.Instance.Add(
                BlockRegister.WoodLogBlockID,
                new SideBlockTemplate("Бревно", true, new TextureCoord(5, 3), new TextureCoord(4, 3)));

            // Leafage block
            BlockRegister.Instance.Add(
                BlockRegister.LeafageBlockID,
                new SideBlockTemplate("Листва", false, new TextureCoord(5, 0)));

            //// Cobblestone slab
            //BlockRegister.Instance.Add(
            //    BlockRegister.CobblestoneSlabBlockID,
            //    new SlabBlock(true, new TextureCoord(1, 3)));

            //// Water block
            //BlockRegister.Instance.Add(
            //    BlockRegister.WaterBlockID,
            //    new LiquidBlockTemplate("Вода", false, new TextureCoord(4, 0), 5));

            //// Lava block
            //BlockRegister.Instance.Add(
            //    BlockRegister.LavaBlockID,
            //    new LiquidBlockTemplate("Лава", false, new TextureCoord(4, 1), 5));

            //// Dandelion block
            //BlockRegister.Instance.Add(
            //    BlockRegister.DandelionBlockID,
            //    new DandelionBlockTemplate("Одуванчик", new TextureCoord(7, 0), new TextureCoord(7, 1), new TextureCoord(7, 2), new TextureCoord(7, 3)));

            //// Grass block
            //BlockRegister.Instance.Add(
            //    BlockRegister.GrassBlockID,
            //    new PlantBlockTemplate("Трава", new TextureCoord(6, 0)));

            //// Wheat block
            //BlockRegister.Instance.Add(
            //    BlockRegister.WheatBlockID,
            //    new PlantBlockTemplate("Пшеница", new TextureCoord(6, 4), new TextureCoord(6, 5), new TextureCoord(6, 6), new TextureCoord(6, 7)));

            //// Stone slab block
            //BlockRegister.Instance.Add(
            //    BlockRegister.StoneSlabBlockID,
            //    new SlabBlockTemplate("Каменная плита", true, new TextureCoord(0, 3)));

            //// Cobblestone slab block
            //BlockRegister.Instance.Add(
            //    BlockRegister.CobblestoneSlabBlockID,
            //    new SlabBlockTemplate("Булыжниковая плита", true, new TextureCoord(1, 3)));

            // Wood planks block
            BlockRegister.Instance.Add(
                BlockRegister.WoodPlanksBlockID,
                new SideBlockTemplate("Доски", true, new TextureCoord(3, 5)));

            // Candy block
            BlockRegister.Instance.Add(
                BlockRegister.CandyBlockID,
                new SideBlockTemplate("Конфетный блок", true, new TextureCoord(4, 6)));

            // Hell stone block
            BlockRegister.Instance.Add(
                BlockRegister.HellStoneBlockID,
                new SideBlockTemplate("Адский камень", true, new TextureCoord(0, 6)));

            //// Not burning firestone block
            //BlockRegister.Instance.Add(
            //    BlockRegister.NotBurningFirestoneBlockID,
            //    new FirestoneBlockTemplate("Адский булыжник", new TextureCoord(0, 7), FirestoneState.NotBurning));

            //// Burning firestone block
            //BlockRegister.Instance.Add(
            //    BlockRegister.BurningFirestoneBlockID,
            //    new FirestoneBlockTemplate("Адский булыжник с серой (горящий)", new TextureCoord(0, 8), FirestoneState.Burning));

            //// Burned out firestone block
            //BlockRegister.Instance.Add(
            //    BlockRegister.BurnedOutFirestoneBlockID,
            //    new FirestoneBlockTemplate("Адский булыжник с серой (потухший)", new TextureCoord(0, 9), FirestoneState.BurnedOut));

            // Hell iron ore block
            BlockRegister.Instance.Add(
                BlockRegister.HellIronOreBlockID,
                new SideBlockTemplate("Адская железная руда", true, new TextureCoord(1, 6)));

            // Hell gold ore block
            BlockRegister.Instance.Add(
                BlockRegister.HellGoldOreBlockID,
                new SideBlockTemplate("Адская золотая руда", true, new TextureCoord(1, 7)));

            // Hell quartz
            BlockRegister.Instance.Add(
                BlockRegister.HellQuartzBlockID,
                new SideBlockTemplate("Чистый адский кварц", true, new TextureCoord(1, 8)));

            // Faded hell quartz
            BlockRegister.Instance.Add(
                BlockRegister.FadedHellQuartzBlockID,
                new SideBlockTemplate("Мутный адский кварц", true, new TextureCoord(1, 9)));

            // Dark hell quartz
            BlockRegister.Instance.Add(
                BlockRegister.DarkHellQuartzBlockID,
                new SideBlockTemplate("Темный адский кварц", true, new TextureCoord(1, 10)));

            //// Narcomany block
            //BlockRegister.Instance.Add(
            //    BlockRegister.NarcomanyBlockID,
            //    new SolidBlockTemplate(true, new TextureCoord(4, 7)));

            #region


            //// Lava block
            //block = new LiquidBlock();
            //(block as LiquidBlock).SetTexturesCoords(new TextureCoord(4, 1));
            //BlockRegister.Instance.Add(BlockRegister.LavaID, block);

            //// Grass
            //block = new PlantBlock();
            //(block as PlantBlock).SetTexturesCoords(new TextureCoord(6, 0));
            //BlockRegister.Instance.Add(BlockRegister.GrassID, block);

            //// Dandelion
            //block = new PlantBlock();
            //(block as PlantBlock).SetTexturesCoords(new TextureCoord(6, 1));
            //BlockRegister.Instance.Add(BlockRegister.Dandelion, block);

            //// Dandelion blooming
            //block = new PlantBlock();
            //(block as PlantBlock).SetTexturesCoords(new TextureCoord(6, 2));
            //BlockRegister.Instance.Add(BlockRegister.DandelionBlooming, block);

            //// Dandelion blank
            //block = new PlantBlock();
            //(block as PlantBlock).SetTexturesCoords(new TextureCoord(6, 3));
            //BlockRegister.Instance.Add(BlockRegister.DandelionBlank, block);
            #endregion

            foreach (BlockTemplate block in BlockRegister.Instance.GetAll())
            {
                block.LoadContent(this);
            }
        }
        private void InitializeBlocks2()
        {
            // Bedrock block
            BlockRegister.Instance.Add(
                BlockRegister.BedrockBlockID,
                new SideBlockTemplate("Коренная порода", true, this.Content.Load<Texture2D>(@"Textures/Blocks/bedrock")));

            // Dirt block
            BlockRegister.Instance.Add(
                BlockRegister.DirtBlockID,
                new SideBlockTemplate("Земля", true, this.Content.Load<Texture2D>(@"Textures/Blocks/dirt")));

            // Sod block
            BlockRegister.Instance.Add(
                BlockRegister.SodBlockID,
                new SideTopBottomBlockTemplate("Газон", true, this.Content.Load<Texture2D>(@"Textures/Blocks/sod")));

            //// Stony sod block
            //BlockRegister.Instance.Add(
            //    BlockRegister.StonySodBlockID,
            //    new SodBlockTemplate("Каменистый газон", true, new TextureCoord(1, 2), new TextureCoord(0, 2), new TextureCoord(2, 2)));

            // Stone block
            BlockRegister.Instance.Add(
                BlockRegister.StoneBlockID,
                new SideBlockTemplate("Камень", true, this.Content.Load<Texture2D>(@"Textures/Blocks/stone")));

            // Cobblestone block
            BlockRegister.Instance.Add(
                BlockRegister.CobblestoneBlockID,
                new SideBlockTemplate("Булыжник", true, this.Content.Load<Texture2D>(@"Textures/Blocks/cobblestone")));

            //// Leafage block
            //BlockRegister.Instance.Add(
            //    BlockRegister.LeafageBlockID,
            //    new CubeBlockTemplate("Листва", false, new TextureCoord(5, 0)));

            // Wood planks block
            BlockRegister.Instance.Add(
                BlockRegister.WoodPlanksBlockID,
                new SideBlockTemplate("Доски", true, this.Content.Load<Texture2D>(@"Textures/Blocks/planks")));

            // Piramide block
            BlockRegister.Instance.Add(
                BlockRegister.StonePiramideBlockID,
                new PiramideBlockTemplate("Пирамида из камня", true, this.Content.Load<Texture2D>(@"Textures/Blocks/stone")));

            // Piramide block
            BlockRegister.Instance.Add(
                BlockRegister.PlanksPiramideBlockID,
                new PiramideBlockTemplate("Пирамида из досок", true, this.Content.Load<Texture2D>(@"Textures/Blocks/planks")));

            foreach (BlockTemplate block in BlockRegister.Instance.GetAll())
            {
                block.LoadContent(this);
            }
        }
        private void InitializeCommands()
        {
            this.commandContext = new GameCommandContext(new Random(), this);
            this.commandRegister = new GameCommandRegister();
            this.commandRegister.Initialize();
        }
        private void InitializeParticles()
        {
            ParticleRegister register = ParticleRegister.Instance;

            Texture2D texture = Content.Load<Texture2D>(@"Textures/particles");

            register.Add(
                ParticleRegister.DandelionSeedParticleID,
                new SimpleParticleTemplate(texture, new Rectangle(0, 42, 7, 8), 1, TimeSpan.FromSeconds(5), false, false));
            register.Add(
                ParticleRegister.FireParticleID,
                new SimpleParticleTemplate(texture, new Rectangle(0, 23, 4, 7), 4, TimeSpan.FromSeconds(0.2f), false, true));
            register.Add(
                ParticleRegister.SmokeParticleID,
                new SimpleParticleTemplate(texture, new Rectangle(0, 33, 6, 6), 5, TimeSpan.FromSeconds(0.2f), true, true));
        }
        private void InitializeStructures()
        {
            StructureRegister register = StructureRegister.Instance;

            // Test cube
            #region
            {
                short a = BlockRegister.AirBlockID;
                short b = BlockRegister.GlassDirtBlockID;
                short c = BlockRegister.CobblestoneBlockID;

                StructurePattern pattern = new StructurePattern("Тестовый куб", new Vector3I(3, 3, 3), new Vector3I(-1, -2, -1));
                pattern.SetLayerXZ(
                    0,
                    new short[,]
                    {
                        {c, b, c},
                        {b, b, b},
                        {c, b, c}
                    });
                pattern.SetLayerXZ(
                    1,
                    new short[,]
                    {
                        {b, b, b},
                        {b, a, b},
                        {b, b, b}
                    });
                pattern.SetLayerXZ(
                    2,
                    new short[,]
                    {
                        {c, b, c},
                        {b, b, b},
                        {c, b, c}
                    });

                register.Add(StructureRegister.TestCubeID, pattern);
            }
            #endregion

            // Tree 1
            #region
            {
                short x = -1;
                //short a = BlockRegister.AirBlockID;
                short w = BlockRegister.WoodLogBlockID;
                short l = BlockRegister.LeafageBlockID;

                StructurePattern pattern = new StructurePattern("Дерево 1", new Vector3I(5, 5, 5), new Vector3I(-2, -5, -2));
                pattern.SetLayerXZ(
                    0,
                    new short[,]
                    {
                        {x, x, x, x, x},
                        {x, x, x, x, x},
                        {x, x, w, x, x},
                        {x, x, x, x, x},
                        {x, x, x, x, x},
                    });
                pattern.SetLayerXZ(
                    1,
                    new short[,]
                    {
                        {x, x, x, x, x},
                        {x, x, x, x, x},
                        {x, x, w, x, x},
                        {x, x, x, x, x},
                        {x, x, x, x, x},
                    });
                pattern.SetLayerXZ(
                    2,
                    new short[,]
                    {
                        {l, l, l, l, l},
                        {l, l, l, l, l},
                        {l, l, w, l, l},
                        {l, l, l, l, l},
                        {l, l, l, l, l},
                    });
                pattern.SetLayerXZ(
                    3,
                    new short[,]
                    {
                        {x, x, x, x, x},
                        {x, l, l, l, x},
                        {x, l, w, l, x},
                        {x, l, l, l, x},
                        {x, x, x, x, x},
                    });
                pattern.SetLayerXZ(
                    4,
                    new short[,]
                    {
                        {x, x, x, x, x},
                        {x, x, l, x, x},
                        {x, l, l, l, x},
                        {x, x, l, x, x},
                        {x, x, x, x, x},
                    });

                register.Add(StructureRegister.Tree1ID, pattern);
            }
            #endregion

        }

        private void CheckTick(GameTime pGameTime)
        {
            this.tickTimeout -= pGameTime.ElapsedGameTime;
            if (this.tickTimeout <= TimeSpan.Zero)
            {
                this.isTick = true;
                while (this.tickTimeout <= TimeSpan.Zero)
                {
                    this.tickTimeout += Core.TickDuration;
                }
                //this.tickTimeout = TimeSpan.FromTicks(-this.tickTimeout.Ticks % Core.TickDuration.Ticks);
                this.totalTicks++;
                this.totalTickTime += Core.TickDuration;
            }
        }

        /* Protected methors */

        protected override void Initialize()
        {
            this.performanceDispecher = new PerformanceDispecher(this);
            //this.Components.Add(this.performanceDispecher);

            this.InitializeBlocks2();
            this.InitializeCommands();
            this.InitializeParticles();
            this.InitializeStructures();

            this.GraphicsDevice.BlendState = BlendState.AlphaBlend;

            base.Initialize();
        }
        protected override void LoadContent()
        {
            this.spriteBatch = new SpriteBatch(GraphicsDevice);

            this.segoeUIMono8 = this.Content.Load<SpriteFont>(@"Fonts\segoeUIMono8");
            segoeUIMono8.LineSpacing = (int) (segoeUIMono8.LineSpacing * 0.8);

            Vector2 textConsoleSize = new Vector2(
                (this.GraphicsDevice.Viewport.Width - 10) * 0.5f,
                100);
            Vector2 textConsolePosition = new Vector2(
                10,
                this.GraphicsDevice.Viewport.Height - 30 - textConsoleSize.Y);
            //this.commandConsole = new CommandConsole<GameCommandContext, GameCommand>(
            //    this,
            //    textConsolePosition,
            //    textConsoleSize,
            //    this.segoeUIMono8,
            //    Color.Black,
            //    Color.Black,
            //    this.commandRegister)
            //{
            //    IsLostFocusByEnter = true
            //};
            //this.commandConsole.SetContext(this.commandContext);

            this.world = new World(this, 0.98f);
            this.world.LoadContent();
            world.BaseChunkCoord = Vector3I.Zero;
            world.Initialize();
            world.CheckForLoadOrCreateChunksRequired();

            Humanoid human = new Human(this, new Vector3(0, 16, 0), 0);
            human.Initialize();
            human.LoadContent();

            this.player = new Player(this, world, human);
            player.Initialize();
            player.LoadContent();

            this.stateManager = new StateManager(this);
            this.stateManager.Initialize();
            this.stateManager.LoadContent();
            this.stateManager.ActiveState = this.stateManager.GetState(typeof(PlayingState));
            (this.stateManager.ActiveState as PlayingState).Player = player;
            (this.stateManager.ActiveState as PlayingState).World = world;
            this.stateManager.LoadContent();
        }
        protected override void UnloadContent()
        {
        }
        protected override void Update(GameTime pGameTime)
        {
            this.CheckTick(pGameTime);

            this.stateManager.Update(pGameTime, this.isTick);
            if (this.isTick)
            {
                this.performanceDispecher.NoteTick();
            }
            this.isTick = false;

            this.performanceDispecher.Update(pGameTime);

            base.Update(pGameTime);
        }
        protected override void Draw(GameTime pGameTime)
        {
            //if (this.IsActive)
            //{
            this.stateManager.Draw(pGameTime);
            //}
        }

        public void DrawDebugText(GameTime pGameTime)
        {
            // Game information
            this.spriteBatch.Begin();
            this.spriteBatch.DrawString(
                this.segoeUIMono8,
                string.Format(
                    "{2} | {0}x{1}",
                    this.graphics.GraphicsDevice.Viewport.Width,
                    this.graphics.GraphicsDevice.Viewport.Height,
                    this.graphics.IsFullScreen ? "Full screen" : "Window"),
                gameInfoScreenPosition,
                Core.DebugTextColor);

            this.spriteBatch.DrawString(
                this.segoeUIMono8,
                string.Format(
                    "Time step: {0}\nFPS: {1}\nUPS: {2}\nTicks: {3}",
                    this.IsFixedTimeStep ? "fixed" : "free",
                    this.performanceDispecher.DrawRate,
                    this.performanceDispecher.UpdateRate,
                    this.performanceDispecher.TickRate),
                new Vector2(this.GraphicsDevice.Viewport.Width - 100, 4),
                Core.DebugTextColor);


            this.spriteBatch.End();

            this.performanceDispecher.NoteDraw();
        }
        public void ToggleFullScreen()
        {
            this.graphics.ToggleFullScreen();
        }

    }
}