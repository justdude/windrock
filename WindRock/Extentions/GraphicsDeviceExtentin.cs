﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace WindRock.Extentions
{
    public static class GraphicsDeviceExtentin
    {
        /* Fields */



        /* Properties */



        /* Constructors */



        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public static float GetAspectRation(this GraphicsDevice pGraphicsDevice)
        {
            return (float) pGraphicsDevice.Viewport.Width / (float) pGraphicsDevice.Viewport.Height;
        }

    }
}