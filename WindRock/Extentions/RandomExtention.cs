﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindRock.Extentions
{
    public static class RandomExtention
    {
        /* Fields */



        /* Properties */



        /* Constructors */



        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public static bool Boolean(this Random pRandom)
        {
            return pRandom.Next(2) == 1;
        }
        public static float Float(this Random pRandom, float pMinValue, float pMaxValue)
        {
            return pMinValue + (float) pRandom.NextDouble() * (pMaxValue - pMinValue);
        }
        public static bool ChanceIn(this Random pRandom,int pValue)
        {
            return pRandom.Next(pValue) == 0;
        }

    }
}