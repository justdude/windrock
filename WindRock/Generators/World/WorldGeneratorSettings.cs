﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using WindRock.Common;
using WindRock.Registers;
using WindRock.Blocks;

namespace WindRock.Generators
{
    public class WorldGeneratorSettings
    {
        /* Fields */

        private int baseHeight;
        private int amplitude;

        private short fillerBlockID;
        //private byte fillerMinCount;
        //private byte fillerMaxCount;

        private short surfaceBlockID;
        private short surfaceAlternativeBlockID;
        private float surfaceAlternativeBlockFactor;
        private byte surfaceMinBlocksCount;
        private byte surfaceMaxBlocksCount;

        private short coverBlockID;
        private short coverAlternativeBlockID;
        private float coverAlternativeBlockFactor;
        //private byte coverMinCount;
        //private byte coverMaxCount;

        private BlockTemplate fillerBlock;
        private BlockTemplate surfaceBlock;
        private BlockTemplate surfaceAlternativeBlock;
        private BlockTemplate coverBlock;
        private BlockTemplate coverAlternativeBlock;

        /* Properties */

        public int BaseHeight
        {
            get
            {
                return this.baseHeight;
            }
        }
        public int Amplitude
        {
            get
            {
                return this.amplitude;
            }
        }

        /* Constructors */

        public WorldGeneratorSettings(
            int pBaseHeight, int pAmplitude,
            short pFillerBlockID,
            short pSurfaceBlockID, short pSurfaceAlternativeBlockID, float pSurfaceAlternativeFactor, byte pSurfaceMinCount, byte pSurfaceMaxCount,
            short pCoverBlockID, short pCoverAlternativeBlockID, float pCoverAlternativeFactor)
        {
            this.baseHeight = pBaseHeight;
            this.amplitude = pAmplitude;

            pSurfaceAlternativeFactor = MathHelper.Clamp(pSurfaceAlternativeFactor, 0, 1);
            if (pSurfaceAlternativeBlockID != BlockRegister.AirBlockID
                && pSurfaceAlternativeFactor == 0)
            {
                throw new ArgumentException("SurfaceAlternativeFactor должен быть больше 0, если задан SurfaceAlternativeID");
            }

            pCoverAlternativeFactor = MathHelper.Clamp(pCoverAlternativeFactor, 0, 1);
            if (pCoverAlternativeBlockID != BlockRegister.AirBlockID
                && pCoverAlternativeFactor == 0)
            {
                throw new ArgumentException("CoverAlternativeFactor должен быть больше 0, если задан CoverAlternativeID");
            }

            this.fillerBlockID = pFillerBlockID;

            this.surfaceBlockID = pSurfaceBlockID;
            this.surfaceAlternativeBlockID = pSurfaceAlternativeBlockID;
            this.surfaceAlternativeBlockFactor = pSurfaceAlternativeFactor;
            this.surfaceMinBlocksCount = pSurfaceMinCount;
            this.surfaceMaxBlocksCount = pSurfaceMaxCount;

            this.coverBlockID = pCoverBlockID;
            this.coverAlternativeBlockID = pCoverAlternativeBlockID;
            this.coverAlternativeBlockFactor = pCoverAlternativeFactor;

            this.fillerBlock = BlockRegister.Instance.Get(this.fillerBlockID);
            this.surfaceBlock = BlockRegister.Instance.Get(this.surfaceBlockID);
            if (this.surfaceAlternativeBlockID != BlockRegister.AirBlockID)
            {
                this.surfaceAlternativeBlock = BlockRegister.Instance.Get(this.surfaceAlternativeBlockID);
            }
            this.coverBlock = BlockRegister.Instance.Get(this.coverBlockID);
            if (this.coverAlternativeBlockID != BlockRegister.AirBlockID)
            {
                this.coverAlternativeBlock = BlockRegister.Instance.Get(this.coverAlternativeBlockID);
            }
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public short GetFiller(out BlockTemplate pBlock)
        {
            pBlock = this.fillerBlock;
            return this.fillerBlockID;
        }
        public short GetSurface(Random pRandom, out BlockTemplate pBlock)
        {
            short blockID = BlockRegister.Undefined;

            if (this.surfaceAlternativeBlockID != BlockRegister.Undefined
                && pRandom.NextDouble() < this.surfaceAlternativeBlockFactor)
            {
                blockID = this.surfaceAlternativeBlockID;
                pBlock = this.surfaceAlternativeBlock;
            }
            else
            {
                blockID = this.surfaceBlockID;
                pBlock = this.surfaceBlock;
            }

            return blockID;
        }
        public short GetCover(Random pRandom, out BlockTemplate pBlock)
        {
            short blockID = BlockRegister.Undefined;

            if (this.coverAlternativeBlockID != BlockRegister.Undefined
                && pRandom.NextDouble() < this.coverAlternativeBlockFactor)
            {
                blockID = this.coverAlternativeBlockID;
                pBlock = this.coverAlternativeBlock;
            }
            else
            {
                blockID = this.coverBlockID;
                pBlock = this.coverBlock;
            }

            return blockID;
        }

        public int GetSurfaceSize(Random pRandom)
        {
            return pRandom.Next(this.surfaceMinBlocksCount, this.surfaceMaxBlocksCount + 1);
        }
    }
}