﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindRock.Generators;
using Microsoft.Xna.Framework;
using GameHelper.Data;
using WindRock.Common;
using WindRock.Registers;
using GameHelper.Extentions;
using GameHelper.Noise;
using System.Diagnostics;
using WindRock.Blocks;

namespace WindRock.Generators
{
    public class WorldGenerator
    {
        /* Fields */

        private int seed;
        private World world;
        private List<StructureStamp> structureGenerators;

        /* Properties */

        public int Seed
        {
            get
            {
                return this.seed;
            }
        }

        /* Constructors */

        public WorldGenerator(int pSeed, World pWorld)
        {
            this.seed = pSeed;
            this.world = pWorld;

            this.structureGenerators = new List<StructureStamp>();
        }

        /* Private methods */

        private Chunk CreateChunkUsingPerlinNoise2D(Vector3I pCoord, WorldGeneratorSettings pSettings)
        {
            Chunk chunk = new Chunk(this.world, pCoord);
            try
            {
                int offsetByX = pCoord.X * Core.ChunkSizes.X;
                int offsetByZ = pCoord.Z * Core.ChunkSizes.Z;

                int[,] heightMap = PerlinNoise.GetIntNoise2D(
                    Core.ChunkSizes.X,
                    Core.ChunkSizes.Z,
                    2,
                    offsetByX,
                    offsetByZ,
                    pSettings.BaseHeight,
                    pSettings.Amplitude,
                    Core.Seed);

                this.FillChunk(ref chunk, heightMap, pSettings);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }

            return chunk;
        }
        private Chunk CreateChunkUsingPerlinNoise3D(Vector3I pCoord, WorldGeneratorSettings pSettings)
        {
            Chunk chunk = new Chunk(this.world, pCoord);
            try
            {
                int offsetByD1 = pCoord.X * Core.ChunkSizes.X;
                int offsetByD2 = pCoord.Y * Core.ChunkSizes.Y;
                int offsetByD3 = pCoord.Z * Core.ChunkSizes.Z;

                float[, ,] densityMap = PerlinNoise.GetNoise3D(
                    Core.ChunkSizes.X,
                    Core.ChunkSizes.Y,
                    Core.ChunkSizes.Z,
                    1,
                    offsetByD1,
                    offsetByD2,
                    offsetByD3,
                    0.5f,
                    0.5f,
                    Core.Seed);

                this.FillChunk(ref chunk, densityMap, pSettings);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }

            return chunk;
        }
        private Chunk CreateChunkUsingPerlinNoise2DAnd3D(Vector3I pCoord, WorldGeneratorSettings pSettings)
        {
            Chunk chunk = new Chunk(this.world, pCoord);
            try
            {
                int offsetByD1 = pCoord.X * Core.ChunkSizes.X;
                int offsetByD2 = pCoord.Y * Core.ChunkSizes.Y;
                int offsetByD3 = pCoord.Z * Core.ChunkSizes.Z;
                int offset = pSettings.BaseHeight;

                int[,] heightMap = PerlinNoise.GetIntNoise2D(
                    Core.ChunkSizes.X,
                    Core.ChunkSizes.Z,
                    2,
                    offsetByD1,
                    offsetByD3,
                    pSettings.BaseHeight,
                    pSettings.Amplitude,
                    Core.Seed);

                float[, ,] densityMap = PerlinNoise.GetNoise3D(
                    Core.ChunkSizes.X,
                    Core.ChunkSizes.Y,
                    Core.ChunkSizes.Z,
                    2,
                    offsetByD1,
                    offsetByD2,
                    offsetByD3,
                    0.5f,
                    0.5f,
                    Core.Seed);

                this.FillChunk(ref chunk, heightMap, densityMap, pSettings);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }

            //// Структуры.
            //this.CreateStructureStamps(chunk);

            //StructureStampCollection stamps = null;
            //if (this.world.TryGetStamps(chunk.Coord, out stamps))
            //{
            //    this.world.ApplyStamps(chunk, stamps, false);
            //}

            return chunk;
        }

        private void FillChunk(ref Chunk pChunk, int[,] pHeightMap, WorldGeneratorSettings pSettings)
        {
            int pMinYInWorld = Core.ChunkSizes.Y * Core.WorldArea.MinY;
            int blocksCount = 0;
            short blockID = BlockRegister.AirBlockID;
            BlockTemplate block = null;

            for (int x = 0; x < Core.ChunkSizes.X; x++)
            {
                for (int z = 0; z < Core.ChunkSizes.Z; z++)
                {
                    for (int y = 0; y < Core.ChunkSizes.Y; y++)
                    {
                        int height = pHeightMap[x, z];

                        int yInWorld = pChunk.GetHeightInWorld(y);
                        if (yInWorld <= height)
                        {
                            //pChunk.SetBlock(x, y, z, BlockRegister.CobblestoneBlockID, true);
                            //continue;

                            if (yInWorld == pMinYInWorld)
                            {
                                // Core.
                                blockID = BlockRegister.BedrockBlockID;
                                block = BlockRegister.Instance.Get(blockID);
                                pChunk.SetBlock(x, y, z, blockID, block.CreateDefaultMetadata());
                            }
                            else if (yInWorld == height)
                            {
                                // Cover.
                                blockID = pSettings.GetCover(new Random(new Vector3I(x, y, z).GetHashCode()), out block);
                                pChunk.SetBlock(x, y, z, blockID, block.CreateDefaultMetadata());
                            }
                            else if (yInWorld >= height - 4)
                            {
                                // Surface.
                                blockID = pSettings.GetSurface(new Random(new Vector3I(x, y, z).GetHashCode()), out block);
                                pChunk.SetBlock(x, y, z, blockID, block.CreateDefaultMetadata());
                            }
                            else
                            {
                                // Filler.
                                blockID = pSettings.GetFiller(out block);
                                pChunk.SetBlock(x, y, z, blockID, block.CreateDefaultMetadata());
                            }

                            blocksCount++;
                        }
                    }
                }
            }

            System.Diagnostics.Debug.WriteLine(
                "Установлено блоков: {0}",
                blocksCount);
        }
        private void FillChunk(ref Chunk pChunk, float[, ,] pDensityMap, WorldGeneratorSettings pSettings)
        {
            int pMinYInWorld = Core.ChunkSizes.Y * Core.WorldArea.MinY;
            int blocksCount = 0;
            short blockID = BlockRegister.AirBlockID;
            BlockTemplate block = null;

            for (int x = 0; x < Core.ChunkSizes.X; x++)
            {
                for (int z = 0; z < Core.ChunkSizes.Z; z++)
                {
                    for (int y = 0; y < Core.ChunkSizes.Y; y++)
                    {
                        float density = pDensityMap[x, y, z];

                        if (density > 0.5f)
                        {
                            blockID = BlockRegister.HellStoneBlockID;
                            block = BlockRegister.Instance.Get(blockID);
                            pChunk.SetBlock(x, y, z, blockID, block.CreateDefaultMetadata());
                        }
                    }
                }
            }

            System.Diagnostics.Debug.WriteLine(
                "Установлено блоков: {0}",
                blocksCount);
        }
        private void FillChunk(ref Chunk pChunk, int[,] pHeightMap, float[, ,] pDensityMap, WorldGeneratorSettings pSettings)
        {
            int minHeight = Core.ChunkSizes.Y * Core.WorldArea.MinY;
            int maxHeight = Core.ChunkSizes.Y * Core.WorldArea.MaxX;
            float densityOfMinHeight = -1.0f;
            float densityOfMaxHeight = 1.0f;
            float densityOfBaseHeight = -0.5f;

            int blocksCount = 0;
            short blockID = BlockRegister.AirBlockID;
            BlockTemplate block = null;

            for (int x = 0; x < Core.ChunkSizes.X; x++)
            {
                for (int z = 0; z < Core.ChunkSizes.Z; z++)
                {
                    float forwardDensity = 0.65f;

                    int surfaceSize = 0;
                    int maxSurfaceSize = 0;
                    int mapHeight = pHeightMap[x, z];

                    for (int y = Core.ChunkSizes.Y - 1; y >= 0; y--)
                    {
                        int height = pChunk.GetHeightInWorld(y);

                        if (height == minHeight)
                        {
                            // Core.
                            blockID = BlockRegister.BedrockBlockID;
                            block = BlockRegister.Instance.Get(blockID);
                            pChunk.SetBlock(x, y, z, blockID, block.CreateDefaultMetadata());
                        }

                        if (pDensityMap[x, y, z] < forwardDensity)
                        {
                            {
                                surfaceSize = 0;
                            }
                        }
                        else
                        {
                            if (height == mapHeight)
                            {
                                // Cover.
                                Random random = new Random(pChunk.GetBlockCoordInWorld(x, y, z).GetHashCode());
                                blockID = pSettings.GetCover(random, out block);
                                pChunk.SetBlock(x, y, z, blockID, block.CreateDefaultMetadata());
                                maxSurfaceSize = pSettings.GetSurfaceSize(random);
                                surfaceSize = 0;

                            }
                            else if (height < mapHeight)
                            {
                                if (surfaceSize < maxSurfaceSize)
                                {
                                    // Surface.
                                    blockID = pSettings.GetSurface(new Random(pChunk.GetBlockCoordInWorld(x, y, z).GetHashCode()), out block);
                                    pChunk.SetBlock(x, y, z, blockID, block.CreateDefaultMetadata());
                                    surfaceSize++;
                                }
                                else
                                {
                                    blockID = pSettings.GetFiller(out block);
                                    pChunk.SetBlock(x, y, z, blockID, block.CreateDefaultMetadata());
                                }

                            }
                        }

                        blocksCount++;
                    }

                }
            }

            System.Diagnostics.Debug.WriteLine(
                "Установлено блоков: {0}",
                blocksCount);
        }

        private void CreateStructureStamps(Chunk pChunk)
        {
            this.CreateTrees(pChunk);
            //this.CreateTestStructureStamp(pChunk);
        }
        private void CreateTestStructureStamp(Chunk pChunk)
        {
            Random random = new Random(pChunk.Coord.GetHashCode() + 32);

            for (int i = 0; i < random.Next(1, 6); i++)
            {
                Vector3I coord = new Vector3I(
                    random.Next(0, Core.ChunkSizes.X),
                    random.Next(0, Core.ChunkSizes.Y),
                    random.Next(0, Core.ChunkSizes.Z));

                StructurePattern pattern = StructureRegister.Instance.Get(StructureRegister.TestCubeID);
                this.world.AddStamps(pattern, coord);
            }

            //if (pChunk.Coord == Vector3I.Zero)
            //{
            //    Vector3I coord = new Vector3I(8, 8, 8);
            //    StructurePattern pattern = StructureRegister.Instance.Get(StructureRegister.TestCubeID);
            //    this.world.AddStamps(pattern, coord);
            //}
        }
        private void CreateTrees(Chunk pChunk)
        {
            Random random = new Random(pChunk.Coord.GetHashCode() + 32);

            bool isCreate = false;

            for (int i = 0; i < 5; i++)
            {
                Vector3I blockCoord = new Vector3I(
                    random.Next(0, Core.ChunkSizes.X),
                    random.Next(0, Core.ChunkSizes.Y),
                    random.Next(0, Core.ChunkSizes.Z));
                short blockID = pChunk.GetBlockID(blockCoord);

                if (blockID == BlockRegister.AirBlockID)
                {
                    blockCoord -= Vector3I.UnitY;
                    while (blockCoord.Y >= 0)
                    {
                        try
                        {
                            blockID = pChunk.GetBlockID(blockCoord);
                            if (blockID == BlockRegister.DirtBlockID
                                || blockID == BlockRegister.StonyDirtBlockID
                                || blockID == BlockRegister.SodBlockID
                                || blockID == BlockRegister.StonySodBlockID)
                            {
                                isCreate = true;
                                break;
                            }

                            blockCoord -= Vector3I.UnitY;
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debug.WriteLine(ex.Message);
                        }
                    }
                }
                else
                {
                    blockCoord += Vector3I.UnitY;
                    while (blockCoord.Y < Core.ChunkSizes.Y)
                    {
                        try
                        {
                            blockID = pChunk.GetBlockID(blockCoord);
                            if (blockID == BlockRegister.AirBlockID)
                            {
                                isCreate = true;
                                break;
                            }

                            blockCoord += Vector3I.UnitY;
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debug.WriteLine(ex.Message);
                        }
                    }
                }

                if (isCreate)
                {
                    StructurePattern pattern = StructureRegister.Instance.Get(StructureRegister.Tree1ID);
                    this.world.AddStamps(pattern, blockCoord);
                }
            }
        }

        /* Protected methods */



        /* Public methods */

        public void Initialize()
        {
        }

        // // //

        //public Chunk CreatePlane(Vector3I pCoord, GeneratorSettings pSettings, int pHeight)
        //{
        //    Chunk chunk = new Chunk(this.world, pCoord);
        //    int pMinYInWorld = Core.ChunkSizes.Y * Core.WorldArea.MinY;

        //    for (int x = 0; x < Core.ChunkSizes.X; x++)
        //    {
        //        for (int z = 0; z < Core.ChunkSizes.Z; z++)
        //        {
        //            for (int y = 0; y < Core.ChunkSizes.Y; y++)
        //            {
        //                int yInWorld = chunk.GetHeightInWorld(y);
        //                if (yInWorld <= pHeight)
        //                {
        //                    if (yInWorld == pMinYInWorld)
        //                    {
        //                        chunk.SetBlock(new Vector3I(x, y, z), BlockRegister.CoreBlockID, true);
        //                    }
        //                    else if (yInWorld == pHeight)
        //                    {
        //                        chunk.SetBlock(new Vector3I(x, y, z), BlockRegister.SodBlockID, true);
        //                    }
        //                    else if (yInWorld >= pHeight - 4)
        //                    {
        //                        chunk.SetBlock(new Vector3I(x, y, z), BlockRegister.DirtBlockID, true);
        //                    }
        //                    else
        //                    {
        //                        chunk.SetBlock(new Vector3I(x, y, z), BlockRegister.StoneBlockID, true);
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    return chunk;
        //}
        public Chunk CreateChunk(Vector3I pCoord, WorldGeneratorSettings pSettings)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            Chunk chunk = this.CreateChunkUsingPerlinNoise2DAnd3D(pCoord, pSettings);
            stopwatch.Stop();
            System.Diagnostics.Debug.WriteLine(
                "Чанк {0}: Создан за {1}",
                pCoord,
                stopwatch.Elapsed);

            return chunk;
        }

        public WorldGeneratorSettings GetDefaultGeneratorSettings()
        {
            throw new NotImplementedException();
        }
    }
}