﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.Data;

namespace WindRock.Generators
{
    public class StructurePattern
    {
        /* Fields */

        private string name;
        private short[, ,] blocks;
        private Vector3I offset;

        /* Properties */

        public string Name
        {
            get
            {
                return this.name;
            }
        }
        public short[, ,] Blocks
        {
            get
            {
                return this.blocks;
            }
        }
        public Vector3I Offset
        {
            get
            {
                return this.offset;
            }
        }

        public int LengthByX
        {
            get
            {
                return this.blocks.GetLength(0);
            }
        }
        public int LengthByY
        {
            get
            {
                return this.blocks.GetLength(1);
            }
        }
        public int LengthByZ
        {
            get
            {
                return this.blocks.GetLength(2);
            }
        }

        /* Constructors */

        public StructurePattern(string pName, short[, ,] pBlocks, Vector3I pOffset)
        {
            this.name = pName;
            this.blocks = pBlocks;
            this.offset = pOffset;
        }
        public StructurePattern(string pName, Vector3I pSize, Vector3I pOffset)
        {
            this.name = pName;
            this.offset = pOffset;

            this.blocks = new short[pSize.X, pSize.Y, pSize.Z];
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public void SetLayerXZ(int pY, short[,] pBlocks)
        {
            if (pY < 0 || pY >= this.blocks.GetLength(1))
            {
                throw new Exception();
            }

            if (pBlocks.GetLength(0) != this.blocks.GetLength(0))
            {
                throw new Exception();
            }

            if (pBlocks.GetLength(1) != this.blocks.GetLength(2))
            {
                throw new Exception();
            }

            for (int x = 0; x < pBlocks.GetLength(0); x++)
            {
                for (int z = 0; z < pBlocks.GetLength(1); z++)
                {
                    this.blocks[x, pY, z] = pBlocks[x, z];
                }
            }
        }
        public Area3I GetArea(Vector3I pCoord)
        {
            return new Area3I(
                pCoord.X + this.Offset.X,
                pCoord.X + this.Offset.X + this.LengthByX - 1,
                pCoord.Y + this.Offset.Y,
                pCoord.Y + this.Offset.Y + this.LengthByY - 1,
                pCoord.Z + this.Offset.Z,
                pCoord.Z + this.Offset.Z + this.LengthByZ - 1);
        }

    }
}