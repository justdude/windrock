﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.Data;
using WindRock.Common;
using WindRock.Registers;
using WindRock.Blocks;

namespace WindRock.Generators
{
    public class StructureStamp
    {
        /* Constants */

        private static readonly Area3I ChunkArea = new Area3I(0, Core.ChunkSizes.X - 1, 0, Core.ChunkSizes.Y - 1, 0, Core.ChunkSizes.Z - 1);

        /* Fields */

        private StructurePattern pattern;
        private Vector3I size;
        private Vector3I patternBlockOrigin;
        private Vector3I chunkBlockOrigin;

        /* Properties */



        /* Constructors */

        private StructureStamp(StructurePattern pPattern, Vector3I pSize, Vector3I pPatternBlockOrigin, Vector3I pChunkBlockOrigin)
        {
            this.pattern = pPattern;
            this.patternBlockOrigin = pPatternBlockOrigin;
            this.chunkBlockOrigin = pChunkBlockOrigin;
            this.size = pSize;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public void PressOn(ref short[, ,] pBlockMap)
        {
            short blockID = BlockRegister.AirBlockID;
            //BlockTemplate block = null;
            //BlockData blockData = null;

            for (int x = 0; x < this.size.X; x++)
            {
                for (int y = 0; y < this.size.Y; y++)
                {
                    for (int z = 0; z < this.size.Z; z++)
                    {
                        blockID = this.pattern.Blocks[x + this.patternBlockOrigin.X, y + this.patternBlockOrigin.Y, z + this.patternBlockOrigin.Z];
                        //block = BlockRegister.Instance.Get(blockID);
                        //blockData = block.CreateData(true);
                        pBlockMap[x + this.chunkBlockOrigin.X, y + this.chunkBlockOrigin.Y, z + this.chunkBlockOrigin.Z] = blockID;
                    }
                }
            }
        }
        public static StructureStamp Create(StructurePattern pPattern, Vector3I pPatternCoord, Vector3I pChunkCoord)
        {
            StructureStamp stamp = null;

            Vector3I patternChoordInChunk = pPatternCoord - pChunkCoord;
            Area3I patternArea = pPattern.GetArea(patternChoordInChunk);

            if (patternArea.Intersects(ChunkArea))
            {
                Area3I stampArea = patternArea.Intersection(ChunkArea);
                Vector3I patternBlockOrigin = stampArea.Min - patternArea.Min;
                Vector3I chunkBlockOrigin = stampArea.Min - ChunkArea.Min;
                stamp = new StructureStamp(pPattern, stampArea.Size, patternBlockOrigin, chunkBlockOrigin);
            }

            return stamp;
        }

    }
}