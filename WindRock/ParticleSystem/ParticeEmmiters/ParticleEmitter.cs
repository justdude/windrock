﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using WindRock.Managers;
using WindRock.Common;

namespace WindRock.ParticleSystem
{
    public abstract class ParticleEmitter
    {
        /* Fields */

        protected Vector3 position;
        protected TimeSpan spawnInterval;
        protected TimeSpan emitTimeout;

        /* Properties */

        public Vector3 Position
        {
            get
            {
                return this.position;
            }
            set
            {
                this.position = value;
            }
        }

        /* Constructors */

        public ParticleEmitter(Vector3 pPosition, TimeSpan pEmitInterval)
        {
            this.position = pPosition;
            this.spawnInterval = pEmitInterval;
            this.emitTimeout = this.spawnInterval;
        }

        /* Private methods */



        /* Protected methods */

        protected abstract void Emit(World pWorld);

        /* Public methods */

        public virtual void Update(GameTime pGameTime, World pWorld)
        {
            this.emitTimeout -= pGameTime.ElapsedGameTime;

            if (this.emitTimeout <= TimeSpan.Zero)
            {
                this.Emit(pWorld);
                this.emitTimeout += this.spawnInterval;
            }
        }

    }
}