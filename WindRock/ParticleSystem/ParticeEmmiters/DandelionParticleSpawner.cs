﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using WindRock.Managers;
using WindRock.Registers;
using GameHelper.Extentions;
using WindRock.Common;

namespace WindRock.ParticleSystem
{
    public class DandelionParticleSpawner : ParticleEmitter
    {
        /* Fields */



        /* Properties */



        /* Constructors */

        public DandelionParticleSpawner(Vector3 pPosition, TimeSpan pSpawnInterval)
            : base(pPosition, pSpawnInterval)
        {
        }

        /* Private methods */



        /* Protected methods */

        protected override void Emit(World pWorld)
        {
            short id = ParticleRegister.DandelionSeedParticleID;
            IParticleTemplate particleTemplate = ParticleRegister.Instance.Get(id);

            Vector3 velocity = new Vector3(
                pWorld.Random.Float(-0.25f, 0.25f),
                pWorld.Random.Float(0.1f, 2.0f),
                pWorld.Random.Float(-0.25f, 0.25f));
            float scale = pWorld.Random.Float(3.0f, 4.0f);

            IParticleData particle = particleTemplate.GetData(
                this.position,
                velocity,
                Color.White,
                0,
                scale,
                pWorld.Random);
            pWorld.AddParticle(particle);
        }

        /* Public methods */



    }
}