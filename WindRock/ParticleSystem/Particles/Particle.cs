﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using WindRock.Common;
using GameHelper.Extentions;
using GameHelper.Cameras;

namespace WindRock.ParticleSystem
{
    // Template interface //
    public interface IParticleTemplate
    {
        Texture2D Texture
        {
            get;
        }
        Rectangle Frame
        {
            get;
        }
        int FrameCount
        {
            get;
        }
        TimeSpan FrameTime
        {
            get;
        }
        bool AllowVerticalFlip
        {
            get;
        }
        bool AllowHorisontalFlip
        {
            get;
        }

        IParticleData GetData(Vector3 pPosition, Vector3 pVelocity, Color pColor, int pFrameIndex, float pScale, Random pRandom);
    }

    // Template class //
    public abstract class ParticleTemplate : IParticleTemplate
    {
        /* Fields */

        protected Texture2D texture;
        protected Rectangle frame;
        protected int frameCount;
        protected TimeSpan frameTime;
        protected bool allowVerticalFlip;
        protected bool allowHorisontalFlip;

        /* Properties */

        public Texture2D Texture
        {
            get
            {
                return this.texture;
            }
        }
        //public Rectangle[] Rectangles
        //{
        //    get
        //    {
        //        return this.rectangles;
        //    }
        //    set
        //    {
        //        this.rectangles = value;
        //    }
        //}
        public Rectangle Frame
        {
            get
            {
                return this.frame;
            }
        }
        public int FrameCount
        {
            get
            {
                return this.frameCount;
            }
        }
        public TimeSpan FrameTime
        {
            get
            {
                return this.frameTime;
            }
        }
        public bool AllowVerticalFlip
        {
            get
            {
                return this.allowVerticalFlip;
            }
        }
        public bool AllowHorisontalFlip
        {
            get
            {
                return this.allowHorisontalFlip;
            }
        }

        /* Constructors */

        public ParticleTemplate(Texture2D pTexture, Rectangle pFrame, int pFrameCount, TimeSpan pFrameTime, bool pAllowVerticalFlip, bool pAllowHorisontalFlip)
        {
            if (pTexture == null)
            {
                throw new NullReferenceException();
            }
            if (pFrame == null)
            {
                throw new NullReferenceException();
            }
            if (pFrameCount == 0)
            {
                throw new Exception();
            }

            this.texture = pTexture;
            this.frame = pFrame;
            this.frameCount = pFrameCount;
            this.frameTime = pFrameTime;
            this.allowVerticalFlip = pAllowVerticalFlip;
            this.allowHorisontalFlip = pAllowHorisontalFlip;
        }

        /* Private methods */



        /* Protected methods */

        protected SpriteEffects GetSpriteEffect(Random pRandom)
        {
            SpriteEffects spriteEffects = SpriteEffects.None;

            if (this.allowVerticalFlip && pRandom.Boolean())
            {
                spriteEffects |= SpriteEffects.FlipVertically;
            }

            if (this.allowHorisontalFlip && pRandom.Boolean())
            {
                spriteEffects |= SpriteEffects.FlipHorizontally;
            }

            return spriteEffects;
        }

        /* Public methods */



        // // //

        public abstract IParticleData GetData(Vector3 pPosition, Vector3 pVelocity, Color pColor, int pFrameIndex, float pScale, Random pRandom);

    }

    // Data interface //
    public interface IParticleData
    {
        Vector3 Position
        {
            get;
            set;
        }
        Vector3 Velocity
        {
            get;
        }
        Color Color
        {
            get;
        }
        bool IsAlive
        {
            get;
        }

        void Update(GameTime pGameTime, World pWorld);
        void Draw(SpriteBatch pSpriteBatch, Vector3 pPositionProjection, float pDistance);
    }

    // Data class //
    public abstract class ParticleData<TTemplate> : IParticleData
      where TTemplate : IParticleTemplate
    {
        /* Fields */

        protected TTemplate template;
        protected Vector3 position;
        protected Vector3 velocity;
        protected Color color;
        protected TimeSpan liveTime;
        protected int frameIndex;
        protected SpriteEffects spriteEffects;
        protected float scale;

        /* Properties */

        public Vector3 Position
        {
            get
            {
                return this.position;
            }
            set
            {
                this.position = value;
            }
        }
        public Vector3 Velocity
        {
            get
            {
                return this.velocity;
            }
        }
        public Color Color
        {
            get
            {
                return this.color;
            }
        }
        public TimeSpan LiveTime
        {
            get
            {
                return this.liveTime;
            }
        }

        public bool IsAlive
        {
            get
            {
                return this.liveTime > TimeSpan.Zero;
            }
        }

        /* Constructors */

        public ParticleData(TTemplate pTemplate, Vector3 pPosition, Vector3 pVelocity, Color pColor, int pFrameIndex, float pScale, SpriteEffects pSpriteEffects)
        {
            this.template = pTemplate;
            this.position = pPosition;
            this.velocity = pVelocity;
            this.color = pColor;
            this.frameIndex = pFrameIndex;
            this.scale = pScale;
            this.spriteEffects = pSpriteEffects;

            this.liveTime = pTemplate.FrameTime;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public virtual void Update(GameTime pGameTime, World pWorld)
        {
            if (this.IsAlive)
            {
                this.position += this.velocity * (float) pGameTime.ElapsedGameTime.TotalSeconds;
                this.liveTime -= pGameTime.ElapsedGameTime;

                if (this.liveTime < TimeSpan.Zero)
                {
                    if (this.frameIndex < this.template.FrameCount)
                    {
                        this.frameIndex++;
                        this.liveTime += this.template.FrameTime;
                    }
                }
            }
        }
        public virtual void Draw(SpriteBatch pSpriteBatch, Vector3 pPositionProjection, float pDistance)
        {
            Rectangle particleFrame = new Rectangle(
                this.template.Frame.X + this.template.Frame.Width * this.frameIndex,
                this.template.Frame.Y,
                this.template.Frame.Width,
                this.template.Frame.Height);
            Vector2 particlePosition = new Vector2(
                pPositionProjection.X - particleFrame.Width * 0.5f,
                pPositionProjection.Y - particleFrame.Height * 0.5f);
            float particleScale = this.scale * (float) Math.Pow(Core.ParticleScaleExponente, pDistance);
            Vector2 offset = new Vector2(
                this.template.Frame.Width * 0.5f,
                this.template.Frame.Height * 0.5f);

            pSpriteBatch.Draw(
               this.template.Texture,
               particlePosition,
               particleFrame,
               this.Color,
               0,
               offset,
               particleScale,
               this.spriteEffects,
               pPositionProjection.Z);

        }

    }
}