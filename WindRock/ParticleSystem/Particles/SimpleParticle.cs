﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace WindRock.ParticleSystem
{
    // Template class //
    public class SimpleParticleTemplate : ParticleTemplate
    {
        /* Fields */



        /* Properties */



        /* Constructors */

        public SimpleParticleTemplate(Texture2D pTexture, Rectangle pFrame, int pFrameCount, TimeSpan pFrameTime, bool pAllowVerticalFlip, bool pAllowHorisontalFlip)
            : base(pTexture, pFrame, pFrameCount, pFrameTime, pAllowVerticalFlip, pAllowHorisontalFlip)
        {
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



        // // //

        public override IParticleData GetData(Vector3 pPosition, Vector3 pVelocity, Color pColor, int pFrameIndex, float pScale, Random pRandom)
        {
            return new SmokeParticleData(this, pPosition, pVelocity, pColor, pFrameIndex, pScale, this.GetSpriteEffect(pRandom));
        }

    }

    // Data class //
    public class SmokeParticleData : ParticleData<SimpleParticleTemplate>
    {
        /* Fields */



        /* Properties */



        /* Constructors */

        public SmokeParticleData(SimpleParticleTemplate pTemplate, Vector3 pPosition, Vector3 pVelocity, Color pColor, int pFrameIndex, float pScale, SpriteEffects pSpriteEffects)
            : base(pTemplate, pPosition, pVelocity, pColor, pFrameIndex, pScale, pSpriteEffects)
        {
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}