﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using WindRock.Common;

namespace WindRock.States
{
    public abstract class State
    {
        /* Properties */

        public WindRockGame Game
        {
            set;
            get;
        }

        /* Constructors */

        public State()
        {
        }

        /* Public Methods */

        public abstract void Initialize();
        public abstract void LoadContent();
        public abstract void Update(GameTime pGameTime, bool pIsTick);
        public abstract void Draw(GameTime pGameTime);

    }
}