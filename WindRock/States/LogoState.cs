﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using WindRock.Common;

namespace WindRock.States
{
    public class LogoState : State
    {
        /* Fields */

        private SpriteBatch spriteBatch;
        private SpriteFont courierNew10BoldSpriteFont;
        private SpriteFont courierNew24BoldSpriteFont;
        private SpriteFont courierNew72BoldSpriteFont;
        private Texture2D blackTexture;
        private Rectangle backgroundRectangle;

        /* Properties */



        /* Constructors */

        public LogoState()
        {
        }

        /* Private methods */

        public void ProcessInput()
        {
            KeyboardState keyboardState = Keyboard.GetState(PlayerIndex.One);
            if (keyboardState.IsKeyDown(Keys.Escape))
            {
                this.Game.Exit();
            }
            else if (keyboardState.IsKeyDown(Keys.Space))
            {
                this.Game.StateManager.ActiveState = this.Game.StateManager.GetState(typeof(PlayingState));
                this.Game.StateManager.ActiveState.Game = this.Game;
                //this.Game.StateManager.ActiveState.Initialize();
            }
        }

        /* Protected methods */



        /* Public methods */

        public override void Initialize()
        {
            this.spriteBatch = new SpriteBatch( this.Game.GraphicsDevice );
            this.backgroundRectangle = new Rectangle( 0, 0, this.Game.GraphicsDevice.Viewport.Width, this.Game.GraphicsDevice.Viewport.Height );
        }
        public override void LoadContent()
        {
            this.courierNew10BoldSpriteFont = this.Game.Content.Load<SpriteFont>( @"Fonts\CourierNew10Bold" );
            this.courierNew24BoldSpriteFont = this.Game.Content.Load<SpriteFont>( @"Fonts\CourierNew24Bold" );
            this.courierNew72BoldSpriteFont = this.Game.Content.Load<SpriteFont>( @"Fonts\CourierNew72Bold" );

            this.blackTexture = new Texture2D( this.Game.GraphicsDevice, 1, 1 );
            this.blackTexture.SetData<Color>( new Color[] { Color.Black } );
        }
        public override void Update(GameTime pGameTime, bool pIsTick)
        {
            this.ProcessInput();
        }
        public override void Draw(GameTime pGameTime)
        {
            string str = null;
            Vector2 strSize;
            Vector2 strPosition;

            this.spriteBatch.Begin();

            this.spriteBatch.Draw(
                this.blackTexture,
                this.backgroundRectangle,
                Color.White );

            str = "WindRock";
            strSize = this.courierNew72BoldSpriteFont.MeasureString( str );
            strPosition = new Vector2(
                this.Game.GraphicsDevice.Viewport.Width / 2 - strSize.X / 2,
                (float) (this.Game.GraphicsDevice.Viewport.Height * 0.4 - strSize.Y / 2) );

            this.spriteBatch.DrawString(
                this.courierNew72BoldSpriteFont,
                str,
                strPosition,
                Color.White );

            str = "Press SPACE to continue";
            strSize = this.courierNew24BoldSpriteFont.MeasureString( str );
            strPosition = new Vector2(
                this.Game.GraphicsDevice.Viewport.Width / 2 - strSize.X / 2,
                (float) (this.Game.GraphicsDevice.Viewport.Height * 0.85 - strSize.Y / 2) );

            this.spriteBatch.DrawString(
                this.courierNew24BoldSpriteFont,
                str,
                strPosition,
                Color.White );

            str = "Or press ESC to exit";
            strSize = this.courierNew10BoldSpriteFont.MeasureString( str );
            strPosition = new Vector2(
                this.Game.GraphicsDevice.Viewport.Width / 2 - strSize.X / 2,
                (float) (this.Game.GraphicsDevice.Viewport.Height * 0.90 - strSize.Y / 2) );

            this.spriteBatch.DrawString(
                this.courierNew10BoldSpriteFont,
                str,
                strPosition,
                Color.White );

            this.spriteBatch.End();
        }

    }
}