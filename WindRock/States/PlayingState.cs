﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using WindRock.Common;
using GameHelper.GUI;
using WindRock.Registers;
using System.IO;

namespace WindRock.States
{
    public class PlayingState : State
    {
        /* Fields */

        private SpriteBatch spriteBatch;
        private Player player;
        private World world;

        private RenderTarget2D renderTarget;
        private Effect fogEffect;

        private bool isMakeScreenshot;

        /* Properties */

        public World World
        {
            set
            {
                this.world = value;
            }
            get
            {
                return this.world;
            }
        }
        public Player Player
        {
            set
            {
                this.player = value;
            }
            get
            {
                return this.player;
            }
        }
        private bool IsMakeScreenshot
        {
            get
            {
                return this.isMakeScreenshot;
            }
            set
            {
                this.isMakeScreenshot = value;
            }
        }

        /* Constructors */

        public PlayingState()
        {
        }

        /* Private Methods */

        private void ProcessInput()
        {
            KeyboardState keyboardState = Keyboard.GetState(PlayerIndex.One);

            //if (keyboardState.IsKeyDown(Keys.Escape))
            //{
            //    this.Game.Exit();
            //}

            if (keyboardState.IsKeyDown(Keys.F2))
            {
                this.isMakeScreenshot = true;
            }

            if (keyboardState.IsKeyDown(Keys.OemPlus))
            {
                if (Core.ParticleScaleExponente < 1)
                {
                    Core.ParticleScaleExponente += 0.01f;
                }
            }
            else if (keyboardState.IsKeyDown(Keys.OemMinus))
            {
                if (Core.ParticleScaleExponente > 0)
                {
                    Core.ParticleScaleExponente -= 0.01f;
                }
            }
        }
        private void DrawAll(GameTime pGameTime)
        {
            this.player.Draw(pGameTime);
            //this.Game.GraphicsDevice.SetRenderTarget(null);
        }

        /* Public Methods */

        public override void Initialize()
        {
            this.spriteBatch = new SpriteBatch(this.Game.GraphicsDevice);
            this.renderTarget = new RenderTarget2D(
                this.Game.GraphicsDevice,
                this.Game.GraphicsDevice.Viewport.Width,
                this.Game.GraphicsDevice.Viewport.Height);

            if (!Directory.Exists(Core.ScreenshotsFolder))
            {
                Directory.CreateDirectory(Core.ScreenshotsFolder);
            }
        }
        public override void LoadContent()
        {   
            // Туман.
            this.fogEffect = this.Game.Content.Load<Effect>(@"Effects\FogEffect");
            this.fogEffect.Parameters["FogColor"].SetValue(Color.White.ToVector4());
            this.fogEffect.Parameters["NearFogDistance"].SetValue(16.0f);
            this.fogEffect.Parameters["FarFogDistance"].SetValue(32.0f);
            this.fogEffect.Parameters["MinFogFactor"].SetValue(0.1f);
            this.fogEffect.Parameters["MaxFogFactor"].SetValue(0.9f);
        }
        public override void Update(GameTime pGameTime, bool pIsTick)
        {
            if (this.Game.IsActive)
            {
                this.ProcessInput();
            }
            this.player.Update(pGameTime);
            BlockRegister.Instance.Update(pGameTime, pIsTick);
            this.world.Update(pGameTime, pIsTick, this.player.Position);
        }
        public override void Draw(GameTime pGameTime)
        {

            //// Сначала рисуем все, что видит игрок, в обьект RenderTarget2D.
            ////RenderTarget2D oldRenderTarget = this.Game.GraphicsDevice.GetRenderTargets()[0].RenderTarget as RenderTarget2D;
            ////this.Game.GraphicsDevice.SetRenderTarget(this.renderTarget);
            //this.player.Draw(pGameTime);

            //// Потом рисуем все это на экран с прикольными пост-эффектами.
            ////this.Game.GraphicsDevice.SetRenderTarget(null);
            ////this.fogEffect.Parameters["CameraPosition"].SetValue(this.player.Camera.Position);

            //this.spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.PointWrap, DepthStencilState.DepthRead, RasterizerState.CullNone);

            ////this.spriteBatch.Draw(
            ////    this.renderTarget,
            ////    Vector2.Zero,
            ////    Color.White);

            //this.spriteBatch.Draw(
            //    this.aimTexture,
            //    this.aimPosition,
            //    Color.White);
            //this.spriteBatch.End();
            this.DrawAll(pGameTime);

            if (this.isMakeScreenshot)
            {
                int width = this.Game.GraphicsDevice.PresentationParameters.BackBufferWidth;
                int height = this.Game.GraphicsDevice.PresentationParameters.BackBufferHeight;

                int[] backBuffer = new int[width * height];
                this.Game.GraphicsDevice.GetBackBufferData<int>(backBuffer);

                Texture2D screenshot = new Texture2D(this.Game.GraphicsDevice, width, height);
                screenshot.SetData<int>(backBuffer);

                DateTime date = DateTime.Now;
                Stream stream = File.Create(
                    string.Format(
                        "{0}/{1}.png",
                        @Core.ScreenshotsFolder,
                        date.ToString("yyyy-dd-MM hh-mm-ss")));

                screenshot.SaveAsPng(stream, width, height);
                stream.Dispose();
                screenshot.Dispose();

                this.isMakeScreenshot = false;
            }
        }
    }
}