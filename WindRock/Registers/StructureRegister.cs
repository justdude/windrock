﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindRock.Common;
using Microsoft.Xna.Framework;
using WindRock.Blocks;
using WindRock.Generators;

namespace WindRock.Registers
{
    public class StructureRegister : Register<StructurePattern>
    {
        /* Constants */

        public const short TestCubeID = 12345;
        public const short Tree1ID = 1;

        /* Fields */

        private static StructureRegister instance;

        /* Properties */

        public static StructureRegister Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new StructureRegister();
                }

                return instance;
            }
        }

        /* Constructors */

        private StructureRegister()
            : base()
        {
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}