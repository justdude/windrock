﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindRock.Registers
{
    public abstract class Register
    {
    }

    public abstract class Register<TItem> : Register
        where TItem : class
    {
        /* Constants */



        /* Fields */

        protected Dictionary<short, TItem> items;

        /* Properties */

        public int Count
        {
            get
            {
                return this.items.Count;
            }
        }

        /* Constructors */

        public Register()
        {
            this.items = new Dictionary<short, TItem>();
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public virtual void Add(short pID, TItem pItem)
        {
            if (pID < 1)
            {
                throw new ArgumentException("pID");
            }
            if (pItem == null)
            {
                throw new ArgumentNullException("pItem");
            }

            this.items.Add(pID, pItem);
        }
        public virtual TItem Get(short pID)
        {
            return
                !this.items.ContainsKey(pID) ?
                null :
                this.items[pID];
        }
        public virtual bool TryGet(short pID, out TItem pItem)
        {
            return this.items.TryGetValue(pID, out pItem);
        }
        public short[] GetKeys()
        {
            return this.items.Keys.ToArray<short>();
        }
        public IEnumerable<TItem> GetAll()
        {
            return this.items.Select(x => x.Value);
        }

    }
}