﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindRock.Common;
using Microsoft.Xna.Framework;
using WindRock.ParticleSystem;

namespace WindRock.Registers
{
    public class ParticleRegister : Register<ParticleTemplate>
    {
        /* Constants */

        // public const short ID = ;
        public const short Shine1ParticleID = 1;
        public const short Shine2ParticleID = 2;
        public const short Shine3ParticleID = 3;
        public const short Shine4ParticleID = 4;
        public const short FireParticleID = 5;
        public const short SmokeParticleID = 7;
        public const short DandelionSeedParticleID = 8;

        /* Fields */

        private static ParticleRegister instance;

        /* Properties */

        public static ParticleRegister Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ParticleRegister();
                }

                return instance;
            }
        }

        /* Constructors */

        private ParticleRegister()
            : base()
        {
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        //public void Update(GameTime pGameTime, bool pIsTick)
        //{
        //    foreach (ParticleTemplate particle in this.items.Values)
        //    {
        //        particle.Update(pGameTime, pIsTick);
        //    }
        //}

    }
}