﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindRock.Common;
using Microsoft.Xna.Framework;
using WindRock.Blocks;

namespace WindRock.Registers
{
    public class BlockRegister : Register<BlockTemplate>
    {
        /* Constants */

        public const short Undefined = -1;
        // public const short ID = ;
        public const short AirBlockID = 0;
        public const short BedrockBlockID = 1;
        public const short DirtBlockID = 2;
        public const short SodBlockID = 3;
        public const short StonyDirtBlockID = 4;
        public const short StonySodBlockID = 5;
        public const short CobblestoneBlockID = 6;
        public const short GrassBlockID = 7;
        public const short IronOreBlockID = 8;
        public const short WaterBlockID = 9;
        public const short GoldOreBlockID = 11;
        public const short LavaBlockID = 13;
        public const short DandelionBlockID = 14;
        //public const short DandelionBlooming = 15;
        //public const short DandelionBlank = 16;
        public const short StoneBlockID = 17;
        public const short GraniteBlockID = 18;
        public const short GlassDirtBlockID = 19;
        public const short LeafageBlockID = 20;
        public const short WoodLogBlockID = 21;
        public const short WheatBlockID = 22;
        public const short CobblestoneSlabBlockID = 23;
        public const short StoneSlabBlockID = 24;
        public const short WoodPlanksBlockID = 25;
        public const short CandyBlockID = 26;
        public const short HellStoneBlockID = 27;
        public const short NotBurningFirestoneBlockID = 28;
        public const short BurningFirestoneBlockID = 29;
        public const short BurnedOutFirestoneBlockID = 30;
        public const short HellIronOreBlockID = 31;
        public const short HellGoldOreBlockID = 32;
        public const short HellQuartzBlockID = 33;
        public const short FadedHellQuartzBlockID = 34;
        public const short DarkHellQuartzBlockID = 35;
        public const short StonePiramideBlockID = 36;
        public const short PlanksPiramideBlockID = 37;

        /* Fields */

        private static BlockRegister instance;

        /* Properties */

        public static BlockRegister Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BlockRegister();
                }

                return instance;
            }
        }

        /* Constructors */

        private BlockRegister()
            : base()
        {
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public void Update(GameTime pGameTime, bool pIsTick)
        {
            foreach (BlockTemplate block in this.items.Values)
            {
                block.Update(pGameTime, pIsTick);
            }
        }

    }
}