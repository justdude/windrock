﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindRock.Entities;
using Microsoft.Xna.Framework;
using WindRock.Common;
using GameHelper.Cameras;

namespace WindRock.Managers
{
    public class MobManager
    {
        /* Fields */

        private List<Mob> mobs;
        private World world;
        private WindRockGame game;

        /* Properties */

        public List<Mob> Mobs
        {
            get
            {
                return this.mobs;
            }
        }
        public int MobsCount
        {
            get
            {
                return this.mobs.Count;
            }
        }
        public WindRockGame Game
        {
            get
            {
                return this.game;
            }
        }

        /* Constructors */

        public MobManager(WindRockGame pGame, World pWorld)
        {
            this.game = pGame;
            this.world = pWorld;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public void Initialize()
        {
            this.mobs = new List<Mob>();
        }
        public void LoadContent()
        {
        }
        public void Update(GameTime pGameTime, bool pIsTick)
        {
            for (int i = 0; i < this.mobs.Count; i++)
            {
                this.mobs[i].Update(pGameTime, this.world);
            }
        }
        public void Draw(Camera pCamera)
        {
            foreach (Mob mob in this.mobs)
            {
                mob.Draw(pCamera);
            }
        }

        // // //

        public void AddEntity(Mob pMob)
        {
            this.mobs.Add(pMob);
        }
        public void RemoveEntity(Mob pMob)
        {
            this.mobs.Remove(pMob);
        }
        public void Transfare(Mob pMob, MobManager pTarget)
        {
            this.mobs.Remove(pMob);
            pTarget.AddEntity(pMob);
        }
    }
}