﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindRock.Entities;
using Microsoft.Xna.Framework;
using WindRock.Common;
using GameHelper.Cameras;
using GameHelper.Data;
using Microsoft.Xna.Framework.Graphics;
using WindRock.ParticleSystem;

namespace WindRock.Managers
{
    public class ParticleManager
    {
        /* Fields */

        private List<IParticleData> particles;
        private List<ParticleEmitter> particleSpawners;
        private World world;

        /* Properties */

        public List<IParticleData> Particles
        {
            get
            {
                return this.particles;
            }
        }
        public int ParticlesCount
        {
            get
            {
                return this.particles.Count;
            }
        }

        /* Constructors */

        public ParticleManager(World pWorld)
        {
            this.world = pWorld;
        }

        /* Private methods */

        private void UpdateParticleSpawners(GameTime pGameTime)
        {
            for (int i = 0; i < this.particleSpawners.Count; i++)
            {
                this.particleSpawners[i].Update(pGameTime, this.world);
            }
        }
        private void UpdateParticles(GameTime pGameTime)
        {
            for (int i = 0; i < this.Particles.Count; i++)
            {
                IParticleData particle = this.particles[i];
                particle.Update(pGameTime, this.world);

                if (!particle.IsAlive)
                {
                    this.RemoveParticle(particle);
                }
            }
        }

        /* Protected methods */



        /* Public methods */

        public void Initialize()
        {
            this.particles = new List<IParticleData>();
            this.particleSpawners = new List<ParticleEmitter>();
        }
        public void LoadContent()
        {
        }
        public void Update(GameTime pGameTime)
        {
            this.UpdateParticleSpawners(pGameTime);
            this.UpdateParticles(pGameTime);
        }
        public void Draw(Camera pCamera, SpriteBatch pSpriteBatch, BoundingFrustum pFrustrum)
        {
            for (int i = 0; i < this.particles.Count; i++)
            {
                if (pFrustrum.Contains(this.particles[i].Position) == ContainmentType.Contains)
                {
                    IParticleData particle = this.particles[i];
                    Vector3 positionProjection = this.world.Game.GraphicsDevice.Viewport.Project(
                        particle.Position,
                        pCamera.Projection,
                        pCamera.View,
                        Matrix.Identity);
                    float distance = (pCamera.Position - particle.Position).Length();
                    particle.Draw(pSpriteBatch, positionProjection, distance);
                }
            }
        }

        // // //

        public void AddParticle(IParticleData pParticle)
        {
            this.particles.Add(pParticle);
        }
        public void RemoveParticle(IParticleData pParticle)
        {
            this.particles.Remove(pParticle);
        }
        public void TransferParticle(IParticleData pParticle, ParticleManager pTarget)
        {
            this.particles.Remove(pParticle);
            pTarget.particles.Add(pParticle);
        }

        public void AddParticleSpawner(ParticleEmitter pParticleSpawner)
        {
            this.particleSpawners.Add(pParticleSpawner);
        }
        public void RemoveParticleSpawner(ParticleEmitter pParticleSpawner)
        {
            this.particleSpawners.Remove(pParticleSpawner);
        }
    }
}