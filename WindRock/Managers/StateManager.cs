﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindRock.States;
using Microsoft.Xna.Framework;
using WindRock.Common;

namespace WindRock.Managers
{
    public class StateManager
    {
        /* Fields */

        private Dictionary<Type, State> stateCache;
        private State activeState;
        private WindRockGame game;

        /* Properties */

        public State ActiveState
        {
            set
            {
                this.activeState = value;
            }
            get
            {
                return this.activeState;
            }
        }
        public WindRockGame Game
        {
            get
            {
                return this.game;
            }
        }

        /* Constructors */

        public StateManager(WindRockGame pGame)
        {
            this.game = pGame;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public void Initialize()
        {
            this.stateCache = new Dictionary<Type, State>();
        }
        public void LoadContent()
        {
            //throw new NotImplementedException();
        }
        public void Update(GameTime pGameTime, bool pIsTick)
        {
            if (this.activeState != null)
            {
                this.activeState.Update(pGameTime, pIsTick);
            }
        }
        public void Draw(GameTime pGameTime)
        {
            if (this.activeState != null)
            {
                this.activeState.Draw(pGameTime);
            }
        }
        public State GetState(Type pStateType)
        {
            //if (!(pStateType is State))
            //{
            //    throw new ArgumentException( "pStateType" );
            //}

            State state = null;

            if (this.stateCache.ContainsKey(pStateType))
            {
                state = this.stateCache[pStateType];
            }
            else
            {
                state = (State) Activator.CreateInstance(pStateType);
                state.Game = this.Game;
                state.Initialize();
                state.LoadContent();
                this.stateCache.Add(pStateType, state);
            }

            return state;
        }
    }
}