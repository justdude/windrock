﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using WindRock.Entities;
using GameHelper.Data;
using WindRock.Common;

namespace WindRock.Managers
{
    public class MobSpawner
    {
        /* Fields */

        //protected WindRockGame game;
        protected World world;
        protected int mobsCountLimit;
        protected TimeSpan spawnInterval;

        private TimeSpan timeSinceLastSpawn;
        private int mobsCountInActiveArea = 0;

        /* Properties */

        //public WindRockGame Game
        //{
        //    get
        //    {
        //        return this.game;
        //    }
        //}

        /* Constructors */

        public MobSpawner(World pWorld, int pSpawnLimit, TimeSpan pSpawnInterval)
        {
            this.world = pWorld;
            this.mobsCountLimit = pSpawnLimit;
            this.spawnInterval = pSpawnInterval;

            this.timeSinceLastSpawn = TimeSpan.Zero;
        }

        /* Private methods */

        private void SpawnMob()
        {
            int x = this.world.Random.Next(this.world.ActiveArea.MinX * Core.ChunkSizes.X, this.world.ActiveArea.MaxX * Core.ChunkSizes.X);
            int y = this.world.Random.Next(this.world.ActiveArea.MinY * Core.ChunkSizes.Y, this.world.ActiveArea.MaxY * Core.ChunkSizes.Y);
            int z = this.world.Random.Next(this.world.ActiveArea.MinZ * Core.ChunkSizes.Z, this.world.ActiveArea.MaxZ * Core.ChunkSizes.Z);

            //if (Sheepfoot.TryAdjustSpawnCoord(ref x, ref y, ref z, this.world))
            //{
            //    float yaw = (float) this.world.Random.NextDouble() * MathHelper.TwoPi;
            //    Mob mob = new Sheepfoot(this.world.Game, new Vector3I(x, y, z), yaw);
            //    mob.Initialize();
            //    mob.LoadContent();
            //}
        }

        /* Protected methods */



        /* Public methods */

        public void Initialize()
        {
        }
        public void LoadContent()
        {
        }
        public void Update(GameTime pGameTime)
        {
            if (this.mobsCountInActiveArea < this.mobsCountLimit)
            {
                this.timeSinceLastSpawn += pGameTime.ElapsedGameTime;

                while (this.timeSinceLastSpawn >= this.spawnInterval)
                {
                    this.SpawnMob();

                    this.timeSinceLastSpawn -= this.spawnInterval;
                }
            }

            this.mobsCountInActiveArea = 0;
            foreach (Chunk chunk in this.world.GetActiveChunks())
            {
                this.mobsCountInActiveArea += chunk.MobManager.MobsCount;
            }
        }

    }
}