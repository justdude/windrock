﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindRock.Blocks
{
    [Flags]
    public enum SlabLayout : byte
    {
        None = 0,
        Top = 1,
        Bottom = 2
    }
}