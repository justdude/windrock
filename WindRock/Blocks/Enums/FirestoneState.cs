﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindRock.Blocks
{
    public enum FirestoneState : byte
    {
        NotBurning = 0,
        Burning = 1,
        BurnedOut = 2
    }
}