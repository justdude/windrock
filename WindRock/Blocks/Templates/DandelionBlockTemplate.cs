﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpoonCraft.Common;
using GameHelper.Data;
using SpoonCraft.Registers;
using Microsoft.Xna.Framework;
using GameHelper.Extentions;
using SpoonCraft.Entities;
using SpoonCraft.ParticleSystem;

namespace SpoonCraft.Blocks
{
    public class DandelionBlockTemplate : PlantBlockTemplate
    {
        /* Constants */

        /// <summary>
        /// Стадия ростка (зеленый).
        /// </summary>
        private const int SproutStage = 0;
        /// <summary>
        /// Стадия цветения (желтый).
        /// </summary>
        private const int BloomStage = 1;
        /// <summary>
        /// Стадия высушености (белый).
        /// </summary>
        private const int DryStage = 2;
        /// <summary>
        /// Стадия обдутости (пришел Абдул и одуванчик обдул).
        /// </summary>
        private const int DeflatedStage = 3;

        /* Fields */

        private TextureCoord deflatedStageTextureCoord;

        /* Properties */

        protected override TextureCoord GetTextureCoord(BlockData pBlockData)
        {
            return
                (pBlockData as PlantBlockData).GrowthStage == DeflatedStage ?
                this.deflatedStageTextureCoord :
                this.growthStagesTextureCoords[(pBlockData as PlantBlockData).GrowthStage];
        }

        /* Constructors */

        public DandelionBlockTemplate(string pName, TextureCoord pSproutStageTextureCoord, TextureCoord pBloomStageTextureCoord, TextureCoord pDryStageTextureCoord, TextureCoord pDeflatedStageTextureCoord)
            : base(pName, new[] { pSproutStageTextureCoord, pBloomStageTextureCoord, pDryStageTextureCoord })
        {
            this.deflatedStageTextureCoord = pDeflatedStageTextureCoord;
        }

        /* Private methods */

        private void Deflate(World pWorld, Vector3I pCoordInWorld, DandelionBlockData pData)
        {
            DandelionBlockData blockData = pWorld.GetBlockData(pCoordInWorld) as DandelionBlockData;

            if (blockData.GrowthStage == DryStage)
            {
                pWorld.RemoveParticleEmitter(blockData.DandelionParticleSpawner);
                blockData.DandelionParticleSpawner = null;

                Vector3 position = new Vector3(
                    pCoordInWorld.X + 0.5f,
                    pCoordInWorld.Y + 0.5f,
                    pCoordInWorld.Z + 0.5f);

                for (int i = 0; i < pWorld.Random.Float(10, 20); i++)
                {
                    short id = ParticleRegister.DandelionSeedParticleID;
                    IParticleTemplate particleTemplate = ParticleRegister.Instance.Get(id);
                    Vector3 velocity = Vector3.Transform(
                        Vector3.Up,
                        Matrix.CreateFromYawPitchRoll(
                            MathHelper.ToRadians(pWorld.Random.Float(-60, 60)),
                            MathHelper.ToRadians(pWorld.Random.Float(-60, 60)),
                            0))
                        * pWorld.Random.Float(2.0f, 3.0f);
                    float scale = pWorld.Random.Float(2.0f, 4.0f);
                    IParticleData particle = particleTemplate.GetData(position, velocity, Color.White, 0, scale, pWorld.Random);
                    pWorld.AddParticle(particle);
                }
            }

            blockData.GrowthStage = DeflatedStage;
        }

        /* Protected methods */



        /* Public methods */

        public override void UpdateData(World pWorld, Vector3I pCoordInWorld, ref Queue<Vector3I> pNewBocksToUpdate)
        {
            DandelionBlockData blockData = pWorld.GetBlockData(pCoordInWorld) as DandelionBlockData;
            switch (blockData.GrowthStage)
            {
                case SproutStage:
                    if (pWorld.Random.OneOf(1))
                    {
                        blockData.GrowthStage = BloomStage;
                        pWorld.SetToUpdate(pCoordInWorld);
                    }
                    break;

                case BloomStage:
                    if (pWorld.Random.OneOf(1))
                    {
                        blockData.GrowthStage = DryStage;

                        blockData.DandelionParticleSpawner = new DandelionParticleSpawner(
                            new Vector3(
                                pCoordInWorld.X + 0.5f,
                                pCoordInWorld.Y + 0.5f,
                                pCoordInWorld.Z + 0.5f),
                            TimeSpan.FromSeconds(5));
                        pWorld.SetToUpdate(pCoordInWorld);
                        pWorld.AddParticleEmitter(blockData.DandelionParticleSpawner);
                    }
                    break;

                case DryStage:
                    if (pWorld.Random.OneOf(100))
                    {
                        this.Deflate(pWorld, pCoordInWorld, blockData);
                        pWorld.SetToUpdate(pCoordInWorld);
                    }
                    break;

                case DeflatedStage:
                    if (pWorld.Random.OneOf(10))
                    {
                        blockData.GrowthStage = BloomStage;
                        pWorld.SetToUpdate(pCoordInWorld);
                    }
                    break;
            }
        }
        public override void Touch(Entity pEntity, World pWorld, Vector3I pCoordInWorld)
        {
            DandelionBlockData blockData = pWorld.GetBlockData(pCoordInWorld) as DandelionBlockData;
            this.Deflate(pWorld, pCoordInWorld, blockData);
        }
        public override void Break(World pWorld, Vector3I pCoordInWorld)
        {
            base.Break(pWorld, pCoordInWorld);

            DandelionBlockData blockData = pWorld.GetBlockData(pCoordInWorld) as DandelionBlockData;
            this.Deflate(pWorld, pCoordInWorld, blockData);
        }
        public override BlockData CreateData(bool pIsNatural)
        {
            return new DandelionBlockData(pIsNatural);
        }
    }
}