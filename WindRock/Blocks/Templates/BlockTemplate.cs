﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using GameHelper.Data;
using WindRock.Common;
using WindRock.Blocks;
using WindRock.Registers;
using WindRock.Entities;
using WindRock.Metadatas;
using GameHelper;
using WindRock.Blocks.Drawing;
using Microsoft.Xna.Framework.Graphics;

namespace WindRock.Blocks
{
    public abstract class BlockTemplate
    {
        /* Fields */

        protected string name;
        protected bool isHard;
        protected bool isOpaque;
        protected Texture2D texture;

        /* Properties */

        public string Name
        {
            get
            {
                return this.name;
            }
        }
        /// <summary>
        /// Блок твердый: сквозь него нельзя пройти и он имеет прочность.
        /// Нетвердый блок ломается одним ударом.
        /// </summary>
        public bool IsHard
        {
            protected set
            {
                this.isHard = value;
            }
            get
            {
                return this.isHard;
            }
        }
        /// <summary>
        /// Блок непрозрачный.
        /// </summary>
        public bool IsOpaque
        {
            protected set
            {
                this.isOpaque = value;
            }
            get
            {
                return this.isOpaque;
            }
        }

        /* Constructors */

        public BlockTemplate(string pName, bool pIsOpaque, bool pIsHard, Texture2D pTexture)
        {
            this.name = pName;
            this.isOpaque = pIsOpaque;
            this.isHard = pIsHard;
            this.texture = pTexture;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public virtual void LoadContent(Game pGame)
        {
        }

        // // //

        public abstract bool IsSideSolid(Direction pSide, BlockMetadata pMetadata);
        public abstract void BuildVertices(Vector3 pPosition, BlockMetadata pMetadata, ref VertexPositionTexture[] pOpaqueBlocksVertices, ref int pOpaqueVertexIndex, ref VertexPositionTexture[] pTransparentBlocksVertices, ref int pTransparentVertexIndex);

        public abstract void BuildVertices2(Vector3 pPosition, short pBlockID, BlockMetadata pMetadata, ref BlockRenderer pBlockRenderManager);

        public abstract int GetVerticesCount(BlockMetadata pMetadata, bool pIsOpaque);

        public virtual Direction GetBlockSides(int pX, int pY, int pZ, BlockContext pContext)
        {
            // Правила определения видимых сторон цельных блоков (используются по умолчанию):
            // 1. если в соседнего блока не сущетсвует, то текущая сторона - видимая;
            // 2. иначе если соседний блок существует, то:
            //    2.1. если соседний блок - НЕ непрозрачный, то текущая сторона - видимая;
            //    2.2. иначе если противоположная сторона соседнего блока - НЕ цельная, то текущая сторона - видимая;
            // 3. иначе текущая сторона - НЕ видимая.

            Direction blockSides = Direction.None;
            BlockTemplate nearbyBlock = null;
            BlockMetadata nearbyBlockData = null;

            short thisBlockID = pContext.Chunk.GetBlockID(pX, pY, pZ);
            short nearbyBlockID = BlockRegister.AirBlockID;

            try
            {
                #region Increase X
                {
                    if (pX + 1 < Core.ChunkSizes.X)
                    {
                        nearbyBlockID = pContext.Chunk.GetBlockID(pX + 1, pY, pZ);
                        if (nearbyBlockID == BlockRegister.AirBlockID)
                        {
                            blockSides |= Direction.IncreaseX;
                        }
                        else
                        {
                            try
                            {
                                nearbyBlock = BlockRegister.Instance.Get(nearbyBlockID);
                                nearbyBlockData = pContext.Chunk.GetBlockData(new Vector3I(pX + 1, pY, pZ));

                                bool isNearbyBlockSideSolid = !nearbyBlock.IsSideSolid(Direction.DecreaseX, nearbyBlockData);
                                bool isNearbyBlockNotSame = nearbyBlockID != thisBlockID;
                                if (isNearbyBlockSideSolid || (!nearbyBlock.isOpaque && isNearbyBlockNotSame))
                                {
                                    blockSides |= Direction.IncreaseX;
                                }
                            }
                            catch (Exception ex)
                            {
                                System.Diagnostics.Debug.WriteLine(ex.Message);
                            }
                        }
                    }
                    else if (pContext.ChunkIncreaseX != null)
                    {
                        nearbyBlockID = pContext.ChunkIncreaseX.GetBlockID(0, pY, pZ);
                        if (nearbyBlockID == BlockRegister.AirBlockID)
                        {
                            blockSides |= Direction.IncreaseX;
                        }
                        else
                        {
                            nearbyBlock = BlockRegister.Instance.Get(nearbyBlockID);
                            nearbyBlockData = pContext.ChunkIncreaseX.GetBlockData(new Vector3I(0, pY, pZ));

                            bool isNearbyBlockSideSolid = !nearbyBlock.IsSideSolid(Direction.DecreaseX, nearbyBlockData);
                            bool isNearbyBlockNotSame = nearbyBlockID != thisBlockID;
                            if (isNearbyBlockSideSolid || (!nearbyBlock.isOpaque && isNearbyBlockNotSame))
                            {
                                blockSides |= Direction.IncreaseX;
                            }
                        }
                    }
                    else
                    {
                        blockSides |= Direction.IncreaseX;
                    }
                }
                #endregion
                #region Decrease X
                {
                    if (pX - 1 >= 0)
                    {
                        nearbyBlockID = pContext.Chunk.GetBlockID(pX - 1, pY, pZ);
                        if (nearbyBlockID == BlockRegister.AirBlockID)
                        {
                            blockSides |= Direction.DecreaseX;
                        }
                        else
                        {
                            nearbyBlock = BlockRegister.Instance.Get(nearbyBlockID);
                            nearbyBlockData = pContext.Chunk.GetBlockData(new Vector3I(pX - 1, pY, pZ));

                            bool isNearbyBlockSideSolid = !nearbyBlock.IsSideSolid(Direction.IncreaseX, nearbyBlockData);
                            bool isNearbyBlockNotSame = nearbyBlockID != thisBlockID;
                            if (isNearbyBlockSideSolid || (!nearbyBlock.isOpaque && isNearbyBlockNotSame))
                            {
                                blockSides |= Direction.DecreaseX;
                            }
                        }
                    }
                    else if (pContext.ChunkDecreaseX != null)
                    {
                        nearbyBlockID = pContext.ChunkDecreaseX.GetBlockID(Core.ChunkSizes.X - 1, pY, pZ);
                        if (nearbyBlockID == BlockRegister.AirBlockID)
                        {
                            blockSides |= Direction.DecreaseX;
                        }
                        else
                        {
                            nearbyBlock = BlockRegister.Instance.Get(nearbyBlockID);
                            nearbyBlockData = pContext.ChunkDecreaseX.GetBlockData(new Vector3I(Core.ChunkSizes.X - 1, pY, pZ));

                            bool isNearbyBlockSideSolid = !nearbyBlock.IsSideSolid(Direction.IncreaseX, nearbyBlockData);
                            bool isNearbyBlockNotSame = nearbyBlockID != thisBlockID;
                            if (isNearbyBlockSideSolid || (!nearbyBlock.isOpaque && isNearbyBlockNotSame))
                            {
                                blockSides |= Direction.DecreaseX;
                            }
                        }
                    }
                    else
                    {
                        blockSides |= Direction.DecreaseX;
                    }
                }
                #endregion

                #region Increase Y
                {
                    if (pY + 1 < Core.ChunkSizes.Y)
                    {
                        nearbyBlockID = pContext.Chunk.GetBlockID(pX, pY + 1, pZ);
                        if (nearbyBlockID == BlockRegister.AirBlockID)
                        {
                            blockSides |= Direction.IncreaseY;
                        }
                        else
                        {
                            nearbyBlock = BlockRegister.Instance.Get(nearbyBlockID);
                            nearbyBlockData = pContext.Chunk.GetBlockData(new Vector3I(pX, pY + 1, pZ));

                            bool isNearbyBlockSideSolid = !nearbyBlock.IsSideSolid(Direction.DecreaseY, nearbyBlockData);
                            bool isNearbyBlockNotSame = nearbyBlockID != thisBlockID;
                            if (isNearbyBlockSideSolid || (!nearbyBlock.isOpaque && isNearbyBlockNotSame))
                            {
                                blockSides |= Direction.IncreaseY;
                            }
                        }
                    }
                    else if (pContext.ChunkIncreaseY != null)
                    {
                        nearbyBlockID = pContext.ChunkIncreaseY.GetBlockID(pX, 0, pZ);
                        if (nearbyBlockID == BlockRegister.AirBlockID)
                        {
                            blockSides |= Direction.IncreaseY;
                        }
                        else
                        {
                            nearbyBlock = BlockRegister.Instance.Get(nearbyBlockID);
                            nearbyBlockData = pContext.ChunkIncreaseY.GetBlockData(new Vector3I(pX, 0, pZ));

                            bool isNearbyBlockSideSolid = !nearbyBlock.IsSideSolid(Direction.DecreaseY, nearbyBlockData);
                            bool isNearbyBlockNotSame = nearbyBlockID != thisBlockID;
                            if (isNearbyBlockSideSolid || (!nearbyBlock.isOpaque && isNearbyBlockNotSame))
                            {
                                blockSides |= Direction.IncreaseY;
                            }
                        }
                    }
                    else
                    {
                        blockSides |= Direction.IncreaseY;
                    }
                }
                #endregion
                #region Decrease Y
                {
                    if (pY - 1 >= 0)
                    {
                        nearbyBlockID = pContext.Chunk.GetBlockID(pX, pY - 1, pZ);
                        if (nearbyBlockID == BlockRegister.AirBlockID)
                        {
                            blockSides |= Direction.DecreaseY;
                        }
                        else
                        {
                            nearbyBlock = BlockRegister.Instance.Get(nearbyBlockID);
                            nearbyBlockData = pContext.Chunk.GetBlockData(new Vector3I(pX, pY - 1, pZ));

                            bool isNearbyBlockSideSolid = !nearbyBlock.IsSideSolid(Direction.IncreaseY, nearbyBlockData);
                            bool isNearbyBlockNotSame = nearbyBlockID != thisBlockID;
                            if (isNearbyBlockSideSolid || (!nearbyBlock.isOpaque && isNearbyBlockNotSame))
                            {
                                blockSides |= Direction.DecreaseY;
                            }
                        }
                    }
                    else if (pContext.ChunkDecreaseY != null)
                    {
                        nearbyBlockID = pContext.ChunkDecreaseY.GetBlockID(pX, Core.ChunkSizes.Y - 1, pZ);
                        if (nearbyBlockID == BlockRegister.AirBlockID)
                        {
                            blockSides |= Direction.DecreaseY;
                        }
                        else
                        {
                            nearbyBlock = BlockRegister.Instance.Get(nearbyBlockID);
                            nearbyBlockData = pContext.ChunkDecreaseY.GetBlockData(new Vector3I(pX, Core.ChunkSizes.Y - 1, pZ));

                            bool isNearbyBlockSideSolid = !nearbyBlock.IsSideSolid(Direction.IncreaseY, nearbyBlockData);
                            bool isNearbyBlockNotSame = nearbyBlockID != thisBlockID;
                            if (isNearbyBlockSideSolid || (!nearbyBlock.isOpaque && isNearbyBlockNotSame))
                            {
                                blockSides |= Direction.DecreaseY;
                            }
                        }
                    }
                    else
                    {
                        blockSides |= Direction.DecreaseY;
                    }
                }
                #endregion

                #region Increase Z
                {
                    if (pZ + 1 < Core.ChunkSizes.Z)
                    {
                        nearbyBlockID = pContext.Chunk.GetBlockID(pX, pY, pZ + 1);
                        if (nearbyBlockID == BlockRegister.AirBlockID)
                        {
                            blockSides |= Direction.IncreaseZ;
                        }
                        else
                        {
                            nearbyBlock = BlockRegister.Instance.Get(nearbyBlockID);
                            nearbyBlockData = pContext.Chunk.GetBlockData(new Vector3I(pX, pY, pZ + 1));

                            bool isNearbyBlockSideSolid = !nearbyBlock.IsSideSolid(Direction.DecreaseZ, nearbyBlockData);
                            bool isNearbyBlockNotSame = nearbyBlockID != thisBlockID;
                            if (isNearbyBlockSideSolid || (!nearbyBlock.isOpaque && isNearbyBlockNotSame))
                            {
                                blockSides |= Direction.IncreaseZ;
                            }
                        }
                    }
                    else if (pContext.ChunkIncreaseZ != null)
                    {
                        nearbyBlock = BlockRegister.Instance.Get(nearbyBlockID);
                        nearbyBlockID = pContext.ChunkIncreaseZ.GetBlockID(pX, pY, 0);
                        if (nearbyBlockID == BlockRegister.AirBlockID)
                        {
                            blockSides |= Direction.IncreaseZ;
                        }
                        else
                        {
                            nearbyBlock = BlockRegister.Instance.Get(nearbyBlockID);
                            nearbyBlockData = pContext.ChunkIncreaseZ.GetBlockData(new Vector3I(pX, pY, 0));

                            bool isNearbyBlockSideSolid = !nearbyBlock.IsSideSolid(Direction.DecreaseZ, nearbyBlockData);
                            bool isNearbyBlockNotSame = nearbyBlockID != thisBlockID;
                            if (isNearbyBlockSideSolid || (!nearbyBlock.isOpaque && isNearbyBlockNotSame))
                            {
                                blockSides |= Direction.IncreaseZ;
                            }
                        }
                    }
                    else
                    {
                        blockSides |= Direction.IncreaseZ;
                    }
                }
                #endregion
                #region Decrease Z
                {
                    if (pZ - 1 >= 0)
                    {
                        nearbyBlockID = pContext.Chunk.GetBlockID(pX, pY, pZ - 1);
                        if (nearbyBlockID == BlockRegister.AirBlockID)
                        {
                            blockSides |= Direction.DecreaseZ;
                        }
                        else
                        {
                            nearbyBlock = BlockRegister.Instance.Get(nearbyBlockID);
                            nearbyBlockData = pContext.Chunk.GetBlockData(new Vector3I(pX, pY, pZ - 1));

                            bool isNearbyBlockSideSolid = !nearbyBlock.IsSideSolid(Direction.IncreaseZ, nearbyBlockData);
                            bool isNearbyBlockNotSame = nearbyBlockID != thisBlockID;
                            if (isNearbyBlockSideSolid || (!nearbyBlock.isOpaque && isNearbyBlockNotSame))
                            {
                                blockSides |= Direction.DecreaseZ;
                            }
                        }
                    }
                    else if (pContext.ChunkDecreaseZ != null)
                    {
                        nearbyBlockID = pContext.ChunkDecreaseZ.GetBlockID(pX, pY, Core.ChunkSizes.Z - 1);
                        if (nearbyBlockID == BlockRegister.AirBlockID)
                        {
                            blockSides |= Direction.DecreaseZ;
                        }
                        else
                        {
                            nearbyBlock = BlockRegister.Instance.Get(nearbyBlockID);
                            nearbyBlockData = pContext.ChunkDecreaseZ.GetBlockData(new Vector3I(pX, pY, Core.ChunkSizes.Z - 1));

                            bool isNearbyBlockSideSolid = !nearbyBlock.IsSideSolid(Direction.IncreaseZ, nearbyBlockData);
                            bool isNearbyBlockNotSame = nearbyBlockID != thisBlockID;
                            if (isNearbyBlockSideSolid || (!nearbyBlock.isOpaque && isNearbyBlockNotSame))
                            {
                                blockSides |= Direction.DecreaseZ;
                            }
                        }
                    }
                    else
                    {
                        blockSides |= Direction.DecreaseZ;
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }

            return blockSides;
        }

        /// <summary>
        /// По умолчанию возвращает коробку 1x1x1 метр, совпадающюю с ячейкой сетки.
        /// </summary>
        public virtual BoundingBox GetBounds(int pX, int pY, int pZ)
        {
            return new BoundingBox(
                new Vector3(pX, pY, pZ),
                new Vector3(pX + 1, pY + 1, pZ + 1));
        }
        public virtual BoundingBox GetBounds(Vector3I pCoord)
        {
            return new BoundingBox(
                new Vector3(pCoord.X, pCoord.Y, pCoord.Z),
                new Vector3(pCoord.X + 1, pCoord.Y + 1, pCoord.Z + 1));
        }

        public abstract BlockMetadata CreateDefaultMetadata();

        /// <summary>
        /// Обновляет состояние шаблона блока.
        /// </summary>
        public virtual void Update(GameTime pGameTime, bool pIsTick)
        {
        }
        /// <summary>
        /// Обновляет состояние данных блока по указаным мировым координатам и инициирует обновление соседних блоков.
        /// </summary>
        public virtual void UpdateData(World pWorld, Vector3I pCoordInWorld, ref Queue<Vector3I> pNewBocksToUpdate)
        {
        }
        public virtual void Break(World pWorld, Vector3I pCoordInWorld)
        {
        }
        public virtual void Touch(Entity pEntity, World pWorld, Vector3I pWorldCoord)
        {
        }

    }
}