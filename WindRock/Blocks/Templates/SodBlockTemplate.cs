﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindRock.Common;
using GameHelper.Data;
using WindRock.Registers;

namespace WindRock.Blocks
{
    public class SodBlockTemplate : SideBlockTemplate
    {
        /* Fields */



        /* Properties */



        /* Constructors */

        public SodBlockTemplate(string pName, bool pIsOpaque, TextureCoord pSideTextureCoord, TextureCoord pTopTextureCoord, TextureCoord pBottomTextureCoord)
            : base(pName, pIsOpaque, pSideTextureCoord, pTopTextureCoord, pBottomTextureCoord)
        {
        }

        /* Private methods */

        private void TryGrownPlantAbove(World pWorld, Vector3I pCoordInWorld)
        {
            Vector3I aboveCoord = pCoordInWorld + Vector3I.UnitY;
            if (pWorld.GetBlockID(aboveCoord) == BlockRegister.AirBlockID)
            {
                if (pWorld.Random.Next(100) == 0)
                {
                    short blockID = 0;
                    BlockTemplate block = null;
                    int randomValue = pWorld.Random.Next(3);
                    switch (randomValue)
                    {
                        case 0:
                            blockID = BlockRegister.GrassBlockID;
                            block = BlockRegister.Instance.Get(blockID);
                            break;
                        case 1:
                            blockID = BlockRegister.WheatBlockID;
                            block = BlockRegister.Instance.Get(blockID);
                            break;
                        case 2:
                            blockID = BlockRegister.DandelionBlockID;
                            block = BlockRegister.Instance.Get(blockID);
                            break;
                    }

                    if (block != null)
                    {
                        pWorld.SetBlockToBuffer(aboveCoord, blockID, block.CreateDefaultMetadata());
                    }
                }
            }
        }
        private void TryConvertBlockToSod(World pWorld, Vector3I pCoordInWorld)
        {
            short aboveBlockID = pWorld.GetBlockID(pCoordInWorld + Vector3I.UnitY);
            //BlockTemplate aboveBlock = BlockRegister.Instance.Get(aboveBlockID);
            if (aboveBlockID == BlockRegister.AirBlockID)
            {
                if (pWorld.Random.Next(10) == 0)
                {
                    short thisBlockID = pWorld.GetBlockID(pCoordInWorld);
                    short newBlockID = BlockRegister.AirBlockID;
                    switch (thisBlockID)
                    {
                        case BlockRegister.DirtBlockID:
                            newBlockID = BlockRegister.SodBlockID;
                            break;
                        case BlockRegister.StonyDirtBlockID:
                            newBlockID = BlockRegister.StonySodBlockID;
                            break;
                    }

                    if (newBlockID != BlockRegister.AirBlockID)
                    {
                        BlockTemplate newBlock = BlockRegister.Instance.Get(newBlockID);
                        pWorld.SetBlockToBuffer(pCoordInWorld, newBlockID, newBlock.CreateDefaultMetadata());
                    }
                }
            }
        }

        /* Protected methods */



        /* Public methods */

        public override void UpdateData(World pWorld, Vector3I pCoordInWorld, ref Queue<Vector3I> pNewBocksToUpdate)
        {
            this.TryGrownPlantAbove(pWorld, pCoordInWorld);

            this.TryConvertBlockToSod(pWorld, pCoordInWorld.PlusX());
            this.TryConvertBlockToSod(pWorld, pCoordInWorld.MinusX());
            this.TryConvertBlockToSod(pWorld, pCoordInWorld.PlusZ());
            this.TryConvertBlockToSod(pWorld, pCoordInWorld.MinusZ());
            this.TryConvertBlockToSod(pWorld, pCoordInWorld.PlusY());
            this.TryConvertBlockToSod(pWorld, pCoordInWorld.PlusY().PlusX());
            this.TryConvertBlockToSod(pWorld, pCoordInWorld.PlusY().MinusX());
            this.TryConvertBlockToSod(pWorld, pCoordInWorld.PlusY().PlusZ());
            this.TryConvertBlockToSod(pWorld, pCoordInWorld.PlusY().MinusZ());
            this.TryConvertBlockToSod(pWorld, pCoordInWorld.MinusY().PlusX());
            this.TryConvertBlockToSod(pWorld, pCoordInWorld.MinusY().MinusX());
            this.TryConvertBlockToSod(pWorld, pCoordInWorld.MinusY().PlusZ());
            this.TryConvertBlockToSod(pWorld, pCoordInWorld.MinusY().MinusZ());
        }

    }
}