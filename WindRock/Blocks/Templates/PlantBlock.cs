﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using SpoonCraft.Common;

namespace SpoonCraft.Engine.Blocks
{
    public class PlantBlock : Block
    {
        /* Fields */

        private TextureCoord textureCoord;

        protected bool canGrow;
        protected PlantBehaviour onSun;
        protected PlantBehaviour onLight;
        protected PlantBehaviour inDarkness;
        protected byte lightLevel;

        /* Properties */

        /* Constructors */

        public PlantBlock()
            : base()
        {
            base.IsHard = false;
            base.IsOpaque = false;
            base.IsSolid = false;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public void SetTexturesCoords(TextureCoord pTextureIndeces)
        {
            this.textureCoord = pTextureIndeces;
        }

        public override int BuildVertices(Vector3 pPosition, BlockSide pBlockSides, ref VertexPositionTextureNoNormal[] pBlocksVertices, int pVertexIndex, BlockContext pContext)
        {
            Vector2 texture_u0_v0 = new Vector2(
                this.textureCoord.Col * Core.BlockTextureProportionalSize.X,
                this.textureCoord.Row * Core.BlockTextureProportionalSize.Y);
            Vector2 texture_u1_v0 = new Vector2(
                (this.textureCoord.Col + 1) * Core.BlockTextureProportionalSize.X,
                this.textureCoord.Row * Core.BlockTextureProportionalSize.Y);
            Vector2 texture_u0_v1 = new Vector2(
                this.textureCoord.Col * Core.BlockTextureProportionalSize.X,
                (this.textureCoord.Row + 1) * Core.BlockTextureProportionalSize.Y);
            Vector2 texture_u1_v1 = new Vector2(
                (this.textureCoord.Col + 1) * Core.BlockTextureProportionalSize.X,
                (this.textureCoord.Row + 1) * Core.BlockTextureProportionalSize.Y);

            // First part (front)
            pBlocksVertices[++pVertexIndex] = new VertexPositionTextureNoNormal( new Vector3( pPosition.X, pPosition.Y + 1, pPosition.Z ), texture_u0_v0 );
            pBlocksVertices[++pVertexIndex] = new VertexPositionTextureNoNormal( new Vector3( pPosition.X, pPosition.Y, pPosition.Z ), texture_u0_v1 );
            pBlocksVertices[++pVertexIndex] = new VertexPositionTextureNoNormal( new Vector3( pPosition.X + 1, pPosition.Y, pPosition.Z + 1 ), texture_u1_v1 );
            pBlocksVertices[++pVertexIndex] = new VertexPositionTextureNoNormal( new Vector3( pPosition.X, pPosition.Y + 1, pPosition.Z ), texture_u0_v0 );
            pBlocksVertices[++pVertexIndex] = new VertexPositionTextureNoNormal( new Vector3( pPosition.X + 1, pPosition.Y, pPosition.Z + 1 ), texture_u1_v1 );
            pBlocksVertices[++pVertexIndex] = new VertexPositionTextureNoNormal( new Vector3( pPosition.X + 1, pPosition.Y + 1, pPosition.Z + 1 ), texture_u1_v0 );

            // First part (back)
            pBlocksVertices[++pVertexIndex] = new VertexPositionTextureNoNormal( new Vector3( pPosition.X, pPosition.Y + 1, pPosition.Z ), texture_u0_v0 );
            pBlocksVertices[++pVertexIndex] = new VertexPositionTextureNoNormal( new Vector3( pPosition.X + 1, pPosition.Y + 1, pPosition.Z + 1 ), texture_u1_v0 );
            pBlocksVertices[++pVertexIndex] = new VertexPositionTextureNoNormal( new Vector3( pPosition.X + 1, pPosition.Y, pPosition.Z + 1 ), texture_u1_v1 );
            pBlocksVertices[++pVertexIndex] = new VertexPositionTextureNoNormal( new Vector3( pPosition.X, pPosition.Y + 1, pPosition.Z ), texture_u0_v0 );
            pBlocksVertices[++pVertexIndex] = new VertexPositionTextureNoNormal( new Vector3( pPosition.X + 1, pPosition.Y, pPosition.Z + 1 ), texture_u1_v1 );
            pBlocksVertices[++pVertexIndex] = new VertexPositionTextureNoNormal( new Vector3( pPosition.X, pPosition.Y, pPosition.Z ), texture_u0_v1 );

            // Second part (front)
            pBlocksVertices[++pVertexIndex] = new VertexPositionTextureNoNormal( new Vector3( pPosition.X, pPosition.Y + 1, pPosition.Z + 1 ), texture_u0_v0 );
            pBlocksVertices[++pVertexIndex] = new VertexPositionTextureNoNormal( new Vector3( pPosition.X, pPosition.Y, pPosition.Z + 1 ), texture_u0_v1 );
            pBlocksVertices[++pVertexIndex] = new VertexPositionTextureNoNormal( new Vector3( pPosition.X + 1, pPosition.Y, pPosition.Z ), texture_u1_v1 );
            pBlocksVertices[++pVertexIndex] = new VertexPositionTextureNoNormal( new Vector3( pPosition.X, pPosition.Y + 1, pPosition.Z + 1 ), texture_u0_v0 );
            pBlocksVertices[++pVertexIndex] = new VertexPositionTextureNoNormal( new Vector3( pPosition.X + 1, pPosition.Y, pPosition.Z ), texture_u1_v1 );
            pBlocksVertices[++pVertexIndex] = new VertexPositionTextureNoNormal( new Vector3( pPosition.X + 1, pPosition.Y + 1, pPosition.Z ), texture_u1_v0 );
            
            // Second part (back)
            pBlocksVertices[++pVertexIndex] = new VertexPositionTextureNoNormal( new Vector3( pPosition.X, pPosition.Y + 1, pPosition.Z + 1 ), texture_u0_v0 );
            pBlocksVertices[++pVertexIndex] = new VertexPositionTextureNoNormal( new Vector3( pPosition.X + 1, pPosition.Y + 1, pPosition.Z ), texture_u1_v0 );
            pBlocksVertices[++pVertexIndex] = new VertexPositionTextureNoNormal( new Vector3( pPosition.X + 1, pPosition.Y, pPosition.Z ), texture_u1_v1 );
            pBlocksVertices[++pVertexIndex] = new VertexPositionTextureNoNormal( new Vector3( pPosition.X, pPosition.Y + 1, pPosition.Z + 1 ), texture_u0_v0 );
            pBlocksVertices[++pVertexIndex] = new VertexPositionTextureNoNormal( new Vector3( pPosition.X + 1, pPosition.Y, pPosition.Z ), texture_u1_v1 );
            pBlocksVertices[++pVertexIndex] = new VertexPositionTextureNoNormal( new Vector3( pPosition.X, pPosition.Y, pPosition.Z + 1 ), texture_u0_v1 );

            return pVertexIndex;
        }

        public override BlockSide GetBlockSides(Chunk pChunk, int pX, int pY, int pZ, out int pVerticesCount)
        {
            pVerticesCount = 24;
            return BlockSide.None;
        }
    }
}