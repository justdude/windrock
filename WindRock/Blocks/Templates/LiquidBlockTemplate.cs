﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using GameHelper.Data;
using SpoonCraft.Blocks;
using SpoonCraft.Common;
using SpoonCraft.Registers;

namespace SpoonCraft.Blocks
{
    public class LiquidBlockTemplate : SolidBlockTemplate
    {
        /* Fields */

        private int updateIntervalTicks;
        private int ticksToUpdate;
        private bool isDataUpdatePossible;

        /* Properties */



        /* Constructors */

        public LiquidBlockTemplate(string pName, Opacity pOpacity, TextureCoord pSideTextureCoord, int pUpdatePerTicks)
            : base(pName, pOpacity, pSideTextureCoord)
        {
            this.IsHard = false;
            this.updateIntervalTicks = pUpdatePerTicks;

            this.ticksToUpdate = this.updateIntervalTicks;
        }

        /* Private methods */



        /* Protected methods */

        protected int BuildSideVertices(Vector3 pPosition, Direction pBlockSide, LiquidBlockData pWaterBlockData, ref VertexPositionTexture[] pBlocksVerteces, int pVertexIndex)
        {
            float a = MathHelper.Clamp(pWaterBlockData.SourceAmount * Core.OneDivMaxWaterAmount, 0, 1);
            TextureCoord textureCoords = GetTextureCoord(pBlockSide);
            Vector2 textureCoord_0_0 = new Vector2(
                textureCoords.Col * Core.BlockTextureProportionalSize.X,
                (textureCoords.Row) * Core.BlockTextureProportionalSize.Y);
            Vector2 textureCoord_1_0 = new Vector2(
                (textureCoords.Col + 1) * Core.BlockTextureProportionalSize.X,
                (textureCoords.Row) * Core.BlockTextureProportionalSize.Y);
            Vector2 textureCoord_0_1 = new Vector2(
                textureCoords.Col * Core.BlockTextureProportionalSize.X,
                (textureCoords.Row + 1) * Core.BlockTextureProportionalSize.Y);
            Vector2 textureCoord_1_1 = new Vector2(
                (textureCoords.Col + 1) * Core.BlockTextureProportionalSize.X,
                (textureCoords.Row + 1) * Core.BlockTextureProportionalSize.Y);
            Vector2 textureCoord_0_1SubA = new Vector2(
                textureCoords.Col * Core.BlockTextureProportionalSize.X,
                (textureCoords.Row + 1 - a) * Core.BlockTextureProportionalSize.Y);
            Vector2 textureCoord_1_1SubA = new Vector2(
                (textureCoords.Col + 1) * Core.BlockTextureProportionalSize.X,
                (textureCoords.Row + 1 - a) * Core.BlockTextureProportionalSize.Y);

            Vector3 point2SubA = new Vector3(Point2.X, Point2.Y * a, Point2.Z);
            Vector3 point3SubA = new Vector3(Point3.X, Point3.Y * a, Point3.Z);
            Vector3 point6SubA = new Vector3(Point6.X, Point6.Y * a, Point6.Z);
            Vector3 point7SubA = new Vector3(Point7.X, Point7.Y * a, Point7.Z);

            switch (pBlockSide)
            {
                case Direction.IncreaseX:
                    {
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point1, textureCoord_1_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point5, textureCoord_0_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + point7SubA, textureCoord_0_1SubA);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point1, textureCoord_1_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + point7SubA, textureCoord_0_1SubA);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + point3SubA, textureCoord_1_1SubA);
                    }
                    break;
                case Direction.DecreaseX:
                    {
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point0, textureCoord_0_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + point2SubA, textureCoord_0_1SubA);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + point6SubA, textureCoord_1_1SubA);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point0, textureCoord_0_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + point6SubA, textureCoord_1_1SubA);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point4, textureCoord_1_1);
                    }
                    break;
                case Direction.IncreaseY:
                    {
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + point2SubA, textureCoord_0_0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + point3SubA, textureCoord_1_0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + point7SubA, textureCoord_1_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + point2SubA, textureCoord_0_0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + point7SubA, textureCoord_1_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + point6SubA, textureCoord_0_1);
                    }
                    break;
                case Direction.DecreaseY:
                    {
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point0, textureCoord_0_0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point4, textureCoord_0_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point5, textureCoord_1_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point0, textureCoord_0_0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point5, textureCoord_1_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point1, textureCoord_1_0);
                    }
                    break;
                case Direction.IncreaseZ:
                    {
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point4, textureCoord_0_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + point6SubA, textureCoord_0_1SubA);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + point7SubA, textureCoord_1_1SubA);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point4, textureCoord_0_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + point7SubA, textureCoord_1_1SubA);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point5, textureCoord_1_1);
                    }
                    break;
                case Direction.DecreaseZ:
                    {
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point0, textureCoord_1_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point1, textureCoord_0_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + point3SubA, textureCoord_0_1SubA);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point0, textureCoord_1_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + point3SubA, textureCoord_0_1SubA);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + point2SubA, textureCoord_1_1SubA);
                    }
                    break;
            }

            return pVertexIndex;
        }

        /* Public methods */

        public override void BuildVertices(Vector3 pPosition, BlockData pBlockData, ref VertexPositionTexture[] pOpaqueBlocksVertices, ref int pOpaqueVertexIndex, ref VertexPositionTexture[] pNotOpaqueBlocksVertices, ref int pNotOpaqueVertexIndex)
        {
            if (pBlockData.VisibleSides != Direction.None)
            {
                bool isOpaque = this.Opacity == Opacity.Opaque;
                VertexPositionTexture[] blocksVertices = isOpaque ? pOpaqueBlocksVertices : pNotOpaqueBlocksVertices;
                int vertexIndex = isOpaque ? pOpaqueVertexIndex : pNotOpaqueVertexIndex;
                LiquidBlockData waterBlockData = pBlockData as LiquidBlockData;

                if (pBlockData.VisibleSides.HasFlag(Direction.IncreaseX))
                {
                    vertexIndex = BuildSideVertices(pPosition, Direction.IncreaseX, waterBlockData, ref blocksVertices, vertexIndex);
                }
                if (pBlockData.VisibleSides.HasFlag(Direction.DecreaseX))
                {
                    vertexIndex = BuildSideVertices(pPosition, Direction.DecreaseX, waterBlockData, ref blocksVertices, vertexIndex);
                }

                if (waterBlockData.SourceAmount >= Core.DefaultWaterAmount)
                {
                    if (pBlockData.VisibleSides.HasFlag(Direction.IncreaseY))
                    {
                        vertexIndex = BuildSideVertices(pPosition, Direction.IncreaseY, waterBlockData, ref blocksVertices, vertexIndex);
                    }
                }
                else if (pBlockData.VisibleSides.HasFlag(Direction.IncreaseX)
                    || pBlockData.VisibleSides.HasFlag(Direction.DecreaseX)
                    || pBlockData.VisibleSides.HasFlag(Direction.IncreaseY)
                    || pBlockData.VisibleSides.HasFlag(Direction.IncreaseZ)
                    || pBlockData.VisibleSides.HasFlag(Direction.DecreaseZ))
                {
                    vertexIndex = BuildSideVertices(pPosition, Direction.IncreaseY, waterBlockData, ref blocksVertices, vertexIndex);
                }

                if (pBlockData.VisibleSides.HasFlag(Direction.DecreaseY))
                {
                    vertexIndex = BuildSideVertices(pPosition, Direction.DecreaseY, waterBlockData, ref blocksVertices, vertexIndex);
                }

                if (pBlockData.VisibleSides.HasFlag(Direction.IncreaseZ))
                {
                    vertexIndex = BuildSideVertices(pPosition, Direction.IncreaseZ, waterBlockData, ref blocksVertices, vertexIndex);
                }
                if (pBlockData.VisibleSides.HasFlag(Direction.DecreaseZ))
                {
                    vertexIndex = BuildSideVertices(pPosition, Direction.DecreaseZ, waterBlockData, ref blocksVertices, vertexIndex);
                }

                if (isOpaque)
                {
                    pOpaqueVertexIndex = vertexIndex;
                }
                else
                {
                    pNotOpaqueVertexIndex = vertexIndex;
                }
            }
        }
        public override void Update(GameTime pGameTime, bool pIsTick)
        {
            if (pIsTick)
            {
                this.ticksToUpdate--;
                if (isDataUpdatePossible = ticksToUpdate <= 0)
                {
                    this.ticksToUpdate = this.updateIntervalTicks;
                }
            }
        }
        public override void UpdateData(World pWorld, Vector3I pCoordInWorld, ref Queue<Vector3I> pNewBocksToUpdate)
        {
            Vector3I belowCoord = pCoordInWorld - Vector3I.UnitY;
            if (World.GetCoordOfChunk(belowCoord).Y < Core.WorldArea.MaxY)
            {
                pWorld.SetAirToBuffer(pCoordInWorld);
            }
            else
            {
                short belowID = pWorld.GetBlockID(belowCoord);
                short thisID = pWorld.GetBlockID(pCoordInWorld);

                if (belowID == BlockRegister.AirBlockID)
                {
                    pWorld.SetBlockToBuffer(belowCoord, thisID, pWorld.GetBlockData(pCoordInWorld));
                    pWorld.SetAirToBuffer(pCoordInWorld);

                    if (!pNewBocksToUpdate.Contains(belowCoord))
                    {
                        pNewBocksToUpdate.Enqueue(belowCoord);
                    }
                }
                else if (belowID == thisID)
                {
                    LiquidBlockData thisData = pWorld.GetBlockData(pCoordInWorld) as LiquidBlockData;
                    LiquidBlockData belowData = pWorld.GetBlockData(belowCoord) as LiquidBlockData;
                    if (belowData.SourceAmount < Core.MaxLiquidAmount)
                    {
                        byte amountDifference = (byte) Math.Min((Core.MaxLiquidAmount - belowData.SourceAmount), thisData.SourceAmount);
                        belowData.SourceAmount += amountDifference;
                        thisData.SourceAmount -= amountDifference;
                    }
                }
            }
        }
        //public override bool UpdateInstance(GameTime pGameTime, World pWorld, Vector3I pCoord)
        //{
        //    bool isUpdated = false;

        //    if (this.isInstancesUpdateRequired)
        //    {
        //        if (pCoord.Y == 0)
        //        {
        //            pWorld.ClearBlockID(pCoord.X, pCoord.Y, pCoord.Z);
        //            isUpdated = true;
        //        }
        //        else if (pWorld.GetBlockID(pCoord.X, pCoord.Y - 1, pCoord.Z) == BlockRegister.AirBlockID)
        //        {
        //            pWorld.SetBlockID(
        //                pCoord.X, pCoord.Y - 1, pCoord.Z,
        //                pWorld.GetBlockID(pCoord.X, pCoord.Y, pCoord.Z),
        //                pWorld.GetBlockData(pCoord.X, pCoord.Y, pCoord.Z));
        //            pWorld.ClearBlockID(pCoord.X, pCoord.Y, pCoord.Z);
        //            isUpdated = true;
        //        }
        //        else if (pWorld.GetBlockID(pCoord.X, pCoord.Y + 1, pCoord.Z) == BlockRegister.WaterID)
        //        {
        //            if (pWorld.GetBlockID(pCoord.X + 1, pCoord.Y, pCoord.Z) == BlockRegister.AirBlockID)
        //            {
        //                pWorld.SetBlockID(
        //                    pCoord.X + 1, pCoord.Y, pCoord.Z,
        //                    pWorld.GetBlockID(pCoord.X, pCoord.Y, pCoord.Z),
        //                    pWorld.GetBlockData(pCoord.X, pCoord.Y, pCoord.Z));
        //                pWorld.ClearBlockID(pCoord.X, pCoord.Y, pCoord.Z);
        //                isUpdated = true;
        //            }
        //            else if (pWorld.GetBlockID(pCoord.X - 1, pCoord.Y, pCoord.Z) == BlockRegister.AirBlockID)
        //            {
        //                pWorld.SetBlockID(
        //                    pCoord.X - 1, pCoord.Y, pCoord.Z,
        //                    pWorld.GetBlockID(pCoord.X, pCoord.Y, pCoord.Z),
        //                    pWorld.GetBlockData(pCoord.X, pCoord.Y, pCoord.Z));
        //                pWorld.ClearBlockID(pCoord.X, pCoord.Y, pCoord.Z);
        //                isUpdated = true;
        //            }
        //            else if (pWorld.GetBlockID(pCoord.X, pCoord.Y, pCoord.Z + 1) == BlockRegister.AirBlockID)
        //            {
        //                pWorld.SetBlockID(
        //                    pCoord.X, pCoord.Y, pCoord.Z + 1,
        //                    pWorld.GetBlockID(pCoord.X, pCoord.Y, pCoord.Z),
        //                    pWorld.GetBlockData(pCoord.X, pCoord.Y, pCoord.Z));
        //                pWorld.ClearBlockID(pCoord.X, pCoord.Y, pCoord.Z);
        //                isUpdated = true;
        //            }
        //            else if (pWorld.GetBlockID(pCoord.X, pCoord.Y, pCoord.Z - 1) == BlockRegister.AirBlockID)
        //            {
        //                pWorld.SetBlockID(
        //                    pCoord.X, pCoord.Y, pCoord.Z - 1,
        //                    pWorld.GetBlockID(pCoord.X, pCoord.Y, pCoord.Z),
        //                    pWorld.GetBlockData(pCoord.X, pCoord.Y, pCoord.Z));
        //                pWorld.ClearBlockID(pCoord.X, pCoord.Y, pCoord.Z);
        //                isUpdated = true;
        //            }
        //        }
        //    }

        //    return isUpdated;
        //}
        public override BlockData CreateData(bool pIsNatural)
        {
            return new LiquidBlockData(pIsNatural)
            {
                SourceAmount = Core.DefaultWaterAmount
            };
        }

    }
}