﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace SpoonCraft.Engine.Blocks
{
    public class LiquidBlock : Block
    {
        /* Fields */

        private static byte maximumAmount = 16;
        private static byte defaultAmount = 12;

        private byte amount;

        private TextureCoord sideTextureCoord;

        /* Properties */

        //public Vector2I SideTextureIndeces
        //{
        //    set
        //    {
        //        this.sideTextureIndeces = value;
        //    }
        //    get
        //    {
        //        return this.sideTextureIndeces;
        //    }
        //}

        /* Constructors */

        public LiquidBlock()
            : base()
        {
            base.IsOpaque = false;

            this.amount = defaultAmount;
        }

        /* Private methods */

        private int BuildSideVertices(Vector3 pPosition, BlockSide pBlockSide, ref VertexPositionTextureNoNormal[] pBlocksVerteces, int pVertexIndex)
        {
            TextureCoord textureCoord = this.sideTextureCoord;
            Vector2 texture_u0_v0 = new Vector2(
                textureCoord.Col * Core.BlockTextureProportionalSize.X,
                textureCoord.Row * Core.BlockTextureProportionalSize.Y);
            Vector2 texture_u1_v0 = new Vector2(
                (textureCoord.Col + 1) * Core.BlockTextureProportionalSize.X,
                textureCoord.Row * Core.BlockTextureProportionalSize.Y);
            Vector2 texture_u0_v1 = new Vector2(
                textureCoord.Col * Core.BlockTextureProportionalSize.X,
                (textureCoord.Row + 1) * Core.BlockTextureProportionalSize.Y);
            Vector2 texture_u1_v1 = new Vector2(
                (textureCoord.Col + 1) * Core.BlockTextureProportionalSize.X,
                (textureCoord.Row + 1) * Core.BlockTextureProportionalSize.Y);

            float topY = pPosition.Y + (float) this.amount / (float) maximumAmount;

            switch (pBlockSide)
            {
                case BlockSide.IncreaseX:
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X + 1, topY, pPosition.Z + 1), texture_u0_v0);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X + 1, topY, pPosition.Z), texture_u1_v0);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X + 1, pPosition.Y, pPosition.Z), texture_u1_v1);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X + 1, topY, pPosition.Z + 1), texture_u0_v0);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X + 1, pPosition.Y, pPosition.Z), texture_u1_v1);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X + 1, pPosition.Y, pPosition.Z + 1), texture_u0_v1);
                    break;
                case BlockSide.DecreaseX:
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X, topY, pPosition.Z), texture_u0_v0);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X, topY, pPosition.Z + 1), texture_u1_v0);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X, pPosition.Y, pPosition.Z + 1), texture_u1_v1);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X, topY, pPosition.Z), texture_u0_v0);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X, pPosition.Y, pPosition.Z + 1), texture_u1_v1);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X, pPosition.Y, pPosition.Z), texture_u0_v1);
                    break;
                case BlockSide.IncreaseY:
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X + 1, topY, pPosition.Z + 1), texture_u0_v1);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X, topY, pPosition.Z + 1), texture_u0_v0);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X, topY, pPosition.Z), texture_u1_v0);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X + 1, topY, pPosition.Z + 1), texture_u0_v1);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X, topY, pPosition.Z), texture_u1_v0);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X + 1, topY, pPosition.Z), texture_u1_v1);
                    break;
                case BlockSide.DecreaseY:
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X, pPosition.Y, pPosition.Z + 1), texture_u0_v0);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X + 1, pPosition.Y, pPosition.Z + 1), texture_u1_v0);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X + 1, pPosition.Y, pPosition.Z), texture_u1_v1);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X, pPosition.Y, pPosition.Z + 1), texture_u0_v0);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X + 1, pPosition.Y, pPosition.Z), texture_u1_v1);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X, pPosition.Y, pPosition.Z), texture_u0_v1);
                    break;
                case BlockSide.IncreaseZ:
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X, topY, pPosition.Z + 1), texture_u0_v0);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X + 1, topY, pPosition.Z + 1), texture_u1_v0);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X + 1, pPosition.Y, pPosition.Z + 1), texture_u1_v1);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X, topY, pPosition.Z + 1), texture_u0_v0);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X + 1, pPosition.Y, pPosition.Z + 1), texture_u1_v1);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X, pPosition.Y, pPosition.Z + 1), texture_u0_v1);
                    break;
                case BlockSide.DecreaseZ:
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X + 1, topY, pPosition.Z), texture_u0_v0);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X, topY, pPosition.Z), texture_u1_v0);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X, pPosition.Y, pPosition.Z), texture_u1_v1);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X + 1, topY, pPosition.Z), texture_u0_v0);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X, pPosition.Y, pPosition.Z), texture_u1_v1);
                    pBlocksVerteces[++pVertexIndex] = new VertexPositionTextureNoNormal(new Vector3(pPosition.X + 1, pPosition.Y, pPosition.Z), texture_u0_v1);
                    break;
            }

            return pVertexIndex;
        }

        /* Protected methods */



        /* Public methods */

        public override BlockSide GetBlockSides(int pX, int pY, int pZ, BlockContext pContext, out int pVerticesCount)
        {
            //pVerticesCount = 0;
            //BlockSide blockSides = BlockSide.None;
            //Block nearbyBlock = null;

            //short nearbyBlockID = BlockRegister.AirBlockID;
            //Chunk nearbyChunk = null;
            //Vector2I nearbyChunkPosition;

            //short thisBlockID = pChunk.GetBlockID(pX, pY, pZ);
            //Block thisBlock = BlockRegister.Instance.GetBlock(thisBlockID);

            //#region Right
            //{

            //    nearbyBlockID = BlockRegister.AirBlockID;
            //    if (pX + 1 < Core.ChunkSize.X)
            //    {
            //        nearbyBlockID = pChunk.GetBlockID(pX + 1, pY, pZ);
            //    }
            //    else
            //    {
            //        nearbyChunkPosition = new Vector2I(
            //            pChunk.Coord.X + 1,
            //            pChunk.Coord.Z);
            //        if (pChunk.World.TryGetChunk(nearbyChunkPosition, out nearbyChunk))
            //        {
            //            nearbyBlockID = nearbyChunk.GetBlockID(0, pY, pZ);
            //        }
            //    }

            //    nearbyBlock = (BlockRegister.Instance.GetBlock(nearbyBlockID));
            //    if (nearbyBlockID == BlockRegister.AirBlockID
            //        || !(thisBlock.IsSolid && nearbyBlock.IsSolid)
            //        || (!nearbyBlock.IsSolid && nearbyBlockID != thisBlockID))
            //    {
            //        blockSides |= BlockSide.IncreaseX;
            //        pVerticesCount += 6;
            //    }
            //}
            //#endregion

            //#region Left
            //{
            //    nearbyBlockID = BlockRegister.AirBlockID;
            //    if (pX - 1 >= 0)
            //    {
            //        nearbyBlockID = pChunk.GetBlockID(pX - 1, pY, pZ);
            //    }
            //    else
            //    {
            //        nearbyChunkPosition = new Vector2I(
            //            pChunk.Coord.X - 1,
            //            pChunk.Coord.Z);
            //        if (pChunk.World.TryGetChunk(nearbyChunkPosition, out nearbyChunk))
            //        {
            //            nearbyBlockID = nearbyChunk.GetBlockID(Core.ChunkSize.X - 1, pY, pZ);
            //        }
            //    }

            //    nearbyBlock = (BlockRegister.Instance.GetBlock(nearbyBlockID));
            //    if (nearbyBlockID == BlockRegister.AirBlockID
            //        || !(thisBlock.IsSolid && nearbyBlock.IsSolid)
            //        || (!nearbyBlock.IsSolid && nearbyBlockID != thisBlockID))
            //    {
            //        blockSides |= BlockSide.DecreaseX;
            //        pVerticesCount += 6;
            //    }
            //}
            //#endregion

            //#region Top
            //{
            //    nearbyBlockID = BlockRegister.AirBlockID;
            //    if (pY + 1 < Core.ChunkSize.Y)
            //    {
            //        nearbyBlockID = pChunk.GetBlockID(pX, pY + 1, pZ);
            //    }

            //    nearbyBlock = (BlockRegister.Instance.GetBlock(nearbyBlockID));
            //    if (nearbyBlockID == BlockRegister.AirBlockID
            //        || !(thisBlock.IsSolid && nearbyBlock.IsSolid)
            //        || (!nearbyBlock.IsSolid && nearbyBlockID != thisBlockID))
            //    {
            //        blockSides |= BlockSide.IncreaseY;
            //        pVerticesCount += 6;
            //    }
            //}
            //#endregion

            //#region Bottom
            //{
            //    nearbyBlockID = BlockRegister.AirBlockID;
            //    if (pY - 1 >= 0)
            //    {
            //        nearbyBlockID = pChunk.GetBlockID(pX, pY - 1, pZ);
            //    }

            //    nearbyBlock = (BlockRegister.Instance.GetBlock(nearbyBlockID));
            //    if (nearbyBlockID == BlockRegister.AirBlockID
            //        || !(thisBlock.IsSolid && nearbyBlock.IsSolid)
            //        || (!nearbyBlock.IsSolid && nearbyBlockID != thisBlockID))
            //    {
            //        blockSides |= BlockSide.DecreaseY;
            //        pVerticesCount += 6;
            //    }
            //}
            //#endregion

            //#region Front
            //{
            //    nearbyBlockID = BlockRegister.AirBlockID;
            //    if (pZ + 1 < Core.ChunkSize.Z)
            //    {
            //        nearbyBlockID = pChunk.GetBlockID(pX, pY, pZ + 1);
            //    }
            //    else
            //    {
            //        nearbyChunkPosition = new Vector2I(
            //            pChunk.Coord.X,
            //            pChunk.Coord.Z + 1);
            //        if (pChunk.World.TryGetChunk(nearbyChunkPosition, out nearbyChunk))
            //        {
            //            nearbyBlockID = nearbyChunk.GetBlockID(pX, pY, 0);
            //        }
            //    }

            //    nearbyBlock = (BlockRegister.Instance.GetBlock(nearbyBlockID));
            //    if (nearbyBlockID == BlockRegister.AirBlockID
            //        || !(thisBlock.IsSolid && nearbyBlock.IsSolid)
            //        || (!nearbyBlock.IsSolid && nearbyBlockID != thisBlockID))
            //    {
            //        blockSides |= BlockSide.IncreaseZ;
            //        pVerticesCount += 6;
            //    }
            //}
            //#endregion

            //#region Back
            //{
            //    nearbyBlockID = BlockRegister.AirBlockID;
            //    if (pZ - 1 >= 0)
            //    {
            //        nearbyBlockID = pChunk.GetBlockID(pX, pY, pZ - 1);
            //    }
            //    else
            //    {
            //        nearbyChunkPosition = new Vector2I(
            //            pChunk.Coord.X,
            //            pChunk.Coord.Z - 1);
            //        if (pChunk.World.TryGetChunk(nearbyChunkPosition, out nearbyChunk))
            //        {
            //            nearbyBlockID = nearbyChunk.GetBlockID(pX, pY, Core.ChunkSize.Z - 1);
            //        }
            //    }

            //    nearbyBlock = (BlockRegister.Instance.GetBlock(nearbyBlockID));
            //    if (nearbyBlockID == BlockRegister.AirBlockID
            //        || !(thisBlock.IsSolid && nearbyBlock.IsSolid)
            //        || (!nearbyBlock.IsSolid && nearbyBlockID != thisBlockID))
            //    {
            //        blockSides |= BlockSide.DecreaseZ;
            //        pVerticesCount += 6;
            //    }
            //}
            //#endregion

            return blockSides;
        }
        public void SetTexturesCoords(TextureCoord pSide)
        {
            this.sideTextureCoord = pSide;
        }
        public override int BuildVertices(Vector3 pPosition, BlockSide pBlockSides, ref VertexPositionTextureNoNormal[] pBlocksVertices, int pVertexIndex, BlockContext pContext)
        {
            //if (pBlockSides != BlockSide.None)
            //{
            //    if ((pBlockSides & BlockSide.DecreaseX) != BlockSide.None)
            //    {
            //        pVertexIndex = BuildSideVertices(pPosition, BlockSide.DecreaseX, ref pBlocksVertices, pVertexIndex);
            //    }
            //    if ((pBlockSides & BlockSide.IncreaseX) != BlockSide.None)
            //    {
            //        pVertexIndex = BuildSideVertices(pPosition, BlockSide.IncreaseX, ref pBlocksVertices, pVertexIndex);
            //    }
            //    if ((pBlockSides & BlockSide.IncreaseY) != BlockSide.None)
            //    {
            //        pVertexIndex = BuildSideVertices(pPosition, BlockSide.IncreaseY, ref pBlocksVertices, pVertexIndex);
            //    }
            //    if ((pBlockSides & BlockSide.DecreaseY) != BlockSide.None)
            //    {
            //        pVertexIndex = BuildSideVertices(pPosition, BlockSide.DecreaseY, ref pBlocksVertices, pVertexIndex);
            //    }
            //    if ((pBlockSides & BlockSide.IncreaseZ) != BlockSide.None)
            //    {
            //        pVertexIndex = BuildSideVertices(pPosition, BlockSide.IncreaseZ, ref pBlocksVertices, pVertexIndex);
            //    }
            //    if ((pBlockSides & BlockSide.DecreaseZ) != BlockSide.None)
            //    {
            //        pVertexIndex = BuildSideVertices(pPosition, BlockSide.DecreaseZ, ref pBlocksVertices, pVertexIndex);
            //    }
            //}

            return pVertexIndex;
        }
    }
}