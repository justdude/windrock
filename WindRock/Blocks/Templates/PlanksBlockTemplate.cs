﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpoonCraft.Common;
using Microsoft.Xna.Framework;

namespace SpoonCraft.Blocks
{
    public class PlanksBlockTemplate : SolidBlockTemplate
    {
        /* Fields */



        /* Properties */



        /* Constructors */

        public PlanksBlockTemplate(TextureCoord pXTextureCoord, TextureCoord pYTextureCoord, TextureCoord pZTextureCoord)
            : base(Opacity.Opaque, pXTextureCoord, pXTextureCoord, pYTextureCoord, pYTextureCoord, pZTextureCoord, pZTextureCoord)
        {
        }

        /* Private methods */



        /* Protected methods */

        protected override int BuildSideVertices(Vector3 pPosition, BlockSide pBlockSide, ref VertexPositionTexture[] pBlocksVerteces, int pVertexIndex)
        {

            TextureCoord textureCoord = GetTextureCoord(pBlockSide);
            Vector2 textureCoord_0_0 = new Vector2(
                textureCoord.Col * Core.BlockTextureProportionalSize.X,
                textureCoord.Row * Core.BlockTextureProportionalSize.Y);
            Vector2 textureCoord_1_0 = new Vector2(
                (textureCoord.Col + 1) * Core.BlockTextureProportionalSize.X,
                textureCoord.Row * Core.BlockTextureProportionalSize.Y);
            Vector2 textureCoord_0_1 = new Vector2(
                textureCoord.Col * Core.BlockTextureProportionalSize.X,
                (textureCoord.Row + 1) * Core.BlockTextureProportionalSize.Y);
            Vector2 textureCoord_1_1 = new Vector2(
                (textureCoord.Col + 1) * Core.BlockTextureProportionalSize.X,
                (textureCoord.Row + 1) * Core.BlockTextureProportionalSize.Y);

            switch (pBlockSide)
            {
                case BlockSide.IncreaseX:
                    {
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point1, textureCoord_1_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point5, textureCoord_0_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point7, textureCoord_0_0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point1, textureCoord_1_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point7, textureCoord_0_0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point3, textureCoord_1_0);
                    }
                    break;
                case BlockSide.DecreaseX:
                    {
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point0, textureCoord_0_0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point2, textureCoord_0_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point6, textureCoord_1_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point0, textureCoord_0_0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point6, textureCoord_1_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point4, textureCoord_1_0);
                    }
                    break;
                case BlockSide.IncreaseY:
                    {
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point2, textureCoord_0_0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point3, textureCoord_1_0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point7, textureCoord_1_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point2, textureCoord_0_0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point7, textureCoord_1_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point6, textureCoord_0_1);
                    }
                    break;
                case BlockSide.DecreaseY:
                    {
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point0, textureCoord_0_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point4, textureCoord_0_0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point5, textureCoord_1_0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point0, textureCoord_0_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point5, textureCoord_1_0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point1, textureCoord_1_1);
                    }
                    break;
                case BlockSide.IncreaseZ:
                    {
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point4, textureCoord_0_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point6, textureCoord_0_0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point7, textureCoord_1_0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point4, textureCoord_0_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point7, textureCoord_1_0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point5, textureCoord_1_1);
                    }
                    break;
                case BlockSide.DecreaseZ:
                    {
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point0, textureCoord_1_0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point1, textureCoord_0_0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point3, textureCoord_0_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point0, textureCoord_1_0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point3, textureCoord_0_1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point2, textureCoord_1_1);
                    }
                    break;
            }

            return pVertexIndex;
        }

        /* Public methods */



    }
}