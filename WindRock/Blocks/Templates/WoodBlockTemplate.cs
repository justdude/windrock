﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using SpoonCraft.Common;

namespace SpoonCraft.Blocks
{
    public class WoodBlockTemplate : SolidBlockTemplate
    {
        /* Fields */

        private VertexPositionTexture[] verticesTemplate;

        /* Properties */



        /* Constructors */

        public WoodBlockTemplate(string pName, Opacity pOpacity, TextureCoord pSideTextureCoord, TextureCoord pTopBottomTextureCoord)
            : base(pName, pOpacity, pSideTextureCoord, pTopBottomTextureCoord)
        {
            this.InitializeVerticesTemplate();
        }

        /* Private methods */

        private int BuildSolidSideVerticesForDockAtDecreaseY(Vector3 pPosition, Direction pBlockSide, ref VertexPositionTexture[] pBlocksVerteces, int pVertexIndex)
        {
            try
            {
                TextureCoord textureCoords = GetTextureCoord(pBlockSide);
                Vector2 texture_u0_v0 = new Vector2(
                    textureCoords.Col * Core.BlockTextureProportionalSize.X,
                    textureCoords.Row * Core.BlockTextureProportionalSize.Y);
                Vector2 texture_u1_v0 = new Vector2(
                    (textureCoords.Col + 1) * Core.BlockTextureProportionalSize.X,
                    textureCoords.Row * Core.BlockTextureProportionalSize.Y);
                Vector2 texture_u0_v1 = new Vector2(
                    textureCoords.Col * Core.BlockTextureProportionalSize.X,
                    (textureCoords.Row + 1) * Core.BlockTextureProportionalSize.Y);
                Vector2 texture_u1_v1 = new Vector2(
                    (textureCoords.Col + 1) * Core.BlockTextureProportionalSize.X,
                    (textureCoords.Row + 1) * Core.BlockTextureProportionalSize.Y);

                switch (pBlockSide)
                {
                    case Direction.IncreaseX:
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X + 1, pPosition.Y + 1, pPosition.Z + 1), texture_u0_v0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X + 1, pPosition.Y + 1, pPosition.Z), texture_u1_v0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X + 1, pPosition.Y, pPosition.Z), texture_u1_v1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X + 1, pPosition.Y + 1, pPosition.Z + 1), texture_u0_v0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X + 1, pPosition.Y, pPosition.Z), texture_u1_v1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X + 1, pPosition.Y, pPosition.Z + 1), texture_u0_v1);
                        break;
                    case Direction.DecreaseX:
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X, pPosition.Y + 1, pPosition.Z), texture_u0_v0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X, pPosition.Y + 1, pPosition.Z + 1), texture_u1_v0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X, pPosition.Y, pPosition.Z + 1), texture_u1_v1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X, pPosition.Y + 1, pPosition.Z), texture_u0_v0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X, pPosition.Y, pPosition.Z + 1), texture_u1_v1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X, pPosition.Y, pPosition.Z), texture_u0_v1);
                        break;
                    case Direction.IncreaseY:
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X + 1, pPosition.Y + 1, pPosition.Z + 1), texture_u0_v1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X, pPosition.Y + 1, pPosition.Z + 1), texture_u0_v0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X, pPosition.Y + 1, pPosition.Z), texture_u1_v0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X + 1, pPosition.Y + 1, pPosition.Z + 1), texture_u0_v1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X, pPosition.Y + 1, pPosition.Z), texture_u1_v0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X + 1, pPosition.Y + 1, pPosition.Z), texture_u1_v1);
                        break;
                    case Direction.DecreaseY:
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X, pPosition.Y, pPosition.Z + 1), texture_u0_v0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X + 1, pPosition.Y, pPosition.Z + 1), texture_u1_v0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X + 1, pPosition.Y, pPosition.Z), texture_u1_v1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X, pPosition.Y, pPosition.Z + 1), texture_u0_v0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X + 1, pPosition.Y, pPosition.Z), texture_u1_v1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X, pPosition.Y, pPosition.Z), texture_u0_v1);
                        break;
                    case Direction.IncreaseZ:
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X, pPosition.Y + 1, pPosition.Z + 1), texture_u0_v0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X + 1, pPosition.Y + 1, pPosition.Z + 1), texture_u1_v0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X + 1, pPosition.Y, pPosition.Z + 1), texture_u1_v1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X, pPosition.Y + 1, pPosition.Z + 1), texture_u0_v0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X + 1, pPosition.Y, pPosition.Z + 1), texture_u1_v1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X, pPosition.Y, pPosition.Z + 1), texture_u0_v1);
                        break;
                    case Direction.DecreaseZ:
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X + 1, pPosition.Y + 1, pPosition.Z), texture_u0_v0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X, pPosition.Y + 1, pPosition.Z), texture_u1_v0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X, pPosition.Y, pPosition.Z), texture_u1_v1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X + 1, pPosition.Y + 1, pPosition.Z), texture_u0_v0);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X, pPosition.Y, pPosition.Z), texture_u1_v1);
                        pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(new Vector3(pPosition.X + 1, pPosition.Y, pPosition.Z), texture_u0_v1);
                        break;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }

            return pVertexIndex;
        }

        /* Protected methods */

        protected void InitializeVerticesTemplate()
        {
            this.verticesTemplate = new VertexPositionTexture[36];
            Vector3 position = Vector3.Zero;//new Vector3(-0.5f, -0.5f, -0.5f);
            int vertexIndex = 0;
            vertexIndex = BuildSolidSideVerticesForDockAtDecreaseY(position, Direction.DecreaseX, ref verticesTemplate, vertexIndex);
            vertexIndex = BuildSolidSideVerticesForDockAtDecreaseY(position, Direction.IncreaseX, ref verticesTemplate, vertexIndex);
            vertexIndex = BuildSolidSideVerticesForDockAtDecreaseY(position, Direction.IncreaseY, ref verticesTemplate, vertexIndex);
            vertexIndex = BuildSolidSideVerticesForDockAtDecreaseY(position, Direction.DecreaseY, ref verticesTemplate, vertexIndex);
            vertexIndex = BuildSolidSideVerticesForDockAtDecreaseY(position, Direction.IncreaseZ, ref verticesTemplate, vertexIndex);
            vertexIndex = BuildSolidSideVerticesForDockAtDecreaseY(position, Direction.DecreaseZ, ref verticesTemplate, vertexIndex);
        }

        /* Public methods */



    }
}