﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using GameHelper.Data;
using WindRock.Blocks;
using WindRock.Common;
using WindRock.Registers;
using WindRock.Metadatas;
using ZImportData.Objects;
using ZImportData.Objects.Data;
using GameHelper;
using WindRock.Blocks.Drawing;
using Microsoft.Xna.Framework.Graphics;

namespace WindRock.Blocks
{
    /// <summary>
    /// Цельный непрозрачный блок.
    /// </summary>
    public class PiramideBlockTemplate : BlockTemplate
    {
        /* Constants */



        /* Fields */

        protected TextureCoord increaseXTextureCoord;
        protected TextureCoord decreaseXTextureCoord;
        protected TextureCoord increaseYTextureCoord;
        protected TextureCoord decreaseYTextureCoord;
        protected TextureCoord increaseZTextureCoord;
        protected TextureCoord decreaseZTextureCoord;

        protected static ZModelData modelData;
        protected static int bodyMeshIndex;
        protected static int bottomMeshIndex;

        /* Properties */



        /* Constructors */

        public PiramideBlockTemplate(string pName, bool pIsOpaque, TextureCoord pIncreaseXTextureCoord, TextureCoord pDecreaseXTextureCoord, TextureCoord pIncreaseYTextureCoord, TextureCoord pDecreaseYTextureCoord, TextureCoord pIncreaseZTextureCoord, TextureCoord pDecreaseZTextureCoord)
            : base(pName, pIsOpaque, true, null)
        {
            this.increaseXTextureCoord = pIncreaseXTextureCoord;
            this.decreaseXTextureCoord = pDecreaseXTextureCoord;
            this.increaseYTextureCoord = pIncreaseYTextureCoord;
            this.decreaseYTextureCoord = pDecreaseYTextureCoord;
            this.increaseZTextureCoord = pIncreaseZTextureCoord;
            this.decreaseZTextureCoord = pDecreaseZTextureCoord;


        }
        public PiramideBlockTemplate(string pName, bool pIsOpaque, TextureCoord pSideTextureCoord)
            : this(pName, pIsOpaque, pSideTextureCoord, pSideTextureCoord, pSideTextureCoord, pSideTextureCoord, pSideTextureCoord, pSideTextureCoord)
        {
        }
        public PiramideBlockTemplate(string pName, bool pIsOpaque, TextureCoord pSideTextureCoord, TextureCoord pTopBottomTextureCoord)
            : this(pName, pIsOpaque, pSideTextureCoord, pSideTextureCoord, pTopBottomTextureCoord, pTopBottomTextureCoord, pSideTextureCoord, pSideTextureCoord)
        {
        }
        public PiramideBlockTemplate(string pName, bool pIsOpaque, TextureCoord pSideTextureCoord, TextureCoord pTopTextureCoord, TextureCoord pBottomTextureCoord)
            : this(pName, pIsOpaque, pSideTextureCoord, pSideTextureCoord, pTopTextureCoord, pBottomTextureCoord, pSideTextureCoord, pSideTextureCoord)
        {
        }

        public PiramideBlockTemplate(string pName, bool pIsOpaque, Texture2D pTexture)
            : base(pName, pIsOpaque, true, pTexture)
        {
        }

        /* Private methods */



        /* Protected methods */

        public override void LoadContent(Game pGame)
        {
            if (PiramideBlockTemplate.modelData == null)
            {
                modelData = pGame.Content.Load<ZModelData>(Core.GetBlockModelPath("piramide"));
                bodyMeshIndex = modelData.MeshIndices["body"];
                bottomMeshIndex = modelData.MeshIndices["bottom"];
            }
        }

        // // //

        protected virtual int BuildSideVertices(Vector3 pPosition, Direction pBlockSide, ref VertexPositionTexture[] pBlocksVerteces, int pVertexIndex)
        {
            TextureCoord textureCoord = GetTextureCoord(pBlockSide);
            Vector2 textureCoordOrigin = new Vector2(textureCoord.Col, textureCoord.Row);

            int meshIndex = -1;
            switch (pBlockSide)
            {
                case Direction.Bottom:
                    meshIndex = bottomMeshIndex;
                    break;
            }

            ZModelMeshData meshData = modelData.Meshes[meshIndex];
            ZModelMeshGeometryData geometry = meshData.Geometries[0];
            ZChannel textureCoordChannel = geometry.GetChannel("TextureCoordinate0");
            Vector2 vertexTextureCoord = Vector2.Zero;
            for (int vertexIndexIndex = 0; vertexIndexIndex < geometry.VertexIndicesCount; vertexIndexIndex++)
            //for (int vertexIndexIndex = geometry.IndexCount - 1; vertexIndexIndex >= 0; vertexIndexIndex--)
            {
                int vertexIndex = geometry.VertexIndices[vertexIndexIndex];
                //Vector2 textureCoordValue = (Vector2) textureCoordChannel.Values[vertexIndex];
                vertexTextureCoord = (textureCoordOrigin + (Vector2) textureCoordChannel.Values[vertexIndex]) * Core.BlockTextureProportionalSize;
                //vertexTextureCoord = new Vector2(textureCoordOrigin.X + textureCoordValue.Y, textureCoordOrigin.Y + (1 - textureCoordValue.X)) * Core.BlockTextureProportionalSize;
                pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(
                    pPosition + meshData.Positions[vertexIndex],
                    vertexTextureCoord);
            }

            return pVertexIndex;
        }
        protected virtual TextureCoord GetTextureCoord(Direction pBlockSide)
        {
            TextureCoord textureCoord = TextureCoord.Zero;

            switch (pBlockSide)
            {
                case Direction.IncreaseX:
                    {
                        textureCoord = this.increaseXTextureCoord;
                    }
                    break;
                case Direction.DecreaseX:
                    {
                        textureCoord = this.decreaseXTextureCoord;
                    }
                    break;
                case Direction.IncreaseY:
                    {
                        textureCoord = this.increaseYTextureCoord;
                    }
                    break;
                case Direction.DecreaseY:
                    {
                        textureCoord = this.decreaseYTextureCoord;
                    }
                    break;
                case Direction.IncreaseZ:
                    {
                        textureCoord = this.increaseZTextureCoord;
                    }
                    break;
                case Direction.DecreaseZ:
                    {
                        textureCoord = this.decreaseZTextureCoord;
                    }
                    break;
            }

            return textureCoord;
        }

        protected void BuildSideVertices2(Vector3 pPosition, int pBlockID, Direction pBlockSide, ref BlockRenderer pBlockRenderManager)
        {
            int meshIndex = -1;
            switch (pBlockSide)
            {
                case Direction.Bottom:
                    meshIndex = bottomMeshIndex;
                    break;
            }

            ZModelMeshData meshData = modelData.Meshes[meshIndex];
            ZModelMeshGeometryData geometry = meshData.Geometries[0];
            ZChannel textureCoordChannel = geometry.GetChannel("TextureCoordinate0");
            Vector2 vertexTextureCoord = Vector2.Zero;
            for (int vertexIndexIndex = 0; vertexIndexIndex < geometry.VertexIndicesCount; vertexIndexIndex++)
            {
                int vertexIndex = geometry.VertexIndices[vertexIndexIndex];
                vertexTextureCoord = (Vector2) textureCoordChannel.Values[vertexIndex];
                //vertexTextureCoord.Y = -vertexTextureCoord.Y;
                BlockRenderGroup group = pBlockRenderManager.GetOrCreateGroup(pBlockID, this.texture, null);
                group.AddVertex(pPosition + meshData.Positions[vertexIndex], vertexTextureCoord);
            }
        }

        /* Public methods */

        public override bool IsSideSolid(Direction pSide, BlockMetadata pMetadata)
        {
            return true;
        }
        public override void BuildVertices(Vector3 pPosition, BlockMetadata pMetadata, ref VertexPositionTexture[] pOpaqueBlocksVertices, ref int pOpaqueVertexIndex, ref VertexPositionTexture[] pTransparentBlocksVertices, ref int pTransparentVertexIndex)
        {
            if (pMetadata.VisibleSides != Direction.None)
            {
                VertexPositionTexture[] blocksVertices = isOpaque ? pOpaqueBlocksVertices : pTransparentBlocksVertices;
                int vertexIndex = isOpaque ? pOpaqueVertexIndex : pTransparentVertexIndex;

                if ((pMetadata.VisibleSides.HasFlag(Direction.Top)))
                {
                    vertexIndex = BuildSideVertices(pPosition, Direction.Top, ref blocksVertices, vertexIndex);
                }
                if ((pMetadata.VisibleSides.HasFlag(Direction.Bottom)))
                {
                    vertexIndex = BuildSideVertices(pPosition, Direction.Bottom, ref blocksVertices, vertexIndex);
                }

                if (isOpaque)
                {
                    pOpaqueVertexIndex = vertexIndex;
                }
                else
                {
                    pTransparentVertexIndex = vertexIndex;
                }
            }
        }

        public override void BuildVertices2(Vector3 pPosition, short pBlockID, BlockMetadata pMetadata, ref BlockRenderer pBlockRenderManager)
        {
            if (pMetadata.VisibleSides != Direction.None)
            {
                if (pMetadata.VisibleSides.HasFlag(Direction.Top)
                    || pMetadata.VisibleSides.HasFlag(Direction.None)
                    || pMetadata.VisibleSides.HasFlag(Direction.South)
                    || pMetadata.VisibleSides.HasFlag(Direction.West)
                    || pMetadata.VisibleSides.HasFlag(Direction.East))
                {
                    ZModelMeshData meshData = modelData.Meshes[bodyMeshIndex];
                    ZModelMeshGeometryData geometry = meshData.Geometries[0];
                    ZChannel textureCoordChannel = geometry.GetChannel("TextureCoordinate0");
                    Vector2 vertexTextureCoord = Vector2.Zero;
                    for (int vertexIndexIndex = 0; vertexIndexIndex < geometry.VertexIndicesCount; vertexIndexIndex++)
                    {
                        int vertexIndex = geometry.VertexIndices[vertexIndexIndex];
                        vertexTextureCoord = (Vector2) textureCoordChannel.Values[vertexIndex];
                        //vertexTextureCoord.Y = -vertexTextureCoord.Y;
                        BlockRenderGroup group = pBlockRenderManager.GetOrCreateGroup(pBlockID, this.texture, null);
                        group.AddVertex(pPosition + meshData.Positions[vertexIndex], vertexTextureCoord);
                    }
                }

                if (pMetadata.VisibleSides.HasFlag(Direction.Bottom))
                {
                    BuildSideVertices2(pPosition, pBlockID, Direction.Bottom, ref pBlockRenderManager);
                }
            }
        }

        public override int GetVerticesCount(BlockMetadata pMetadata, bool pIsOpaque)
        {
            int verticesCount = 0;

            if (this.isOpaque
                && pMetadata.VisibleSides != Direction.None)
            {
                if ((pMetadata.VisibleSides.HasFlag(Direction.Bottom)))
                {
                    verticesCount += modelData.Meshes[bottomMeshIndex].Geometries[0].VertexIndicesCount;
                }
            }

            return verticesCount;
        }

        public override BlockMetadata CreateDefaultMetadata()
        {
            return new BlockMetadata();
        }

    }
}