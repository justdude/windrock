﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using GameHelper.Data;
using SpoonCraft.Blocks;
using SpoonCraft.Common;
using SpoonCraft.Registers;

namespace SpoonCraft.Blocks
{
    public class SlabBlockTemplate : SolidBlockTemplate
    {
        /* Constants */

        protected static Vector3 Point8 = new Vector3(0, 0.5f, 0);
        protected static Vector3 Point9 = new Vector3(1, 0.5f, 0);
        protected static Vector3 Point10 = new Vector3(0, 0.5f, 1);
        protected static Vector3 Point11 = new Vector3(1, 0.5f, 1);

        /* Fields */



        /* Properties */



        /* Constructors */

        public SlabBlockTemplate(string pName, Opacity pOpacity, TextureCoord pSideTextureCoord)
            : base(pName, pOpacity, pSideTextureCoord)
        {
        }

        /* Private methods */



        /* Protected methods */

        protected int BuildSideVerticesForTopAndBottomLayout(Vector3 pPosition, Direction pBlockSide, ref VertexPositionTexture[] pBlocksVerteces, int pVertexIndex)
        {
            try
            {
                TextureCoord textureCoords = GetTextureCoord(pBlockSide);
                Vector2 textureCoord00 = new Vector2(
                    textureCoords.Col * Core.BlockTextureProportionalSize.X,
                    textureCoords.Row * Core.BlockTextureProportionalSize.Y);
                Vector2 textureCoord10 = new Vector2(
                    (textureCoords.Col + 1) * Core.BlockTextureProportionalSize.X,
                    textureCoords.Row * Core.BlockTextureProportionalSize.Y);
                Vector2 textureCoord01 = new Vector2(
                    textureCoords.Col * Core.BlockTextureProportionalSize.X,
                    (textureCoords.Row + 1) * Core.BlockTextureProportionalSize.Y);
                Vector2 textureCoord11 = new Vector2(
                    (textureCoords.Col + 1) * Core.BlockTextureProportionalSize.X,
                    (textureCoords.Row + 1) * Core.BlockTextureProportionalSize.Y);

                switch (pBlockSide)
                {
                    case Direction.IncreaseX:
                        {
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point1, textureCoord11);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point5, textureCoord01);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point7, textureCoord00);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point1, textureCoord11);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point7, textureCoord00);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point3, textureCoord10);
                        }
                        break;
                    case Direction.DecreaseX:
                        {
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point0, textureCoord01);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point2, textureCoord00);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point6, textureCoord10);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point0, textureCoord01);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point6, textureCoord10);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point4, textureCoord11);
                        }
                        break;
                    case Direction.IncreaseY:
                        {
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point2, textureCoord00);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point3, textureCoord10);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point7, textureCoord11);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point2, textureCoord00);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point7, textureCoord11);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point6, textureCoord01);
                        }
                        break;
                    case Direction.DecreaseY:
                        {
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point0, textureCoord00);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point4, textureCoord01);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point5, textureCoord11);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point0, textureCoord00);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point5, textureCoord11);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point1, textureCoord10);
                        }
                        break;
                    case Direction.IncreaseZ:
                        {
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point4, textureCoord01);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point6, textureCoord00);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point7, textureCoord10);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point4, textureCoord01);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point7, textureCoord10);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point5, textureCoord11);
                        }
                        break;
                    case Direction.DecreaseZ:
                        {
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point0, textureCoord11);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point1, textureCoord01);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point3, textureCoord00);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point0, textureCoord11);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point3, textureCoord00);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point2, textureCoord10);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }

            return pVertexIndex;
        }
        protected int BuildSideVerticesForTopLayout(Vector3 pPosition, Direction pBlockSide, ref VertexPositionTexture[] pBlocksVerteces, int pVertexIndex)
        {
            try
            {
                TextureCoord textureCoords = GetTextureCoord(pBlockSide);
                Vector2 textureCoord_0_0 = new Vector2(
                    textureCoords.Col * Core.BlockTextureProportionalSize.X,
                    textureCoords.Row * Core.BlockTextureProportionalSize.Y);
                Vector2 textureCoord_1_0 = new Vector2(
                    (textureCoords.Col + 1) * Core.BlockTextureProportionalSize.X,
                    textureCoords.Row * Core.BlockTextureProportionalSize.Y);
                Vector2 textureCoord_0_1 = new Vector2(
                    textureCoords.Col * Core.BlockTextureProportionalSize.X,
                    (textureCoords.Row + 1) * Core.BlockTextureProportionalSize.Y);
                Vector2 textureCoord_1_1 = new Vector2(
                    (textureCoords.Col + 1) * Core.BlockTextureProportionalSize.X,
                    (textureCoords.Row + 1) * Core.BlockTextureProportionalSize.Y);
                Vector2 textureCoord_0_05 = new Vector2(
                    textureCoords.Col * Core.BlockTextureProportionalSize.X,
                    (textureCoords.Row + 0.5f) * Core.BlockTextureProportionalSize.Y);
                Vector2 textureCoord_1_05 = new Vector2(
                    (textureCoords.Col + 1) * Core.BlockTextureProportionalSize.X,
                    (textureCoords.Row + 0.5f) * Core.BlockTextureProportionalSize.Y);

                switch (pBlockSide)
                {
                    case Direction.IncreaseX:
                        {
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point3, textureCoord_1_0);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point9, textureCoord_1_05);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point11, textureCoord_0_05);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point3, textureCoord_1_05);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point11, textureCoord_0_05);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point7, textureCoord_0_0);
                        }
                        break;
                    case Direction.DecreaseX:
                        {
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point2, textureCoord_0_0);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point6, textureCoord_1_0);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point10, textureCoord_1_05);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point2, textureCoord_0_0);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point10, textureCoord_1_05);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point8, textureCoord_0_05);
                        }
                        break;
                    case Direction.IncreaseY:
                        {
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point2, textureCoord_0_0);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point3, textureCoord_1_0);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point7, textureCoord_1_1);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point2, textureCoord_0_0);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point7, textureCoord_1_1);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point6, textureCoord_0_1);
                        }
                        break;
                    case Direction.DecreaseY:
                        {
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point8, textureCoord_0_0);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point10, textureCoord_1_0);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point11, textureCoord_1_1);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point8, textureCoord_0_0);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point11, textureCoord_1_1);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point9, textureCoord_0_1);
                        }
                        break;
                    case Direction.IncreaseZ:
                        {
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point6, textureCoord_0_0);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point7, textureCoord_1_0);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point11, textureCoord_1_05);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point6, textureCoord_0_0);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point11, textureCoord_1_05);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point10, textureCoord_0_05);
                        }
                        break;
                    case Direction.DecreaseZ:
                        {
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point2, textureCoord_1_0);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point8, textureCoord_1_05);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point9, textureCoord_0_05);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point2, textureCoord_1_0);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point9, textureCoord_0_05);
                            pBlocksVerteces[++pVertexIndex] = new VertexPositionTexture(pPosition + Point3, textureCoord_0_0);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }

            return pVertexIndex;
        }

        /* Public methods */

        public override bool IsSideSolid(Direction pSide, BlockData pBlockData)
        {
            SlabBlockData slabBlockData = pBlockData as SlabBlockData;
            bool isSideSolid = false;

            if (slabBlockData.Layout.HasFlag(SlabLayout.Top | SlabLayout.Bottom))
            {
                isSideSolid = true;
            }
            else if (slabBlockData.Layout.HasFlag(SlabLayout.Top))
            {
                isSideSolid = pSide == Direction.IncreaseY;
            }
            else //if (slabBlockData.Layout.HasFlag(SlabLayout.Bottom))
            {
                isSideSolid = pSide == Direction.DecreaseY;
            }

            return isSideSolid;
        }
        public override void BuildVertices(Vector3 pPosition, BlockData pBlockData, ref VertexPositionTexture[] pOpaqueBlocksVertices, ref int pOpaqueVertexIndex, ref VertexPositionTexture[] pNotOpaqueBlocksVertices, ref int pNotOpaqueVertexIndex)
        {
            if (pBlockData.VisibleSides != Direction.None)
            {
                bool isOpaque = this.Opacity == Opacity.Opaque;
                VertexPositionTexture[] blocksVertices = isOpaque ? pOpaqueBlocksVertices : pNotOpaqueBlocksVertices;
                int vertexIndex = isOpaque ? pOpaqueVertexIndex : pNotOpaqueVertexIndex;
                SlabBlockData slabBlockData = pBlockData as SlabBlockData;

                if (slabBlockData.Layout.HasFlag(SlabLayout.Top | SlabLayout.Bottom))
                {
                    #region
                    if ((pBlockData.VisibleSides.HasFlag(Direction.IncreaseX)))
                    {
                        vertexIndex = base.BuildSideVertices(pPosition, Direction.IncreaseX, ref blocksVertices, vertexIndex);
                    }
                    if ((pBlockData.VisibleSides.HasFlag(Direction.DecreaseX)))
                    {
                        vertexIndex = base.BuildSideVertices(pPosition, Direction.DecreaseX, ref blocksVertices, vertexIndex);
                    }

                    if ((pBlockData.VisibleSides.HasFlag(Direction.IncreaseY)))
                    {
                        vertexIndex = base.BuildSideVertices(pPosition, Direction.IncreaseY, ref blocksVertices, vertexIndex);
                    }
                    if ((pBlockData.VisibleSides.HasFlag(Direction.DecreaseY)))
                    {
                        vertexIndex = base.BuildSideVertices(pPosition, Direction.DecreaseY, ref blocksVertices, vertexIndex);
                    }

                    if ((pBlockData.VisibleSides.HasFlag(Direction.IncreaseZ)))
                    {
                        vertexIndex = base.BuildSideVertices(pPosition, Direction.IncreaseZ, ref blocksVertices, vertexIndex);
                    }
                    if ((pBlockData.VisibleSides.HasFlag(Direction.DecreaseZ)))
                    {
                        vertexIndex = base.BuildSideVertices(pPosition, Direction.DecreaseZ, ref blocksVertices, vertexIndex);
                    }
                    #endregion
                }
                else if (slabBlockData.Layout.HasFlag(SlabLayout.Top))
                {
                    #region
                    if (pBlockData.VisibleSides.HasFlag(Direction.IncreaseX))
                    {
                        vertexIndex = this.BuildSideVerticesForTopLayout(pPosition, Direction.IncreaseX, ref blocksVertices, vertexIndex);
                    }
                    if (pBlockData.VisibleSides.HasFlag(Direction.DecreaseX))
                    {
                        vertexIndex = this.BuildSideVerticesForTopLayout(pPosition, Direction.DecreaseX, ref blocksVertices, vertexIndex);
                    }

                    if (pBlockData.VisibleSides.HasFlag(Direction.IncreaseY))
                    {
                        vertexIndex = this.BuildSideVerticesForTopLayout(pPosition, Direction.IncreaseY, ref blocksVertices, vertexIndex);
                    }
                    // Если хоть одна из сторон X+, X-, Y-, Z+ или Z- видимая, то отображать нижнюю грань.
                    if (pBlockData.VisibleSides.HasFlag(Direction.IncreaseX)
                        || pBlockData.VisibleSides.HasFlag(Direction.DecreaseX)
                        || pBlockData.VisibleSides.HasFlag(Direction.DecreaseY)
                        || pBlockData.VisibleSides.HasFlag(Direction.IncreaseZ)
                        || pBlockData.VisibleSides.HasFlag(Direction.DecreaseZ))
                    {
                        vertexIndex = this.BuildSideVerticesForTopLayout(pPosition, Direction.DecreaseY, ref blocksVertices, vertexIndex);
                    }

                    if (pBlockData.VisibleSides.HasFlag(Direction.IncreaseZ))
                    {
                        vertexIndex = this.BuildSideVerticesForTopLayout(pPosition, Direction.IncreaseZ, ref blocksVertices, vertexIndex);
                    }
                    if (pBlockData.VisibleSides.HasFlag(Direction.DecreaseZ))
                    {
                        vertexIndex = this.BuildSideVerticesForTopLayout(pPosition, Direction.DecreaseZ, ref blocksVertices, vertexIndex);
                    }

                    #endregion
                }
                else //if (slabBlockData.Layout.HasFlag(SlabLayout.Bottom))
                {
                    //#region
                    //if (pBlockData.VisibleSides.HasFlag(BlockSide.IncreaseX))
                    //{
                    //    vertexIndex = this.BuildSideVerticesForTopLayout(pPosition, BlockSide.IncreaseX, ref blocksVertices, vertexIndex);
                    //}
                    //if (pBlockData.VisibleSides.HasFlag(BlockSide.DecreaseX))
                    //{
                    //    vertexIndex = this.BuildSideVerticesForTopLayout(pPosition, BlockSide.DecreaseX, ref blocksVertices, vertexIndex);
                    //}

                    //// Если хоть одна из сторон X+, X-, Y+, Z+ или Z- НЕ видимая, то отображать верхнюю грань.
                    //// То же самое можно записать так:
                    ////    если все из перечисленых сторон видимые, то НЕ то отображать верхнюю грань;
                    ////    иначе отображать.
                    //if (pBlockData.VisibleSides.HasFlag(BlockSide.IncreaseY))
                    //{
                    //    vertexIndex = this.BuildSideVerticesForTopLayout(pPosition, BlockSide.IncreaseY, ref blocksVertices, vertexIndex);
                    //}
                    //if (!pBlockData.VisibleSides.HasFlag(BlockSide.IncreaseX | BlockSide.DecreaseX | BlockSide.IncreaseY | BlockSide.IncreaseZ | BlockSide.DecreaseZ))
                    //{
                    //    vertexIndex = this.BuildSideVerticesForTopLayout(pPosition, BlockSide.DecreaseY, ref blocksVertices, vertexIndex);
                    //}

                    //if (pBlockData.VisibleSides.HasFlag(BlockSide.IncreaseZ))
                    //{
                    //    vertexIndex = this.BuildSideVerticesForTopLayout(pPosition, BlockSide.IncreaseZ, ref blocksVertices, vertexIndex);
                    //}
                    //if (pBlockData.VisibleSides.HasFlag(BlockSide.DecreaseZ))
                    //{
                    //    vertexIndex = this.BuildSideVerticesForTopLayout(pPosition, BlockSide.DecreaseZ, ref blocksVertices, vertexIndex);
                    //}

                    //#endregion
                }

                if (isOpaque)
                {
                    pOpaqueVertexIndex = vertexIndex;
                }
                else
                {
                    pNotOpaqueVertexIndex = vertexIndex;
                }
            }
        }
        public override int GetVerticesCount(BlockData pBlockData, Opacity pOpacity)
        {
            int verticesCount = 0;

            if (this.Opacity == pOpacity
                && pBlockData.VisibleSides != Direction.None)
            {
                SlabBlockData slabBlockData = pBlockData as SlabBlockData;

                if (slabBlockData.Layout.HasFlag(SlabLayout.Top | SlabLayout.Bottom))
                {
                    #region
                    if ((pBlockData.VisibleSides.HasFlag(Direction.IncreaseX)))
                    {
                        verticesCount += 6;
                    }
                    if ((pBlockData.VisibleSides.HasFlag(Direction.DecreaseX)))
                    {
                        verticesCount += 6;
                    }

                    if ((pBlockData.VisibleSides.HasFlag(Direction.IncreaseY)))
                    {
                        verticesCount += 6;
                    }
                    if ((pBlockData.VisibleSides.HasFlag(Direction.DecreaseY)))
                    {
                        verticesCount += 6;
                    }

                    if ((pBlockData.VisibleSides.HasFlag(Direction.IncreaseZ)))
                    {
                        verticesCount += 6;
                    }
                    if ((pBlockData.VisibleSides.HasFlag(Direction.DecreaseZ)))
                    {
                        verticesCount += 6;
                    }
                    #endregion
                }
                else if (slabBlockData.Layout.HasFlag(SlabLayout.Top))
                {
                    #region
                    if (pBlockData.VisibleSides.HasFlag(Direction.IncreaseX))
                    {
                        verticesCount += 6;
                    }
                    if (pBlockData.VisibleSides.HasFlag(Direction.DecreaseX))
                    {
                        verticesCount += 6;
                    }

                    if (pBlockData.VisibleSides.HasFlag(Direction.IncreaseY))
                    {
                        verticesCount += 6;
                    }
                    // Если хоть одна из сторон X+, X-, Y-, Z+ или Z- НЕ видимая, то отображать нижнюю грань.
                    // То же самое можно записать так:
                    //    если все из перечисленых сторон видимые, то НЕ то отображать нижнюю грань;
                    //    иначе отображать.
                    if (pBlockData.VisibleSides.HasFlag(Direction.IncreaseX)
                        || pBlockData.VisibleSides.HasFlag(Direction.DecreaseX)
                        || pBlockData.VisibleSides.HasFlag(Direction.DecreaseY)
                        || pBlockData.VisibleSides.HasFlag(Direction.IncreaseZ)
                        || pBlockData.VisibleSides.HasFlag(Direction.DecreaseZ))
                    {
                        verticesCount += 6;
                    }

                    if (pBlockData.VisibleSides.HasFlag(Direction.IncreaseZ))
                    {
                        verticesCount += 6;
                    }
                    if (pBlockData.VisibleSides.HasFlag(Direction.DecreaseZ))
                    {
                        verticesCount += 6;
                    }

                    #endregion
                }
                else //if (slabBlockData.Layout.HasFlag(SlabLayout.Bottom))
                {
                    //#region
                    //if (pBlockData.VisibleSides.HasFlag(BlockSide.IncreaseX))
                    //{
                    //    verticesCount += 6;
                    //}
                    //if (pBlockData.VisibleSides.HasFlag(BlockSide.DecreaseX))
                    //{
                    //    verticesCount += 6;
                    //}

                    //// Если хоть одна из сторон X+, X-, Y+, Z+ или Z- НЕ видимая, то отображать верхнюю грань.
                    //// То же самое можно записать так:
                    ////    если все из перечисленых сторон видимые, то НЕ то отображать верхнюю грань;
                    ////    иначе отображать.
                    //if (pBlockData.VisibleSides.HasFlag(BlockSide.IncreaseY))
                    //{
                    //    verticesCount += 6;
                    //}
                    //if (!pBlockData.VisibleSides.HasFlag(BlockSide.IncreaseX | BlockSide.DecreaseX | BlockSide.IncreaseY | BlockSide.IncreaseZ | BlockSide.DecreaseZ))
                    //{
                    //    verticesCount += 6;
                    //}

                    //if (pBlockData.VisibleSides.HasFlag(BlockSide.IncreaseZ))
                    //{
                    //    verticesCount += 6;
                    //}
                    //if (pBlockData.VisibleSides.HasFlag(BlockSide.DecreaseZ))
                    //{
                    //    verticesCount += 6;
                    //}

                    //#endregion
                }
            }

            return verticesCount;
        }

        public override BlockData CreateData(bool pIsNatural)
        {
            return new SlabBlockData(pIsNatural);
        }
        public override Direction GetBlockSides(int pX, int pY, int pZ, BlockContext pContext)
        {
            // Основные правла копируют правла цельных блоков.

            // Дополнительные правила для осей X и Z:
            // (действуют перед правилом 2.1)
            // 1. если соседний блок - плита, то:
            //    1.1. если значения layout соседней плиты включает значение текущей, то текущая сторона - НЕ видимая;
            //    1.2. иначе текущая сторона - видимая;
            // 2. иначе применить основные правила.

            Direction blockSides = Direction.None;

            short thisBlockID = pContext.Chunk.GetBlockID(pX, pY, pZ);
            short nearbyBlockID = BlockRegister.AirBlockID;

            BlockTemplate nearbyBlock = null;

            SlabBlockData thisSlabBlockData = pContext.Chunk.GetBlockData(new Vector3I(pX, pY, pZ)) as SlabBlockData;
            BlockData nearbyBlockData = null;

            #region Increase X
            {
                nearbyBlockID =
                    pX + 1 < Core.ChunkSizes.X ? pContext.Chunk.GetBlockID(pX + 1, pY, pZ) :
                    pContext.ChunkIncreaseX != null ? pContext.ChunkIncreaseX.GetBlockID(0, pY, pZ) :
                    BlockRegister.AirBlockID;

                if (nearbyBlockID == BlockRegister.AirBlockID)
                {
                    blockSides |= Direction.IncreaseX;
                }
                else
                {
                    nearbyBlock = (BlockRegister.Instance.Get(nearbyBlockID));
                    nearbyBlockData =
                        pX + 1 < Core.ChunkSizes.X ?
                        pContext.Chunk.GetBlockData(new Vector3I(pX + 1, pY, pZ)) :
                        pContext.ChunkIncreaseX.GetBlockData(new Vector3I(0, pY, pZ));

                    if (nearbyBlock is SlabBlockTemplate)
                    {
                        if (!(nearbyBlockData as SlabBlockData).Layout.HasFlag(thisSlabBlockData.Layout))
                        {
                            blockSides |= Direction.IncreaseX;
                        }
                    }
                    else if (!nearbyBlock.IsSideSolid(Direction.DecreaseX, nearbyBlockData)
                        || (nearbyBlock.Opacity != Opacity.Opaque && nearbyBlockID != thisBlockID))
                    {
                        blockSides |= Direction.IncreaseX;
                    }
                }
            }
            #endregion
            #region Decrease X
            {
                nearbyBlockID =
                    pX - 1 >= 0 ? pContext.Chunk.GetBlockID(pX - 1, pY, pZ) :
                    pContext.ChunkDecreaseX != null ? pContext.ChunkDecreaseX.GetBlockID(Core.ChunkSizes.X - 1, pY, pZ) :
                    BlockRegister.AirBlockID;

                if (nearbyBlockID == BlockRegister.AirBlockID)
                {
                    blockSides |= Direction.DecreaseX;
                }
                else
                {
                    nearbyBlock = (BlockRegister.Instance.Get(nearbyBlockID));
                    nearbyBlockData =
                        pX - 1 >= 0 ?
                        pContext.Chunk.GetBlockData(new Vector3I(pX - 1, pY, pZ)) :
                        pContext.ChunkDecreaseX.GetBlockData(new Vector3I(Core.ChunkSizes.X - 1, pY, pZ));

                    if (nearbyBlock is SlabBlockTemplate)
                    {
                        if (!(nearbyBlockData as SlabBlockData).Layout.HasFlag(thisSlabBlockData.Layout))
                        {
                            blockSides |= Direction.DecreaseX;
                        }
                    }
                    else if (!nearbyBlock.IsSideSolid(Direction.IncreaseX, nearbyBlockData)
                        || (nearbyBlock.Opacity != Opacity.Opaque && nearbyBlockID != thisBlockID))
                    {
                        blockSides |= Direction.DecreaseX;
                    }
                }
            }
            #endregion

            #region Increase Z
            {
                nearbyBlockID =
                    pZ + 1 < Core.ChunkSizes.Z ? pContext.Chunk.GetBlockID(pX, pY, pZ + 1) :
                    pContext.ChunkIncreaseZ != null ? pContext.ChunkIncreaseZ.GetBlockID(pX, pY, 0) :
                    BlockRegister.AirBlockID;

                if (nearbyBlockID == BlockRegister.AirBlockID)
                {
                    blockSides |= Direction.IncreaseZ;
                }
                else
                {
                    nearbyBlock = (BlockRegister.Instance.Get(nearbyBlockID));
                    nearbyBlockData =
                        pZ + 1 < Core.ChunkSizes.Z ?
                        pContext.Chunk.GetBlockData(new Vector3I(pX, pY, pZ + 1)) :
                        pContext.ChunkIncreaseZ.GetBlockData(new Vector3I(pX, pY, 0));

                    if (nearbyBlock is SlabBlockTemplate)
                    {
                        if (!(nearbyBlockData as SlabBlockData).Layout.HasFlag(thisSlabBlockData.Layout))
                        {
                            blockSides |= Direction.IncreaseZ;
                        }
                    }
                    else if (!nearbyBlock.IsSideSolid(Direction.DecreaseZ, nearbyBlockData)
                        || (nearbyBlock.Opacity != Opacity.Opaque && nearbyBlockID != thisBlockID))
                    {
                        blockSides |= Direction.IncreaseZ;
                    }
                }
            }
            #endregion
            #region Decrease Z
            {
                nearbyBlockID =
                    pZ - 1 >= 0 ? pContext.Chunk.GetBlockID(pX, pY, pZ - 1) :
                    pContext.ChunkDecreaseZ != null ? pContext.ChunkDecreaseZ.GetBlockID(pX, pY, Core.ChunkSizes.Z - 1) :
                    BlockRegister.AirBlockID;

                if (nearbyBlockID == BlockRegister.AirBlockID)
                {
                    blockSides |= Direction.DecreaseZ;
                }
                else
                {
                    nearbyBlock = (BlockRegister.Instance.Get(nearbyBlockID));
                    nearbyBlock = (BlockRegister.Instance.Get(nearbyBlockID));
                    nearbyBlockData =
                        pZ - 1 >= 0 ?
                        pContext.Chunk.GetBlockData(new Vector3I(pX, pY, pZ - 1)) :
                        pContext.ChunkDecreaseZ.GetBlockData(new Vector3I(pX, pY, Core.ChunkSizes.Z - 1));

                    if (nearbyBlock is SlabBlockTemplate)
                    {
                        if (!(nearbyBlockData as SlabBlockData).Layout.HasFlag(thisSlabBlockData.Layout))
                        {
                            blockSides |= Direction.DecreaseX;
                        }
                    }
                    else if (!nearbyBlock.IsSideSolid(Direction.IncreaseZ, nearbyBlockData)
                        || (nearbyBlock.Opacity != Opacity.Opaque && nearbyBlockID != thisBlockID))
                    {
                        blockSides |= Direction.DecreaseZ;
                    }
                }
            }
            #endregion

            #region Increase Y
            {
                nearbyBlockID =
                    pY + 1 < Core.ChunkSizes.Y ? pContext.Chunk.GetBlockID(pX, pY + 1, pZ) :
                    BlockRegister.AirBlockID;

                if (nearbyBlockID == BlockRegister.AirBlockID)
                {
                    blockSides |= Direction.IncreaseY;
                }
                else
                {
                    nearbyBlock = (BlockRegister.Instance.Get(nearbyBlockID));
                    nearbyBlockData = pContext.Chunk.GetBlockData(new Vector3I(pX, pY + 1, pZ));

                    if (!nearbyBlock.IsSideSolid(Direction.DecreaseY, nearbyBlockData)
                        || (nearbyBlock.Opacity != Opacity.Opaque && nearbyBlockID != thisBlockID))
                    {
                        blockSides |= Direction.IncreaseY;
                    }
                }
            }
            #endregion
            #region Decrease Y
            {
                nearbyBlockID =
                    pY - 1 >= 0 ? pContext.Chunk.GetBlockID(pX, pY - 1, pZ) :
                    BlockRegister.AirBlockID;

                if (nearbyBlockID == BlockRegister.AirBlockID)
                {
                    blockSides |= Direction.DecreaseY;
                }
                else
                {
                    nearbyBlock = (BlockRegister.Instance.Get(nearbyBlockID));
                    nearbyBlockData = pContext.Chunk.GetBlockData(new Vector3I(pX, pY - 1, pZ));

                    if (!nearbyBlock.IsSideSolid(Direction.IncreaseY, nearbyBlockData)
                        || (nearbyBlock.Opacity != Opacity.Opaque && nearbyBlockID != thisBlockID))
                    {
                        blockSides |= Direction.DecreaseY;
                    }
                }
            }
            #endregion

            return blockSides;
        }

    }
}