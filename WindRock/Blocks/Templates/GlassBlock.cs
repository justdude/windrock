﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpoonCraft.Engine.Blocks
{
    public class NotOpaqueSolidBlock : SolidBlock
    {
        /* Fields */



        /* Properties */



        /* Constructors */

        public NotOpaqueSolidBlock()
            : base()
        {
            base.IsHard = true;
            base.IsOpaque = false;
            base.IsSolid = true;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}