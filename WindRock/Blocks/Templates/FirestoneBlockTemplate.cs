﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpoonCraft.Common;
using GameHelper.Data;
using Microsoft.Xna.Framework;
using SpoonCraft.Registers;
using GameHelper.Extentions;
using SpoonCraft.ParticleSystem;

namespace SpoonCraft.Blocks
{
    public class FirestoneBlockTemplate : SolidBlockTemplate
    {
        /* Fields */

        private FirestoneState state;

        /* Properties */



        /* Constructors */

        public FirestoneBlockTemplate(string pName, TextureCoord pSideTextureCoord, FirestoneState pState)
            : base(pName, Opacity.Opaque, pSideTextureCoord)
        {
            this.state = pState;
        }

        /* Private methods */

        private TimeSpan GetParticleLiveTime(Random pRandom)
        {
            return TimeSpan.FromSeconds(1 + (float) pRandom.NextDouble() * 3);
        }

        /* Protected methods */



        /* Public methods */

        public override void UpdateData(World pWorld, Vector3I pCoordInWorld, ref Queue<Vector3I> pNewBocksToUpdate)
        {
            //base.UpdateData(pWorld, pCoordInWorld, ref pNewBocksToUpdate);

            switch (this.state)
            {
                case FirestoneState.NotBurning:
                    break;

                case FirestoneState.Burning:
                    Random random = pWorld.Random;
                    if (random.OneOf(50))
                    {
                        // Пыхнуть частичкой
                        short id = pWorld.Random.Boolean() ? ParticleRegister.FireParticleID : ParticleRegister.SmokeParticleID;
                        IParticleTemplate particleTemplate = ParticleRegister.Instance.Get(id);
                        Vector3 position = new Vector3(
                            pCoordInWorld.X + pWorld.Random.Float(-0.1f, 1.1f),
                            pCoordInWorld.Y + pWorld.Random.Float(0.0f, 1.0f),
                            pCoordInWorld.Z + pWorld.Random.Float(-0.1f, 1.1f));

                        Vector3 velocity = Vector3.Zero;
                        switch (id)
                        {
                            case ParticleRegister.FireParticleID:
                                velocity = new Vector3(0, (float) random.NextDouble() * 0.5f, 0);
                                break;
                            case ParticleRegister.SmokeParticleID:
                                velocity = new Vector3(0, 1f + (float) random.NextDouble(), 0);
                                break;
                        }

                        Color color = Color.White;
                        TimeSpan liveTime = this.GetParticleLiveTime(random);
                        float scale = pWorld.Random.Float(5.0f, 10.0f);
                        IParticleData particle = particleTemplate.GetData(position, velocity, color, 0, scale, pWorld.Random);
                        pWorld.AddParticle(particle);
                    }

                    // Попытка погаснуть или выгореть
                    if (random.OneOf(20))
                    {
                        if (random.OneOf(10))
                        {
                            // Выгореть
                            pWorld.SetBlockToBuffer(
                                pCoordInWorld,
                                BlockRegister.BurnedOutFirestoneBlockID,
                                pWorld.GetBlockData(pCoordInWorld));
                        }
                        else
                        {
                            // Погаснуть
                            pWorld.SetBlockToBuffer(
                                pCoordInWorld,
                                BlockRegister.NotBurningFirestoneBlockID,
                                pWorld.GetBlockData(pCoordInWorld));
                        }
                    }
                    //else
                    //{
                    //    pNewBocksToUpdate.Enqueue(Chunk.GetCoordInChunk(pCoordInWorld));
                    //}

                    break;

                case FirestoneState.BurnedOut:
                    break;
            }

            //pNewBocksToUpdate.Enqueue(Chunk.GetCoordInChunk(pCoordInWorld));
        }
    }
}