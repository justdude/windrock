﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using SpoonCraft.Blocks;
using SpoonCraft.Common;
using GameHelper.Data;

namespace SpoonCraft.Blocks
{
    public class PlantBlockTemplate : BlockTemplate
    {
        /* Fields */

        protected static Vector3 Point0 = new Vector3(0, 0, 0);
        protected static Vector3 Point1 = new Vector3(1, 0, 0);
        protected static Vector3 Point2 = new Vector3(0, 1, 0);
        protected static Vector3 Point3 = new Vector3(1, 1, 0);
        protected static Vector3 Point4 = new Vector3(0, 0, 1);
        protected static Vector3 Point5 = new Vector3(1, 0, 1);
        protected static Vector3 Point6 = new Vector3(0, 1, 1);
        protected static Vector3 Point7 = new Vector3(1, 1, 1);

        protected TextureCoord[] growthStagesTextureCoords;

        /* Properties */

        public int GrownStagesCount
        {
            get
            {
                return this.growthStagesTextureCoords.Length;
            }
        }

        /* Constructors */

        public PlantBlockTemplate(string pName, params TextureCoord[] pGrowthStagesTextureCoords)
            : base(pName, Opacity.NotOpaque, false)
        {
            this.growthStagesTextureCoords = pGrowthStagesTextureCoords;
        }

        /* Private methods */



        /* Protected methods */

        protected virtual TextureCoord GetTextureCoord(BlockData pBlockData)
        {
            return this.growthStagesTextureCoords[(pBlockData as PlantBlockData).GrowthStage];
        }

        /* Public methods */

        public override void UpdateData(World pWorld, Vector3I pCoordInWorld, ref Queue<Vector3I> pNewBocksToUpdate)
        {
            PlantBlockData plantBlockData = pWorld.GetBlockData(pCoordInWorld) as PlantBlockData;
            if (plantBlockData.GrowthStage < this.GrownStagesCount - 1)
            {
                plantBlockData.GrowthStage++;
            }
        }
        public override void BuildVertices(Vector3 pPosition, BlockData pBlockData, ref VertexPositionTexture[] pOpaqueBlocksVertices, ref int pOpaqueVertexIndex, ref VertexPositionTexture[] pNotOpaqueBlocksVertices, ref int pNotOpaqueVertexIndex)
        {
            if (pBlockData.VisibleSides != Direction.None)
            {
                TextureCoord textureCoord = GetTextureCoord(pBlockData);
                Vector2 textureCoord_0_0 = new Vector2(
                    textureCoord.Col * Core.BlockTextureProportionalSize.X,
                    textureCoord.Row * Core.BlockTextureProportionalSize.Y);
                Vector2 textureCoord_1_0 = new Vector2(
                    (textureCoord.Col + 1) * Core.BlockTextureProportionalSize.X,
                    textureCoord.Row * Core.BlockTextureProportionalSize.Y);
                Vector2 textureCoord_0_1 = new Vector2(
                    textureCoord.Col * Core.BlockTextureProportionalSize.X,
                    (textureCoord.Row + 1) * Core.BlockTextureProportionalSize.Y);
                Vector2 textureCoord_1_1 = new Vector2(
                    (textureCoord.Col + 1) * Core.BlockTextureProportionalSize.X,
                    (textureCoord.Row + 1) * Core.BlockTextureProportionalSize.Y);

                // Плоскость 1, сторона 1
                pNotOpaqueBlocksVertices[++pNotOpaqueVertexIndex] = new VertexPositionTexture(pPosition + Point0, textureCoord_1_1);
                pNotOpaqueBlocksVertices[++pNotOpaqueVertexIndex] = new VertexPositionTexture(pPosition + Point5, textureCoord_0_1);
                pNotOpaqueBlocksVertices[++pNotOpaqueVertexIndex] = new VertexPositionTexture(pPosition + Point7, textureCoord_0_0);
                pNotOpaqueBlocksVertices[++pNotOpaqueVertexIndex] = new VertexPositionTexture(pPosition + Point0, textureCoord_1_1);
                pNotOpaqueBlocksVertices[++pNotOpaqueVertexIndex] = new VertexPositionTexture(pPosition + Point7, textureCoord_0_0);
                pNotOpaqueBlocksVertices[++pNotOpaqueVertexIndex] = new VertexPositionTexture(pPosition + Point2, textureCoord_1_0);

                // Плоскость 1, сторона 2
                pNotOpaqueBlocksVertices[++pNotOpaqueVertexIndex] = new VertexPositionTexture(pPosition + Point7, textureCoord_0_0);
                pNotOpaqueBlocksVertices[++pNotOpaqueVertexIndex] = new VertexPositionTexture(pPosition + Point5, textureCoord_0_1);
                pNotOpaqueBlocksVertices[++pNotOpaqueVertexIndex] = new VertexPositionTexture(pPosition + Point0, textureCoord_1_1);
                pNotOpaqueBlocksVertices[++pNotOpaqueVertexIndex] = new VertexPositionTexture(pPosition + Point2, textureCoord_1_0);
                pNotOpaqueBlocksVertices[++pNotOpaqueVertexIndex] = new VertexPositionTexture(pPosition + Point7, textureCoord_0_0);
                pNotOpaqueBlocksVertices[++pNotOpaqueVertexIndex] = new VertexPositionTexture(pPosition + Point0, textureCoord_1_1);

                // Плоскость 2, сторона 1
                pNotOpaqueBlocksVertices[++pNotOpaqueVertexIndex] = new VertexPositionTexture(pPosition + Point1, textureCoord_1_1);
                pNotOpaqueBlocksVertices[++pNotOpaqueVertexIndex] = new VertexPositionTexture(pPosition + Point4, textureCoord_0_1);
                pNotOpaqueBlocksVertices[++pNotOpaqueVertexIndex] = new VertexPositionTexture(pPosition + Point6, textureCoord_0_0);
                pNotOpaqueBlocksVertices[++pNotOpaqueVertexIndex] = new VertexPositionTexture(pPosition + Point1, textureCoord_1_1);
                pNotOpaqueBlocksVertices[++pNotOpaqueVertexIndex] = new VertexPositionTexture(pPosition + Point6, textureCoord_0_0);
                pNotOpaqueBlocksVertices[++pNotOpaqueVertexIndex] = new VertexPositionTexture(pPosition + Point3, textureCoord_1_0);

                // Плоскость 2, сторона 2
                pNotOpaqueBlocksVertices[++pNotOpaqueVertexIndex] = new VertexPositionTexture(pPosition + Point6, textureCoord_0_0);
                pNotOpaqueBlocksVertices[++pNotOpaqueVertexIndex] = new VertexPositionTexture(pPosition + Point4, textureCoord_0_1);
                pNotOpaqueBlocksVertices[++pNotOpaqueVertexIndex] = new VertexPositionTexture(pPosition + Point1, textureCoord_1_1);
                pNotOpaqueBlocksVertices[++pNotOpaqueVertexIndex] = new VertexPositionTexture(pPosition + Point3, textureCoord_1_0);
                pNotOpaqueBlocksVertices[++pNotOpaqueVertexIndex] = new VertexPositionTexture(pPosition + Point6, textureCoord_0_0);
                pNotOpaqueBlocksVertices[++pNotOpaqueVertexIndex] = new VertexPositionTexture(pPosition + Point1, textureCoord_1_1);
            }
        }
        public override BlockData CreateData(bool pIsNatural)
        {
            return new PlantBlockData(pIsNatural);
        }
        public override bool IsSideSolid(Direction pSide, BlockData pBlockData)
        {
            return false;
        }
        public override int GetVerticesCount(BlockData pBlockData, Opacity pOpacity)
        {
            return pBlockData.VisibleSides != Direction.None ? 24 : 0;
        }

    }
}