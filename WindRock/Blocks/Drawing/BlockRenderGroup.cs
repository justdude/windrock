﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using GameHelper.Cameras;

namespace WindRock.Blocks.Drawing
{
    public class BlockRenderGroup : IDisposable
    {
        /* Fields */

        private BlockRenderer blockRenderManager;
        private Texture2D texture;
        private Effect effect;
        private Color defaultColor;

        private VertexBuffer vertexBuffer;
        private int primitiveCount;

        private List<VertexPositionColorTexture> vertices;

        /* Properties */

        public Texture2D Texture
        {
            get
            {
                return this.texture;
            }
            set
            {
                this.texture = value;
            }
        }
        public Effect Effect
        {
            get
            {
                return this.effect;
            }
        }
        public Color DefaultColor
        {
            get
            {
                return this.defaultColor;
            }
        }

        /* Constructors */

        public BlockRenderGroup(BlockRenderer pBlockRenderManager, Texture2D pTexture, Effect pEffect)
        {
            this.blockRenderManager = pBlockRenderManager;
            this.texture = pTexture;
            this.effect = pEffect;

            this.defaultColor = Color.Transparent;
            this.vertices = new List<VertexPositionColorTexture>();
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public void AddVertex(Vector3 pPosition, Vector2 pTextureCoord, Color pColor)
        {
            this.vertices.Add(new VertexPositionColorTexture(pPosition, pColor, pTextureCoord));
        }
        public void AddVertex(Vector3 pPosition, Vector2 pTextureCoord)
        {
            this.vertices.Add(new VertexPositionColorTexture(pPosition, Color.Transparent, pTextureCoord));
        }
        public void GenerateVertexBuffer()
        {
            this.vertexBuffer = new VertexBuffer(
                this.blockRenderManager.GraphicsDevice,
                typeof(VertexPositionColorTexture),
                this.vertices.Count,
                BufferUsage.WriteOnly);
            this.vertexBuffer.SetData<VertexPositionColorTexture>(this.vertices.ToArray());

            this.primitiveCount = this.vertices.Count / 3;
            this.vertices = null;
        }
        public void Draw(Camera pCamera, Matrix pWorld)
        {
            Effect currentEffect = this.effect ?? this.blockRenderManager.DefaultEffect;
            Texture2D textureMap = this.texture ?? this.blockRenderManager.DefaultTexture;

            if (currentEffect == null)
            {
                throw new NullReferenceException("Effect is not initialized.");
            }

            if (currentEffect == null)
            {
                throw new NullReferenceException("Texture is not initialized.");
            }

            currentEffect.Parameters["WorldViewProjection"].SetValue(pWorld * pCamera.View * pCamera.Projection);
            currentEffect.Parameters["TextureMap"].SetValue(textureMap);

            foreach (EffectPass pass in currentEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                this.blockRenderManager.GraphicsDevice.SetVertexBuffer(this.vertexBuffer);
                this.blockRenderManager.GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, 0, this.primitiveCount);
            }
        }
        public void Draw(Camera pCamera)
        {
            this.Draw(pCamera, Matrix.Identity);
        }

        /* IDisposable */

        public void Dispose()
        {
            this.vertexBuffer.Dispose();
        }

    }
}