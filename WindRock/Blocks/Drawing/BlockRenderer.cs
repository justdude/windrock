﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using GameHelper.Cameras;

namespace WindRock.Blocks.Drawing
{
    public class BlockRenderer : IDisposable
    {
        /* Fields */

        private GraphicsDevice graphicsDevice;
        private Dictionary<int, BlockRenderGroup> groups;
        private Texture2D defaultTexture;
        private Effect defaultEffect;

        /* Properties */

        public GraphicsDevice GraphicsDevice
        {
            get
            {
                return this.graphicsDevice;
            }
        }
        public Texture2D DefaultTexture
        {
            get
            {
                return this.defaultTexture;
            }
            //set
            //{
            //    this.defaultTexture = value;
            //}
        }
        public Effect DefaultEffect
        {
            get
            {
                return this.defaultEffect;
            }
            //set
            //{
            //    this.defaultEffect = value;
            //}
        }

        /* Constructors */

        public BlockRenderer(GraphicsDevice pGraphicsDevice, Texture2D pDefaultTexture, Effect pDefaultEffect)
        {
            this.graphicsDevice = pGraphicsDevice;
            this.defaultTexture = pDefaultTexture;
            this.defaultEffect = pDefaultEffect;

            this.groups = new Dictionary<int, BlockRenderGroup>();
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public void CreateGroup(int pGroupKey, Texture2D pTexture, Effect pEffect)
        {
            this.groups.Add(pGroupKey, new BlockRenderGroup(this, pTexture, pEffect));
        }
        public BlockRenderGroup GetOrCreateGroup(int pGroupKey, Texture2D pTexture, Effect pEffect)
        {
            BlockRenderGroup group = null;
            if (!this.groups.TryGetValue(pGroupKey, out group))
            {
                group = new BlockRenderGroup(this, pTexture, pEffect);
                this.groups.Add(pGroupKey, group);
            }

            return group;
        }
        public void GenerateVertexBuffers()
        {
            foreach (BlockRenderGroup group in this.groups.Values)
            {
                group.GenerateVertexBuffer();
            }
        }
        //public void Draw(Camera pCamera, Matrix pWorld)
        //{
        //    foreach (BlockRenderGroup group in this.groups.Values)
        //    {
        //        group.Draw(pCamera, pWorld);
        //    }
        //}
        public void Draw(Camera pCamera)
        {
            foreach (BlockRenderGroup group in this.groups.Values)
            {
                group.Draw(pCamera);
            }
        }

        /* IDisposable */

        public void Dispose()
        {
            foreach (IDisposable group in this.groups.Values)
            {
                group.Dispose();
            }
        }

    }
}