﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.Scripting;

namespace WindRock.Scripting
{
    public abstract class GameCommand : Command<GameCommandContext>
    {
        /* Fields */



        /* Properties */



        /* Constructors */



        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}