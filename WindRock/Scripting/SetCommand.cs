﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.Scripting;
using GameHelper.Data;
using WindRock.Common;
using WindRock.Common;
using WindRock.Blocks;
using WindRock.Registers;

namespace WindRock.Scripting
{
    public class SetCommand : GameCommand
    {
        /* Fields */

        private IntValueVector3 coordIntValueVector3;
        private ISingleIntValue idSingleIntValue;

        /* Properties */



        /* Constructors */

        public SetCommand(IntValueVector3 pCoords, ISingleIntValue pID)
        {
            this.coordIntValueVector3 = pCoords;
            this.idSingleIntValue = pID;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public override CommandResult Execute(GameCommandContext pContext)
        {
            short id = 0;
            BlockTemplate block = null;
            Vector3I[] coords = this.coordIntValueVector3.Get(pContext);
            for (int i = 0; i < coords.Length; i++)
            {
                id = (short) this.idSingleIntValue.GetSingle(pContext);
                block = BlockRegister.Instance.Get(id);
                if (block != null)
                {
                    pContext.Game.World.SetBlockToBuffer(coords[i], id, block.CreateDefaultMetadata());
                }
            }

            return CommandResult.Completed();
        }

    }
}