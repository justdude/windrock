﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.Scripting;

namespace WindRock.Scripting
{
    public class GameCommandRegister : CommandRegister<GameCommandContext, GameCommand>
    {
        /* Fields */



        /* Properties */



        /* Constructors */



        /* Private methods */

        private GameCommand GetProperSetCommand(Value[] pValues, GameCommandContext pContext)
        {
            GameCommand command = null;

            if (pValues.Length == 2
                && pValues[0] is IntValueVector3
                && pValues[1] is ISingleIntValue)
            {
                command = new SetCommand(pValues[0] as IntValueVector3, pValues[1] as ISingleIntValue);
            }

            return command;
        }

        /* Protected methods */



        /* Public methods */

        public void Initialize()
        {
            this.commandSelectors.Add("set", this.GetProperSetCommand);
        }

    }
}