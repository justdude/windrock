﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.Scripting;

namespace WindRock.Scripting
{
    public class GameCommandContext : CommandContext
    {
        /* Fields */

        private WindRockGame game;

        /* Properties */

        public WindRockGame Game
        {
            get
            {
                return this.game;
            }
        }

        /* Constructors */

        public GameCommandContext(Random pRandom, WindRockGame pGame)
            : base(pRandom)
        {
            this.game = pGame;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}