﻿using System;

namespace WindRock
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (WindRockGame game = new WindRockGame())
            {
                game.Run();
            }
        }
    }
#endif
}