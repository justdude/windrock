﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpoonCraft.Cameras
{
    public class Camera
    {
        #region Fields

        private SpoonCraftGame game;
        private Vector3 position;
        private float nearPlane = 0.01f;
        private float farPlane = 1000.0f;

        #endregion

        #region Properties

        protected SpoonCraftGame Game
        {
            get
            {
                return this.game;
            }
        }

        //public Matrix World
        //{
        //    set;
        //    get;
        //}

        public Matrix View
        {
            set;
            get;
        }

        public Matrix Projection
        {
            set;
            get;
        }

        public Vector3 Position
        {
            set
            {
                this.position = value;
                CalculateView();
            }
            get
            {
                return this.position;
            }
        }

        #endregion

        #region Constructors

        public Camera(SpoonCraftGame pGame)
        {
            this.game = pGame;
        }

        #endregion

        #region Private methods
        #endregion

        #region Protected methods

        protected virtual void CalculateView()
        {

        }

        protected virtual void CalculateProjection()
        {
            this.Projection = Matrix.CreatePerspectiveFieldOfView(
               MathHelper.ToRadians(45),
               this.game.GraphicsDevice.Viewport.AspectRatio,
               this.nearPlane,
               this.farPlane );
        }

        #endregion

        #region Public Methods

        public virtual void Initialize()
        {
            CalculateView();
            CalculateProjection();
        }

        public virtual void Update(SpoonCraftGame pGame)
        {
        }

        #endregion
    }
}