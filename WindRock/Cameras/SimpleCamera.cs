﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace SpoonCraft.Cameras
{
    public class SimpleCamera : Camera
    {
        // Fields

        private Vector3 target;

        // Properties

        public Vector3 Target
        {
            set
            {
                this.target = value;
                CalculateView();
            }
            get
            {
                return this.target;
            }
        }

        // Constructors

        public SimpleCamera(SpoonCraftGame pGame)
            : base( pGame )
        {
            this.target = Vector3.Zero;
            base.Initialize();
        }

        // Private methods



        // Protected methods

        protected override void CalculateView()
        {
            this.View = Matrix.CreateLookAt( this.Position, this.target, Vector3.Up );

            base.CalculateView();
        }

        // Public methods

        public override void Update(SpoonCraftGame pGame)
        {
            base.Update( pGame );
        }

    }
}