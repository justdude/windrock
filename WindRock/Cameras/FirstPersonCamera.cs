﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using SpoonCraft.Engine;

namespace SpoonCraft.Cameras
{
    public class FirstPersonCamera : Camera
    {
        /* Fields */

        private float movementSpeed;
        private float rotationSpeed;
        private float xRotation;
        private float yRotation;
        private Vector3 target;

        /* Properties */

        public Vector3 Target
        {
            get
            {
                return this.target;
            }
        }
        public float XRotation
        {
            set
            {
                this.xRotation = value;
                CalculateView();
            }
            get
            {
                return this.xRotation;
            }
        }
        public float YRotation
        {
            set
            {
                this.yRotation = value;
                CalculateView();
            }
            get
            {
                return this.yRotation;
            }
        }

        /* Constructors */

        public FirstPersonCamera(SpoonCraftGame pGame)
            : base( pGame )
        {
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public override void Initialize()
        {
            this.movementSpeed = 8.8f;
            //this.rotationSpeed = 1.0f;
            this.yRotation = 0.0f;
            this.xRotation = 0.0f;

            base.Initialize();
        }

        public override void Update(SpoonCraftGame pGame)
        {
            CalculateView();

            base.Update( pGame );
        }

        protected override void CalculateView()
        {
            Matrix rotationMatrix = Matrix.CreateRotationX( MathHelper.ToRadians( this.xRotation ) ) * Matrix.CreateRotationY( MathHelper.ToRadians( this.yRotation ) );
            Vector3 rotatedCameraTarget = Vector3.Transform( Vector3.Forward, rotationMatrix );
            this.target = this.Position + rotatedCameraTarget;
            Vector3 rotatedCameraUpVector = Vector3.Transform( Vector3.Up, rotationMatrix );
            this.View = Matrix.CreateLookAt( this.Position, this.target, rotatedCameraUpVector );

            base.CalculateView();
        }

        public void Move(GameTime pGameTime, Direction pDirection)
        {
            float v = this.movementSpeed * pGameTime.ElapsedGameTime.Milliseconds / 1000.0f;
            float a = MathHelper.ToRadians( this.xRotation );
            float b = MathHelper.ToRadians( this.yRotation );

            if ((pDirection & Direction.Forward) > 0)
            {
                Vector3 offset = new Vector3(
                    -(float) (v * Math.Sin( b ) * Math.Cos( a )),
                    (float) (v * Math.Sin( a )),
                    -(float) (v * Math.Cos( b ) * Math.Cos( a )) );
                this.Position += offset;
            }
            else if ((pDirection & Direction.Backward) > 0)
            {
                Vector3 offset = new Vector3(
                    -(float) (v * Math.Sin( b ) * Math.Cos( a )),
                    (float) (v * Math.Sin( a )),
                    -(float) (v * Math.Cos( b ) * Math.Cos( a )) );
                this.Position -= offset;
            }

            if ((pDirection & Direction.Leftward) > 0)
            {
                Vector3 offset = new Vector3(
                    -(float) (v * Math.Cos( b )),
                    0,
                    (float) (v * Math.Sin( b )) );
                this.Position += offset;
            }
            else if ((pDirection & Direction.Rightward) > 0)
            {
                Vector3 offset = new Vector3(
                    -(float) (v * Math.Cos( b )),
                    0,
                    (float) (v * Math.Sin( b )) );
                this.Position -= offset;
            }

            if ((pDirection & Direction.Upward) > 0)
            {
                this.Position += new Vector3( 0, 1, 0 ) * v;
            }
            else if ((pDirection & Direction.Downward) > 0)
            {
                this.Position += new Vector3( 0, -1, 0 ) * v;
            }

            CalculateView();

        }

        public void Move(GameTime pGameTime, Vector3 pDirection)
        {
            float v = this.movementSpeed * pGameTime.ElapsedGameTime.Milliseconds / 1000.0f;
            float a = MathHelper.ToRadians( this.xRotation );
            float b = MathHelper.ToRadians( this.yRotation );

            if (pDirection.Z == Vector3.Forward.Z)
            {
                Vector3 offset = new Vector3(
                    -(float) (v * Math.Sin( b ) * Math.Cos( a )),
                    (float) (v * Math.Sin( a )),
                    -(float) (v * Math.Cos( b ) * Math.Cos( a )) );
                this.Position += offset;
            }
            else if (pDirection.Z == Vector3.Backward.Z)
            {
                Vector3 offset = new Vector3(
                    -(float) (v * Math.Sin( b ) * Math.Cos( a )),
                    (float) (v * Math.Sin( a )),
                    -(float) (v * Math.Cos( b ) * Math.Cos( a )) );
                this.Position -= offset;
            }

            if (pDirection.X == Vector3.Left.X)
            {
                Vector3 offset = new Vector3(
                    -(float) (v * Math.Cos( b )),
                    0,
                    (float) (v * Math.Sin( b )) );
                this.Position += offset;
            }
            else if (pDirection.X == Vector3.Right.X)
            {
                Vector3 offset = new Vector3(
                    -(float) (v * Math.Cos( b )),
                    0,
                    (float) (v * Math.Sin( b )) );
                this.Position -= offset;
            }

            if (pDirection.Y == Vector3.Up.Y)
            {
                this.Position += new Vector3( 0, 1, 0 ) * v;
            }
            else if (pDirection.Y == Vector3.Down.Y)
            {
                this.Position += new Vector3( 0, -1, 0 ) * v;
            }

            CalculateView();

        }

        //public void Move(GameTime pGameTime, Vector3 pOffset)
        //{
        //    this.Position += pOffset;
        //    CalculateView();

        //}


    }
}