﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpoonCraft.Cameras
{
    public class Class1
    {
        #region Enums
        #endregion

        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Class1()
        {
        }

        #endregion

        #region Private methods
        #endregion

        #region Protected methods
        #endregion

        #region Public Methods

        /// <summary>
        /// Default string view of class instance.
        /// </summary>
        /// <returns>String view of class instance.</returns>
        public override string ToString()
        {
            return base.ToString();
        }

        #endregion
    }
}