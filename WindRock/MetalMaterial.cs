﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpoonCraft.Damaging;

namespace SpoonCraft.Materials
{
    public class MetalMaterial:Material
    {
        /* Fields */



        /* Properties */



        /* Constructors */

        public MetalMaterial(string pName, Damage pResistance)
            : base(pName, pResistance)
        {
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}