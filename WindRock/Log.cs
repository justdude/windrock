﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace SpoonCraft
{
    public class Log
    {
        /* Constants */

        private const int MaxMessagesCount = 20;

        /* Fields */

        private List<string> messages;

        private Game game;
        private SpriteBatch spriteBatch;
        private SpriteFont SegoeUIMono8;
        private int messageNumber;
        private int scrollPosition;
        private int displayMessagesCount;
        //private Rectangle rectangle;

        /* Properties */



        /* Constructors */

        public Log(Game pGame)
        {
            this.game = pGame;
            //this.rectangle = pRectangle;

            this.spriteBatch = new SpriteBatch(this.game.GraphicsDevice);
            this.SegoeUIMono8 = this.game.Content.Load<SpriteFont>(@"Fonts\SegoeUIMono8");
            this.messages = new List<string>(MaxMessagesCount);
            this.displayMessagesCount = 5;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public void Draw()
        {
            this.spriteBatch.Begin();

            int y = this.game.GraphicsDevice.Viewport.Height - this.displayMessagesCount * 10 - 10;
            for (int messageIndex = this.scrollPosition; messageIndex < this.scrollPosition + this.displayMessagesCount; messageIndex++)
            {
                if (messageIndex >= this.messages.Count)
                {
                    break;
                }

                this.spriteBatch.DrawString(
                    this.SegoeUIMono8,
                    this.messages[messageIndex],
                    new Vector2(10, y),
                    Color.White);
                y += 10;
            }

            this.spriteBatch.End();
        }

        // // //

        public void Write(string pMessage)
        {
            if (this.messages.Count == MaxMessagesCount)
            {
                this.messages.RemoveAt(0);
            }

            this.messages.Add(
                string.Format("{0:D3}| {1}",
                    ++this.messageNumber,
                    pMessage));
            //if (this.scrollPosition + this.displayMessagesCount >= this.messages.Count - 1)
            //{
            //    this.scrollPosition++;
            //}
        }
        public void ScrollTo(int pPosition)
        {
            this.scrollPosition = (int) MathHelper.Clamp(pPosition, 0, this.messages.Count - this.displayMessagesCount);
        }
        public void ScrollAt(int pOffset)
        {
            this.scrollPosition = (int) MathHelper.Clamp(this.scrollPosition + pOffset, 0, this.messages.Count - this.displayMessagesCount);
        }

    }
}