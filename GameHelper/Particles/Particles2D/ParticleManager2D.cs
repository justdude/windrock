﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameHelper.Particles.Particles2D
{
    public class ParticleManager2D
    {
        /* Fields */

        protected List<Particle2D> particles;
        protected List<ParticleEmitter2D> emitters;
        protected Random random;
        protected int maxParticleCount;
        protected int maxEmitterCount;

        /* Properties */

        public Random Random
        {
            get
            {
                return this.random;
            }
        }
        public int MaxParticleCount
        {
            get
            {
                return this.maxParticleCount;
            }
            set
            {
                this.maxParticleCount = value;
            }
        }
        public int MaxEmitterCount
        {
            get
            {
                return this.maxEmitterCount;
            }
            set
            {
                this.maxEmitterCount = value;
            }
        }
        public int ParticlesCount
        {
            get
            {
                return this.particles == null ? 0 : this.particles.Count;
            }
        }
        public int ParticlesAvailableCount
        {
            get
            {
                return
                    this.particles == null ?
                    (this.maxParticleCount <= 0 ? int.MaxValue : this.maxParticleCount) :
                    (this.maxParticleCount <= 0 ? int.MaxValue : this.maxParticleCount - this.particles.Count);
            }
        }

        /* Constructors */

        public ParticleManager2D(Random pRandom = null)
        {
            this.random = pRandom ?? new Random();

            this.particles = new List<Particle2D>();
            this.emitters = new List<ParticleEmitter2D>();
        }

        /* Private methods */

        public virtual void UpdateEmitters(GameTime pGameTime)
        {
            ParticleEmitter2D emitter = null;
            for (int i = 0; i < this.emitters.Count; )
            {
                emitter = this.emitters[i];
                emitter.Update(pGameTime);

                if (emitter.IsAlive)
                {
                    i++;
                }
                else
                {
                    this.emitters.RemoveAt(i);
                }
            }
        }
        public virtual void UpdateParticles(GameTime pGameTime)
        {
            Particle2D particle = null;
            for (int i = 0; i < this.particles.Count; )
            {
                particle = this.particles[i];
                particle.Update(pGameTime);

                if (particle.IsAlive)
                {
                    i++;
                }
                else
                {
                    this.particles.RemoveAt(i);
                }
            }
        }

        /* Protected methods */



        /* Public methods */

        public virtual void Update(GameTime pGameTime)
        {
            this.UpdateEmitters(pGameTime);
            this.UpdateParticles(pGameTime);
        }
        public void Draw(SpriteBatch pSpriteBatch)
        {
            for (int i = 0; i < this.particles.Count; i++)
            {
                this.particles[i].Draw(pSpriteBatch);
            }
        }

        // // //

        public void AddEmitter(ParticleEmitter2D pEmitter)
        {
            if (pEmitter == null)
            {
                throw new ArgumentException("pEmitter");
            }

            if (this.maxEmitterCount > 0
                && this.emitters.Count >= this.maxEmitterCount)
            {
                throw new Exception("Exceeded the number of emitters.");
            }

            this.emitters.Add(pEmitter);
        }
        public void RemoveEmitter(ParticleEmitter2D pEmitter)
        {
            if (pEmitter == null)
            {
                throw new ArgumentException("pEmitter");
            }

            this.emitters.Remove(pEmitter);
        }
        public void RemoveAllEmitters()
        {
            this.emitters.Clear();
        }
        public void AddParticle(Particle2D pParticle)
        {
            if (pParticle == null)
            {
                throw new ArgumentException("pEmitter");
            }

            if (this.maxParticleCount > 0
                && this.particles.Count < this.maxParticleCount)
            {
                this.particles.Add(pParticle);
            }
        }
        public void RemoveParticle(Particle2D pParticle)
        {
            if (pParticle == null)
            {
                throw new ArgumentException("pParticle");
            }

            this.particles.Remove(pParticle);
        }
        public void RemoveAllParticles()
        {
            this.particles.Clear();
        }

    }
}