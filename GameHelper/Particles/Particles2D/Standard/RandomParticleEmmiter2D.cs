﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using GameHelper.Data;
using Microsoft.Xna.Framework;
using GameHelper.Data.RandomData;

namespace GameHelper.Particles.Particles2D.Standard
{
    public class RandomParticleEmmiter2D : ParticleEmitter2D
    {
        /* Fields */

        protected Texture2D[] particleTextures;
        protected RandomVector2 particlePosition;
        protected RandomVector2 particleVelocity;
        protected RandomFloat particleRotation;
        protected RandomFloat particleAngularVelocity;
        protected RandomFloat particleScale;
        protected RandomVector4 particleColor;
        protected RandomTimeSpan particleLiveTime;

        /* Properties */

        public Texture2D[] ParticleTextures
        {
            get
            {
                return this.particleTextures;
            }
        }
        public RandomVector2 ParticlePosition
        {
            get
            {
                return this.particlePosition;
            }
            set
            {
                this.particlePosition = value;
            }
        }
        public RandomVector2 ParticleVelocity
        {
            get
            {
                return this.particleVelocity;
            }
            set
            {
                this.particleVelocity = value;
            }
        }
        public RandomFloat ParticleRotation
        {
            get
            {
                return this.particleRotation;
            }
            set
            {
                this.particleRotation = value;
            }
        }
        public RandomFloat ParticleAngularVelocity
        {
            get
            {
                return this.particleAngularVelocity;
            }
            set
            {
                this.particleAngularVelocity = value;
            }
        }
        public RandomFloat ParticleScale
        {
            get
            {
                return this.particleScale;
            }
            set
            {
                this.particleScale = value;
            }
        }
        public RandomVector4 ParticleColor
        {
            get
            {
                return this.particleColor;
            }
            set
            {
                this.particleColor = value;
            }
        }
        public RandomTimeSpan ParticleLiveTime
        {
            get
            {
                return this.particleLiveTime;
            }
            set
            {
                this.particleLiveTime = value;
            }
        }

        /* Constructors */

        public RandomParticleEmmiter2D(ParticleManager2D pParticleManager, Texture2D[] pParticleTextures, RandomVector2 pParticlePosition, RandomTimeSpan pParticleLiveTime)
            : base(pParticleManager, TimeSpan.MaxValue)
        {
            this.particleTextures = pParticleTextures;
            this.particlePosition = pParticlePosition;
            this.particleLiveTime = pParticleLiveTime;

            this.particleVelocity = RandomVector2.Zero;
            this.particleRotation = RandomFloat.Zero;
            this.particleAngularVelocity = RandomFloat.Zero;
            this.particleScale = RandomFloat.One;
            this.particleColor = RandomVector4.One;
        }

        /* Private methods */



        /* Protected methods */

        protected override void CreateParticles()
        {
            if (this.CanCreateParticle())
            {
                Random random = this.particleManager.Random;
                SmoothScaledParticle2D particle = new SmoothScaledParticle2D(
                    this.particleTextures[random.Next(this.particleTextures.Length)],
                    this.particlePosition.GetValue(random),
                    this.particleVelocity.GetValue(random),
                    this.particleRotation.GetValue(random),
                    this.particleAngularVelocity.GetValue(random),
                    this.particleScale.GetValue(random),
                    new Color(this.particleColor.GetValue(random)),
                    this.particleLiveTime.GetValue(random));
                this.particleManager.AddParticle(particle);
            }
        }

        /* Public methods */



    }
}