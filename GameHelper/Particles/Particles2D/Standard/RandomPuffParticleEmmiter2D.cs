﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using GameHelper.Data;
using GameHelper.Data.RandomData;
using Microsoft.Xna.Framework;

namespace GameHelper.Particles.Particles2D.Standard
{
    public class RandomPuffParticleEmmiter2D : RandomParticleEmmiter2D
    {
        /* Fields */

        private RandomInt particlesPuffCount;

        /* Properties */



        /* Constructors */

        public RandomPuffParticleEmmiter2D(ParticleManager2D pParticleManager, Texture2D[] pParticleTextures, Vector2 pParticlePosition, RandomTimeSpan pParticleLiveTime, RandomInt pParticlesPuffCount)
            : base(pParticleManager, pParticleTextures, new RandomVector2(pParticlePosition), pParticleLiveTime)
        {
            this.particlesPuffCount = pParticlesPuffCount;
        }

        /* Private methods */



        /* Protected methods */

        protected override void CreateParticles()
        {
            //if (this.CanCreateParticle())
            //{
            Random random = this.particleManager.Random;
            int particlesPuffCountValue = this.particlesPuffCount.GetValue(random);
            int count = Math.Min(particlesPuffCountValue, this.particleManager.ParticlesAvailableCount);

            for (int i = 0; i < count; i++)
            {
                PuffParticle2D particle = new PuffParticle2D(
                    this.particleTextures[random.Next(this.particleTextures.Length)],
                    this.particlePosition.GetValue(random),
                    this.particleVelocity.GetValue(random),
                    this.particleRotation.GetValue(random),
                    this.particleAngularVelocity.GetValue(random),
                    this.particleScale.GetValue(random),
                    new Color(this.particleColor.GetValue(random)),
                    this.particleLiveTime.GetValue(random));
                this.particleManager.AddParticle(particle);
            }

            this.isAlive = false;
            //}
        }

        /* Public methods */



    }
}