﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace GameHelper.Particles.Particles2D.Standard
{
    public class SmoothScaledParticle2D : Particle2D
    {
        /* Fields */

        private TimeSpan scaleUpDuration;
        private TimeSpan scaleDownDuration;
        private float baseScale;
        private TimeSpan baseLifeTime;
        private ParticleState state;

        /* Properties */

        public TimeSpan ScaleUpDuration
        {
            get
            {
                return this.scaleUpDuration;
            }
            set
            {
                this.scaleUpDuration = value;
            }
        }
        public TimeSpan ScaleDownDuration
        {
            get
            {
                return this.scaleDownDuration;
            }
            set
            {
                this.scaleDownDuration = value;
            }
        }
        public override bool IsAlive
        {
            get
            {
                return this.state != ParticleState.Dead;
            }
        }

        /* Constructors */

        public SmoothScaledParticle2D(Texture2D pTexture, Vector2 pPosition, Vector2 pVelocity, float pRotation, float pAngularVelocity, float pScale, Color pColor, TimeSpan pLifeTime)
            : base(pTexture, pPosition, pVelocity, pRotation, pAngularVelocity, pScale, pColor, TimeSpan.Zero)
        {
            this.scaleUpDuration = TimeSpan.FromSeconds(1f);
            this.scaleDownDuration = TimeSpan.FromSeconds(1f);

            this.scale = 0;
            this.baseScale = pScale;
            this.baseLifeTime = pLifeTime;
            this.state = ParticleState.None;
        }

        /* Private methods */



        /* Protected methods */

        protected override void ProcessUpdate(GameTime pGameTime)
        {
            base.ProcessUpdate(pGameTime);

            switch (this.state)
            {
                case ParticleState.None:
                    this.state = ParticleState.BeforeLife;
                    this.lifeTime = this.scaleUpDuration;
                    break;

                case ParticleState.BeforeLife:
                    if (this.lifeTime <= TimeSpan.Zero)
                    {
                        this.scale = this.baseScale;
                        this.state = ParticleState.Life;
                        this.lifeTime = this.baseLifeTime;
                    }
                    else
                    {
                        float scaleFactor = 1 - (float) this.lifeTime.Ticks / (float) this.scaleUpDuration.Ticks;
                        this.scale = this.baseScale * scaleFactor;
                    }
                    break;

                case ParticleState.Life:
                    if (this.lifeTime <= TimeSpan.Zero)
                    {
                        this.state = ParticleState.AfterLife;
                        this.lifeTime = this.scaleDownDuration;
                    }
                    break;

                case ParticleState.AfterLife:
                    if (this.lifeTime <= TimeSpan.Zero)
                    {
                        this.scale = 0;
                        this.state = ParticleState.Dead;
                    }
                    else
                    {
                        float scaleFactor = (float) this.lifeTime.Ticks / (float) this.scaleDownDuration.Ticks;
                        this.scale = this.baseScale * scaleFactor;
                    }
                    break;
            }
        }

        /* Public methods */



    }
}