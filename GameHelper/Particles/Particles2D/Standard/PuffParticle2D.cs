﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace GameHelper.Particles.Particles2D.Standard
{
    public class PuffParticle2D : Particle2D
    {
        /* Fields */

        private float baseScale;
        private Vector4 baseColor;
        private TimeSpan baseLifeTime;

        /* Properties */



        /* Constructors */

        public PuffParticle2D(Texture2D pTexture, Vector2 pPosition, Vector2 pVelocity, float pRotation, float pAngularVelocity, float pScale, Color pColor, TimeSpan pLifeTime)
            : base(pTexture, pPosition, pVelocity, pRotation, pAngularVelocity, 0, Color.Transparent, pLifeTime)
        {
            this.baseScale = pScale;
            this.baseColor = pColor.ToVector4();
            this.baseLifeTime = pLifeTime;
        }

        /* Private methods */



        /* Protected methods */

        protected override void ProcessUpdate(GameTime pGameTime)
        {
            base.ProcessUpdate(pGameTime);

            float factor = (float) ((double) this.lifeTime.Ticks / (double) this.baseLifeTime.Ticks);
            this.scale = this.baseScale * (1 - factor);
            this.color =
                factor < 0.5f ?
                new Color(this.baseColor) :
                new Color(this.baseColor * new Vector4(1, 1, 1, factor * 0.5f));
        }

        /* Public methods */



    }
}