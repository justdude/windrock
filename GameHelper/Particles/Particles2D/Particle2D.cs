﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace GameHelper.Particles.Particles2D
{
    public class Particle2D
    {
        /* Fields */

        protected Texture2D texture;
        protected Vector2 position;
        protected Vector2 velocity;
        protected float rotation;
        protected float angularVelocity;
        protected float scale;
        protected Color color;
        protected TimeSpan lifeTime;
        protected bool isAlive;

        /* Properties */

        public virtual bool IsAlive
        {
            get
            {
                return this.isAlive;
            }
        }

        /* Constructors */

        public Particle2D(Texture2D pTexture, Vector2 pPosition, Vector2 pVelocity, float pRotation, float pAngularVelocity, float pScale, Color pColor, TimeSpan pLifeTime)
        {
            this.texture = pTexture;
            this.position = pPosition;
            this.velocity = pVelocity;
            this.rotation = pRotation;
            this.angularVelocity = pAngularVelocity;
            this.scale = pScale;
            this.color = pColor;
            this.lifeTime = pLifeTime;
            this.isAlive = true;
        }

        /* Private methods */



        /* Protected methods */

        protected virtual void ProcessUpdate(GameTime pGameTime)
        {
            float time = (float) pGameTime.ElapsedGameTime.TotalSeconds;
            this.position += this.velocity * time;
            this.rotation += this.angularVelocity * time;

            this.lifeTime -= pGameTime.ElapsedGameTime;
            if (this.lifeTime <= TimeSpan.Zero)
            {
                this.isAlive = false;
            }
        }

        /* Public methods */

        public virtual void Update(GameTime pGameTime)
        {
            if (this.IsAlive)
            {
                this.ProcessUpdate(pGameTime);
            }
        }
        public virtual void Draw(SpriteBatch pSpriteBatch)
        {
            pSpriteBatch.Draw(
                this.texture,
                this.position,
                null,
                this.color,
                this.rotation,
                new Vector2(this.texture.Bounds.Width * 0.5f, this.texture.Bounds.Height * 0.5f),
                this.scale,
                SpriteEffects.None,
                0f);
        }

    }
}