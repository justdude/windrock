﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GameHelper.Particles.Particles3D
{
    public abstract class ParticleEmitter3D
    {
        /* Fields */

        protected ParticleManager3D particleManager;
        protected TimeSpan lifeTime;
        protected double chanceToCreateParticle;
        protected bool isAlive;

        /* Properties */

        public bool IsAlive
        {
            get
            {
                return this.isAlive;
            }
        }
        public double ChanceToCreateParticle
        {
            get
            {
                return this.chanceToCreateParticle;
            }
            set
            {
                this.chanceToCreateParticle = Math.Max(0, Math.Min(1, value));
            }
        }
        
        /* Constructors */

        public ParticleEmitter3D(ParticleManager3D pParticleManager, TimeSpan pLiveTime)
        {
            if (pParticleManager == null)
            {
                throw new ArgumentNullException("pParticleManager");
            }

            this.particleManager = pParticleManager;
            this.lifeTime = pLiveTime;
            this.chanceToCreateParticle = 1.0;
            this.isAlive = true;
        }

        /* Private methods */



        /* Protected methods */

        protected virtual bool CanCreateParticle()
        {
            return this.particleManager.Random.NextDouble() <= this.chanceToCreateParticle;
        }
        protected abstract void CreateParticles();

        /* Public methods */

        public virtual void Update(GameTime pGameTime)
        {
            if (this.IsAlive)
            {
                this.lifeTime -= pGameTime.ElapsedGameTime;
                this.isAlive &= this.lifeTime > TimeSpan.Zero;
                this.CreateParticles();
            }
        }

    }
}