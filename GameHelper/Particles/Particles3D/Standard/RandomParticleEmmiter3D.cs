﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using GameHelper.Data;
using Microsoft.Xna.Framework;
using GameHelper.Data.RandomData;

namespace GameHelper.Particles.Particles3D.Standard
{
    public class RandomParticleEmmiter3D : ParticleEmitter3D
    {
        /* Fields */

        protected RandomVector3 particlePosition;
        protected RandomVector3 particleVelocity;
        protected RandomFloat particleScale;
        protected RandomVector4 particleColor;
        protected RandomTimeSpan particleLiveTime;

        /* Properties */

        public RandomVector3 ParticlePosition
        {
            get
            {
                return this.particlePosition;
            }
            set
            {
                this.particlePosition = value;
            }
        }
        public RandomVector3 ParticleVelocity
        {
            get
            {
                return this.particleVelocity;
            }
            set
            {
                this.particleVelocity = value;
            }
        }
        public RandomFloat ParticleScale
        {
            get
            {
                return this.particleScale;
            }
            set
            {
                this.particleScale = value;
            }
        }
        public RandomVector4 ParticleColor
        {
            get
            {
                return this.particleColor;
            }
            set
            {
                this.particleColor = value;
            }
        }
        public RandomTimeSpan ParticleLiveTime
        {
            get
            {
                return this.particleLiveTime;
            }
            set
            {
                this.particleLiveTime = value;
            }
        }

        /* Constructors */

        public RandomParticleEmmiter3D(ParticleManager3D pParticleManager, RandomVector3 pParticlePosition, RandomTimeSpan pParticleLiveTime)
            : base(pParticleManager, TimeSpan.MaxValue)
        {
            this.particlePosition = pParticlePosition;
            this.particleLiveTime = pParticleLiveTime;

            this.particleVelocity = RandomVector3Position.Zero;
            this.particleScale = RandomFloat.One;
            this.particleColor = RandomVector4.One;
        }

        /* Private methods */



        /* Protected methods */

        protected override void CreateParticles()
        {
            if (this.CanCreateParticle())
            {
                Random random = this.particleManager.Random;
                int groupIndex = this.particleManager.Random.Next(this.particleManager.GroupCount);
                SmoothScaledParticle3D particle = new SmoothScaledParticle3D(
                    this.particlePosition.GetValue(random),
                    this.particleVelocity.GetValue(random),
                    this.particleScale.GetValue(random),
                    new Color(this.particleColor.GetValue(random)),
                    this.particleLiveTime.GetValue(random));
                this.particleManager.AddParticle(particle, groupIndex);
            }
        }

        /* Public methods */



    }
}