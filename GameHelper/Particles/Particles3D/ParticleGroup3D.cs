﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using GameHelper.Cameras;

namespace GameHelper.Particles.Particles3D
{
    public class ParticleGroup3D
    {
        /* Fields */

        protected ParticleManager3D particleManager;
        private List<Particle3D> particles;
        private VertexPositionTexture[] vertices;
        private bool isVerticesUpdateRequired;
        private Texture2D texture;
        private Effect effect;
        private Vector2 size;

        /* Properties */

        public Texture2D Texture
        {
            get
            {
                return this.texture;
            }
        }
        public List<Particle3D> Particles
        {
            get
            {
                return this.particles;
            }
        }
        public int ParticleCount
        {
            get
            {
                return this.particles.Count;
            }
        }
        public Vector2 Size
        {
            get
            {
                return this.size;
            }
            set
            {
                this.size = value;
            }
        }

        /* Constructors */

        public ParticleGroup3D(ParticleManager3D pParticleManager, Texture2D pTexture, Effect pEffect)
        {
            this.particleManager = pParticleManager;
            this.texture = pTexture;
            this.effect = pEffect;

            this.size = new Vector2(pTexture.Width, pTexture.Height);
            this.particles = new List<Particle3D>();
        }
        public ParticleGroup3D(ParticleManager3D pParticleManager, Texture2D pTexture)
            : this(pParticleManager, pTexture, null)
        {
        }

        /* Private methods */

        public void UpdateVertices()
        {
            this.vertices = new VertexPositionTexture[this.particles.Count * 6];
            int vertexIndex = -1;
            Vector3 position = Vector3.Zero;
            for (int particleIndex = 0; particleIndex < this.particles.Count; particleIndex++)
            {
                position = this.particles[particleIndex].Position;
                this.vertices[++vertexIndex] = new VertexPositionTexture(position, new Vector2(0, 1)); // 0
                this.vertices[++vertexIndex] = new VertexPositionTexture(position, new Vector2(1, 1)); // 1
                this.vertices[++vertexIndex] = new VertexPositionTexture(position, new Vector2(1, 0)); // 2
                this.vertices[++vertexIndex] = new VertexPositionTexture(position, new Vector2(0, 1)); // 0
                this.vertices[++vertexIndex] = new VertexPositionTexture(position, new Vector2(1, 0)); // 2
                this.vertices[++vertexIndex] = new VertexPositionTexture(position, new Vector2(0, 0)); // 3
            }

            this.isVerticesUpdateRequired = false;
        }

        /* Protected methods */



        /* Public methods */

        public void UpdateParticles(GameTime pGameTime)
        {
            Particle3D particle = null;
            for (int i = 0; i < this.particles.Count; i++)
            {
                particle = this.particles[i];
                particle.Update(pGameTime);

                if (particle.IsAlive)
                {
                    i++;
                }
                else
                {
                    this.particles.RemoveAt(i);
                    this.isVerticesUpdateRequired = true;
                }
            }
        }
        public void Draw(GraphicsDevice pGraphicsDevice, Camera pCamera, Matrix pWorld)
        {
            if (this.vertices != null && this.vertices.Length > 0)
            {
                Effect particleEffect = this.effect ?? this.particleManager.DefaultEffect;
                if (particleEffect == null)
                {
                    throw new NullReferenceException();
                }

                particleEffect.Parameters["World"].SetValue(pWorld);
                particleEffect.Parameters["View"].SetValue(pCamera.View);
                particleEffect.Parameters["Projection"].SetValue(pCamera.Projection);
                particleEffect.Parameters["CameraPosition"].SetValue(pCamera.Position);
                particleEffect.Parameters["UpVector"].SetValue(pCamera.UpVector);
                particleEffect.Parameters["TextureMap"].SetValue(this.texture);
                particleEffect.Parameters["Size"].SetValue(this.size);

                foreach (EffectPass pass in particleEffect.CurrentTechnique.Passes)
                {
                    pass.Apply();
                    pGraphicsDevice.DrawUserPrimitives<VertexPositionTexture>(
                        PrimitiveType.TriangleList,
                        this.vertices,
                        0,
                        (int) (this.vertices.Length / 3));
                }
            }
        }

        public void AddParticle(Particle3D pParticle)
        {
            this.particles.Add(pParticle);
            this.isVerticesUpdateRequired = true;
        }
        //public void RemoveParticle(Particle3D pParticle)
        //{
        //    this.particles.Remove(pParticle);
        //    this.isVerticesUpdateRequired = true;
        //}
        //public void RemoveAllParticles()
        //{
        //    this.particles.Clear();
        //    this.isVerticesUpdateRequired = true;
        //}

    }
}