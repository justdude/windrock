﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameHelper.Cameras;

namespace GameHelper.Particles.Particles3D
{
    public class ParticleManager3D
    {
        /* Fields */

        protected List<ParticleEmitter3D> emitters;
        protected Random random;
        protected int maxParticleCount;
        protected int maxEmitterCount;
        protected Dictionary<int, ParticleGroup3D> groups;
        private int groupKey;
        private Effect defaultEffect;

        /* Properties */

        public Random Random
        {
            get
            {
                return this.random;
            }
        }
        public int MaxParticleCount
        {
            get
            {
                return this.maxParticleCount;
            }
            set
            {
                this.maxParticleCount = value;
            }
        }
        public int MaxEmitterCount
        {
            get
            {
                return this.maxEmitterCount;
            }
            set
            {
                this.maxEmitterCount = value;
            }
        }
        public int ParticlesCount
        {
            get
            {
                return this.groups.Sum(x => x.Value.ParticleCount);
            }
        }
        public int ParticlesAvailableCount
        {
            get
            {
                return this.maxParticleCount <= 0 ? int.MaxValue : this.maxParticleCount - this.ParticlesCount;
            }
        }
        public Effect DefaultEffect
        {
            get
            {
                return this.defaultEffect;
            }
            set
            {
                this.defaultEffect = value;
            }
        }
        public int GroupCount
        {
            get
            {
                return this.groups.Count;
            }
        }

        /* Constructors */

        public ParticleManager3D(Effect pDefaultEffect, Random pRandom)
        {
            this.defaultEffect = pDefaultEffect;
            this.random = pRandom ?? new Random();

            this.groups = new Dictionary<int, ParticleGroup3D>();
            this.emitters = new List<ParticleEmitter3D>();

            this.groupKey = -1;
        }
        public ParticleManager3D(Effect pDefaultEffect)
            : this(pDefaultEffect, null)
        {
        }
        public ParticleManager3D(Random pRandom)
            : this(null, pRandom)
        {
        }
        public ParticleManager3D()
            : this(null, null)
        {
        }

        /* Private methods */

        public virtual void UpdateEmitters(GameTime pGameTime)
        {
            ParticleEmitter3D emitter = null;
            for (int i = 0; i < this.emitters.Count; )
            {
                emitter = this.emitters[i];
                emitter.Update(pGameTime);

                if (emitter.IsAlive)
                {
                    i++;
                }
                else
                {
                    this.emitters.RemoveAt(i);
                }
            }
        }
        public virtual void UpdateGroups(GameTime pGameTime)
        {
            for (int i = 0; i < this.groups.Count; i++)
            {
                this.groups[i].UpdateParticles(pGameTime);
            }

            for (int i = 0; i < this.groups.Count; i++)
            {
                this.groups[i].UpdateVertices();
            }

        }

        /* Protected methods */



        /* Public methods */

        public virtual void Update(GameTime pGameTime)
        {
            this.UpdateEmitters(pGameTime);
            this.UpdateGroups(pGameTime);
        }
        public void Draw(GraphicsDevice pGraphicsDevice, Camera pCamera, Matrix pWorld)
        {
            for (int i = 0; i < groups.Count; i++)
            {
                this.groups[i].Draw(pGraphicsDevice, pCamera, pWorld);
            }
        }

        // // //

        public int AddGroup(Texture2D pTexture)
        {
            this.groups.Add(++this.groupKey, new ParticleGroup3D(this, pTexture));
            return groupKey;
        }
        public int AddGroup(Texture2D pTexture, Effect pEffect)
        {
            this.groups.Add(++this.groupKey, new ParticleGroup3D(this, pTexture, pEffect));
            return groupKey;
        }
        public int AddGroup(Texture2D pTexture, float pScale)
        {
            ParticleGroup3D group = new ParticleGroup3D(this, pTexture);
            group.Size *= pScale;
            this.groups.Add(++this.groupKey, group);
            return groupKey;
        }
        public int AddGroup(Texture2D pTexture, Vector2 pSize)
        {
            ParticleGroup3D group = new ParticleGroup3D(this, pTexture);
            group.Size = pSize;
            this.groups.Add(++this.groupKey, group);
            return groupKey;
        }
        public void AddEmitter(ParticleEmitter3D pEmitter)
        {
            if (pEmitter == null)
            {
                throw new ArgumentException("pEmitter");
            }

            if (this.maxEmitterCount > 0
                && this.emitters.Count >= this.maxEmitterCount)
            {
                throw new Exception("Exceeded the number of emitters.");
            }

            this.emitters.Add(pEmitter);
        }
        public void RemoveEmitter(ParticleEmitter3D pEmitter)
        {
            if (pEmitter == null)
            {
                throw new ArgumentException("pEmitter");
            }

            this.emitters.Remove(pEmitter);
        }
        public void RemoveAllEmitters()
        {
            this.emitters.Clear();
        }
        public void AddParticle(Particle3D pParticle, int pGroupKey)
        {
            if (pParticle == null)
            {
                throw new ArgumentException("pEmitter");
            }
            if (!this.groups.ContainsKey(pGroupKey))
            {
                throw new KeyNotFoundException();
            }

            if (this.ParticlesAvailableCount > 0)
            {
                this.groups[pGroupKey].AddParticle(pParticle);
            }
        }
        //public void RemoveParticle(Particle3D pParticle, int pGroupKey)
        //{
        //    if (pParticle == null)
        //    {
        //        throw new ArgumentException("pParticle");
        //    }
        //}

    }
}