﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace GameHelper.Particles.Particles3D
{
    public class Particle3D
    {
        /* Fields */

        protected Vector3 position;
        protected Vector3 velocity;
        protected float scale;
        protected Color color;
        protected TimeSpan lifeTime;
        protected bool isAlive;

        /* Properties */

        public virtual bool IsAlive
        {
            get
            {
                return this.isAlive;
            }
        }
        public Vector3 Position
        {
            get
            {
                return this.position;
            }
        }

        /* Constructors */

        public Particle3D(Vector3 pPosition, Vector3 pVelocity, float pScale, Color pColor, TimeSpan pLifeTime)
        {
            this.position = pPosition;
            this.velocity = pVelocity;
            this.scale = pScale;
            this.color = pColor;
            this.lifeTime = pLifeTime;
            this.isAlive = true;
        }

        /* Private methods */



        /* Protected methods */

        protected virtual void ProcessUpdate(GameTime pGameTime)
        {
            float time = (float) pGameTime.ElapsedGameTime.TotalSeconds;
            this.position += this.velocity * time;

            this.lifeTime -= pGameTime.ElapsedGameTime;
            if (this.lifeTime <= TimeSpan.Zero)
            {
                this.isAlive = false;
            }
        }

        /* Public methods */

        public virtual void Update(GameTime pGameTime)
        {
            if (this.IsAlive)
            {
                this.ProcessUpdate(pGameTime);
            }
        }

    }
}