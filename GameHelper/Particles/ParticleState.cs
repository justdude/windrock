﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Particles
{
    public enum ParticleState : byte
    {
        None = 0,
        BeforeLife = 1,
        Life = 2,
        AfterLife = 3,
        Dead = 4
    }
}