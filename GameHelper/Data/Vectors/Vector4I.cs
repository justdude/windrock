﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Data
{
    public struct Vector4I
    {
        /* Fields */

        public int A;
        public int B;
        public int C;
        public int D;

        /* Properties */



        /* Constructors */

        public Vector4I(int pA, int pB, int pC, int pD)
        {
            this.A = pA;
            this.B = pB;
            this.C = pC;
            this.D = pD;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public override bool Equals(object pObject)
        {
            bool result = false;
            if (pObject is Vector4I)
            {
                Vector4I other = (Vector4I) pObject;
                result = this == other;
            }
            return result;
        }

        public static bool operator ==(Vector4I pValue1, Vector4I pValue2)
        {
            return
                pValue1.A == pValue2.A
                && pValue1.B == pValue2.B
                && pValue1.C == pValue2.C
                && pValue1.D == pValue2.D;
        }

        public static bool operator !=(Vector4I pValue1, Vector4I pValue2)
        {
            return
                pValue1.A != pValue2.A
                || pValue1.B != pValue2.B
                || pValue1.C != pValue2.C
                || pValue1.D != pValue2.D;
        }

        public override string ToString()
        {
            return string.Format(
                "{{ {0}, {1}, {2}, {3} }}",
                this.A,
                this.B,
                this.C,
                this.D );
        }

    }
}