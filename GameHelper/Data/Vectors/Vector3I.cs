﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GameHelper.Data
{
    public struct Vector3I
    {
        /* Fields */

        public int X;
        public int Y;
        public int Z;

        /* Properties */

        public static Vector3I Zero
        {
            get
            {
                return new Vector3I(0, 0, 0);
            }
        }
        public static Vector3I UnitX
        {
            get
            {
                return new Vector3I(1, 0, 0);
            }
        }
        public static Vector3I UnitY
        {
            get
            {
                return new Vector3I(0, 1, 0);
            }
        }
        public static Vector3I UnitZ
        {
            get
            {
                return new Vector3I(0, 0, 1);
            }
        }

        /* Constructors */

        public Vector3I(int pX, int pY, int pZ)
        {
            this.X = pX;
            this.Y = pY;
            this.Z = pZ;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public override bool Equals(object pObject)
        {
            bool result = false;
            if (pObject is Vector3I)
            {
                Vector3I other = (Vector3I) pObject;
                result = this == other;
            }
            return result;
        }

        public static Vector3I operator +(Vector3I pValue1, Vector3I pValue2)
        {
            return new Vector3I(
                pValue1.X + pValue2.X,
                pValue1.Y + pValue2.Y,
                pValue1.Z + pValue2.Z);
        }
        public static Vector3I operator -(Vector3I pValue1, Vector3I pValue2)
        {
            return new Vector3I(
                pValue1.X - pValue2.X,
                pValue1.Y - pValue2.Y,
                pValue1.Z - pValue2.Z);
        }
        public static bool operator ==(Vector3I pValue1, Vector3I pValue2)
        {
            return
                pValue1.X == pValue2.X
                && pValue1.Y == pValue2.Y
                && pValue1.Z == pValue2.Z;
        }
        public static bool operator !=(Vector3I pValue1, Vector3I pValue2)
        {
            return
                pValue1.X != pValue2.X
                || pValue1.Y != pValue2.Y
                || pValue1.Z != pValue2.Z;
        }
        public static Vector3I operator -(Vector3I pVector)
        {
            return new Vector3I(-pVector.X, -pVector.Y, -pVector.Z);
        }

        public override string ToString()
        {
            return string.Format(
                "{{ {0}, {1}, {2} }}",
                this.X,
                this.Y,
                this.Z);
        }

        public override int GetHashCode()
        {
            return
                (this.X * 73856093) ^
                (this.Y * 19349663) ^
                (this.Z * 83492791);// (this.X << 16) ^ (this.Z & 0x0000FFFF) ^ ((this.Y & 0x0000FFFF) << 8);
        }

        public static implicit operator Vector3(Vector3I pValue)
        {
            return new Vector3(pValue.X, pValue.Y, pValue.Z);
        }

        public Vector3I PlusX()
        {
            return this + Vector3I.UnitX;
        }
        public Vector3I MinusX()
        {
            return this - Vector3I.UnitX;
        }
        public Vector3I PlusY()
        {
            return this + Vector3I.UnitY;
        }
        public Vector3I MinusY()
        {
            return this - Vector3I.UnitY;
        }
        public Vector3I PlusZ()
        {
            return this + Vector3I.UnitZ;
        }
        public Vector3I MinusZ()
        {
            return this - Vector3I.UnitZ;
        }

        public float Length()
        {
            return (float) Math.Sqrt(this.X * this.X + this.Y * this.Y + this.Z * this.Z);
        }
    }
}