﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Data
{
    public struct Vector2I
    {
        /* Fields */

        public int X;
        public int Y;

        /* Properties */

        public static Vector2I IncrementX
        {
            get
            {
                return new Vector2I(1, 0);
            }
        }
        public static Vector2I DecrementX
        {
            get
            {
                return new Vector2I(-1, 0);
            }
        }
        public static Vector2I IncrementY
        {
            get
            {
                return new Vector2I(0, 1);
            }
        }
        public static Vector2I DecrementY
        {
            get
            {
                return new Vector2I(0, -1);
            }
        }

        /* Constructors */

        public Vector2I(int pX, int pZ)
        {
            this.X = pX;
            this.Y = pZ;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public override bool Equals(object pObject)
        {
            bool result = false;
            if (pObject is Vector2I)
            {
                Vector2I other = (Vector2I) pObject;
                result =
                    this.X == other.X
                    && this.Y == other.Y;
            }
            return result;
        }
        public static bool operator ==(Vector2I pValue1, Vector2I pValue2)
        {
            return
                pValue1.X == pValue2.X
                && pValue1.Y == pValue2.Y;
        }
        public static bool operator !=(Vector2I pValue1, Vector2I pValue2)
        {
            return
                pValue1.X != pValue2.X
                || pValue1.Y != pValue2.Y;
        }
        public static Vector2I operator +(Vector2I pValue1, Vector2I pValue2)
        {
            return new Vector2I(
                pValue1.X + pValue2.X,
                pValue1.Y + pValue2.Y);
        }
        public static Vector2I operator -(Vector2I pValue1, Vector2I pValue2)
        {
            return new Vector2I(
                pValue1.X - pValue2.X,
                pValue1.Y - pValue2.Y);
        }
        public override int GetHashCode()
        {
            //return (this.X << 16) | (this.Z & 0x0000FFFF);
            return
                (this.X * 63061489) ^
                (this.Y * 53471161);
        }
        public override string ToString()
        {
            return string.Format(
                "({0}; {1})",
                this.X,
                this.Y);
        }

    }
}