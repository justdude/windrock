﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Data
{
    public struct Area3I
    {
        /* Fields */

        public int MinX;
        public int MinY;
        public int MinZ;
        public int MaxX;
        public int MaxY;
        public int MaxZ;

        /* Properties */

        public static Area3I Zero
        {
            get
            {
                return new Area3I(0, 0, 0, 0, 0, 0);
            }
        }

        public int LengthByX
        {
            get
            {
                return this.MaxX - this.MinX;
            }
        }
        public int LengthByY
        {
            get
            {
                return this.MaxY - this.MinY;
            }
        }
        public int LengthByZ
        {
            get
            {
                return this.MaxZ - this.MinZ;
            }
        }
        public Vector3I Size
        {
            get
            {
                return new Vector3I(
                    this.MaxX - this.MinX + 1,
                    this.MaxY - this.MinY + 1,
                    this.MaxZ - this.MinZ + 1);
            }
        }
        public Vector3I Min
        {
            get
            {
                return new Vector3I(this.MinX, this.MinY, this.MinZ);
            }
        }
        public Vector3I Max
        {
            get
            {
                return new Vector3I(this.MaxX, this.MaxY, this.MaxZ);
            }
        }

        /* Constructors */

        public Area3I(int pMinX, int pMaxX, int pMinY, int pMaxY, int pMinZ, int pMaxZ)
        {
            this.MinX = pMinX;
            this.MinY = pMinY;
            this.MinZ = pMinZ;
            this.MaxX = pMaxX;
            this.MaxY = pMaxY;
            this.MaxZ = pMaxZ;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public bool Contains(Vector3I pCoord)
        {
            return
                pCoord.X >= this.MinX && pCoord.X <= this.MaxX
                && pCoord.Y >= this.MinY && pCoord.Y <= this.MaxY
                && pCoord.Z >= this.MinZ && pCoord.Z <= this.MaxZ;
        }
        public static Area3I Union(Area3I pArea1, Area3I pArea2)
        {
            return new Area3I(
                Math.Min(pArea1.MinX, pArea2.MinX),
                Math.Max(pArea1.MaxX, pArea2.MaxX),
                Math.Min(pArea1.MinY, pArea2.MinY),
                Math.Max(pArea1.MaxY, pArea2.MaxY),
                Math.Min(pArea1.MinZ, pArea2.MinZ),
                Math.Max(pArea1.MaxZ, pArea2.MaxZ));
        }
        public static bool Intersects(Area3I pArea1, Area3I pArea2)
        {
            Area3I mergedArea = Area3I.Union(pArea1, pArea2);
            bool result =
                mergedArea.LengthByX <= pArea1.LengthByX + pArea2.LengthByX
                && mergedArea.LengthByY <= pArea1.LengthByY + pArea2.LengthByY
                && mergedArea.LengthByZ <= pArea1.LengthByZ + pArea2.LengthByZ;

            return result;
        }
        public static Area3I Intersection(Area3I pArea1, Area3I pArea2)
        {
            Area3I intersection = Area3I.Zero;

            if (Area3I.Intersects(pArea1, pArea2))
            {
                intersection = new Area3I(
                    Math.Max(pArea1.MinX, pArea2.MinX),
                    Math.Min(pArea1.MaxX, pArea2.MaxX),
                    Math.Max(pArea1.MinY, pArea2.MinY),
                    Math.Min(pArea1.MaxY, pArea2.MaxY),
                    Math.Max(pArea1.MinZ, pArea2.MinZ),
                    Math.Min(pArea1.MaxZ, pArea2.MaxZ));
            }

            return intersection;
        }

        // // //

        public bool Intersects(Area3I pArea)
        {
            return Intersects(this, pArea);
        }
        public Area3I Intersection(Area3I pArea)
        {
            return Intersection(this, pArea);
        }
        public Area3I Union(Area3I pArea)
        {
            return Union(this, pArea);
        }

        // // //

        public override string ToString()
        {
            return string.Format(
                "{{X:[{0};{1}], Y:[{2};{3}], Z:[{4};{5}]}}",
                this.MinX,
                this.MaxX,
                this.MinY,
                this.MaxY,
                this.MinZ,
                this.MaxZ);
        }
    }
}