﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.Extentions;
using Microsoft.Xna.Framework;

namespace GameHelper.Data.RandomData
{
    public class RandomVector2
    {
        /* Fields */

        private Vector2 minValue;
        private Vector2 maxValue;

        /* Properties */

        public static RandomVector2 Zero
        {
            get
            {
                return new RandomVector2(Vector2.Zero, Vector2.Zero);
            }
        }

        public Vector2 MinValue
        {
            get
            {
                return this.minValue;
            }
        }
        public Vector2 MaxValue
        {
            get
            {
                return this.maxValue;
            }
        }

        /* Constructors */

        public RandomVector2(Vector2 pMinValue, Vector2 pMaxValue)
        {
            if (!(pMinValue.X <= pMaxValue.X
                && pMinValue.Y <= pMaxValue.Y))
            {
                throw new Exception("MaxValue cannot be less then MinValue.");
            }

            this.minValue = pMinValue;
            this.maxValue = pMaxValue;
        }
        public RandomVector2(Vector2 pValue)
            : this(pValue, pValue)
        {
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public Vector2 GetValue(Random pRandom)
        {
            return pRandom.Vector2(this.minValue, this.maxValue);
        }
        public override string ToString()
        {
            return string.Format(
                "{{{0:F3}?{1:F3}}}",
                this.minValue,
                this.maxValue);
        }

    }
}