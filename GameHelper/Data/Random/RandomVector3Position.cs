﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.Extentions;
using Microsoft.Xna.Framework;

namespace GameHelper.Data.RandomData
{
    public class RandomVector3Position : RandomVector3
    {
        /* Fields */

        private Vector3 minValue;
        private Vector3 maxValue;

        /* Properties */

        public Vector3 MinValue
        {
            get
            {
                return this.minValue;
            }
        }
        public Vector3 MaxValue
        {
            get
            {
                return this.maxValue;
            }
        }

        public static RandomVector3Position Zero
        {
            get
            {
                return new RandomVector3Position(Vector3.Zero, Vector3.Zero);
            }
        }

        /* Constructors */

        public RandomVector3Position(Vector3 pMinValue, Vector3 pMaxValue)
        {
            if (!(pMinValue.X <= pMaxValue.X
                && pMinValue.Y <= pMaxValue.Y
                && pMinValue.Z <= pMaxValue.Z))
            {
                throw new Exception("MaxValue cannot be less then MinValue.");
            }

            this.minValue = pMinValue;
            this.maxValue = pMaxValue;
        }
        public RandomVector3Position(Vector3 pValue)
            : this(pValue, pValue)
        {
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public override Vector3 GetValue(Random pRandom)
        {
            return this.minValue == this.maxValue ? this.minValue : pRandom.Vector3(this.minValue, this.maxValue);
        }
        public override string ToString()
        {
            return string.Format(
                "{{{0:F3}?{1:F3}}}",
                this.minValue,
                this.maxValue);
        }

    }
}