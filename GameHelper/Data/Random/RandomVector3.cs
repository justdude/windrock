﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.Extentions;
using Microsoft.Xna.Framework;

namespace GameHelper.Data.RandomData
{
    public abstract class RandomVector3
    {
        /* Fields */



        /* Properties */



        /* Constructors */



        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public abstract Vector3 GetValue(Random pRandom);
        
    }
}