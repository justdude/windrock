﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.Extentions;

namespace GameHelper.Data.RandomData
{
    public class RandomFloat
    {
        /* Fields */

        private float minValue;
        private float maxValue;

        /* Properties */

        public static RandomFloat Zero
        {
            get
            {
                return new RandomFloat(0, 0);
            }
        }
        public static RandomFloat One
        {
            get
            {
                return new RandomFloat(1, 1);
            }
        }

        public float MinValue
        {
            get
            {
                return this.minValue;
            }
        }
        public float MaxValue
        {
            get
            {
                return this.maxValue;
            }
        }

        /* Constructors */

        public RandomFloat(float pMinValue, float pMaxValue)
        {
            if (pMinValue > pMaxValue)
            {
                throw new Exception("MaxValue cannot be less then MinValue.");
            }

            this.minValue = pMinValue;
            this.maxValue = pMaxValue;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public float GetValue(Random pRandom)
        {
            return this.minValue == this.maxValue ?
                this.minValue :
                pRandom.Float(this.minValue, this.maxValue);
        }
        public override string ToString()
        {
            return string.Format(
                "{{{0:F3}?{1:F3}}}",
                this.minValue,
                this.maxValue);
        }

    }
}