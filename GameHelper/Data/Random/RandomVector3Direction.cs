﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.Extentions;
using Microsoft.Xna.Framework;
using GameHelper.Extentions;

namespace GameHelper.Data.RandomData
{
    public class RandomVector3Direction : RandomVector3
    {
        /* Fields */

        private float minLength;
        private float maxLength;
        private float minYaw;
        private float maxYaw;
        private float minPinch;
        private float maxPinch;

        /* Properties */

        public float MinLength
        {
            get
            {
                return this.minLength;
            }
        }
        public float MaxLength
        {
            get
            {
                return this.maxLength;
            }
        }
        public float MinYaw
        {
            get
            {
                return this.minYaw;
            }
        }
        public float MaxYaw
        {
            get
            {
                return this.maxYaw;
            }
        }
        public float MinPinch
        {
            get
            {
                return this.minPinch;
            }
        }
        public float MaxPinch
        {
            get
            {
                return this.maxPinch;
            }
        }

        //public static RandomVector3Direction One
        //{
        //    get
        //    {
        //    }
        //}

        /* Constructors */

        public RandomVector3Direction(float pMinLength, float pMaxLength, float pMinYaw, float pMaxYaw, float pMinPinch, float pMaxPinch)
        {
            this.minLength = Math.Min(pMinLength, pMaxLength);
            this.maxLength = Math.Max(pMinLength, pMaxLength);
            this.minYaw = MathHelper.Min(pMinYaw, pMaxYaw);
            this.maxYaw = MathHelper.Max(pMinYaw, pMaxYaw);
            this.minPinch = MathHelper.Clamp(pMinPinch, -MathHelper.PiOver4, MathHelper.PiOver4);
            this.maxPinch = MathHelper.Clamp(pMaxPinch, -MathHelper.PiOver4, MathHelper.PiOver4);
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public override Vector3 GetValue(Random pRandom)
        {
            Vector3 vector = Vector3.Forward * pRandom.Float(this.minLength, this.maxLength);
            float yaw = pRandom.Float(this.minYaw, this.maxYaw);
            float pinch = pRandom.Float(this.minPinch, this.maxPinch);

            return Vector3.Transform(vector, Matrix.CreateFromYawPitchRoll(yaw, pinch, 0));
        }
        public override string ToString()
        {
            return string.Format(
                "{{{0:F3}?{1:F3}} & ({2:F3}?{3:F3}) & ({4:F3}?{5:F3})}",
                this.minLength,
                this.maxLength,
                this.minYaw,
                this.maxYaw,
                this.minPinch,
                this.maxPinch);
        }

        public static RandomVector3Direction CreateByLength(float pMinLength, float pMaxLength)
        {
            return new RandomVector3Direction(pMinLength, pMaxLength, 0, MathHelper.TwoPi, -MathHelper.PiOver4, MathHelper.PiOver4);
        }
        public static RandomVector3Direction CreateByLengthAndYaw(float pMinLength, float pMaxLength, float pMinYaw, float pMaxYaw)
        {
            return new RandomVector3Direction(pMinLength, pMaxLength, pMinYaw, pMaxYaw, -MathHelper.PiOver4, MathHelper.PiOver4);
        }
        public static RandomVector3Direction CreateByLengthAndPinch(float pMinLength, float pMaxLength, float pMinPinch, float pMaxPinch)
        {
            return new RandomVector3Direction(pMinLength, pMaxLength, 0, MathHelper.TwoPi, pMinPinch, pMaxPinch);
        }
    }
}