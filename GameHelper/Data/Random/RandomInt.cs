﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Data.RandomData
{
    public class RandomInt
    {
        /* Fields */

        private int minValue;
        private int maxValue;

        /* Properties */

        public int MinValue
        {
            get
            {
                return this.minValue;
            }
        }
        public int MaxValue
        {
            get
            {
                return this.maxValue;
            }
        }

        /* Constructors */

        public RandomInt(int pMinValue, int pMaxValue)
        {
            if (pMinValue > pMaxValue)
            {
                throw new Exception("MaxValue cannot be less then MinValue.");
            }

            this.minValue = pMinValue;
            this.maxValue = pMaxValue;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public int GetValue(Random pRandom)
        {
            return pRandom.Next(this.minValue, this.maxValue + 1);
        }
        public override string ToString()
        {
            return string.Format(
                "{{{0}?{1}}}",
                this.minValue,
                this.maxValue);
        }

    }
}