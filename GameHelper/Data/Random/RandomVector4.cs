﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.Extentions;
using Microsoft.Xna.Framework;

namespace GameHelper.Data.RandomData
{
    public class RandomVector4
    {
        /* Fields */

        private Vector4 minValue;
        private Vector4 maxValue;

        /* Properties */

        public static RandomVector4 Zero
        {
            get
            {
                return new RandomVector4(Vector4.Zero, Vector4.Zero);
            }
        }
        public static RandomVector4 One
        {
            get
            {
                return new RandomVector4(Vector4.One, Vector4.One);
            }
        }

        public Vector4 MinValue
        {
            get
            {
                return this.minValue;
            }
        }
        public Vector4 MaxValue
        {
            get
            {
                return this.maxValue;
            }
        }

        /* Constructors */

        public RandomVector4(Vector4 pMinValue, Vector4 pMaxValue)
        {
            if (!(pMinValue.X <= pMaxValue.X
                && pMinValue.Y <= pMaxValue.Y
                && pMinValue.Z <= pMaxValue.Z
                && pMinValue.W <= pMaxValue.W))
            {
                throw new Exception("MaxValue cannot be less then MinValue.");
            }

            this.minValue = pMinValue;
            this.maxValue = pMaxValue;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public Vector4 GetValue(Random pRandom)
        {
            return pRandom.Vector4(this.minValue, this.maxValue);
        }
        public override string ToString()
        {
            return string.Format(
                "{{{0:F3}?{1:F3}}}",
                this.minValue,
                this.maxValue);
        }

    }
}