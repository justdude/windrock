﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.Extentions;

namespace GameHelper.Data.RandomData
{
    public class RandomTimeSpan
    {
        /* Fields */

        private TimeSpan minValue;
        private TimeSpan maxValue;

        /* Properties */

        public TimeSpan MinValue
        {
            get
            {
                return this.minValue;
            }
        }
        public TimeSpan MaxValue
        {
            get
            {
                return this.maxValue;
            }
        }

        /* Constructors */

        public RandomTimeSpan(TimeSpan pMinValue, TimeSpan pMaxValue)
        {
            if (pMinValue > pMaxValue)
            {
                throw new Exception("MaxValue cannot be less then MinValue.");
            }

            this.minValue = pMinValue;
            this.maxValue = pMaxValue;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public TimeSpan GetValue(Random pRandom)
        {
            return pRandom.TimeSpan(this.minValue, this.maxValue);
        }
        public override string ToString()
        {
            return string.Format(
                "{{{0:F3}?{1:F3}}}",
                this.minValue,
                this.maxValue);
        }

    }
}