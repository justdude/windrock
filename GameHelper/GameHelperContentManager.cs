﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using System.ComponentModel.Design;
using Microsoft.Xna.Framework.Graphics;

namespace GameHelper
{
    public class GameHelperContentManager : ContentManager
    {
        /* Fields */

        private static GameHelperContentManager instance;

        /* Properties */

        public static GameHelperContentManager Instance
        {
            get
            {
                if (instance == null)
                {
                    GameHelperServiceProvider serviceProvider = new GameHelperServiceProvider();
                    serviceProvider.AddService<IGraphicsDeviceService>(new GameHelperGraphicsDeviceService());
                    instance = new GameHelperContentManager(serviceProvider, "GameHelperContent");
                }

                return instance;
            }
        }

        /* Constructors */

        private GameHelperContentManager(IServiceProvider pServiceProvider, string pRootDirectory)
            : base(pServiceProvider, pRootDirectory)
        {
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}