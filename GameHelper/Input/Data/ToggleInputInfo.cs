﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace GameHelper.Input
{
    public class ToggleInputInfo
    {
        /* Fields */

        private Keys key;
        private bool isPressed;
        private bool isToggled;
        private bool isHandled;

        /* Properties */

        public bool IsPressed
        {
            set
            {
                if (!this.isPressed && value)
                {
                    this.isToggled = !this.isToggled;
                }

                if (this.isPressed != value)
                {
                    this.isPressed = value;
                    this.isHandled = false;
                }
            }
            get
            {
                return this.isPressed;
            }
        }
        public bool IsReleased
        {
            set
            {
                if (!this.isPressed && !value)
                {
                    this.isToggled = !this.isToggled;
                }

                if (this.isPressed == value)
                {
                    this.isPressed = !value;
                    this.isHandled = false;
                }
            }
            get
            {
                return !this.isPressed;
            }
        }
        public bool IsToggled
        {
            get
            {
                return this.isToggled;
            }
            set
            {
                if (this.isToggled != value)
                {
                    this.isToggled = value;
                    this.isHandled = false;
                }
            }
        }
        public bool IsHandled
        {
            get
            {
                return this.isHandled;
            }
        }
        public bool IsToggledAndNotHundled
        {
            get
            {
                return this.isToggled && !this.isHandled;
            }
        }
        public bool IsPressedAndNotHundled
        {
            get
            {
                return this.IsPressed && !this.isHandled;
            }
        }
        public bool IsReleasedAndNotHundled
        {
            get
            {
                return !this.IsPressed && !this.isHandled;
            }
        }

        /* Constructors */

        public ToggleInputInfo(Keys pKey)
        {
            this.key = pKey;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public void Update(KeyboardState pKeyboardState)
        {
            this.IsPressed = pKeyboardState.IsKeyDown(this.key);
        }

        // // //

        public void Toggle()
        {
            this.isToggled = !this.isToggled;
            this.isHandled = false;
        }

    }
}