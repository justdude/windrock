﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Input.Data
{
    [Flags]
    public enum SpecialKeys : byte
    {
        None = 0,
        LeftShift = 1,
        RightShift = 2,
        LeftControl = 4,
        RightControl = 8,
        LeftAlt = 16,
        RightAlt = 32
    }
}