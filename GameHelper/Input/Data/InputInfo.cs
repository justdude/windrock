﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GameHelper.Input
{
    public class InputInfo
    {
        /* Fields */

        protected bool isPressed;
        protected bool isHandled;
        protected TimeSpan timeout;
        protected bool isTimeoutEnabled;

        /* Properties */

        public bool IsPressed
        {
            set
            {
                if (this.isPressed != value)
                {
                    this.isPressed = value;
                    this.isHandled = false;
                }
            }
            get
            {
                return this.isPressed;
            }
        }
        public bool IsReleased
        {
            set
            {
                if (this.isPressed == value)
                {
                    this.isPressed = !value;
                    this.isHandled = false;
                }
            }
            get
            {
                return !this.isPressed;
            }
        }
        public bool IsHandled
        {
            get
            {
                return this.isHandled;
            }
        }
        public bool IsPressedAndNotHundled
        {
            get
            {
                return this.IsPressed && !this.isHandled;
            }
        }
        public bool IsReleasedAndNotHundled
        {
            get
            {
                return !this.IsPressed && !this.isHandled;
            }
        }
        public bool IsTimeoutEnabled
        {
            get
            {
                return this.isTimeoutEnabled;
            }
            set
            {
                this.isTimeoutEnabled = value;
            }
        }
        public TimeSpan Timeout
        {
            get
            {
                return this.timeout;
            }
        }

        /* Constructors */

        public InputInfo()
        {
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public void SetHandled()
        {
            this.isHandled = true;
        }
        public void SetTimeout(TimeSpan pTimeout)
        {
            this.timeout = pTimeout;
            this.isTimeoutEnabled = true;
        }
        public void SetHandledWithTimeout(TimeSpan pTimeout)
        {
            this.isHandled = true;
            this.timeout = pTimeout;
            this.isTimeoutEnabled = true;
        }
        public void UpdateTimeout(GameTime pGameTime)
        {
            this.timeout -= pGameTime.ElapsedGameTime;
            if (this.timeout < TimeSpan.Zero)
            {
                this.timeout = TimeSpan.Zero;
                this.isTimeoutEnabled = false;
                this.isHandled = false;
            }
        }

    }
}