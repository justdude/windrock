﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Input.Data
{
    [Flags]
    public enum SwitchKeys : byte
    {
        None = 0,
        CapsLock = 1,
        NumLock = 2,
        ScrollLock = 4,

    }
}