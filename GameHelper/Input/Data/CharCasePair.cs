﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Input
{
    public class CharCasePair
    {
        /* Fields */

        public char Lower;
        public char Upper;

        /* Properties */



        /* Constructors */

        public CharCasePair(char pLower, char pUpper)
        {
            this.Lower = pLower;
            this.Upper = pUpper;
        }
        public CharCasePair(char pChar)
        {
            this.Lower = pChar;
            this.Upper = pChar;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}