﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Input
{
    [Flags]
    public enum MouseButtons : byte
    {
        None = 0,
        LeftButton = 1,
        RightButton = 2,
        MiddleButton = 4
    }
}