﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace GameHelper.Input
{
    public class KeyInputInfo : InputInfo
    {
        /* Fields */

        private Keys key;

        /* Properties */

        public Keys Key
        {
            get
            {
                return this.key;
            }
        }

        /* Constructors */

        public KeyInputInfo(Keys pKey)
        {
            this.key = pKey;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}