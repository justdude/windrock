﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace GameHelper.Input
{
    public class CustomKeyInputManager
    {
        /* Fields */

        private Dictionary<string, KeyInputInfo> keyInfoItems;

        /* Properties */

        public KeyInputInfo this[string pName]
        {
            get
            {
                return this.keyInfoItems[pName];
            }
        }
        
        /* Constructors */

        public CustomKeyInputManager()
        {
            this.keyInfoItems = new Dictionary<string, KeyInputInfo>();
        }

        /* Private methods */

        private void UpdateKey(GameTime pGameTime, KeyInputInfo pKey, KeyboardState pKeyboardState)
        {
            pKey.IsPressed = pKeyboardState.IsKeyDown(pKey.Key);
            if (pKey.IsTimeoutEnabled)
            {
                pKey.UpdateTimeout(pGameTime);
            }
        }

        /* Protected methods */



        /* Public methods */

        public void Update(GameTime pGameTime)
        {
            KeyboardState keyboardState = Keyboard.GetState();
            foreach (KeyInputInfo keyInfo in this.keyInfoItems.Values)
            {
                this.UpdateKey(pGameTime, keyInfo, keyboardState);
            }
        }

        // // //

        public void Clear()
        {
            this.keyInfoItems.Clear();
        }
        public KeyInputInfo RegisterKey(string pName, Keys pKey)
        {
            this.keyInfoItems.Add(pName, new KeyInputInfo(pKey));
            return this.keyInfoItems.Last().Value;
        }
        public KeyInputInfo GetKeyInfo(string pName)
        {
            return this.keyInfoItems[pName];
        }

    }
}