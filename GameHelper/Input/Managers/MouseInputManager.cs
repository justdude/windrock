﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using GameHelper.Data;

namespace GameHelper.Input
{
    public class MouseInputManager
    {
        /* Constants */

        private const float OneDiv120 = 1f / 120f;

        /* Fields */

        private Vector2I position;
        private InputInfo leftMouseButton;
        private InputInfo middleMouseButton;
        private InputInfo rightMouseButton;
        private float handledScrollValue;
        private float notHandledScrollValue;
        
        private MouseButtons heldButtons;
        private MouseButtons pressedButtons;
        private MouseButtons releasedButtons;

        //private Vector2I pressButtonPosition;
        //MouseButtons lastPressedButtons;

        /* Properties */

        public Vector2I Position
        {
            get
            {
                return this.position;
            }
        }
        public InputInfo LeftMouseButton
        {
            get
            {
                return this.leftMouseButton;
            }
        }
        public InputInfo MiddleMouseButton
        {
            get
            {
                return this.middleMouseButton;
            }
        }
        public InputInfo RightMouseButton
        {
            get
            {
                return this.rightMouseButton;
            }
        }

        public MouseButtons HeldButtons
        {
            get
            {
                return this.heldButtons;
            }
        }
        public MouseButtons PressedButtons
        {
            get
            {
                return this.pressedButtons;
            }
        }
        public MouseButtons ReleasedButtons
        {
            get
            {
                return this.releasedButtons;
            }
        }

        /* Constructors */

        public MouseInputManager()
        {
            this.leftMouseButton = new InputInfo();
            this.middleMouseButton = new InputInfo();
            this.rightMouseButton = new InputInfo();
        }

        /* Private methods */

        private void UpdateButtons(MouseButtons pHeldButtons)
        {
            MouseButtons temp = pHeldButtons ^ this.heldButtons;
            this.pressedButtons = temp & pHeldButtons;
            this.releasedButtons = temp & ~pHeldButtons;
            this.heldButtons = pHeldButtons;
        }

        /* Protected methods */



        /* Public methods */

        public void Update(GameTime pGameTime)
        {
            MouseState mouseState = Mouse.GetState();

            this.position = new Vector2I(mouseState.X, mouseState.Y);

            MouseButtons newHeldButtons = MouseButtons.None;

            this.leftMouseButton.IsPressed = mouseState.LeftButton == ButtonState.Pressed;
            if (this.leftMouseButton.IsPressed)
            {
                newHeldButtons |= MouseButtons.LeftButton;
            }
            if (this.leftMouseButton.IsTimeoutEnabled)
            {
                this.leftMouseButton.UpdateTimeout(pGameTime);
            }

            this.middleMouseButton.IsPressed = mouseState.MiddleButton == ButtonState.Pressed;
            if (this.middleMouseButton.IsPressed)
            {
                newHeldButtons |= MouseButtons.MiddleButton;
            }
            if (this.middleMouseButton.IsTimeoutEnabled)
            {
                this.middleMouseButton.UpdateTimeout(pGameTime);
            }

            this.rightMouseButton.IsPressed = mouseState.RightButton == ButtonState.Pressed;
            if (this.rightMouseButton.IsPressed)
            {
                newHeldButtons |= MouseButtons.RightButton;
            }
            if (this.rightMouseButton.IsTimeoutEnabled)
            {
                this.rightMouseButton.UpdateTimeout(pGameTime);
            }

            this.UpdateButtons(newHeldButtons);

            this.notHandledScrollValue = mouseState.ScrollWheelValue - this.handledScrollValue;
        }
        public void SetWheelHandled(float pNotches)
        {
            this.handledScrollValue += Math.Min(this.notHandledScrollValue, pNotches * 120f);
            this.notHandledScrollValue = 0;
        }
        public float GetWheelNotches()
        {
            return this.notHandledScrollValue * OneDiv120;
        }

    }
}