﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using GameHelper.Input.Data;

namespace GameHelper.Input
{
    public class KeyInputManager
    {
        /* Fields */

        private KeyboardState keyboardState;
        private Dictionary<Keys, CharCasePair> letterKeys;
        private ToggleInputInfo capsLockInfo;

        private bool isKeyPressed;
        private bool isKeyHandled;
        private TimeSpan timeout;
        private TimeSpan repeatDelay;
        private TimeSpan repeatTimeout;
        private Keys pressedKey;
        private bool isFirstTimePressed;

        private SpecialKeys specialKeys;

        private Keys heldKey;

        /* Properties */

        public bool IsUpperCase
        {
            get
            {
                bool isShiftPressed =
                    this.specialKeys.HasFlag(SpecialKeys.LeftShift)
                    || this.specialKeys.HasFlag(SpecialKeys.LeftShift);

                return isShiftPressed != this.capsLockInfo.IsToggled;
            }
        }
        public bool IsKeyPressedAndNotHandled
        {
            get
            {
                return this.isKeyPressed && !this.isKeyHandled;
            }
        }
        public Keys PressedKey
        {
            get
            {
                return this.pressedKey;
            }
        }
        public Keys HeldKey
        {
            get
            {
                return this.heldKey;
            }
        }

        public SpecialKeys SpecialKeys
        {
            get
            {
                return this.specialKeys;
            }
        }

        /* Constructors */

        public KeyInputManager(TimeSpan pRepeatDelay, TimeSpan pRepeatTimeout)
        {
            this.repeatDelay = pRepeatDelay;
            this.repeatTimeout = pRepeatTimeout;

            this.letterKeys = new Dictionary<Keys, CharCasePair>();
            {
                this.letterKeys.Add((Keys.A), new CharCasePair('a', 'A'));
                this.letterKeys.Add((Keys.B), new CharCasePair('b', 'B'));
                this.letterKeys.Add((Keys.C), new CharCasePair('c', 'C'));
                this.letterKeys.Add((Keys.D), new CharCasePair('d', 'D'));
                this.letterKeys.Add((Keys.E), new CharCasePair('e', 'E'));
                this.letterKeys.Add((Keys.F), new CharCasePair('f', 'F'));
                this.letterKeys.Add((Keys.G), new CharCasePair('g', 'G'));
                this.letterKeys.Add((Keys.H), new CharCasePair('h', 'H'));
                this.letterKeys.Add((Keys.I), new CharCasePair('i', 'I'));
                this.letterKeys.Add((Keys.J), new CharCasePair('j', 'J'));
                this.letterKeys.Add((Keys.K), new CharCasePair('k', 'K'));
                this.letterKeys.Add((Keys.L), new CharCasePair('l', 'L'));
                this.letterKeys.Add((Keys.M), new CharCasePair('m', 'M'));
                this.letterKeys.Add((Keys.N), new CharCasePair('n', 'N'));
                this.letterKeys.Add((Keys.O), new CharCasePair('o', 'O'));
                this.letterKeys.Add((Keys.P), new CharCasePair('p', 'P'));
                this.letterKeys.Add((Keys.Q), new CharCasePair('q', 'Q'));
                this.letterKeys.Add((Keys.R), new CharCasePair('r', 'R'));
                this.letterKeys.Add((Keys.S), new CharCasePair('s', 'S'));
                this.letterKeys.Add((Keys.T), new CharCasePair('t', 'T'));
                this.letterKeys.Add((Keys.U), new CharCasePair('u', 'U'));
                this.letterKeys.Add((Keys.V), new CharCasePair('v', 'V'));
                this.letterKeys.Add((Keys.W), new CharCasePair('w', 'W'));
                this.letterKeys.Add((Keys.X), new CharCasePair('x', 'X'));
                this.letterKeys.Add((Keys.Y), new CharCasePair('y', 'Y'));
                this.letterKeys.Add((Keys.Z), new CharCasePair('z', 'Z'));
                this.letterKeys.Add((Keys.Space), new CharCasePair(' '));
                this.letterKeys.Add((Keys.OemBackslash), new CharCasePair('/'));
                this.letterKeys.Add((Keys.OemComma), new CharCasePair(',', '<'));
                this.letterKeys.Add((Keys.OemPeriod), new CharCasePair('.', '>'));
                this.letterKeys.Add((Keys.OemQuestion), new CharCasePair('/', '?'));
                this.letterKeys.Add((Keys.OemQuotes), new CharCasePair('\'', '"'));
                this.letterKeys.Add((Keys.OemPipe), new CharCasePair('\\', '|'));
                this.letterKeys.Add((Keys.OemOpenBrackets), new CharCasePair('[', '{'));
                this.letterKeys.Add((Keys.OemCloseBrackets), new CharCasePair(']', '}'));
                this.letterKeys.Add((Keys.OemSemicolon), new CharCasePair(';', ':'));
                this.letterKeys.Add((Keys.D0), new CharCasePair('0', ')'));
                this.letterKeys.Add((Keys.D1), new CharCasePair('1', '!'));
                this.letterKeys.Add((Keys.D2), new CharCasePair('2', '@'));
                this.letterKeys.Add((Keys.D3), new CharCasePair('3', '#'));
                this.letterKeys.Add((Keys.D4), new CharCasePair('4', '$'));
                this.letterKeys.Add((Keys.D5), new CharCasePair('5', '%'));
                this.letterKeys.Add((Keys.D6), new CharCasePair('6', '^'));
                this.letterKeys.Add((Keys.D7), new CharCasePair('7', '&'));
                this.letterKeys.Add((Keys.D8), new CharCasePair('8', '*'));
                this.letterKeys.Add((Keys.D9), new CharCasePair('9', '('));
                this.letterKeys.Add((Keys.NumPad0), new CharCasePair('0'));
                this.letterKeys.Add((Keys.NumPad1), new CharCasePair('1'));
                this.letterKeys.Add((Keys.NumPad2), new CharCasePair('2'));
                this.letterKeys.Add((Keys.NumPad3), new CharCasePair('3'));
                this.letterKeys.Add((Keys.NumPad4), new CharCasePair('4'));
                this.letterKeys.Add((Keys.NumPad5), new CharCasePair('5'));
                this.letterKeys.Add((Keys.NumPad6), new CharCasePair('6'));
                this.letterKeys.Add((Keys.NumPad7), new CharCasePair('7'));
                this.letterKeys.Add((Keys.NumPad8), new CharCasePair('8'));
                this.letterKeys.Add((Keys.NumPad9), new CharCasePair('9'));
                this.letterKeys.Add((Keys.OemPlus), new CharCasePair('=', '+'));
                this.letterKeys.Add((Keys.OemMinus), new CharCasePair('-', '_'));
                this.letterKeys.Add((Keys.Add), new CharCasePair('+'));
                this.letterKeys.Add((Keys.Subtract), new CharCasePair('-'));
                this.letterKeys.Add((Keys.Multiply), new CharCasePair('*'));
                this.letterKeys.Add((Keys.Divide), new CharCasePair('/'));
                this.letterKeys.Add((Keys.Back), null);
                this.letterKeys.Add((Keys.Delete), null);
                this.letterKeys.Add((Keys.Tab), new CharCasePair('\t'));
                this.letterKeys.Add((Keys.Enter), new CharCasePair('\n'));
                this.letterKeys.Add((Keys.Left), null);
                this.letterKeys.Add((Keys.Right), null);
                this.letterKeys.Add((Keys.Up), null);
                this.letterKeys.Add((Keys.Down), null);
                this.letterKeys.Add((Keys.PageUp), null);
                this.letterKeys.Add((Keys.PageDown), null);
                //this.letterKeys.Add((Keys.Up), null);
                //this.letterKeys.Add((Keys.Down), null);
            };

            //this.keyboardState = Keyboard.GetState();

            this.capsLockInfo = new ToggleInputInfo(Keys.CapsLock);
            //this.capsLockInfo.IsToggled = this.keyboardState.IsKeyDown(Keys.CapsLock);
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public void Update(GameTime pGameTime)
        {
            this.keyboardState = Keyboard.GetState();

            this.capsLockInfo.Update(keyboardState);

            this.specialKeys = SpecialKeys.None;

            if (keyboardState.IsKeyDown(Keys.LeftShift))
            {
                this.specialKeys |= SpecialKeys.LeftShift;
            }
            if (keyboardState.IsKeyDown(Keys.RightShift))
            {
                this.specialKeys |= SpecialKeys.RightShift;
            }

            if (keyboardState.IsKeyDown(Keys.LeftControl))
            {
                this.specialKeys |= SpecialKeys.LeftControl;
            }
            if (keyboardState.IsKeyDown(Keys.RightControl))
            {
                this.specialKeys |= SpecialKeys.RightControl;
            }

            if (keyboardState.IsKeyDown(Keys.LeftAlt))
            {
                this.specialKeys |= SpecialKeys.LeftAlt;
            }
            if (keyboardState.IsKeyDown(Keys.RightAlt))
            {
                this.specialKeys |= SpecialKeys.RightAlt;
            }

            Keys newPressedKey = Keys.None;

            //foreach (Keys key in this.keyboardState.GetPressedKeys())
            //{
            //    if (this.letterKeys.ContainsKey(key))
            //    {
            //        newPressedKey = key;
            //        break;
            //    }
            //}

            Keys[] pressedKeys = this.keyboardState.GetPressedKeys();
            if (pressedKeys.Length > 0)
            {
                newPressedKey = pressedKeys[0];
            }
            this.heldKey = newPressedKey;


            if (newPressedKey == Keys.None)
            {
                if (this.isKeyPressed)
                {
                    this.isKeyPressed = false;
                    this.isKeyHandled = false;
                    this.timeout = TimeSpan.Zero;
                }
            }
            else //if(newPressedKey != Keys.None)
            {
                if (newPressedKey != this.pressedKey)
                {
                    this.isKeyPressed = true;
                    this.isKeyHandled = false;
                    this.isFirstTimePressed = true;
                    this.timeout = this.repeatDelay;
                }
                else //if (newPressedKey == this.pressedKey)
                {
                    if (this.isFirstTimePressed)
                    {
                        if (this.timeout > TimeSpan.Zero)
                        {
                            this.timeout -= pGameTime.ElapsedGameTime;
                        }
                        else
                        {
                            this.isKeyHandled = false;
                            this.isFirstTimePressed = false;
                            this.timeout += this.repeatTimeout;
                        }
                    }
                    else //if(!this.isFirstTimePressed)
                    {
                        if (this.timeout > TimeSpan.Zero)
                        {
                            this.timeout -= pGameTime.ElapsedGameTime;
                        }
                        else
                        {
                            this.isKeyHandled = false;
                            this.timeout += this.repeatTimeout;
                        }
                    }
                }
            }

            this.pressedKey = newPressedKey;
        }

        // // //

        //public CharCasePair GetPressedCharCasePair()
        //{
        //    return this.pressedKey == Keys.None ? null : this.letterKeys[this.pressedKey];
        //}
        public bool IsPressedCharKey()
        {
            if (this.pressedKey == Keys.None)
            {
                throw new InvalidOperationException("Невозможно получить символ для даного значения pressedKey.");
            }

            return this.letterKeys[this.pressedKey] != null;
        }
        public char GetPressedChar()
        {
            //if (this.pressedKey == Keys.None)
            //{
            //    throw new InvalidOperationException("Невозможно получить символ для даного значения pressedKey.");
            //}

            char pressedChar = default(char);

            if (this.pressedKey != Keys.None)
            {
                CharCasePair charCasePair = null;
                if (this.letterKeys.TryGetValue(this.pressedKey, out charCasePair)
                    && charCasePair != null)
                {
                    pressedChar = this.IsUpperCase ? charCasePair.Upper : charCasePair.Lower;
                }
            }

            return pressedChar;
        }
        public void SetPressedKeyHandled()
        {
            this.isKeyHandled = true;
        }
    }
}