﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Scripting
{
    public class DummyValueVector : DummyValue
    {
        public override int Priority
        {
            get
            {
                return 7;
            }
        }

        public DummyValueVector()
        {
        }

        public override Value CreateValue(Value pValue1, Value pValue2)
        {
            Value value = null;

            if (pValue1 is IntValue)
            {
                if (pValue2 is IntValue)
                {
                    value = new IntValueVector2(pValue1 as IntValue, pValue2 as IntValue);
                }
                else if (pValue2 is IntValueVector2)
                {
                    value = new IntValueVector3(pValue1 as IntValue, pValue2 as IntValueVector2);
                }
                else if (pValue2 is FloatValue)
                {
                    value = new FloatValueVector2(
                        (pValue1 as IntValue).ToFloatValue(),
                        pValue2 as FloatValue);
                }
                else if (pValue2 is FloatValueVector2)
                {
                    value = new FloatValueVector3(
                        (pValue1 as IntValue).ToFloatValue(),
                        pValue2 as FloatValueVector2);
                }
            }
            else if (pValue1 is FloatValue)
            {
                if (pValue2 is IntValue)
                {
                    value = new FloatValueVector2(
                        pValue1 as FloatValue,
                        (pValue2 as IntValue).ToFloatValue());
                }
                else if (pValue2 is IntValueVector2)
                {
                    value = new FloatValueVector3(
                        pValue1 as FloatValue,
                        (pValue2 as IntValueVector2).ToFloatValueVector2());
                }
                else if (pValue2 is FloatValue)
                {
                    value = new FloatValueVector2(
                        pValue1 as FloatValue,
                        pValue2 as FloatValue);
                }
                else if (pValue2 is FloatValueVector2)
                {
                    value = new FloatValueVector3(
                        pValue1 as FloatValue,
                        pValue2 as FloatValueVector2);
                }
            }

            return value;
        }
    }
}