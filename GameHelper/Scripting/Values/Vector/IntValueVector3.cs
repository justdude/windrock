﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using GameHelper.Data;

namespace GameHelper.Scripting
{
    public class IntValueVector3 : Value<Vector3I>
    {
        private IntValue intValue1;
        private IntValue intValue2;
        private IntValue intValue3;

        public IntValueVector3(IntValue pIntValue1, IntValue pIntValue2, IntValue pIntValue3)
        {
            this.intValue1 = pIntValue1;
            this.intValue2 = pIntValue2;
            this.intValue3 = pIntValue3;
        }
        public IntValueVector3(IntValue pIntValue1, IntValueVector2 pIntValueVector2)
        {
            this.intValue1 = pIntValue1;
            this.intValue2 = pIntValueVector2.IntValue1;
            this.intValue3 = pIntValueVector2.IntValue2;
        }

        public override Vector3I[] Get(CommandContext pContext)
        {
            int[] values1 = this.intValue1.Get(pContext);
            int[] values2 = this.intValue2.Get(pContext);
            int[] values3 = this.intValue3.Get(pContext);
            Vector3I[] vectors = new Vector3I[values1.Length * values2.Length * values3.Length];

            for (int i1 = 0; i1 < values1.Length; i1++)
            {
                for (int i2 = 0; i2 < values2.Length; i2++)
                {
                    for (int i3 = 0; i3 < values3.Length; i3++)
                    {
                        vectors[i1 * values1.Length * values2.Length + i2 * values2.Length + i3] = new Vector3I(values1[i1], values2[i2], values3[i3]);
                    }
                }
            }

            return vectors;
        }
        public override string ToString()
        {
            return string.Format(
                "{0};{1};{2}",
                this.intValue1.ToString(),
                this.intValue2.ToString(),
                this.intValue3.ToString());
        }
        public FloatValueVector3 ToFloatValueVector3()
        {
            return new FloatValueVector3(
                this.intValue1.ToFloatValue(),
                this.intValue2.ToFloatValue(),
                this.intValue3.ToFloatValue());
        }
    }
}