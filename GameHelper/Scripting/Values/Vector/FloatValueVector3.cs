﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace GameHelper.Scripting
{
    public class FloatValueVector3 : Value<Vector3>
    {
        private FloatValue floatValue1;
        private FloatValue floatValue2;
        private FloatValue floatValue3;

        public FloatValueVector3(FloatValue pFloatValue1, FloatValue pFloatValue2, FloatValue pFloatValue3)
        {
            this.floatValue1 = pFloatValue1;
            this.floatValue2 = pFloatValue2;
            this.floatValue3 = pFloatValue3;
        }
        public FloatValueVector3(FloatValue pFloatValue1, FloatValueVector2 pFloatValueVector2)
        {
            this.floatValue1 = pFloatValue1;
            this.floatValue2 = pFloatValueVector2.FloatValue1;
            this.floatValue3 = pFloatValueVector2.FloatValue2;
        }

        public override Vector3[] Get(CommandContext pContext)
        {
            float[] values1 = this.floatValue1.Get(pContext);
            float[] values2 = this.floatValue2.Get(pContext);
            float[] values3 = this.floatValue3.Get(pContext);
            Vector3[] vectors = new Vector3[values1.Length * values2.Length * values3.Length];

            for (int i1 = 0; i1 < values1.Length; i1++)
            {
                for (int i2 = 0; i2 < values2.Length; i2++)
                {
                    for (int i3 = 0; i3 < values3.Length; i3++)
                    {
                        vectors[i1 * values1.Length * values2.Length + i2 * values2.Length + i3] = new Vector3(values1[i1], values2[i2], values3[i3]);
                    }
                }
            }

            return vectors;
        }
        public override string ToString()
        {
            return string.Format(
                "{0};{1};{2}",
                this.floatValue1.ToString(),
                this.floatValue2.ToString(),
                this.floatValue3.ToString());
        }
    }
}