﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;


namespace GameHelper.Scripting
{
    public class FloatValueVector2 : Value<Vector2>
    {
        private FloatValue floatValue1;
        private FloatValue floatValue2;

        public FloatValue FloatValue1
        {
            get
            {
                return this.floatValue1;
            }
        }
        public FloatValue FloatValue2
        {
            get
            {
                return this.floatValue2;
            }
        }

        public FloatValueVector2(FloatValue pFloatValue1, FloatValue pFloatValue2)
        {
            this.floatValue1 = pFloatValue1;
            this.floatValue2 = pFloatValue2;
        }

        public override Vector2[] Get(CommandContext pContext)
        {
            float[] values1 = this.floatValue1.Get(pContext);
            float[] values2 = this.floatValue2.Get(pContext);
            Vector2[] vectors = new Vector2[values1.Length * values2.Length];

            for (int i = 0; i < values1.Length; i++)
            {
                for (int j = 0; j < values2.Length; j++)
                {
                    vectors[i * values1.Length + j] = new Vector2(values1[i], values2[j]);
                }
            }

            return vectors;
        }
        public override string ToString()
        {
            return string.Format(
                "{0};{1}",
                this.floatValue1.ToString(),
                this.floatValue2.ToString());
        }
    }
}