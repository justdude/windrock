﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.Data;


namespace GameHelper.Scripting
{
    public class IntValueVector2 : Value<Vector2I>
    {
        private IntValue intValue1;
        private IntValue intValue2;

        public IntValue IntValue1
        {
            get
            {
                return this.intValue1;
            }
        }
        public IntValue IntValue2
        {
            get
            {
                return this.intValue2;
            }
        }

        public IntValueVector2(IntValue pIntValue1, IntValue pIntValue2)
        {
            this.intValue1 = pIntValue1;
            this.intValue2 = pIntValue2;
        }

        public override Vector2I[] Get(CommandContext pContext)
        {
            int[] values1 = this.intValue1.Get(pContext);
            int[] values2 = this.intValue2.Get(pContext);
            Vector2I[] vectors = new Vector2I[values1.Length * values2.Length];

            for (int i = 0; i < values1.Length; i++)
            {
                for (int j = 0; j < values2.Length; j++)
                {
                    vectors[i * values1.Length + j] = new Vector2I(values1[i], values2[j]);
                }
            }

            return vectors;
        }
        public override string ToString()
        {
            return string.Format(
                "{0};{1}",
                this.intValue1.ToString(),
                this.intValue2.ToString());
        }
        public FloatValueVector2 ToFloatValueVector2()
        {
            return new FloatValueVector2(
                this.intValue1.ToFloatValue(),
                this.intValue2.ToFloatValue());
        }
    }
}