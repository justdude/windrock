﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Scripting
{
    public interface ISingleIntValue : ISingleValue<int>
    {
        ISingleFloatValue ToISingleFloatValue();
    }
}