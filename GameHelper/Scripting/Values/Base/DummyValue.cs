﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Scripting
{
    public abstract class DummyValue : Value
    {
        public abstract int Priority
        {
            get;
        }
        //public abstract bool IsMultyValue
        //{
        //    get;
        //}

        public abstract Value CreateValue(Value pValue1, Value pValue2);
    }
}