﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Scripting
{
    public abstract class Value
    {
        private static readonly DummyVariantValue DefaultDummyVariantValue = new DummyVariantValue();
        private static readonly DummyRandomValue DefaultDummyRandomValue = new DummyRandomValue();
        private static readonly DummyValueList DefaultDummyValueList = new DummyValueList();
        private static readonly DummyValueRange DefaultDummyValueRange = new DummyValueRange();
        private static readonly DummyValueVector DefaultDummyValueVector = new DummyValueVector();

        private enum ParserState : int
        {
            None = 0,
            Int = 1,
            CanBeFloat = 2,
            Float = 3,
            CanBeNegativeInt = 4,
            Word = 5
        }

        private static bool TryGetContsValue(string pValueString, ParserState pState, out Value pValue)
        {
            switch (pState)
            {
                case ParserState.Int:
                    pValue = new ConstIntValue(Int32.Parse(pValueString));
                    break;
                case ParserState.Float:
                    pValue = new ConstFloatValue(Single.Parse(pValueString));
                    break;
                case ParserState.Word:
                    pValue = new ConstWordValue(pValueString);
                    break;
                default:
                    pValue = null;
                    break;
            }

            return pValue != null;
        }

        /// <summary>
        /// Выполняет попытку схлопнуть стак до размеров одного элемента.
        /// </summary>
        /// <param name="pValueStack">Стек.</param>
        private static void CollapseStack(ref Stack<Value> pValueStack)
        {
            // Пример:
            // Int(1) Int(2) Int(3) Variant()
            // Int(1) VariantInt(Int(2) Int(3)) <-- create
            // VariantInt(Int(1) Int(2) Int(3)) <-- merge

            Stack<Value> tempStack = new Stack<Value>(pValueStack.Count);
            Value currentValue = null;
            int oldValuesCount = 0;
            //bool isCollapsed = false;

            do
            {
                oldValuesCount = pValueStack.Count();

                //isCollapsed = false;
                while (pValueStack.Count > 1)
                {
                    currentValue = pValueStack.Pop();

                    if (currentValue is DummyValue
                        && pValueStack.Count >= 2)
                    {
                        Value value2 = pValueStack.Pop();
                        Value value1 = pValueStack.Pop();
                        Value collapsedValue = (currentValue as DummyValue).CreateValue(value1, value2);

                        if (collapsedValue == null)
                        {
                            pValueStack.Push(value1);
                            pValueStack.Push(value2);
                        }
                        else
                        {
                            currentValue = null;
                            pValueStack.Push(collapsedValue);
                            //isCollapsed = true;
                        }
                    }
                    //else if (currentValue is ICompositeValue)
                    //{
                    //    Value value1 = pValueStack.Pop();
                    //    Value collapsedValue = (currentValue as ICompositeValue).Merge(value1);

                    //    if (collapsedValue == null)
                    //    {
                    //        pValueStack.Push(value1);
                    //    }
                    //    else
                    //    {
                    //        currentValue = null;
                    //        pValueStack.Push(collapsedValue);
                    //        //isCollapsed = true;
                    //    }
                    //}

                    if (currentValue != null)
                    {
                        tempStack.Push(currentValue);
                    }
                }

                while (tempStack.Count > 0)
                {
                    pValueStack.Push(tempStack.Pop());
                }
            }
            while (pValueStack.Count < oldValuesCount);
        }
        private static void PushDummyValueToValueStack(ref Stack<Value> pMainValueStack, ref Stack<DummyValue> pDummyValueStack, DummyValue pDummyValue)
        {
            if (pDummyValueStack.Count > 0)
            {
                if (pDummyValue.Priority < pDummyValueStack.Peek().Priority)
                {
                    while (pDummyValueStack.Count > 0
                        && pDummyValueStack.Peek().Priority > pDummyValue.Priority)
                    {
                        pMainValueStack.Push(pDummyValueStack.Pop());
                    }
                }

                pDummyValueStack.Push(pDummyValue);
            }
            else
            {
                pDummyValueStack.Push(pDummyValue);
            }
        }

        public static Value Parse(string pString)
        {
            Value resultValue = null;
            Value constValue = null;

            Stack<Value> mainValueStack = new Stack<Value>();
            Stack<DummyValue> dummyValueStack = new Stack<DummyValue>();
            int startIndex = 0;

            ParserState state = ParserState.None;
            bool isError = false;

            for (int index = 0; index < pString.Length && !isError; index++)
            {
                switch (pString[index])
                {
                    case '|': // Variant
                        if (index > startIndex
                            && Value.TryGetContsValue(pString.Substring(startIndex, index - startIndex), state, out constValue))
                        {
                            mainValueStack.Push(constValue);
                            Value.PushDummyValueToValueStack(ref mainValueStack, ref dummyValueStack, Value.DefaultDummyVariantValue);
                            startIndex = index + 1;
                            state = ParserState.None;
                        }
                        else
                        {
                            isError = true;
                        }
                        break;
                    case ',': // List
                        if (index > startIndex
                            && Value.TryGetContsValue(pString.Substring(startIndex, index - startIndex), state, out constValue))
                        {
                            mainValueStack.Push(constValue);
                            Value.PushDummyValueToValueStack(ref mainValueStack, ref dummyValueStack, Value.DefaultDummyValueList);
                            startIndex = index + 1;
                            state = ParserState.None;
                        }
                        else
                        {
                            isError = true;
                        }
                        break;
                    case ':': // Range
                        if (index > startIndex
                            && Value.TryGetContsValue(pString.Substring(startIndex, index - startIndex), state, out constValue))
                        {
                            mainValueStack.Push(constValue);
                            Value.PushDummyValueToValueStack(ref mainValueStack, ref dummyValueStack, Value.DefaultDummyValueRange);
                            startIndex = index + 1;
                            state = ParserState.None;
                        }
                        else
                        {
                            isError = true;
                        }
                        break;
                    case '?': // Random
                        if (index > startIndex
                            && Value.TryGetContsValue(pString.Substring(startIndex, index - startIndex), state, out constValue))
                        {
                            mainValueStack.Push(constValue);
                            Value.PushDummyValueToValueStack(ref mainValueStack, ref dummyValueStack, Value.DefaultDummyRandomValue);
                            startIndex = index + 1;
                            state = ParserState.None;
                        }
                        else
                        {
                            isError = true;
                        }
                        break;
                    case ';': // Vector
                        if (index > startIndex
                            && Value.TryGetContsValue(pString.Substring(startIndex, index - startIndex), state, out constValue))
                        {
                            mainValueStack.Push(constValue);
                            Value.PushDummyValueToValueStack(ref mainValueStack, ref dummyValueStack, Value.DefaultDummyValueVector);
                            startIndex = index + 1;
                            state = ParserState.None;
                        }
                        else
                        {
                            isError = true;
                        }
                        break;
                    default:
                        #region
                        switch (state)
                        {
                            case ParserState.None:
                                {
                                    if (Char.IsDigit(pString[index]))
                                    {
                                        //valueString += pValueString[index];
                                        state = ParserState.Int;
                                    }
                                    else if (pString[index] == '-')
                                    {
                                        //valueString += pValueString[index];
                                        state = ParserState.CanBeNegativeInt;
                                    }
                                    else if (Char.IsLetter(pString[index]))
                                    {
                                        //valueString += pValueString[index];
                                        state = ParserState.Word;
                                    }
                                    else
                                    {
                                        isError = true;
                                    }
                                }
                                break;
                            case ParserState.Int:
                                {
                                    if (Char.IsDigit(pString[index]))
                                    {
                                        //valueString += pValueString[index];
                                        //state = ParserState.IsAnInt;
                                    }
                                    else if (pString[index] == '.')
                                    {
                                        //valueString += pValueString[index];
                                        state = ParserState.CanBeFloat;
                                    }
                                    else
                                    {
                                        isError = true;
                                    }
                                }
                                break;
                            case ParserState.CanBeFloat:
                                {
                                    if (Char.IsDigit(pString[index]))
                                    {
                                        //valueString += pValueString[index];
                                        state = ParserState.Float;
                                    }
                                    else
                                    {
                                        isError = true;
                                    }
                                }
                                break;
                            case ParserState.Float:
                                {
                                    if (Char.IsDigit(pString[index]))
                                    {
                                        //valueString += pValueString[index];
                                    }
                                    else
                                    {
                                        isError = true;
                                    }
                                }
                                break;
                            case ParserState.CanBeNegativeInt:
                                {
                                    if (Char.IsDigit(pString[index]))
                                    {
                                        //valueString += pValueString[index];
                                        state = ParserState.Int;
                                    }
                                    else
                                    {
                                        isError = true;
                                    }
                                }
                                break;
                            case ParserState.Word:
                                {
                                    if (Char.IsDigit(pString[index])
                                        || Char.IsLetter(pString[index]))
                                    {
                                        //valueString += pValueString[index];
                                    }
                                    else
                                    {
                                        isError = true;
                                    }
                                }
                                break;
                        }
                        #endregion
                        break;
                }
            }

            if (!isError)
            {
                if (Value.TryGetContsValue(pString.Substring(startIndex, pString.Length - startIndex), state, out constValue))
                {
                    mainValueStack.Push(constValue);

                    while (dummyValueStack.Count > 0)
                    {
                        mainValueStack.Push(dummyValueStack.Pop());
                    }

                    // Схлопнуть весь стак в одно значение.
                    Value.CollapseStack(ref mainValueStack);

                    if (mainValueStack.Count == 1)
                    {
                        resultValue = mainValueStack.Pop();
                    }
                }
            }

            return resultValue;
        }
    }

    public abstract class Value<T> : Value
    {
        public abstract T[] Get(CommandContext pContext);
    }
}