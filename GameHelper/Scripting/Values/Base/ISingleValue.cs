﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Scripting
{
    public interface ISingleValue<T>
    {
        T GetSingle(CommandContext pContext);
    }
}