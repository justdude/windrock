﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Scripting
{
    public abstract class IntValue : Value<int>
    {
        public abstract FloatValue ToFloatValue();
    }
}