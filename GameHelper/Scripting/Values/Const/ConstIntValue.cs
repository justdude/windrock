﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Scripting
{
    public class ConstIntValue : IntValue, ISingleIntValue
    {
        private int value;

        public ConstIntValue(int pValue)
        {
            this.value = pValue;
        }

        public override int[] Get(CommandContext pContext)
        {
            return new int[]
            { 
                this.value 
            };
        }
        public int GetSingle(CommandContext pContext)
        {
            return this.Get(pContext)[0];
        }
        public override string ToString()
        {
            return this.value.ToString();
        }
        public override FloatValue ToFloatValue()
        {
            return new ConstFloatValue(this.value);
        }
        public ISingleFloatValue ToISingleFloatValue()
        {
            return new ConstFloatValue(this.value);
        }
    }
}