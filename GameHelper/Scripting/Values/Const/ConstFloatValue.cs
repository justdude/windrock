﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Scripting
{
    public class ConstFloatValue : FloatValue, ISingleFloatValue
    {
        private float value;

        public ConstFloatValue(float pValue)
        {
            this.value = pValue;
        }
        public override string ToString()
        {
            return string.Format(
                "{0}",
                this.value);
        }
        public override float[] Get(CommandContext pContext)
        {
            return new float[]
            {
                this.value
            };
        }
        public float GetSingle(CommandContext pContext)
        {
            return this.Get(pContext)[0];
        }
    }
    public class ConstWordValue : WordValue, ISingleWordValue
    {
        private string value;

        public ConstWordValue(string pValue)
        {
            this.value = pValue;
        }

        public override string[] Get(CommandContext pContext)
        {
            return new string[] { this.value };
        }
        public string GetSingle(CommandContext pContext)
        {
            return this.Get(pContext)[0];
        }
        public override string ToString()
        {
            return this.value;
        }
    }
}