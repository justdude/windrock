﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Scripting
{
    public class FloatRandomValue : FloatValue, ICompositeValue, ISingleFloatValue
    {
        private ISingleValue<float> minFloatValue;
        private ISingleValue<float> maxFloatValue;

        public FloatRandomValue(ISingleValue<float> pMinFloatValue, ISingleValue<float> pMaxFloatValue)
        {
            this.minFloatValue = pMinFloatValue;
            this.maxFloatValue = pMaxFloatValue;
        }

        public override float[] Get(CommandContext pContext)
        {
            float[] resultValues = null;

            float minValue = this.minFloatValue.GetSingle(pContext);
            float maxValue = this.maxFloatValue.GetSingle(pContext);
            if (maxValue > minValue)
            {
                resultValues = new float[] 
                { 
                    minValue + (float)pContext.Random.NextDouble()* maxValue
                };
            }
            else
            {
                resultValues = new float[0];
            }

            return resultValues;
        }
        public float GetSingle(CommandContext pContext)
        {
            return this.Get(pContext)[0];
        }
        public override string ToString()
        {
            return string.Format(
                "{0}:{1}",
                this.minFloatValue.ToString(),
                this.maxFloatValue.ToString());
        }
    }
}