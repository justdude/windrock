﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Scripting
{
    public class IntRandomValue : IntValue, ICompositeValue, ISingleIntValue
    {
        private ISingleIntValue minIntValue;
        private ISingleIntValue maxIntValue;

        public IntRandomValue(ISingleIntValue pMinIntValue, ISingleIntValue pMaxIntValue)
        {
            this.minIntValue = pMinIntValue;
            this.maxIntValue = pMaxIntValue;
        }

        public override int[] Get(CommandContext pContext)
        {
            int[] resultValues = null;

            int minValue = this.minIntValue.GetSingle(pContext);
            int maxValue = this.maxIntValue.GetSingle(pContext);
            if (maxValue > minValue)
            {
                resultValues = new int[] 
                { 
                    pContext.Random.Next(minValue, maxValue + 1) 
                };
            }
            else
            {
                resultValues = new int[0];
            }

            return resultValues;
        }
        public int GetSingle(CommandContext pContext)
        {
            return this.Get(pContext)[0];
        }
        public override string ToString()
        {
            return string.Format(
                "{0}:{1}",
                this.minIntValue.ToString(),
                this.maxIntValue.ToString());
        }
        public override FloatValue ToFloatValue()
        {
            return new FloatRandomValue(
                (this.minIntValue as ISingleIntValue).ToISingleFloatValue(),
                (this.maxIntValue as ISingleIntValue).ToISingleFloatValue());
        }
        public ISingleFloatValue ToISingleFloatValue()
        {
            return new FloatRandomValue(
                this.minIntValue.ToISingleFloatValue(),
                this.maxIntValue.ToISingleFloatValue());
        }
    }
}