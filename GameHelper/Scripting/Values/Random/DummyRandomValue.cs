﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Scripting
{
    public class DummyRandomValue : DummyValue
    {
        public override int Priority
        {
            get
            {
                return 11;
            }
        }

        public DummyRandomValue()
        {
        }

        public override Value CreateValue(Value pValue1, Value pValue2)
        {
            Value value = null;

            if (pValue1 is ISingleIntValue)
            {
                if (pValue2 is ISingleIntValue)
                {
                    value = new IntRandomValue(pValue1 as ISingleIntValue, pValue2 as ISingleIntValue);
                }
                if (pValue2 is ISingleFloatValue)
                {
                    value = new FloatRandomValue(
                        (pValue1 as ISingleIntValue).ToISingleFloatValue(),
                        pValue2 as ISingleFloatValue);
                }
            }
            else if (pValue1 is ISingleFloatValue)
            {
                if (pValue2 is ISingleIntValue)
                {
                    value = new FloatRandomValue(
                        pValue1 as ISingleFloatValue,
                        (pValue2 as ISingleIntValue).ToISingleFloatValue());
                }
                if (pValue2 is ISingleFloatValue)
                {
                    value = new FloatRandomValue(
                        pValue1 as ISingleFloatValue,
                        pValue2 as ISingleFloatValue);
                }
            }

            return value;
        }
    }
}