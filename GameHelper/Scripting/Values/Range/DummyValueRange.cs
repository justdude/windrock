﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Scripting
{
    public class DummyValueRange : DummyValue
    {
        public override int Priority
        {
            get
            {
                return 8;
            }
        }

        public DummyValueRange()
        {
        }

        public override Value CreateValue(Value pValue1, Value pValue2)
        {
            Value value = null;

            if (pValue1 is ISingleIntValue
                && pValue2 is ISingleIntValue)
            {
                value = new IntValueRange(pValue1 as ISingleIntValue, pValue2 as ISingleIntValue);
            }

            return value;
        }
    }
}