﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Scripting
{
    public class IntValueRange : IntValue, ICompositeValue
    {

        private ISingleIntValue minIntValue;
        private ISingleIntValue maxIntValue;

        public IntValueRange(ISingleIntValue pMinIntValue, ISingleIntValue pMaxIntValue)
        {
            this.minIntValue = pMinIntValue;
            this.maxIntValue = pMaxIntValue;
        }

        public override int[] Get(CommandContext pContext)
        {
            int[] resultValues = null;

            int minValue = this.minIntValue.GetSingle(pContext);
            int maxValue = this.maxIntValue.GetSingle(pContext);
            if (maxValue > minValue)
            {
                int resultValuesLength = maxValue - minValue + 1;
                resultValues = new int[resultValuesLength];
                for (int i = 0; i < resultValuesLength; i++)
                {
                    resultValues[i] = minValue + i;
                }
            }
            else
            {
                resultValues = new int[0];
            }

            return resultValues;
        }
        public override string ToString()
        {
            return string.Format(
                "{0}:{1}",
                this.minIntValue.ToString(),
                this.maxIntValue.ToString());
        }
        public override FloatValue ToFloatValue()
        {
            return null;
        }
    }
}