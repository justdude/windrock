﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace GameHelper.Scripting
{
    public class IntValueList : IntValue, ICompositeValue
    {
        private IntValue[] intValues;

        public IntValueList(IntValue pIntValue1, IntValue pIntValue2)
        {
            this.intValues = new IntValue[]
            {
                pIntValue1,
                pIntValue2
            };
        }
        public IntValueList(IntValue pIntValue, IntValueList pIntListValue)
        {
            this.intValues = new IntValue[pIntListValue.intValues.Length + 1];
            this.intValues[0] = pIntValue;
            pIntListValue.intValues.CopyTo(this.intValues, 1);
        }
        public IntValueList(IntValueList pIntListValue, IntValue pIntValue)
        {
            this.intValues = new IntValue[pIntListValue.intValues.Length + 1];
            pIntListValue.intValues.CopyTo(this.intValues, 0);
            this.intValues[this.intValues.Length - 1] = pIntValue;
        }
        public IntValueList(IntValueList pIntListValue1, IntValueList pIntListValue2)
        {
            this.intValues = new IntValue[pIntListValue1.intValues.Length + pIntListValue2.intValues.Length];
            pIntListValue1.intValues.CopyTo(this.intValues, 0);
            pIntListValue2.intValues.CopyTo(this.intValues, pIntListValue1.intValues.Length);
        }

        public override int[] Get(CommandContext pContext)
        {
            List<int> resultList = new List<int>();
            for (int i = 0; i < this.intValues.Length; i++)
            {
                resultList.AddRange(this.intValues[i].Get(pContext));
            }

            return resultList.ToArray<int>();
        }
        public int GetSingle(CommandContext pContext)
        {
            return this.Get(pContext)[0];
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < this.intValues.Length - 1; i++)
            {
                stringBuilder.AppendFormat(
                    "{0} ",
                    this.intValues[i].ToString());
            }
            stringBuilder.Append(intValues[intValues.Length - 1].ToString());

            return stringBuilder.ToString();
        }
        public override FloatValue ToFloatValue()
        {
            FloatValue[] floatValues = new FloatValue[this.intValues.Length];
            for (int i = 0; i < this.intValues.Length; i++)
            {
                floatValues[i] = this.intValues[i].ToFloatValue();
            }

            return new FloatValueList(floatValues);
        }
    }
}