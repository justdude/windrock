﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Scripting
{
    public class DummyValueList : DummyValue
    {
        public override int Priority
        {
            get
            {
                return 5;
            }
        }
        //public override bool IsMultyValue
        //{
        //    get
        //    {
        //        return true;
        //    }
        //}

        public DummyValueList()
        {
        }

        public override Value CreateValue(Value pValue1, Value pValue2)
        {
            Value value = null;

            if (pValue1 != null && pValue2 != null)
            {
                if (pValue1 is WordValue
                    && pValue2 is WordValue)
                {
                    // TODO: Create WordValueList.
                }
                if (pValue1 is IntValue)
                {
                    if (pValue2 is IntValue)
                    {
                        // Create IntValueList.
                        #region ...
                        if (pValue1 is IntValueList)
                        {
                            if (pValue2 is IntValueList)
                            {
                                value = new IntValueList(pValue1 as IntValueList, pValue2 as IntValueList);
                            }
                            else //if (pValue2 is IntValue)
                            {
                                value = new IntValueList(pValue1 as IntValueList, pValue2 as IntValue);
                            }
                        }
                        else //if (pValue1 is IntValue)
                        {
                            if (pValue2 is IntValueList)
                            {
                                value = new IntValueList(pValue1 as IntValue, pValue2 as IntValueList);
                            }
                            else //if (pValue2 is IntValue)
                            {
                                value = new IntValueList(pValue1 as IntValue, pValue2 as IntValue);
                            }
                        }
                        #endregion
                    }
                    else if (pValue2 is FloatValue)
                    {
                        // Create FloatValueList.
                        #region ...
                        if (pValue1 is IntValueList)
                        {
                            if (pValue2 is FloatValueList)
                            {
                                value = new FloatValueList(
                                    (pValue1 as IntValueList).ToFloatValue(),
                                    pValue2 as FloatValueList);
                            }
                            else //if (pValue2 is FloatValue)
                            {
                                value = new FloatValueList(
                                    (pValue1 as IntValueList).ToFloatValue(),
                                    pValue2 as FloatValue);
                            }
                        }
                        else //if (pValue1 is IntValue)
                        {
                            if (pValue2 is FloatValueList)
                            {
                                value = new FloatValueList(
                                    (pValue1 as IntValue).ToFloatValue(),
                                    pValue2 as FloatValueList);
                            }
                            else //if (pValue2 is FloatValue)
                            {
                                value = new FloatValueList(
                                    (pValue1 as IntValue).ToFloatValue(),
                                    pValue2 as FloatValue);
                            }
                        }
                        #endregion
                    }
                }
                else if (pValue1 is FloatValue)
                {
                    if (pValue2 is IntValue)
                    {
                        // Create FloatValueList.
                        #region ...
                        if (pValue1 is FloatValueList)
                        {
                            if (pValue2 is IntValueList)
                            {
                                value = new FloatValueList(
                                    pValue1 as FloatValueList,
                                    (pValue2 as IntValueList).ToFloatValue());
                            }
                            else //if (pValue2 is IntValue)
                            {
                                value = new FloatValueList(
                                    pValue1 as FloatValueList,
                                    (pValue2 as IntValue).ToFloatValue());
                            }
                        }
                        else //if (pValue1 is FloatValue)
                        {
                            if (pValue2 is IntValueList)
                            {
                                value = new FloatValueList(
                                    pValue1 as FloatValue,
                                    (pValue2 as IntValueList).ToFloatValue());
                            }
                            else //if (pValue2 is IntValue)
                            {
                                value = new FloatValueList(
                                    pValue1 as FloatValue,
                                    (pValue2 as IntValue).ToFloatValue());
                            }
                        }
                        #endregion
                    }
                    else if (pValue2 is FloatValue)
                    {
                        // Create FloatValueList.
                        #region ...
                        if (pValue1 is FloatValueList)
                        {
                            if (pValue2 is FloatValueList)
                            {
                                value = new FloatValueList(pValue1 as FloatValueList, pValue2 as FloatValueList);
                            }
                            else //if (pValue2 is FloatValue)
                            {
                                value = new FloatValueList(pValue1 as FloatValueList, pValue2 as FloatValue);
                            }
                        }
                        else //if (pValue1 is FloatValue)
                        {
                            if (pValue2 is FloatValueList)
                            {
                                value = new FloatValueList(pValue1 as FloatValue, pValue2 as FloatValueList);
                            }
                            else //if (pValue2 is FloatValue)
                            {
                                value = new FloatValueList(pValue1 as FloatValue, pValue2 as FloatValue);
                            }
                        }
                        #endregion
                    }
                }
            }

            return value;
        }
    }
}