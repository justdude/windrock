﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace GameHelper.Scripting
{
    public class FloatValueList : FloatValue, ICompositeValue
    {
        private FloatValue[] floatValues;

        public FloatValueList(FloatValue pFloatValue1, FloatValue pFloatValue2)
        {
            this.floatValues = new FloatValue[]
            {
                pFloatValue1,
                pFloatValue2
            };
        }
        public FloatValueList(FloatValue pFloatValue, FloatValueList pFloatListValue)
        {
            this.floatValues = new FloatValue[pFloatListValue.floatValues.Length + 1];
            this.floatValues[0] = pFloatValue;
            pFloatListValue.floatValues.CopyTo(this.floatValues, 1);
        }
        public FloatValueList(FloatValueList pFloatListValue, FloatValue pFloatValue)
        {
            this.floatValues = new FloatValue[pFloatListValue.floatValues.Length + 1];
            pFloatListValue.floatValues.CopyTo(this.floatValues, 0);
            this.floatValues[this.floatValues.Length - 1] = pFloatValue;
        }
        public FloatValueList(FloatValueList pFloatListValue1, FloatValueList pFloatListValue2)
        {
            this.floatValues = new FloatValue[pFloatListValue1.floatValues.Length + pFloatListValue2.floatValues.Length];
            pFloatListValue1.floatValues.CopyTo(this.floatValues, 0);
            pFloatListValue2.floatValues.CopyTo(this.floatValues, pFloatListValue1.floatValues.Length);
        }
        public FloatValueList(FloatValue[] pFloatValues)
        {
            this.floatValues = pFloatValues;
        }

        public override float[] Get(CommandContext pContext)
        {
            List<float> resultList = new List<float>();
            for (int i = 0; i < this.floatValues.Length; i++)
            {
                resultList.AddRange(this.floatValues[i].Get(pContext));
            }

            return resultList.ToArray<float>();
        }
        public float GetSingle(CommandContext pContext)
        {
            return this.Get(pContext)[0];
        }
        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < this.floatValues.Length - 1; i++)
            {
                stringBuilder.AppendFormat(
                    "{0} ",
                    this.floatValues[i].ToString());
            }
            stringBuilder.Append(floatValues[floatValues.Length - 1].ToString());

            return stringBuilder.ToString();
        }
    }
}