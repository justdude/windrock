﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Scripting
{
    public class VariantIntValue : IntValue, ICompositeValue, ISingleIntValue
    {
        private IntValue[] intValues;

        //public IntValue[] IntValues
        //{
        //    get
        //    {
        //        return this.intValues;
        //    }
        //}

        public VariantIntValue(IntValue pIntValue1, IntValue pIntValue2)
        {
            this.intValues = new IntValue[]
            {
                pIntValue1,
                pIntValue2
            };
        }
        public VariantIntValue(IntValue pIntValue, VariantIntValue pIntVariantValue)
        {
            this.intValues = new IntValue[pIntVariantValue.intValues.Length + 1];
            this.intValues[0] = pIntValue;
            pIntVariantValue.intValues.CopyTo(this.intValues, 1);
        }
        public VariantIntValue(VariantIntValue pIntVariantValue, IntValue pIntValue)
        {
            this.intValues = new IntValue[pIntVariantValue.intValues.Length + 1];
            pIntVariantValue.intValues.CopyTo(this.intValues, 0);
            this.intValues[this.intValues.Length - 1] = pIntValue;
        }
        public VariantIntValue(VariantIntValue pIntVariantValue1, VariantIntValue pIntVariantValue2)
        {
            this.intValues = new IntValue[pIntVariantValue1.intValues.Length + pIntVariantValue2.intValues.Length];
            pIntVariantValue1.intValues.CopyTo(this.intValues, 0);
            pIntVariantValue2.intValues.CopyTo(this.intValues, pIntVariantValue1.intValues.Length);
        }

        public override int[] Get(CommandContext pContext)
        {
            return this.intValues[pContext.Random.Next(this.intValues.Length)].Get(pContext);
        }
        public int GetSingle(CommandContext pContext)
        {
            return this.Get(pContext)[0];
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < this.intValues.Length - 1; i++)
            {
                stringBuilder.AppendFormat(
                    "{0}|",
                    this.intValues[i].ToString());
            }
            stringBuilder.Append(intValues[intValues.Length - 1].ToString());

            return stringBuilder.ToString();
        }

        public override FloatValue ToFloatValue()
        {
            FloatValue[] floatValues = new FloatValue[this.intValues.Length];
            for (int i = 0; i < this.intValues.Length; i++)
            {
                floatValues[i] = this.intValues[i].ToFloatValue();
            }

            return new VariantFloatValue(floatValues);
        }
        public ISingleFloatValue ToISingleFloatValue()
        {
            FloatValue[] floatValues = new FloatValue[this.intValues.Length];
            for (int i = 0; i < this.intValues.Length; i++)
            {
                floatValues[i] = this.intValues[i].ToFloatValue();
            }

            return new VariantFloatValue(floatValues);
        }
    }
}