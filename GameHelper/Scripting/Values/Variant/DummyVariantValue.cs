﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Scripting
{
    public class DummyVariantValue : DummyValue
    {
        public override int Priority
        {
            get
            {
                return 10;
            }
        }

        public DummyVariantValue()
        {
        }

        public override Value CreateValue(Value pValue1, Value pValue2)
        {
            Value value = null;

            if (pValue1 != null && pValue2 != null)
            {
                if (pValue1 is WordValue && pValue2 is WordValue)
                {
                    // Create VariantWorldValue.
                    if (pValue1 is VariantWordValue)
                    {
                        if (pValue2 is VariantWordValue)
                        {
                            value = new VariantWordValue(pValue1 as VariantWordValue, pValue2 as VariantWordValue);
                        }
                        else //if (pValue2 is WordValue)
                        {
                            value = new VariantWordValue(pValue1 as VariantWordValue, pValue2 as WordValue);
                        }
                    }
                    else //if (pValue1 is WordValue)
                    {
                        if (pValue2 is VariantWordValue)
                        {
                            value = new VariantWordValue(pValue1 as WordValue, pValue2 as VariantWordValue);
                        }
                        else //if (pValue2 is WordValue)
                        {
                            value = new VariantWordValue(pValue1 as WordValue, pValue2 as WordValue);
                        }
                    }
                }
                if (pValue1 is IntValue)
                {
                    if (pValue2 is IntValue)
                    {
                        // Create VariantIntValue.
                        #region ...
                        if (pValue1 is VariantIntValue)
                        {
                            if (pValue2 is VariantIntValue)
                            {
                                value = new VariantIntValue(pValue1 as VariantIntValue, pValue2 as VariantIntValue);
                            }
                            else //if (pValue2 is IntValue)
                            {
                                value = new VariantIntValue(pValue1 as VariantIntValue, pValue2 as IntValue);
                            }
                        }
                        else //if (pValue1 is IntValue)
                        {
                            if (pValue2 is VariantIntValue)
                            {
                                value = new VariantIntValue(pValue1 as IntValue, pValue2 as VariantIntValue);
                            }
                            else //if (pValue2 is IntValue)
                            {
                                value = new VariantIntValue(pValue1 as IntValue, pValue2 as IntValue);
                            }
                        }
                        #endregion
                    }
                    else if (pValue2 is FloatValue)
                    {
                        // Create VariantFloatValue.
                        #region ...
                        if (pValue1 is VariantIntValue)
                        {
                            if (pValue2 is VariantFloatValue)
                            {
                                value = new VariantFloatValue(
                                    (pValue1 as VariantIntValue).ToFloatValue(),
                                    pValue2 as VariantFloatValue);
                            }
                            else //if (pValue2 is FloatValue)
                            {
                                value = new VariantFloatValue(
                                    (pValue1 as VariantIntValue).ToFloatValue(),
                                    pValue2 as FloatValue);
                            }
                        }
                        else //if (pValue1 is IntValue)
                        {
                            if (pValue2 is VariantFloatValue)
                            {
                                value = new VariantFloatValue(
                                    (pValue1 as IntValue).ToFloatValue(),
                                    pValue2 as VariantFloatValue);
                            }
                            else //if (pValue2 is FloatValue)
                            {
                                value = new VariantFloatValue(
                                    (pValue1 as IntValue).ToFloatValue(),
                                    pValue2 as FloatValue);
                            }
                        }
                        #endregion
                    }
                }
                else if (pValue1 is FloatValue)
                {
                    if (pValue2 is FloatValue)
                    {
                        // Create VariantFloatValue.
                        #region ...
                        if (pValue1 is VariantFloatValue)
                        {
                            if (pValue2 is VariantFloatValue)
                            {
                                value = new VariantFloatValue(pValue1 as VariantFloatValue, pValue2 as VariantFloatValue);
                            }
                            else //if (pValue2 is FloatValue)
                            {
                                value = new VariantFloatValue(pValue1 as VariantFloatValue, pValue2 as FloatValue);
                            }
                        }
                        else //if (pValue1 is FloatValue)
                        {
                            if (pValue2 is VariantFloatValue)
                            {
                                value = new VariantFloatValue(pValue1 as FloatValue, pValue2 as VariantFloatValue);
                            }
                            else //if (pValue2 is FloatValue)
                            {
                                value = new VariantFloatValue(pValue1 as FloatValue, pValue2 as FloatValue);
                            }
                        }
                        #endregion
                    }
                    else if (pValue2 is IntValue)
                    {
                        // Create VariantFloatValue.
                        #region ...
                        if (pValue1 is VariantFloatValue)
                        {
                            if (pValue2 is VariantIntValue)
                            {
                                value = new VariantFloatValue(
                                    pValue1 as VariantFloatValue,
                                    (pValue2 as VariantIntValue).ToFloatValue());
                            }
                            else //if (pValue2 is IntValue)
                            {
                                value = new VariantFloatValue(
                                    pValue1 as VariantFloatValue,
                                    (pValue2 as IntValue).ToFloatValue());
                            }
                        }
                        else //if (pValue1 is FloatValue)
                        {
                            if (pValue2 is VariantIntValue)
                            {
                                value = new VariantFloatValue(
                                    pValue1 as FloatValue,
                                    (pValue2 as VariantIntValue).ToFloatValue());
                            }
                            else //if (pValue2 is IntValue)
                            {
                                value = new VariantFloatValue(
                                    pValue1 as FloatValue,
                                    (pValue2 as IntValue).ToFloatValue());
                            }
                        }
                        #endregion
                    }
                }
            }

            return value;
        }
    }
}