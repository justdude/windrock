﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Scripting
{
    public class VariantWordValue : WordValue, ICompositeValue, ISingleWordValue
    {
        private WordValue[] stringValues;

        public VariantWordValue(WordValue pWordValue1, WordValue pWordValue2)
        {
            this.stringValues = new WordValue[]
            {
                pWordValue1,
                pWordValue2
            };
        }
        public VariantWordValue(WordValue pWordValue, VariantWordValue pWordVariantValue)
        {
            this.stringValues = new WordValue[pWordVariantValue.stringValues.Length + 1];
            this.stringValues[0] = pWordValue;
            pWordVariantValue.stringValues.CopyTo(this.stringValues, 1);
        }
        public VariantWordValue(VariantWordValue pWordVariantValue, WordValue pWordValue)
        {
            this.stringValues = new WordValue[pWordVariantValue.stringValues.Length + 1];
            pWordVariantValue.stringValues.CopyTo(this.stringValues, 0);
            this.stringValues[this.stringValues.Length - 1] = pWordValue;
        }
        public VariantWordValue(VariantWordValue pWordVariantValue1, VariantWordValue pWordVariantValue2)
        {
            this.stringValues = new WordValue[pWordVariantValue1.stringValues.Length + pWordVariantValue2.stringValues.Length];
            pWordVariantValue1.stringValues.CopyTo(this.stringValues, 0);
            pWordVariantValue2.stringValues.CopyTo(this.stringValues, pWordVariantValue1.stringValues.Length);
        }
        public VariantWordValue(WordValue[] pWordValues)
        {
            this.stringValues = pWordValues;
        }

        public override string[] Get(CommandContext pContext)
        {
            return this.stringValues[pContext.Random.Next(this.stringValues.Length)].Get(pContext);
        }
        public string GetSingle(CommandContext pContext)
        {
            return this.Get(pContext)[0];
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < this.stringValues.Length - 1; i++)
            {
                stringBuilder.AppendFormat(
                    "{0}|",
                    this.stringValues[i]);
            }
            stringBuilder.Append(stringValues[stringValues.Length - 1]);

            return stringBuilder.ToString();
        }
    }
}