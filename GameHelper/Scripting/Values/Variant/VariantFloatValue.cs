﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Scripting
{
    public class VariantFloatValue : FloatValue, ICompositeValue, ISingleFloatValue
    {
        private FloatValue[] floatValues;

        public VariantFloatValue(FloatValue pFloatValue1, FloatValue pFloatValue2)
        {
            this.floatValues = new FloatValue[]
            {
                pFloatValue1,
                pFloatValue2
            };
        }
        public VariantFloatValue(FloatValue pFloatValue, VariantFloatValue pFloatVariantValue)
        {
            this.floatValues = new FloatValue[pFloatVariantValue.floatValues.Length + 1];
            this.floatValues[0] = pFloatValue;
            pFloatVariantValue.floatValues.CopyTo(this.floatValues, 1);
        }
        public VariantFloatValue(VariantFloatValue pFloatVariantValue, FloatValue pFloatValue)
        {
            this.floatValues = new FloatValue[pFloatVariantValue.floatValues.Length + 1];
            pFloatVariantValue.floatValues.CopyTo(this.floatValues, 0);
            this.floatValues[this.floatValues.Length - 1] = pFloatValue;
        }
        public VariantFloatValue(VariantFloatValue pFloatVariantValue1, VariantFloatValue pFloatVariantValue2)
        {
            this.floatValues = new FloatValue[pFloatVariantValue1.floatValues.Length + pFloatVariantValue2.floatValues.Length];
            pFloatVariantValue1.floatValues.CopyTo(this.floatValues, 0);
            pFloatVariantValue2.floatValues.CopyTo(this.floatValues, pFloatVariantValue1.floatValues.Length);
        }
        public VariantFloatValue(FloatValue[] pFloatValues)
        {
            this.floatValues = pFloatValues;
        }

        public override float[] Get(CommandContext pContext)
        {
            return this.floatValues[pContext.Random.Next(this.floatValues.Length)].Get(pContext);
        }
        public float GetSingle(CommandContext pContext)
        {
            return this.Get(pContext)[0];
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < this.floatValues.Length - 1; i++)
            {
                stringBuilder.AppendFormat(
                    "{0}|",
                    this.floatValues[i].ToString());
            }
            stringBuilder.Append(floatValues[floatValues.Length - 1].ToString());

            return stringBuilder.ToString();
        }
    }
}