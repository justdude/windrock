﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GameHelper.Scripting
{
    public class CommandContext
    {
        protected Random random;

        public Random Random
        {
            get
            {
                return this.random;
            }
        }
        
        public CommandContext(Random pRandom)
        {
            this.random = pRandom;
        }
    }
}