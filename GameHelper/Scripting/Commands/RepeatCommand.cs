﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Scripting
{
    public class RepeatCommand : Command
    {
        private Command command;
        private ISingleIntValue commandRepetitionsValue;

        public RepeatCommand(Command pCommand, ISingleIntValue pCommandRepetitionsValue)
        {
            this.command = pCommand;
            this.commandRepetitionsValue = pCommandRepetitionsValue;
        }

        public override CommandResult Execute(CommandContext pContext)
        {
            CommandResult result = CommandResult.Completed();

            int commandRepetitions = this.commandRepetitionsValue.GetSingle(pContext);
            for (int i = 0; i < commandRepetitions; i++)
            {
                if (this.command.Execute(pContext).Status == CommandStatus.Failed)
                {
                    result = CommandResult.Failed();
                    break;
                }
            }

            return result;
        }
    }
}