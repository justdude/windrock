﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Scripting
{
    public enum CommandStatus : byte
    {
        Undefined = 0,
        Completed = 1,
        InProgress = 2,
        Failed = 3
    }

    public struct CommandResult
    {
        /* Fields */

        private CommandStatus status;
        private string message;

        private static readonly CommandResult defaultCompletedCommandResult = new CommandResult(CommandStatus.Completed, "Command completed.");
        private static readonly CommandResult defaultInProgressCommandResult = new CommandResult(CommandStatus.InProgress, "Command in progress.");
        private static readonly CommandResult defaultFailedCommandResult = new CommandResult(CommandStatus.Completed, "Command failed.");

        /* Properties */

        public CommandStatus Status
        {
            get
            {
                return this.status;
            }
        }
        
        /* Constructors */

        private CommandResult(CommandStatus pStatus, string pMessage)
        {
            this.status = pStatus;
            this.message = pMessage;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public static CommandResult Completed(string pMessage)
        {
            return new CommandResult(CommandStatus.Completed, pMessage);
        }
        public static CommandResult Completed()
        {
            return defaultCompletedCommandResult;
        }
        public static CommandResult InProgress(string pMessage)
        {
            return new CommandResult(CommandStatus.InProgress, pMessage);
        }
        public static CommandResult InProgress()
        {
            return defaultInProgressCommandResult;
        }
        public static CommandResult Failed(string pMessage)
        {
            return new CommandResult(CommandStatus.Failed, pMessage);
        }
        public static CommandResult Failed()
        {
            return defaultFailedCommandResult;
        }

    }
}