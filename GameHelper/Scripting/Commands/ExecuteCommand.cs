﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.Data;
using Microsoft.Xna.Framework;

namespace GameHelper.Scripting
{
    public class ExecuteCommand : Command
    {
        private Value[] values;

        public ExecuteCommand(Value[] pValues)
        {
            this.values = pValues;
        }

        public override CommandResult Execute(CommandContext pContext)
        {
            CommandResult result = CommandResult.Completed();

            for (int valueIndex = 0; valueIndex < this.values.Length; valueIndex++)
            {
                if (this.values[valueIndex] is IntValue)
                {
                    IntValue intValue = this.values[valueIndex] as IntValue;
                    int[] results = intValue.Get(pContext);
                    if (results.Length > 0)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        for (int i = 0; i < results.Length - 1; i++)
                        {
                            stringBuilder.AppendFormat(
                                "{0} ",
                                results[i]);
                        }
                        stringBuilder.Append(results[results.Length - 1]);
                        Console.WriteLine(stringBuilder.ToString());
                    }
                    else
                    {
                        Console.WriteLine();
                    }
                }
                else if (this.values[valueIndex] is FloatValue)
                {
                    FloatValue floatValue = this.values[valueIndex] as FloatValue;
                    float[] results = floatValue.Get(pContext);
                    if (results.Length > 0)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        for (int i = 0; i < results.Length - 1; i++)
                        {
                            stringBuilder.AppendFormat(
                                "{0} ",
                                results[i]);
                        }
                        stringBuilder.Append(results[results.Length - 1]);
                        Console.WriteLine(stringBuilder.ToString());
                    }
                    else
                    {
                        Console.WriteLine();
                    }
                }
                else if (this.values[valueIndex] is WordValue)
                {
                    WordValue wordValue = this.values[valueIndex] as WordValue;
                    string[] results = wordValue.Get(pContext);
                    if (results.Length > 0)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        for (int i = 0; i < results.Length - 1; i++)
                        {
                            stringBuilder.AppendFormat(
                                "{0} ",
                                results[i]);
                        }
                        stringBuilder.Append(results[results.Length - 1]);
                        Console.WriteLine(stringBuilder.ToString());
                    }
                    else
                    {
                        Console.WriteLine();
                    }
                }
                else if (this.values[valueIndex] is IntValueVector2)
                {
                    IntValueVector2 wordValue = this.values[valueIndex] as IntValueVector2;
                    Vector2I[] results = wordValue.Get(pContext);
                    if (results.Length > 0)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        for (int i = 0; i < results.Length - 1; i++)
                        {
                            stringBuilder.AppendFormat(
                                "{0} ",
                                results[i]);
                        }
                        stringBuilder.Append(results[results.Length - 1]);
                        Console.WriteLine(stringBuilder.ToString());
                    }
                    else
                    {
                        Console.WriteLine();
                    }
                }
                else if (this.values[valueIndex] is IntValueVector3)
                {
                    IntValueVector3 wordValue = this.values[valueIndex] as IntValueVector3;
                    Vector3I[] results = wordValue.Get(pContext);
                    if (results.Length > 0)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        for (int i = 0; i < results.Length - 1; i++)
                        {
                            stringBuilder.AppendFormat(
                                "{0} ",
                                results[i]);
                        }
                        stringBuilder.Append(results[results.Length - 1]);
                        Console.WriteLine(stringBuilder.ToString());
                    }
                    else
                    {
                        Console.WriteLine();
                    }
                }
                else if (this.values[valueIndex] is FloatValueVector2)
                {
                    FloatValueVector2 wordValue = this.values[valueIndex] as FloatValueVector2;
                    Vector2[] results = wordValue.Get(pContext);
                    if (results.Length > 0)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        for (int i = 0; i < results.Length - 1; i++)
                        {
                            stringBuilder.AppendFormat(
                                "{0} ",
                                results[i]);
                        }
                        stringBuilder.Append(results[results.Length - 1]);
                        Console.WriteLine(stringBuilder.ToString());
                    }
                    else
                    {
                        Console.WriteLine();
                    }
                }
                else if (this.values[valueIndex] is FloatValueVector3)
                {
                    FloatValueVector3 wordValue = this.values[valueIndex] as FloatValueVector3;
                    Vector3[] results = wordValue.Get(pContext);
                    if (results.Length > 0)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        for (int i = 0; i < results.Length - 1; i++)
                        {
                            stringBuilder.AppendFormat(
                                "{0} ",
                                results[i]);
                        }
                        stringBuilder.Append(results[results.Length - 1]);
                        Console.WriteLine(stringBuilder.ToString());
                    }
                    else
                    {
                        Console.WriteLine();
                    }
                }
                else
                {
                    result = CommandResult.Failed();
                }

            }

            return result;
        }
    }
}