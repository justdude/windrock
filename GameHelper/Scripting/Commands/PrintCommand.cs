﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Scripting
{
    public class PrintCommand : Command
    {
        private Value value;

        public PrintCommand(Value pValue)
        {
            this.value = pValue;
        }

        public override CommandResult Execute(CommandContext pContext)
        {
            Console.WriteLine(this.value.ToString());

            return CommandResult.Completed();
        }
    }
}