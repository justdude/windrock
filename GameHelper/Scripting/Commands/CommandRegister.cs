﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace GameHelper.Scripting
{
    public class CommandRegister<TContext, TCommand>
        where TContext : CommandContext
        where TCommand : Command<TContext>
    {
        public delegate TCommand GetProperCommandDelegate(Value[] pValues, TContext pContext);

        protected Dictionary<string, GetProperCommandDelegate> commandSelectors;

        public CommandRegister()
        {
            this.commandSelectors = new Dictionary<string, GetProperCommandDelegate>();
            //this.commands.Add("print", this.GetProperPrintCommand);
            //this.commands.Add("p", this.GetProperPrintCommand);
            //this.commands.Add("execute", this.GetProperExecuteCommand);
            //this.commands.Add("e", this.GetProperExecuteCommand);
            //this.commands.Add("repeat", this.GetProperRepeatCommand);
            //this.commands.Add("r", this.GetProperRepeatCommand);
        }
        protected bool TryGetCommand(string pCommandName, Value[] pValues, out TCommand pCommand, TContext pContext)
        {
            pCommand = default(TCommand);
            GetProperCommandDelegate getProperCommand = null;
            if (this.commandSelectors.TryGetValue(pCommandName, out getProperCommand))
            {
                pCommand = getProperCommand(pValues, pContext);
            }

            return pCommand != null;
        }
        
        public void Add(string pCommandName, GetProperCommandDelegate pGetProperCommandMethod)
        {
            this.commandSelectors.Add(pCommandName, pGetProperCommandMethod);
        }
        public TCommand Parse(string pCommandString, TContext pContext)
        {
            TCommand command = default(TCommand);

            string[] tokens = pCommandString.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            string commandName = tokens[0];
            GetProperCommandDelegate getProperCommand = null;
            if (this.commandSelectors.TryGetValue(commandName, out getProperCommand))
            {
                Value[] values = new Value[tokens.Length - 1];
                bool isValueANull = true;
                for (int i = 1; i < tokens.Length; i++)
                {
                    values[i - 1] = Value.Parse(tokens[i]);
                    isValueANull = values[i - 1] == null;
                    if (isValueANull)
                    {
                        break;
                    }
                }

                if (!isValueANull)
                {
                    command = getProperCommand(values, pContext);
                }
            }

            return command;
        }
    }

    public class CommandRegister : CommandRegister<CommandContext, Command>
    {
        private Command GetProperBlankCommand(Value[] pValues, CommandContext pContext)
        {
            return new BlankCommand();
        }
        private Command GetProperPrintCommand(Value[] pValues, CommandContext pContext)
        {
            Command command = null;

            if (pValues.Length == 1)
            {
                command = new PrintCommand(pValues[0]);
            }

            return command;
        }
        private Command GetProperExecuteCommand(Value[] pValues, CommandContext pContext)
        {
            Command command = null;

            if (pValues.Length > 0)
            {
                command = new ExecuteCommand(pValues);
            }

            return command;
        }
        private Command GetProperRepeatCommand(Value[] pValues, CommandContext pContext)
        {
            Command command = null;

            if (pValues.Length > 2)
            {
                if (pValues[0] is ISingleIntValue
                    && pValues[1] is ISingleValue<string>)
                {
                    string commandName = (pValues[1] as ISingleValue<string>).GetSingle(pContext);
                    Command commandToRepeat = null;
                    if (this.TryGetCommand(commandName, pValues.Skip(2).ToArray<Value>(), out commandToRepeat, pContext))
                    {
                        command = new RepeatCommand(commandToRepeat, pValues[0] as ISingleIntValue);
                    }
                }
            }

            return command;
        }

        public void RegisterDefaultCommands()
        {
            if (!this.commandSelectors.ContainsKey("print"))
            {
                this.commandSelectors.Add("print", this.GetProperPrintCommand);
            }
            if (!this.commandSelectors.ContainsKey("p"))
            {
                this.commandSelectors.Add("p", this.GetProperPrintCommand);
            }

            if (!this.commandSelectors.ContainsKey("execute"))
            {
                this.commandSelectors.Add("execute", this.GetProperExecuteCommand);
            }
            if (!this.commandSelectors.ContainsKey("e"))
            {
                this.commandSelectors.Add("e", this.GetProperExecuteCommand);
            }

            if (!this.commandSelectors.ContainsKey("repeat"))
            {
                this.commandSelectors.Add("repeat", this.GetProperRepeatCommand);
            }
            if (!this.commandSelectors.ContainsKey("r"))
            {
                this.commandSelectors.Add("r", this.GetProperRepeatCommand);
            }
        }
    }
}