﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Scripting
{
    public abstract class Command<T> where T : CommandContext
    {
        public abstract CommandResult Execute(T pContext);
    }

    public class Command : Command<CommandContext>
    {
        public override CommandResult Execute(CommandContext pContext)
        {
            // Too lazy to do something... ( =__= )
            return CommandResult.Completed();
        }
    }
}