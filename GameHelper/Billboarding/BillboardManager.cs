﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameHelper.Cameras;

namespace GameHelper.Billboarding
{
    public class BillboardManager
    {
        /* Fields */

        private Dictionary<int, BillboardGroup> groups;
        private int groupKey;
        private Effect defaultEffect;

        /* Properties */

        public Effect DefaultEffect
        {
            get
            {
                return this.defaultEffect;
            }
        }

        /* Constructors */

        public BillboardManager(Effect pDefaultEffect)
        {
            this.defaultEffect = pDefaultEffect;

            this.groups = new Dictionary<int, BillboardGroup>();
            this.groupKey = -1;
        }
        public BillboardManager()
            : this(null)
        {
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public int AddGroup(Texture2D pTexture)
        {
            this.groups.Add(++this.groupKey, new BillboardGroup(this, pTexture));
            return this.groupKey;
        }
        public int AddGroup(Texture2D pTexture, Effect pEffect)
        {
            this.groups.Add(++this.groupKey, new BillboardGroup(this, pTexture, pEffect));
            return this.groupKey;
        }
        public int AddGroup(Texture2D pTexture, Vector2 pSize)
        {
            this.groups.Add(++this.groupKey, new BillboardGroup(this, pTexture, pSize));
            return this.groupKey;
        }
        public int AddGroup(Texture2D pTexture, Effect pEffect, Vector2 pSize)
        {
            this.groups.Add(++this.groupKey, new BillboardGroup(this, pTexture, pEffect, pSize));
            return this.groupKey;
        }
        public int AddGroup(Texture2D pTexture, float pScale)
        {
            this.groups.Add(++this.groupKey, new BillboardGroup(this, pTexture, pScale));
            return this.groupKey;
        }

        public int AddBillboard(Vector3 pPosition, int pGroupKey)
        {
            return this.groups[pGroupKey].AddBillboard(pPosition);
        }
        public void RemoveBillboard(int pBillboardKey, int pGroupKey)
        {
            this.groups[pGroupKey].RemoveBillboard(pBillboardKey);
        }

        public void UpdateVertices()
        {
            foreach (BillboardGroup group in this.groups.Values)
            {
                group.UpdateVertices();
            }
        }
        public void UpdateVertices(int pGroupKey)
        {
            this.groups[pGroupKey].UpdateVertices();
        }

        public void Draw(GraphicsDevice pGraphicsDevice, Camera pCamera, Matrix pWorld, int pGroupKey)
        {
            this.groups[pGroupKey].Draw(pGraphicsDevice, pCamera, pWorld);
        }
        public void Draw(GraphicsDevice pGraphicsDevice, Camera pCamera, int pGroupKey)
        {
            this.Draw(pGraphicsDevice, pCamera, Matrix.Identity, pGroupKey);
        }
        public void Draw(GraphicsDevice pGraphicsDevice, Camera pCamera, Matrix pWorld)
        {
            foreach (BillboardGroup group in this.groups.Values)
            {
                group.Draw(pGraphicsDevice, pCamera, pWorld);
            }
        }
        public void Draw(GraphicsDevice pGraphicsDevice, Camera pCamera)
        {
            this.Draw(pGraphicsDevice, pCamera, Matrix.Identity);
        }

    }
}