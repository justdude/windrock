﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using GameHelper.Cameras;

namespace GameHelper.Billboarding
{
    public class BillboardGroup
    {
        /* Fields */

        private BillboardManager billboardManager;
        private Dictionary<int, Vector3> positions;
        private VertexPositionTexture[] vertices;
        private int positionKey;
        private Texture2D texture;
        private Effect effect;
        private Vector2 size;

        /* Properties */

        public Texture2D Texture
        {
            get
            {
                return this.texture;
            }
            set
            {
                this.texture = value;
            }
        }
        public Vector2 Size
        {
            get
            {
                return this.size;
            }
            set
            {
                this.size = value;
            }
        }
        public Effect Effect
        {
            get
            {
                return this.effect;
            }
        }

        /* Constructors */

        public BillboardGroup(BillboardManager pBillboardManager, Texture2D pTexture, Effect pEffect)
        {
            this.billboardManager = pBillboardManager;
            this.texture = pTexture;
            this.effect = pEffect;

            this.size = new Vector2(pTexture.Width, pTexture.Height);
            this.positions = new Dictionary<int, Vector3>();
            this.positionKey = -1;
        }
        public BillboardGroup(BillboardManager pBillboardManager, Texture2D pTexture)
            : this(pBillboardManager, pTexture, null)
        {
        }
        public BillboardGroup(BillboardManager pBillboardManager, Texture2D pTexture, Effect pEffect, Vector2 pSize)
            : this(pBillboardManager, pTexture, pEffect)
        {
            this.size = pSize;
        }
        public BillboardGroup(BillboardManager pBillboardManager, Texture2D pTexture, Vector2 pSize)
            : this(pBillboardManager, pTexture, null)
        {
            this.size = pSize;
        }
        public BillboardGroup(BillboardManager pBillboardManager, Texture2D pTexture, float pScale)
            : this(pBillboardManager, pTexture, null)
        {
            this.size *= pScale;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public int AddBillboard(Vector3 pPosition)
        {
            this.positions.Add(++this.positionKey, pPosition);
            return this.positionKey;
        }
        public void RemoveBillboard(int pBillboardKey)
        {
            this.positions.Remove(pBillboardKey);
        }

        public void UpdateVertices()
        {
            this.vertices = new VertexPositionTexture[this.positions.Count * 6];
            int vertexIndex = -1;
            foreach (Vector3 position in this.positions.Values)
            {
                this.vertices[++vertexIndex] = new VertexPositionTexture(position, new Vector2(0, 1)); // 0
                this.vertices[++vertexIndex] = new VertexPositionTexture(position, new Vector2(1, 1)); // 1
                this.vertices[++vertexIndex] = new VertexPositionTexture(position, new Vector2(1, 0)); // 2
                this.vertices[++vertexIndex] = new VertexPositionTexture(position, new Vector2(0, 1)); // 0
                this.vertices[++vertexIndex] = new VertexPositionTexture(position, new Vector2(1, 0)); // 2
                this.vertices[++vertexIndex] = new VertexPositionTexture(position, new Vector2(0, 0)); // 3
            }
        }
        public void Draw(GraphicsDevice pGraphicsDevice, Camera pCamera, Matrix pWorld)
        {
            Effect billboardEffect = this.effect ?? this.billboardManager.DefaultEffect;

            if (billboardEffect == null)
            {
                throw new NullReferenceException();
            }

            billboardEffect.Parameters["World"].SetValue(pWorld);
            billboardEffect.Parameters["View"].SetValue(pCamera.View);
            billboardEffect.Parameters["Projection"].SetValue(pCamera.Projection);
            billboardEffect.Parameters["CameraPosition"].SetValue(pCamera.Position);
            billboardEffect.Parameters["UpVector"].SetValue(pCamera.UpVector);
            billboardEffect.Parameters["TextureMap"].SetValue(this.texture);
            billboardEffect.Parameters["Size"].SetValue(this.size);

            foreach (EffectPass pass in billboardEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                pGraphicsDevice.DrawUserPrimitives<VertexPositionTexture>(
                    PrimitiveType.TriangleList,
                    this.vertices,
                    0,
                    (int) (this.vertices.Length / 3));
            }
        }

    }
}