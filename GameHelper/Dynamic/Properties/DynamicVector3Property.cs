﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.Dynamic.Modifiers;
using Microsoft.Xna.Framework;

namespace GameHelper.Dynamic.Properties
{
    public class DynamicVector3Property
    {
        /* Fields */

        private Vector3 value;
        private List<DynamicVector3Modifier> increments;
        private List<DynamicFloatModifier> multipliers;

        /* Properties */

        public Vector3 Value
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;
            }
        }

        /* Constructors */

        public DynamicVector3Property(Vector3 pValue)
        {
            this.value = pValue;
        }

        /* Private methods */

        private Vector3 GetTotalIncrement(TimeSpan pTimeStamp)
        {
            Vector3 result = Vector3.Zero;
            for (int i = 0; i < this.increments.Count; i++)
            {
                result += this.increments[i].Get(pTimeStamp);
            }

            return result;
        }
        private float GetTotalMultipliers(TimeSpan pTimeStamp)
        {
            float result = 0;
            for (int i = 0; i < this.multipliers.Count; i++)
            {
                result += this.multipliers[i].Get(pTimeStamp);
            }

            return result;
        }

        /* Protected methods */



        /* Public methods */

        public void AddIncrement(DynamicVector3Modifier pIncrement)
        {
            if (this.increments == null)
            {
                this.increments = new List<DynamicVector3Modifier>();
            }

            this.increments.Add(pIncrement);
        }
        public void RemoveIncrement(DynamicVector3Modifier pIncrement)
        {
            this.increments.Remove(pIncrement);

            if (this.increments.Count == 0)
            {
                this.increments = null;
            }
        }
        public void AddMultiplier(DynamicFloatModifier pMultiplier)
        {
            if (this.multipliers == null)
            {
                this.multipliers = new List<DynamicFloatModifier>();
            }

            this.multipliers.Add(pMultiplier);
        }
        public void RemoveMultiplier(DynamicFloatModifier pMultiplier)
        {
            this.multipliers.Remove(pMultiplier);

            if (this.multipliers.Count == 0)
            {
                this.multipliers = null;
            }
        }

        public Vector3 GetValue(TimeSpan pTimeStamp)
        {
            Vector3 increment = this.GetTotalIncrement(pTimeStamp);
            float multipliers = 1 + this.GetTotalMultipliers(pTimeStamp);
            return (Vector3) (this.value * multipliers) + increment;
        }

    }
}