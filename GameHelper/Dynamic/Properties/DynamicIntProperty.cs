﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.Dynamic.Modifiers;

namespace GameHelper.Dynamic.Properties
{
    public class DynamicIntProperty
    {
        /* Fields */

        private int value;
        private List<DynamicIntModifier> increments;
        private List<DynamicFloatModifier> multipliers;

        /* Properties */

        public int Value
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;
            }
        }

        /* Constructors */

        public DynamicIntProperty(int pValue)
        {
            this.value = pValue;
        }

        /* Private methods */

        private int GetTotalIncrement(TimeSpan pTimeStamp)
        {
            int result = 0;
            for (int i = 0; i < this.increments.Count; i++)
            {
                result += this.increments[i].Get(pTimeStamp);
            }

            return result;
        }
        private float GetTotalMultipliers(TimeSpan pTimeStamp)
        {
            float result = 0;
            for (int i = 0; i < this.multipliers.Count; i++)
            {
                result += this.multipliers[i].Get(pTimeStamp);
            }

            return result;
        }

        /* Protected methods */



        /* Public methods */

        public void AddIncrement(DynamicIntModifier pIncrement)
        {
            if (this.increments == null)
            {
                this.increments = new List<DynamicIntModifier>();
            }

            this.increments.Add(pIncrement);
        }
        public void RemoveIncrement(DynamicIntModifier pIncrement)
        {
            this.increments.Remove(pIncrement);

            if (this.increments.Count == 0)
            {
                this.increments = null;
            }
        }
        public void AddMultiplier(DynamicFloatModifier pMultiplier)
        {
            if (this.multipliers == null)
            {
                this.multipliers = new List<DynamicFloatModifier>();
            }

            this.multipliers.Add(pMultiplier);
        }
        public void RemoveMultiplier(DynamicFloatModifier pMultiplier)
        {
            this.multipliers.Remove(pMultiplier);

            if (this.multipliers.Count == 0)
            {
                this.multipliers = null;
            }
        }

        public int GetValue(TimeSpan pTimeStamp)
        {
            int increment = this.GetTotalIncrement(pTimeStamp);
            float multipliers = 1 + this.GetTotalMultipliers(pTimeStamp);
            return (int) (this.value * multipliers) + increment;
        }

    }
}