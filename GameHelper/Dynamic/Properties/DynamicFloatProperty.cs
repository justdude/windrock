﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.Dynamic.Modifiers;

namespace GameHelper.Dynamic.Properties
{
    public class DynamicFloatProperty
    {
        /* Fields */

        private float value;
        private List<DynamicFloatModifier> increments;
        private List<DynamicFloatModifier> multipliers;

        /* Properties */

        public float Value
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;
            }
        }

        /* Constructors */

        public DynamicFloatProperty(float pValue)
        {
            this.value = pValue;
        }

        /* Private methods */

        private float GetTotalIncrement(TimeSpan pTimeStamp)
        {
            float result = 0;
            for (int i = 0; i < this.increments.Count; i++)
            {
                result += this.increments[i].Get(pTimeStamp);
            }

            return result;
        }
        private float GetTotalMultipliers(TimeSpan pTimeStamp)
        {
            float result = 0;
            for (int i = 0; i < this.multipliers.Count; i++)
            {
                result += this.multipliers[i].Get(pTimeStamp);
            }

            return result;
        }

        /* Protected methods */



        /* Public methods */

        public void AddIncrement(DynamicFloatModifier pIncrement)
        {
            if (this.increments == null)
            {
                this.increments = new List<DynamicFloatModifier>();
            }

            this.increments.Add(pIncrement);
        }
        public void RemoveIncrement(DynamicFloatModifier pIncrement)
        {
            this.increments.Remove(pIncrement);

            if (this.increments.Count == 0)
            {
                this.increments = null;
            }
        }
        public void AddMultiplier(DynamicFloatModifier pMultiplier)
        {
            if (this.multipliers == null)
            {
                this.multipliers = new List<DynamicFloatModifier>();
            }

            this.multipliers.Add(pMultiplier);
        }
        public void RemoveMultiplier(DynamicFloatModifier pMultiplier)
        {
            this.multipliers.Remove(pMultiplier);

            if (this.multipliers.Count == 0)
            {
                this.multipliers = null;
            }
        }

        public float GetValue(TimeSpan pTimeStamp)
        {
            float increment = this.GetTotalIncrement(pTimeStamp);
            float multipliers = 1 + this.GetTotalMultipliers(pTimeStamp);
            return (float) (this.value * multipliers) + increment;
        }

    }
}