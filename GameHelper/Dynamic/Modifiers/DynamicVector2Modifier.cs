﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GameHelper.Dynamic.Modifiers
{
    public class DynamicVector2Modifier
    {
        /* Fields */

        private Vector2 value;

        /* Properties */

        public Vector2 Value
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;
            }
        }

        /* Constructors */

        public DynamicVector2Modifier(Vector2 pValue)
        {
            this.value = pValue;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public Vector2 Get(TimeSpan pTimeStamp)
        {
            return this.value;
        }

    }
}