﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GameHelper.Dynamic.Modifiers
{
    public class DynamicVector4Modifier
    {
        /* Fields */

        private Vector4 value;

        /* Properties */

        public Vector4 Value
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;
            }
        }

        /* Constructors */

        public DynamicVector4Modifier(Vector4 pValue)
        {
            this.value = pValue;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public Vector4 Get(TimeSpan pTimeStamp)
        {
            return this.value;
        }

    }
}