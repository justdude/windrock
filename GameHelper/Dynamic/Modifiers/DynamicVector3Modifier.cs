﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GameHelper.Dynamic.Modifiers
{
    public class DynamicVector3Modifier
    {
        /* Fields */

        private Vector3 value;

        /* Properties */

        public Vector3 Value
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;
            }
        }

        /* Constructors */

        public DynamicVector3Modifier(Vector3 pValue)
        {
            this.value = pValue;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public Vector3 Get(TimeSpan pTimeStamp)
        {
            return this.value;
        }

    }
}