﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Dynamic.Modifiers
{
    public class DynamicFloatModifier
    {
        /* Fields */

        private float value;

        /* Properties */

        public float Value
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;
            }
        }

        /* Constructors */

        public DynamicFloatModifier(float pValue)
        {
            this.value = pValue;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public float Get(TimeSpan pTimeStamp)
        {
            return this.value;
        }

    }
}