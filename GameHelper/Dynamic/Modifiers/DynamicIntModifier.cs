﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Dynamic.Modifiers
{
    public class DynamicIntModifier
    {
        /* Fields */

        private int value;

        /* Properties */

        public int Value
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;
            }
        }

        /* Constructors */

        public DynamicIntModifier(int pValue)
        {
            this.value = pValue;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public int Get(TimeSpan pTimeStamp)
        {
            return this.value;
        }

    }
}