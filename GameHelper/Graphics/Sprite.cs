﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameHelper.Graphics
{
    public class Sprite
    {
        /* Fields */

        private int frameRowIndex;
        private int startFrameIndex;
        private int framesCount;
        private TimeSpan frameDuration;
        private TimeSpan animationTime;
        private Rectangle frame;
        private Texture2D texture;
        private Vector2 frameOffset;
        private SpriteAnimationsState animationState;
        private SpriteAnimationRepetition animationRepetition;
        private float fps;

        /* Properties */

        public int FramesCount
        {
            get
            {
                return this.framesCount;
            }
        }
        public float FPS
        {
            set
            {
                this.fps = value;

                this.frameDuration =
                    this.fps > 0 ?
                    TimeSpan.FromSeconds( 1f / this.fps ) :
                    TimeSpan.MaxValue;
            }
            get
            {
                return this.fps;
            }
        }
        public TimeSpan FrameDuration
        {
            get
            {
                return this.frameDuration;
            }
        }
        public Rectangle Frame
        {
            get
            {
                return this.frame;
            }
        }
        public Texture2D Texture
        {
            get
            {
                return this.texture;
            }
        }
        public Vector2 FrameOffset
        {
            get
            {
                return this.frameOffset;
            }
        }

        /* Constructors */

        public Sprite(Texture2D pTexture, int pFrameWidth, int pFrameHeight, int pFrameRowIndex, int pStartFrameIndex, Vector2 pFrameOffset, int pFramesCount, float pFPS)
        {
            this.texture = pTexture;
            this.frame = new Rectangle( 0, 0, pFrameWidth, pFrameHeight );
            this.frameRowIndex = pFrameRowIndex;
            this.startFrameIndex = pStartFrameIndex;
            this.framesCount = pFramesCount;
            this.FPS = Math.Max( pFPS, 0 );
            this.frameOffset = pFrameOffset;

            this.animationRepetition = SpriteAnimationRepetition.Loop;
            this.animationState = SpriteAnimationsState.Stoped;
            this.UpdateFrame();
        }
        public Sprite(Texture2D pTexture, int pFrameWidth, int pFrameHeight, int pFrameRowIndex, int pStartFrameIndex, int pFramesCount, float pFPS)
            : this( pTexture, pFrameWidth, pFrameHeight, pFrameRowIndex, pStartFrameIndex, Vector2.Zero, pFramesCount, pFPS )
        {
        }
        public Sprite(Texture2D pTexture, int pFrameWidth, int pFrameHeight, int pFrameRowIndex, int pStartFrameIndex, Vector2 pFrameOffset)
            : this( pTexture, pFrameWidth, pFrameHeight, pFrameRowIndex, pStartFrameIndex, pFrameOffset, 1, 0 )
        {
        }
        public Sprite(Texture2D pTexture, int pFrameWidth, int pFrameHeight, int pFrameRowIndex, int pStartFrameIndex)
            : this( pTexture, pFrameWidth, pFrameHeight, pFrameRowIndex, pStartFrameIndex, Vector2.Zero, 1, 0 )
        {
        }
        public Sprite(Texture2D pTexture, int pFrameWidth, int pFrameHeight, Vector2 pFrameOffset)
            : this( pTexture, pFrameWidth, pFrameHeight, 0, 0, pFrameOffset, 1, 0 )
        {
        }
        public Sprite(Texture2D pTexture, int pFrameWidth, int pFrameHeight)
            : this( pTexture, pFrameWidth, pFrameHeight, 0, 0, Vector2.Zero, 1, 0 )
        {
        }
        public Sprite(Texture2D pTexture)
            : this( pTexture, pTexture.Width, pTexture.Height, 0, 0, Vector2.Zero, 1, 0 )
        {
        }


        /* Private methods */

        public Vector2 GetFrameCoords(TimeSpan pAnimationDuration)
        {
            return
                this.framesCount <= 1 ?
                new Vector2( this.startFrameIndex, this.frameRowIndex ) :
                new Vector2(
                    this.startFrameIndex + (int) (this.animationTime.TotalMilliseconds / this.frameDuration.TotalMilliseconds % this.framesCount),
                    this.frameRowIndex );
        }

        private void UpdateFrame()
        {
            Vector2 frameCoords = this.GetFrameCoords( this.animationTime );
            this.frame.X = (int) (frameCoords.X * this.frame.Width + this.frameOffset.X);
            this.frame.Y = (int) (frameCoords.Y * this.frame.Height + this.frameOffset.Y);
        }

        /* Protected methods */



        /* Public methods */

        public void Update(GameTime pGameTime)
        {
            if (this.animationState == SpriteAnimationsState.Playing)
            {
                this.animationTime += pGameTime.ElapsedGameTime;
                this.UpdateFrame();
            }
        }

        // // //

        public void Pause()
        {
            this.animationState = SpriteAnimationsState.Paused;
        }
        public void Resume()
        {
            this.animationState = SpriteAnimationsState.Playing;
        }
        public void Stop()
        {
            this.animationTime = TimeSpan.Zero;
            this.animationState = SpriteAnimationsState.Stoped;
        }
        public void Reset()
        {
            this.animationTime = TimeSpan.Zero;
        }
        public void Play()
        {
            this.animationTime = TimeSpan.Zero;
            this.animationState = SpriteAnimationsState.Playing;

        }
        public void PlayOnce()
        {
            this.animationTime = TimeSpan.Zero;
            this.animationRepetition = SpriteAnimationRepetition.Once;
            this.animationState = SpriteAnimationsState.Playing;

        }
        public void PlayLoop()
        {
            this.animationTime = TimeSpan.Zero;
            this.animationRepetition = SpriteAnimationRepetition.Loop;
            this.animationState = SpriteAnimationsState.Playing;
        }
    }
}