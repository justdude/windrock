﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Graphics
{
    public enum SpriteAnimationRepetition : byte
    {
        None = 0,
        Once = 1,
        Loop = 2
    }
}