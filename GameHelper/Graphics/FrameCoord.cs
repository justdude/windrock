﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Data
{
    public struct FrameCoord
    {
        /* Fields */

        public int Row;
        public int Column;

        /* Properties */

        public static FrameCoord Zero
        {
            get
            {
                return new FrameCoord(0, 0);
            }
        }

        /* Constructors */

        public FrameCoord(int pRow, int pColumn)
        {
            this.Row = pRow;
            this.Column = pColumn;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        //public override bool Equals(object pObject)
        //{
        //    bool result = false;
        //    if (pObject is Vector2I)
        //    {
        //        Vector2I other = (Vector2I) pObject;
        //        result =
        //            this.Row == other.X
        //            && this.Column == other.Y;
        //    }
        //    return result;
        //}
        //public static bool operator ==(Vector2I pValue1, Vector2I pValue2)
        //{
        //    return
        //        pValue1.X == pValue2.X
        //        && pValue1.Y == pValue2.Y;
        //}
        //public static bool operator !=(Vector2I pValue1, Vector2I pValue2)
        //{
        //    return
        //        pValue1.X != pValue2.X
        //        || pValue1.Y != pValue2.Y;
        //}
        //public static Vector2I operator +(Vector2I pValue1, Vector2I pValue2)
        //{
        //    return new Vector2I(
        //        pValue1.X + pValue2.X,
        //        pValue1.Y + pValue2.Y);
        //}
        //public static Vector2I operator -(Vector2I pValue1, Vector2I pValue2)
        //{
        //    return new Vector2I(
        //        pValue1.X - pValue2.X,
        //        pValue1.Y - pValue2.Y);
        //}
        public override string ToString()
        {
            return string.Format(
                "({0}; {1})",
                this.Row,
                this.Column);
        }

    }
}