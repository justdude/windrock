﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Graphics
{
    public enum SpriteAnimationsState : byte
    {
        None = 0,
        Playing = 1,
        Stoped = 2,
        Paused = 3
    }
}