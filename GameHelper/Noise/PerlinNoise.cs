﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.Data;
using GameHelper.Extentions;

namespace GameHelper.Noise
{
    public static class PerlinNoise
    {
        /* Fields */



        /* Properties */



        /* Constructors */



        /* Private methods */

        private static void InterpolateTempNoise2D(ref float[,] pTempNoise, int pStepX, int pStepY)
        {
            int baseX = 0;
            int baseY = 0;

            for (int x = 0; x < pTempNoise.GetLength(0) - 1; x++)
            {
                if (x >= baseX + pStepX)
                {
                    baseX += pStepX;
                }

                baseY = 0;
                for (int y = 0; y < pTempNoise.GetLength(1) - 1; y++)
                {
                    if (y >= baseY + pStepY)
                    {
                        baseY += pStepY;
                    }

                    if (x % pStepX != 0 || y % pStepY != 0)
                    {
                        float a = (float) (x - baseX) / (float) pStepX;
                        float b = (float) (y - baseY) / (float) pStepY;
                        pTempNoise[x, y] =
                            pTempNoise[baseX, baseY] * (1 - a) * (1 - b) +
                            pTempNoise[baseX, baseY + pStepY] * (1 - a) * b +
                            pTempNoise[baseX + pStepX, baseY] * a * (1 - b) +
                            pTempNoise[baseX + pStepX, baseY + pStepY] * a * b;
                    }
                }
            }
        }
        private static void InterpolateTempNoise3D(ref float[, ,] pTempNoise, int pStepD1, int pStepD2, int pStepD3)
        {
            int baseD1 = 0;
            int baseD2 = 0;
            int baseD3 = 0;

            float oneDivStepD1 = 1.0f / pStepD1;
            float oneDivStepD2 = 1.0f / pStepD2;
            float oneDivStepD3 = 1.0f / pStepD3;

            for (int d1 = 0; d1 < pTempNoise.GetLength(0) - 1; d1++)
            {
                if (d1 >= baseD1 + pStepD1)
                {
                    baseD1 += pStepD1;
                }

                baseD2 = 0;
                for (int d2 = 0; d2 < pTempNoise.GetLength(1) - 1; d2++)
                {
                    if (d2 >= baseD2 + pStepD2)
                    {
                        baseD2 += pStepD2;
                    }

                    baseD3 = 0;
                    for (int d3 = 0; d3 < pTempNoise.GetLength(2) - 1; d3++)
                    {
                        if (d3 >= baseD3 + pStepD3)
                        {
                            baseD3 += pStepD3;
                        }

                        if (d1 % pStepD1 != 0 || d2 % pStepD2 != 0 || d3 % pStepD3 != 0)
                        {
                            try
                            {
                                float a = (float) (d1 - baseD1) * oneDivStepD1;
                                float b = (float) (d2 - baseD2) * oneDivStepD2;
                                float c = (float) (d3 - baseD3) * oneDivStepD3;
                                pTempNoise[d1, d2, d3] =
                                    pTempNoise[baseD1, baseD2, baseD3] * (1 - a) * (1 - b) * (1 - c) +
                                    pTempNoise[baseD1, baseD2 + pStepD2, baseD3] * (1 - a) * b * (1 - c) +
                                    pTempNoise[baseD1 + pStepD1, baseD2, baseD3] * a * (1 - b) * (1 - c) +
                                    pTempNoise[baseD1 + pStepD1, baseD2 + pStepD2, baseD3] * a * b * (1 - c) +
                                    pTempNoise[baseD1, baseD2, baseD3 + pStepD3] * (1 - a) * (1 - b) * c +
                                    pTempNoise[baseD1, baseD2 + pStepD2, baseD3 + pStepD3] * (1 - a) * b * c +
                                    pTempNoise[baseD1 + pStepD1, baseD2, baseD3 + pStepD3] * a * (1 - b) * c +
                                    pTempNoise[baseD1 + pStepD1, baseD2 + pStepD2, baseD3 + pStepD3] * a * b * c;
                            }
                            catch (Exception ex)
                            {
                                System.Diagnostics.Debug.WriteLine(ex.Message);
                            }
                        }
                    }
                }
            }
        }

        private static void InterpolateTempNoise2D(ref int[,] pTempNoise, int pStepD1, int pStepD2)
        {
            int baseD1 = 0;
            int baseD2 = 0;

            for (int x = 0; x < pTempNoise.GetLength(0) - 1; x++)
            {
                if (x >= baseD1 + pStepD1)
                {
                    baseD1 += pStepD1;
                }

                baseD2 = 0;
                for (int y = 0; y < pTempNoise.GetLength(1) - 1; y++)
                {
                    if (y >= baseD2 + pStepD2)
                    {
                        baseD2 += pStepD2;
                    }

                    if (x % pStepD1 != 0 || y % pStepD2 != 0)
                    {
                        float a = (float) (x - baseD1) / (float) pStepD1;
                        float b = (float) (y - baseD2) / (float) pStepD2;
                        pTempNoise[x, y] = (int) (
                            pTempNoise[baseD1, baseD2] * (1 - a) * (1 - b) +
                            pTempNoise[baseD1, baseD2 + pStepD2] * (1 - a) * b +
                            pTempNoise[baseD1 + pStepD1, baseD2] * a * (1 - b) +
                            pTempNoise[baseD1 + pStepD1, baseD2 + pStepD2] * a * b);
                    }
                }
            }
        }
        private static void InterpolateTempNoise3D(ref int[, ,] pTempNoise, int pStepD1, int pStepD2, int pStepD3)
        {
            int baseD1 = 0;
            int baseD2 = 0;
            int baseD3 = 0;

            float oneDivStepD1 = 1.0f / pStepD1;
            float oneDivStepD2 = 1.0f / pStepD2;
            float oneDivStepD3 = 1.0f / pStepD3;

            for (int d1 = 0; d1 < pTempNoise.GetLength(0) - 1; d1++)
            {
                if (d1 >= baseD1 + pStepD1)
                {
                    baseD1 += pStepD1;
                }

                baseD2 = 0;
                for (int d2 = 0; d2 < pTempNoise.GetLength(1) - 1; d2++)
                {
                    if (d2 >= baseD2 + pStepD2)
                    {
                        baseD2 += pStepD2;
                    }

                    for (int d3 = 0; d3 < pTempNoise.GetLength(2); d3++)
                    {
                        if (d3 >= baseD3 + pStepD3)
                        {
                            baseD3 += pStepD3;
                        }

                        if (d1 % pStepD1 != 0 || d2 % pStepD2 != 0 || d3 % pStepD3 != 0)
                        {
                            float a = (float) (d1 - baseD1) * oneDivStepD1;
                            float b = (float) (d2 - baseD2) * oneDivStepD2;
                            float c = (float) (d3 - baseD3) * oneDivStepD3;
                            pTempNoise[d1, d2, d3] = (int) (
                                pTempNoise[baseD1, baseD2, baseD3] * (1 - a) * (1 - b) * (1 - c) +
                                pTempNoise[baseD1, baseD2 + pStepD2, baseD3] * (1 - a) * b * (1 - c) +
                                pTempNoise[baseD1 + pStepD1, baseD2, baseD3] * a * (1 - b) * (1 - c) +
                                pTempNoise[baseD1 + pStepD1, baseD2 + pStepD2, baseD3] * a * b * (1 - c) +
                                pTempNoise[baseD1, baseD2, baseD3 + pStepD3] * (1 - a) * (1 - b) * c +
                                pTempNoise[baseD1, baseD2 + pStepD2, baseD3 + pStepD3] * (1 - a) * b * c +
                                pTempNoise[baseD1 + pStepD1, baseD2, baseD3 + pStepD3] * a * (1 - b) * c +
                                pTempNoise[baseD1 + pStepD1, baseD2 + pStepD2, baseD3 + pStepD3] * a * b * c);
                        }
                    }
                }
            }
        }

        /* Protected methods */



        /* Public methods */

        /// <summary>
        /// Генерирует двумерный шум Перлина.
        /// </summary>
        /// <param name="pSizeD1">Размер марицы по оси X.</param>
        /// <param name="pSizeD2">Размер марицы по оси Y.</param>
        /// <param name="pSteps">Количество шагов генерации.</param>
        /// <param name="pSeed">Зерно генерации.</param>
        /// <returns>Двумерный шум.</returns>
        public static float[,] GetNoise2D(int pSizeD1, int pSizeD2, int pSteps, int pOffsetByD1, int pOffsetByD2, float pBaseValue, float pAmplitude, int pSeed)
        {
            int maxSteps = (int) Math.Floor(Math.Log(Math.Min(pSizeD1, pSizeD2), 2));
            if (pSteps > maxSteps)
            {
                throw new Exception();
            }

            int minSize = (int) Math.Pow(pSteps, 2);
            if (pSizeD1 < minSize)
            {
                throw new Exception();
            }
            if (pSizeD2 < minSize)
            {
                throw new Exception();
            }

            float[,] noise = new float[pSizeD1, pSizeD2];

            int frequency = 1;
            float amplitude = 1.0f;

            for (int step = 0; step < pSteps; step++)
            {
                int stepX = (int) (pSizeD1 / frequency);
                int stepY = (int) (pSizeD2 / frequency);

                float[,] tempNoise = new float[pSizeD1 + 1, pSizeD2 + 1];
                for (int x = 0; x <= pSizeD1; x += stepX)
                {
                    for (int y = 0; y <= pSizeD2; y += stepY)
                    {
                        int seed = new Vector2I(x + pOffsetByD1, y + pOffsetByD2).GetHashCode() + frequency + pSeed;
                        Random random = new Random(seed);
                        tempNoise[x, y] = pBaseValue + (float) random.Float(-pAmplitude, pAmplitude);
                    }
                }
                InterpolateTempNoise2D(ref tempNoise, stepX, stepY);

                for (int x = 0; x < pSizeD1; x++)
                {
                    for (int y = 0; y < pSizeD2; y++)
                    {
                        noise[x, y] += tempNoise[x, y];
                    }
                }

                frequency *= 2;
                amplitude *= 0.5f;
            }

            return noise;
        }
        public static float[, ,] GetNoise3D(int pSizeD1, int pSizeD2, int pSizeD3, int pSteps, int pOffsetByD1, int pOffsetByD2, int pOffsetByD3, float pBaseValue, float pAmplitude, int pSeed)
        {
            int maxSteps = (int) Math.Floor(Math.Log(Math.Min(Math.Min(pSizeD1, pSizeD2), pSizeD3), 2));
            if (pSteps > maxSteps)
            {
                throw new Exception();
            }

            int minSize = (int) Math.Pow(pSteps, 2);
            if (pSizeD1 < minSize)
            {
                throw new Exception();
            }
            if (pSizeD2 < minSize)
            {
                throw new Exception();
            }
            if (pSizeD3 < minSize)
            {
                throw new Exception();
            }

            float[, ,] noise = new float[pSizeD1, pSizeD2, pSizeD3];

            int frequency = 1;
            float amplitude = (pAmplitude - pBaseValue) * 0.5f;
            float valueRange = pAmplitude - pBaseValue;

            for (int step = 0; step < pSteps; step++)
            {
                int stepD1 = (int) (pSizeD1 / frequency);
                int stepD2 = (int) (pSizeD2 / frequency);
                int stepD3 = (int) (pSizeD3 / frequency);

                float[, ,] tempNoise = new float[pSizeD1 + 1, pSizeD2 + 1, pSizeD3 + 1];
                for (int d1 = 0; d1 <= pSizeD1; d1 += stepD1)
                {
                    for (int d2 = 0; d2 <= pSizeD2; d2 += stepD2)
                    {
                        for (int d3 = 0; d3 <= pSizeD3; d3 += stepD3)
                        {
                            int seed = new Vector3I(d1 + pOffsetByD1, d2 + pOffsetByD2, d3 + pOffsetByD3).GetHashCode() + frequency + pSeed;
                            Random random = new Random(seed);
                            tempNoise[d1, d2, d3] = pBaseValue + (float) random.Float(-pAmplitude, pAmplitude);
                        }
                    }
                }

                InterpolateTempNoise3D(ref tempNoise, stepD1, stepD2, stepD3);

                for (int d1 = 0; d1 < pSizeD1; d1++)
                {
                    for (int d2 = 0; d2 < pSizeD2; d2++)
                    {
                        for (int d3 = 0; d3 < pSizeD3; d3++)
                        {
                            noise[d1, d2, d3] += tempNoise[d1, d2, d3];
                        }
                    }
                }

                frequency *= 2;
                amplitude = (int) (amplitude * 0.5f);
            }

            return noise;
        }

        public static int[,] GetIntNoise2D(int pSizeD1, int pSizeD2, int pSteps, int pOffsetByD1, int pOffsetByD2, int pBaseValue, int pAmplitude, int pSeed)
        {
            int maxSteps = (int) Math.Floor(Math.Log(Math.Min(pSizeD1, pSizeD2), 2));
            if (pSteps > maxSteps)
            {
                throw new Exception();
            }

            int minSize = (int) Math.Pow(pSteps, 2);
            if (pSizeD1 < minSize)
            {
                throw new Exception();
            }
            if (pSizeD2 < minSize)
            {
                throw new Exception();
            }

            int[,] noise = new int[pSizeD1, pSizeD2];

            int frequency = 1;

            for (int step = 0; step < pSteps; step++)
            {
                int stepD1 = (int) (pSizeD1 / frequency);
                int stepD2 = (int) (pSizeD2 / frequency);

                int[,] tempNoise = new int[pSizeD1 + 1, pSizeD2 + 1];
                for (int d1 = 0; d1 <= pSizeD1; d1 += stepD1)
                {
                    for (int d2 = 0; d2 <= pSizeD2; d2 += stepD2)
                    {
                        int seed = new Vector2I(d1 + pOffsetByD1, d2 + pOffsetByD2).GetHashCode() + frequency + pSeed;
                        Random random = new Random(seed);
                        tempNoise[d1, d2] = pBaseValue + random.Next(-pAmplitude, pAmplitude + 1);
                    }
                }
                InterpolateTempNoise2D(ref tempNoise, stepD1, stepD2);

                for (int d1 = 0; d1 < pSizeD1; d1++)
                {
                    for (int d2 = 0; d2 < pSizeD2; d2++)
                    {
                        noise[d1, d2] += tempNoise[d1, d2];
                    }
                }

                frequency *= 2;
            }

            return noise;
        }
        public static int[, ,] GetIntNoise3D(int pSizeD1, int pSizeD2, int pSizeD3, int pSteps, int pOffsetByD1, int pOffsetByD2, int pOffsetByD3, int pBaseValue, int pAmplitude, int pSeed)
        {
            int maxSteps = (int) Math.Floor(Math.Log(Math.Min(Math.Min(pSizeD1, pSizeD2), pSizeD3), 2));
            if (pSteps > maxSteps)
            {
                throw new Exception();
            }

            int minSize = (int) Math.Pow(pSteps, 2);
            if (pSizeD1 < minSize)
            {
                throw new Exception();
            }
            if (pSizeD2 < minSize)
            {
                throw new Exception();
            }
            if (pSizeD3 < minSize)
            {
                throw new Exception();
            }

            int[, ,] noise = new int[pSizeD1, pSizeD2, pSizeD3];

            int frequency = 1;

            for (int step = 0; step < pSteps; step++)
            {
                int stepD1 = (int) (pSizeD1 / frequency);
                int stepD2 = (int) (pSizeD2 / frequency);
                int stepD3 = (int) (pSizeD3 / frequency);

                int[, ,] tempNoise = new int[pSizeD1 + 1, pSizeD2 + 1, pSizeD3 + 1];
                for (int d1 = 0; d1 <= pSizeD1; d1 += stepD1)
                {
                    for (int d2 = 0; d2 <= pSizeD2; d2 += stepD2)
                    {
                        for (int d3 = 0; d3 <= pSizeD3; d3 += stepD3)
                        {
                            int seed = new Vector3I(d1 + pOffsetByD1, d2 + pOffsetByD2, d3 + pOffsetByD3).GetHashCode() + frequency + pSeed;
                            Random random = new Random(seed);
                            tempNoise[d1, d2, d3] = pBaseValue + random.Next(-pAmplitude, pAmplitude + 1);
                        }
                    }
                }

                InterpolateTempNoise3D(ref tempNoise, stepD1, stepD2, stepD3);

                for (int d1 = 0; d1 < pSizeD1; d1++)
                {
                    for (int d2 = 0; d2 < pSizeD2; d2++)
                    {
                        for (int d3 = 0; d3 < pSizeD3; d3++)
                        {
                            noise[d1, d2, d3] += tempNoise[d1, d2, d3];
                        }
                    }
                }

                frequency *= 2;
            }

            return noise;
        }

        /// <summary>
        /// Нормализирует двумерный шум.
        /// </summary>
        /// <param name="pNoise">Двумерный шум.</param>
        /// <returns>Нормализированый двумерный шум.</returns>
        public static float[,] Normalize(float[,] pNoise)
        {
            float minValue = 0;
            for (int x = 0; x < pNoise.GetLength(0); x++)
            {
                for (int y = 0; y < pNoise.GetLength(1); y++)
                {
                    if (pNoise[x, y] < minValue)
                    {
                        minValue = pNoise[x, y];
                    }
                }
            }

            float maxValue = 1;
            for (int x = 0; x < pNoise.GetLength(0); x++)
            {
                for (int y = 0; y < pNoise.GetLength(1); y++)
                {
                    if (pNoise[x, y] > maxValue)
                    {
                        maxValue = pNoise[x, y];
                    }
                }
            }

            float[,] normalizedNoise = new float[pNoise.GetLength(0), pNoise.GetLength(1)];
            float scale = 1.0f / (maxValue - minValue);
            for (int x = 0; x < pNoise.GetLength(0); x++)
            {
                for (int y = 0; y < pNoise.GetLength(1); y++)
                {
                    normalizedNoise[x, y] = (pNoise[x, y] - minValue) * scale;
                }
            }

            return normalizedNoise;
        }

        /// <summary>
        /// Трансформирует двумерный шум.
        /// </summary>
        /// <param name="pNoise">Двумерный шум.</param>
        /// <param name="pScale">Масштаб амплитуды.</param>
        /// <param name="pOffset">Смещение амплитуды.</param>
        /// <returns>Трансформированный двумерный шум.</returns>
        public static float[,] Transform(float[,] pNoise, float pScale, float pOffset)
        {
            float[,] transformedNoise = new float[pNoise.GetLength(0), pNoise.GetLength(1)];
            for (int x = 0; x < pNoise.GetLength(0); x++)
            {
                for (int y = 0; y < pNoise.GetLength(1); y++)
                {
                    transformedNoise[x, y] = pNoise[x, y] * pScale + pOffset;
                }
            }

            return transformedNoise;
        }

        /// <summary>
        /// Трансформирует двумерный шум с преведением значений к целочисленому типу.
        /// </summary>
        /// <param name="pNoise">Двумерный шум.</param>
        /// <param name="pScale">Масштаб амплитуды.</param>
        /// <param name="pOffset">Смещение амплитуды.</param>
        /// <returns>Трансформированный двумерный шум с целочислеными значениями.</returns>
        public static int[,] TransformToInt(float[,] pNoise, float pScale, int pOffset)
        {
            int[,] transformedNoise = new int[pNoise.GetLength(0), pNoise.GetLength(1)];
            for (int x = 0; x < pNoise.GetLength(0); x++)
            {
                for (int y = 0; y < pNoise.GetLength(1); y++)
                {
                    transformedNoise[x, y] = (int) (pNoise[x, y] * pScale) + pOffset;
                }
            }

            return transformedNoise;
        }
    }
}