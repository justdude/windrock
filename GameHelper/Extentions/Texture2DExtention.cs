﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace GameHelper.Extentions
{
    public static class Texture2DExtention
    {
        /* Fields */



        /* Properties */



        /* Constructors */

        

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public static Texture2D Cut(this Texture2D pSourceTexture, Rectangle pSourceRectangle)
        {
            Texture2D texture = new Texture2D(pSourceTexture.GraphicsDevice, pSourceRectangle.Width, pSourceRectangle.Height);
            Color[] data = new Color[pSourceRectangle.Width * pSourceRectangle.Height];
            pSourceTexture.GetData<Color>(0, pSourceRectangle, data, 0, data.Length);
            texture.SetData<Color>(data);

            return texture;
        }

    }
}