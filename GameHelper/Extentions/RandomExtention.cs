﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GameHelper.Extentions
{
    public static class RandomExtention
    {
        /* Fields */



        /* Properties */



        /* Constructors */



        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public static bool Boolean(this Random pRandom)
        {
            return pRandom.Next(2) == 1;
        }
        public static float Float(this Random pRandom, float pMinValue, float pMaxValue)
        {
            return pMinValue + (float) pRandom.NextDouble() * (pMaxValue - pMinValue);
        }
        public static bool OneOf(this Random pRandom, int pValue)
        {
            return pRandom.Next(pValue) == 0;
        }
        public static Vector2 Vector2(this Random pRandom, Vector2 pMinValue, Vector2 pMaxValue)
        {
            return new Vector2(
                pMinValue.X + (float) pRandom.NextDouble() * (pMaxValue.X - pMinValue.X),
                pMinValue.Y + (float) pRandom.NextDouble() * (pMaxValue.Y - pMinValue.Y));
        }
        public static Vector3 Vector3(this Random pRandom, Vector3 pMinValue, Vector3 pMaxValue)
        {
            return new Vector3(
                pMinValue.X + (float) pRandom.NextDouble() * (pMaxValue.X - pMinValue.X),
                pMinValue.Y + (float) pRandom.NextDouble() * (pMaxValue.Y - pMinValue.Y),
                pMinValue.Z + (float) pRandom.NextDouble() * (pMaxValue.Z - pMinValue.Z));
        }
        public static Vector4 Vector4(this Random pRandom, Vector4 pMinValue, Vector4 pMaxValue)
        {
            return new Vector4(
                pMinValue.X + (float) pRandom.NextDouble() * (pMaxValue.X - pMinValue.X),
                pMinValue.Y + (float) pRandom.NextDouble() * (pMaxValue.Y - pMinValue.Y),
                pMinValue.Z + (float) pRandom.NextDouble() * (pMaxValue.Z - pMinValue.Z),
                pMinValue.W + (float) pRandom.NextDouble() * (pMaxValue.W - pMinValue.W));
        }
        public static Vector4 Vector4(this Random pRandom, Vector4 pMinValue, Vector4 pMaxValue, float pWeight)
        {
            return new Vector4(
                pMinValue.X + (float) pRandom.NextDouble() * (pMaxValue.X - pMinValue.X),
                pMinValue.Y + (float) pRandom.NextDouble() * (pMaxValue.Y - pMinValue.Y),
                pMinValue.Z + (float) pRandom.NextDouble() * (pMaxValue.Z - pMinValue.Z),
                pWeight);
        }
        public static TimeSpan TimeSpan(this Random pRandom, TimeSpan pMinValue, TimeSpan pMaxValue)
        {
            return new TimeSpan(pMinValue.Ticks + (int) (pRandom.Next((int) (pMaxValue.Ticks - pMinValue.Ticks))));
        }

    }
}