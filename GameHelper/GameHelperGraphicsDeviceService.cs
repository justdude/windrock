﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using System.Windows.Forms;

namespace GameHelper
{
    public class GameHelperGraphicsDeviceService : IGraphicsDeviceService
    {
        /* Fields */

        private static GameHelperGraphicsDeviceService instance;

        private GraphicsDevice graphicsDevice;
        private Control control;

        /* Properties */

        public GraphicsDevice GraphicsDevice
        {
            get
            {
                return this.graphicsDevice;
            }
        }

        /* Constructors */

        public GameHelperGraphicsDeviceService()
        {
            this.control = new Control();

            PresentationParameters presentationParameters = new PresentationParameters();
            presentationParameters.RenderTargetUsage = RenderTargetUsage.PreserveContents;
            presentationParameters.DeviceWindowHandle = this.control.Handle; 
            presentationParameters.BackBufferWidth = 1000;
            presentationParameters.BackBufferHeight = 1000;
            presentationParameters.IsFullScreen = false;
            presentationParameters.DepthStencilFormat = DepthFormat.Depth24Stencil8;

            this.graphicsDevice = new GraphicsDevice(
                GraphicsAdapter.DefaultAdapter,
                GraphicsProfile.HiDef,
                presentationParameters);

            if (this.DeviceCreated != null)
            {
                this.DeviceCreated(this.graphicsDevice, new EventArgs());
            }
        }

        /* Events */

        public event EventHandler<EventArgs> DeviceCreated;
        public event EventHandler<EventArgs> DeviceDisposing;
        public event EventHandler<EventArgs> DeviceReset;
        public event EventHandler<EventArgs> DeviceResetting;

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}