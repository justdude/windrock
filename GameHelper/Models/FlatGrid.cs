﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameHelper.Cameras;

namespace GameHelper.Models
{

    public class FlatGrid
    {
        /* Fields */

        private Game game;

        private Vector3 position;
        private Flatness flatness;
        private Color color;

        private VertexPositionColor[] vertices;
        private short[] indices;
        private VertexBuffer vertexBuffer;

        private Effect effect;

        private float dimention1From;
        private float dimention2From;
        private float dimention1To;
        private float dimention2To;
        private float dimention1Step;
        private float dimention2Step;
        private float centerMarkerSize;

        private float minDimention1VertexValue;
        private float maxDimention1VertexValue;
        private float minDimention2VertexValue;
        private float maxDimention2VertexValue;

        /* Properties */



        /* Constructors */

        public FlatGrid(Game pGame, Vector3 pPosition, Flatness pFlatness, Color pColor, float pDimention1From, float pDimention1To, float pDimention1Step, float pDimention2From, float pDimention2To, float pDimention2Step, float pCenterMarkerSize)
        {
            this.game = pGame;
            this.position = pPosition;
            this.flatness = pFlatness;
            this.color = pColor;
            this.dimention1From = pDimention1From;
            this.dimention1To = pDimention1To;
            this.dimention1Step = pDimention1Step;
            this.dimention2From = pDimention2From;
            this.dimention2To = pDimention2To;
            this.dimention2Step = pDimention2Step;
            this.centerMarkerSize = pCenterMarkerSize;

            this.Initialize();
        }
        public FlatGrid(Game pGame, Vector3 pPosition, Flatness pDimentions, Color pColor, float pDimention1From, float pDimention1To, float pDimention1Step, float pDimention2From, float pDimention2To, float pDimention2Step)
            : this(pGame, pPosition, pDimentions, pColor, pDimention1From, pDimention1To, pDimention1Step, pDimention2From, pDimention2To, pDimention2Step, 0)
        {
        }

        /* Private methods */

        private void Initialize()
        {
            // Dimention 1 lines count
            for (int i = 0; i * this.dimention1Step >= this.dimention1From; i--)
            {
                this.minDimention1VertexValue = i * this.dimention1Step;
            }
            for (int i = 0; i * this.dimention1Step <= this.dimention1To; i++)
            {
                this.maxDimention1VertexValue = i * this.dimention1Step;
            }
            int dimention1LinesCount = (int) ((this.maxDimention1VertexValue - this.minDimention1VertexValue) / this.dimention1Step + 1);
            if (this.dimention1From < this.minDimention1VertexValue)
            {
                dimention1LinesCount++;
            }
            if (this.dimention1To > this.maxDimention1VertexValue)
            {
                dimention1LinesCount++;
            }

            // Dimention 2 lines count
            for (int i = 0; i * this.dimention2Step >= this.dimention2From; i--)
            {
                this.minDimention2VertexValue = i * this.dimention2Step;
            }
            for (int i = 0; i * this.dimention2Step <= this.dimention2To; i++)
            {
                this.maxDimention2VertexValue = i * this.dimention2Step;
            }
            int dimention2LinesCount = (int) ((this.maxDimention2VertexValue - this.minDimention2VertexValue) / this.dimention2Step + 1);
            if (this.dimention2From < this.minDimention2VertexValue)
            {
                dimention2LinesCount++;
            }
            if (this.dimention2To > this.maxDimention2VertexValue)
            {
                dimention2LinesCount++;
            }

            // Center marker vertices count.
            int centerMarkerVerticesCount = 0;
            if (this.centerMarkerSize > 0)
            {
                centerMarkerVerticesCount = 8;
            }

            // Creating vertices
            int totalLinesCount = dimention1LinesCount + dimention2LinesCount + centerMarkerVerticesCount;
            int totalVerticesCount = dimention1LinesCount * dimention2LinesCount;
            this.vertices = new VertexPositionColor[totalVerticesCount];
            this.indices = new short[totalVerticesCount];

            int vertexIntex = -1;

            // Lines along dimention 1
            if (this.dimention2From < this.minDimention2VertexValue)
            {
                this.AddLineAlongDimention1(this.dimention2From, ref vertexIntex);
            }
            if (this.dimention2To > this.maxDimention2VertexValue)
            {
                this.AddLineAlongDimention1(this.dimention2To, ref vertexIntex);
            }
            for (float dimention2Value = this.minDimention1VertexValue; dimention2Value <= this.maxDimention2VertexValue; dimention2Value += this.dimention2Step)
            {
                this.AddLineAlongDimention1(dimention2Value, ref vertexIntex);
            }

            // Lines along dimention 2
            if (this.dimention1From < this.minDimention1VertexValue)
            {
                this.AddLineAlongDimention2(this.dimention1From, ref vertexIntex);
            }
            if (this.dimention1To > this.maxDimention1VertexValue)
            {
                this.AddLineAlongDimention2(this.dimention1To, ref vertexIntex);
            }
            for (float dimention1Value = this.minDimention1VertexValue; dimention1Value <= this.maxDimention1VertexValue; dimention1Value += this.dimention1Step)
            {
                this.AddLineAlongDimention2(dimention1Value, ref vertexIntex);
            }

            // Center marker vertices.
            if (centerMarkerVerticesCount > 0)
            {
                AddCenterMarkerLines(ref vertexIntex);
            }

            // Effect.
            this.effect = this.game.Content.Load<Effect>(@"Effects\SimpleEffect");
            this.vertexBuffer = new VertexBuffer(this.game.GraphicsDevice, typeof(VertexPositionColor), this.vertices.Length, BufferUsage.None);
        }
        private Vector3 GetVertexPosition(float pDimention1Value, float pDimention2Value)
        {
            Vector3 vertexPosition = this.position;

            switch (this.flatness)
            {
                case Flatness.XY:
                    vertexPosition += new Vector3(pDimention1Value, pDimention2Value, 0);
                    break;

                case Flatness.XZ:
                    vertexPosition += new Vector3(pDimention1Value, 0, pDimention2Value);
                    break;

                case Flatness.YZ:
                    vertexPosition += new Vector3(0, pDimention1Value, pDimention2Value);
                    break;
            }

            return vertexPosition;
        }
        private void AddLineAlongDimention1(float pDimention2Value, ref int pVertexIndex)
        {
            this.vertices[++pVertexIndex] = new VertexPositionColor(
                this.GetVertexPosition(this.dimention1From, pDimention2Value),
                this.color);
            this.indices[pVertexIndex] = (short) pVertexIndex;

            this.vertices[++pVertexIndex] = new VertexPositionColor(
                this.GetVertexPosition(this.dimention1To, pDimention2Value),
                this.color);
            this.indices[pVertexIndex] = (short) pVertexIndex;
        }
        private void AddLineAlongDimention2(float pDimention1Value, ref int pVertexIndex)
        {
            this.vertices[++pVertexIndex] = new VertexPositionColor(
                this.GetVertexPosition(pDimention1Value, this.dimention2From),
                this.color);
            this.indices[pVertexIndex] = (short) pVertexIndex;

            this.vertices[++pVertexIndex] = new VertexPositionColor(
                this.GetVertexPosition(pDimention1Value, this.dimention2To),
                this.color);
            this.indices[pVertexIndex] = (short) pVertexIndex;
        }
        private void AddCenterMarkerLines(ref int pVertexIndex)
        {
            // Line 1
            this.vertices[++pVertexIndex] = new VertexPositionColor(
                this.GetVertexPosition(0, -this.centerMarkerSize),
                this.color);
            this.indices[pVertexIndex] = (short) pVertexIndex;

            this.vertices[++pVertexIndex] = new VertexPositionColor(
                this.GetVertexPosition(-this.centerMarkerSize, 0),
                this.color);
            this.indices[pVertexIndex] = (short) pVertexIndex;

            // Line 2
            this.vertices[++pVertexIndex] = new VertexPositionColor(
                this.GetVertexPosition(-this.centerMarkerSize, 0),
                this.color);
            this.indices[pVertexIndex] = (short) pVertexIndex;

            this.vertices[++pVertexIndex] = new VertexPositionColor(
                this.GetVertexPosition(0, this.centerMarkerSize),
                this.color);
            this.indices[pVertexIndex] = (short) pVertexIndex;

            // Line 3
            this.vertices[++pVertexIndex] = new VertexPositionColor(
                this.GetVertexPosition(0, this.centerMarkerSize),
                this.color);
            this.indices[pVertexIndex] = (short) pVertexIndex;

            this.vertices[++pVertexIndex] = new VertexPositionColor(
                this.GetVertexPosition(this.centerMarkerSize, 0),
                this.color);
            this.indices[pVertexIndex] = (short) pVertexIndex;

            // Line 3
            this.vertices[++pVertexIndex] = new VertexPositionColor(
                this.GetVertexPosition(this.centerMarkerSize, 0),
                this.color);
            this.indices[pVertexIndex] = (short) pVertexIndex;

            this.vertices[++pVertexIndex] = new VertexPositionColor(
                this.GetVertexPosition(0, -this.centerMarkerSize),
                this.color);
            this.indices[pVertexIndex] = (short) pVertexIndex;
        }

        /* Protected methods */



        /* Public methods */

        public void Draw(Camera pCamera)
        {
            this.effect.Parameters["World"].SetValue(Matrix.Identity);
            this.effect.Parameters["View"].SetValue(pCamera.View);
            this.effect.Parameters["Projection"].SetValue(pCamera.Projection);

            this.game.GraphicsDevice.SetVertexBuffer(vertexBuffer);
            this.effect.CurrentTechnique.Passes[0].Apply();
            this.game.GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionColor>(
                PrimitiveType.LineList,
                this.vertices,
                0,
                this.vertices.Length,
                this.indices,
                0,
                (int) (this.indices.Length * 0.5f));
        }

    }
}