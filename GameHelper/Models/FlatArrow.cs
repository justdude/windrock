﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameHelper.Cameras;

namespace GameHelper.Models
{
    public class FlatArrow
    {
        /* Fields */

        private Game game;
        private VertexPositionColor[] vertices;
        private short[] indices;
        private VertexBuffer vertexBuffer;
        private Effect effect;

        private Vector3 position;
        private Color color;
        private float length;
        private float baseLength;
        private float headLength;
        private float thickness;
        private float baseThicknessFactor;
        private float headThicknessFactor;

        //private float yaw;
        //private float pinch;
        //private float roll;

        private bool isFill;

        private Vector3[] points;
        private PrimitiveType primitiveType;
        private int primitiveCount;

        private Matrix transform;

        /* Properties */

        public bool IsFill
        {
            set
            {
                this.isFill = value;

                UpdateIndices();
            }
            get
            {
                return this.isFill;
            }
        }

        /* Constructors */

        public FlatArrow(Game pGame, Vector3 pPosition, Color pColor, bool pIsFill, float pThickness, float pBaseLength, float pBaseThicknessFactor, float pHeadLength, float pHeadThicknessFactor, float pLength)
        {
            this.game = pGame;
            this.position = pPosition;
            this.color = pColor;
            this.isFill = pIsFill;
            this.position = pPosition;
            this.thickness = pThickness;
            this.baseLength = pBaseLength;
            this.headLength = pHeadLength;
            this.baseThicknessFactor = MathHelper.Clamp(pBaseThicknessFactor, 0, 1);
            this.headThicknessFactor = Math.Max(pHeadThicknessFactor, 1);
            this.length = pLength;

            this.Initialize();
        }

        /* Private methods */

        private void Initialize()
        {
            this.vertices = new VertexPositionColor[9];
            this.effect = this.game.Content.Load<Effect>(@"Effects\SimpleEffect");
            this.vertexBuffer = new VertexBuffer(this.game.GraphicsDevice, typeof(VertexPositionColor), this.vertices.Length, BufferUsage.None);

            this.UpdatePoints();
            this.UpdateIndices();
        }
        private void UpdatePoints()
        {
            float t = this.thickness * 0.5f;
            float t1 = t * this.baseThicknessFactor;
            float t2 = t * this.headThicknessFactor;

            this.points = new Vector3[9];
            this.points[0] = new Vector3(0, 0, -this.baseLength - this.length - this.headLength);
            this.points[1] = new Vector3(t2, 0, -this.baseLength - this.length);
            this.points[2] = new Vector3(t, 0, -this.baseLength - this.length);
            this.points[3] = new Vector3(t, 0, -this.baseLength);
            this.points[4] = new Vector3(t1, 0, 0);
            this.points[5] = new Vector3(-t1, 0, 0);
            this.points[6] = new Vector3(-t, 0, -this.baseLength);
            this.points[7] = new Vector3(-t, 0, -this.baseLength - this.length);
            this.points[8] = new Vector3(-t2, 0, -this.baseLength - this.length);
        }
        private void UpdateIndices()
        {
            if (this.isFill)
            {
                this.indices = new short[]
                { 
                    0, 1, 2,
                    0, 2, 3,
                    0, 3, 4,
                    0, 4, 5,
                    0, 5, 6,
                    0, 6, 7,
                    0, 7, 8
                };
                this.primitiveCount = 7;
                this.primitiveType = PrimitiveType.TriangleList;
            }
            else
            {
                this.indices = new short[]
                { 
                    0, 1,
                    1, 2,
                    2, 3, 
                    3, 4, 
                    4, 5, 
                    5, 6, 
                    6, 7,
                    7, 8,
                    8, 0
                };
                this.primitiveCount = 9;
                this.primitiveType = PrimitiveType.LineList;
            }
        }
        private void UpdateVertices()
        {
            this.UpdatePoints();
            for (int i = 0; i < this.points.Length; i++)
            {
                this.vertices[i] = new VertexPositionColor(
                    this.position + Vector3.Transform(this.points[i], this.transform),
                    this.color);
            }
        }

        /* Protected methods */

        public void Update(Vector3 pPosition, float pLength, float pYaw, float pPinch, float pRoll)
        {
            this.position = pPosition;
            this.length = pLength;
            this.transform = Matrix.CreateFromYawPitchRoll(pYaw, pPinch, pRoll);
            this.UpdateVertices();
        }
        public void Update(float pLength, float pYaw, float pPinch, float pRoll)
        {
            this.length = pLength;
            this.transform = Matrix.CreateFromYawPitchRoll(pYaw, pPinch, pRoll);
            this.UpdateVertices();
        }
        public void Update(Vector3 pPosition, float pYaw, float pPinch, float pRoll)
        {
            this.position = pPosition;
            this.transform = Matrix.CreateFromYawPitchRoll(pYaw, pPinch, pRoll);
            this.UpdateVertices();
        }
        public void Update(float pYaw, float pPinch, float pRoll)
        {
            this.transform = Matrix.CreateFromYawPitchRoll(pYaw, pPinch, pRoll);
            this.UpdateVertices();
        }
        //public void Update(float pLength, float pYaw, float pPinch, float pRoll)
        //{
        //    this.length = pLength;
        //    Matrix transform = Matrix.CreateFromYawPitchRoll(pYaw, pPinch, pRoll);
        //    this.UpdateVertices();
        //}

        /* Public methods */

        public void Draw(Camera pCamera)
        {
            RasterizerState oldRasterizerState = this.game.GraphicsDevice.RasterizerState;
            this.game.GraphicsDevice.RasterizerState = RasterizerState.CullNone;

            this.effect.Parameters["World"].SetValue(Matrix.Identity);
            this.effect.Parameters["View"].SetValue(pCamera.View);
            this.effect.Parameters["Projection"].SetValue(pCamera.Projection);

            this.game.GraphicsDevice.SetVertexBuffer(vertexBuffer);
            this.effect.CurrentTechnique.Passes[0].Apply();
            this.game.GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionColor>(
                this.primitiveType,
                this.vertices,
                0,
                this.vertices.Length,
                this.indices,
                0,
                this.primitiveCount);

            this.game.GraphicsDevice.RasterizerState = oldRasterizerState;
        }

    }
}