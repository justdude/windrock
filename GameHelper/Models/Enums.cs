﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.Models
{
    public enum Flatness : byte
    {
        XY,
        XZ,
        YZ
    };
}