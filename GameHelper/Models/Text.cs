﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameHelper.Cameras;

namespace GameHelper.Models
{
    public class Text
    {
        /* Fields */

        private Game game;
        private VertexPositionTexture[] vertices;
        private short[] indices;
        private VertexBuffer vertexBuffer;
        private Effect effect;

        private SpriteFont font;
        private Vector3 position;
        private Color color;
        private string message;

        private Matrix transform;

        private RenderTarget2D renderTarget;
        //private Texture2D texture = 

        /* Properties */



        /* Constructors */

        public Text(Game pGame, Vector3 pPosition, string pMessage, SpriteFont pFont, Color pColor)
        {
            this.game = pGame;
            this.position = pPosition;
            this.message = pMessage;
            this.font = pFont;
            this.color = pColor;

            this.Initialize();
        }

        /* Private methods */

        private void Initialize()
        {
            this.effect = this.game.Content.Load<Effect>(@"Effects\CustomEffect");
            this.UpdateTexture();
            this.UpdateVertices();
            this.UpdateIndices();
            this.vertexBuffer = new VertexBuffer(this.game.GraphicsDevice, typeof(VertexPositionTexture), this.vertices.Length, BufferUsage.None);

        }
        private void UpdateTexture()
        {
            Vector2 textureSize = this.font.MeasureString(this.message);
            this.renderTarget = new RenderTarget2D(this.game.GraphicsDevice, (int) textureSize.X, (int) textureSize.Y);

            this.game.GraphicsDevice.SetRenderTarget(this.renderTarget);
            SpriteBatch spriteBatch = new SpriteBatch(this.game.GraphicsDevice);
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullNone);
            spriteBatch.DrawString(
                this.font,
                this.message,
                Vector2.Zero,
                this.color);
            spriteBatch.End();
            this.game.GraphicsDevice.SetRenderTarget(null);
        }
        private void UpdateIndices()
        {
            this.indices = new short[]
            { 
                0, 1, 3,
                0, 3, 2
            };
        }
        private void UpdateVertices()
        {
            Vector2 halfTextSizes = this.font.MeasureString(this.message) * 0.01f * 0.5f;
            this.vertices = new VertexPositionTexture[]
            {
                new VertexPositionTexture( 
                    Vector3.Transform( new Vector3(halfTextSizes.X, halfTextSizes.Y, 0), this.transform),
                    new Vector2(0, 0)),
                new VertexPositionTexture( 
                    Vector3.Transform( new Vector3(-halfTextSizes.X, halfTextSizes.Y, 0), this.transform),
                    new Vector2(0, 1) ),                    
                new VertexPositionTexture( 
                    Vector3.Transform( new Vector3(halfTextSizes.X, -halfTextSizes.Y, 0), this.transform),
                    new Vector2(1, 0)),
                new VertexPositionTexture(
                    Vector3.Transform( new Vector3(-halfTextSizes.X, -halfTextSizes.Y, 0), this.transform),
                    new Vector2(1, 1)),
            };
        }

        /* Protected methods */

        public void Update(Vector3 pPosition, float pYaw, float pPinch, float pRoll)
        {
            this.position = pPosition;
            this.transform = Matrix.CreateFromYawPitchRoll(pYaw, pPinch, pRoll);
            this.UpdateVertices();
        }
        public void Update(Vector3 pPosition, Vector3 pTargetPosition)
        {
            this.position = pPosition;
            this.transform = Matrix.CreateLookAt(pPosition, pTargetPosition, Vector3.Up);
            this.UpdateVertices();
        }

        /* Public methods */

        public void Draw(Camera pCamera)
        {
            RasterizerState oldRasterizerState = this.game.GraphicsDevice.RasterizerState;
            this.game.GraphicsDevice.RasterizerState = RasterizerState.CullNone;

            this.effect.Parameters["WorldViewProjection"].SetValue(pCamera.View * pCamera.Projection);
            this.effect.Parameters["Texture"].SetValue(this.renderTarget);

            this.game.GraphicsDevice.SetVertexBuffer(vertexBuffer);
            this.effect.CurrentTechnique.Passes[0].Apply();
            this.game.GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionTexture>(
                PrimitiveType.TriangleList,
                this.vertices,
                0,
                this.vertices.Length,
                this.indices,
                0,
                2);

            this.game.GraphicsDevice.RasterizerState = oldRasterizerState;
        }

    }
}