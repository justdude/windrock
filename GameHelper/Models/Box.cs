﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameHelper.Cameras;

namespace GameHelper.Models
{
    public class Box
    {
        /* Fields */

        private Game game;
        private VertexPositionColor[] vertices;
        private short[] indices;
        private VertexBuffer vertexBuffer;
        private Effect effect;

        private Vector3 min;
        private Vector3 max;
        private Color color;

        /* Properties */

        private Vector3 Min
        {
            set
            {
                this.min = value;
                this.UpdateVertices();
            }
            get
            {
                return this.min;
            }
        }
        private Vector3 Max
        {
            set
            {
                this.max = value;
                this.UpdateVertices();
            }
            get
            {
                return this.max;
            }
        }

        /* Constructors */

        public Box(Game pGame, Color pColor, Vector3 pMin, Vector3 pMax)
        {
            this.game = pGame;
            this.color = pColor;
            this.min = pMin;
            this.max = pMax;

            this.Initialize();
        }
        public Box(Game pGame, Color pColor, BoundingBox pBounds)
            : this(pGame, pColor, pBounds.Min, pBounds.Max)
        {
        }

        /* Private methods */

        private void Initialize()
        {
            this.UpdateVertices();
            this.indices = new short[24]
            {
                0, 1,
                2, 3,
                4, 5,
                6, 7,
                0, 2,
                1, 3,
                4, 6,
                5, 7,
                0, 4,
                1, 5,
                2, 6,
                3, 7
            };
            this.effect = this.game.Content.Load<Effect>(@"Effects\SimpleEffect");
            this.vertexBuffer = new VertexBuffer(this.game.GraphicsDevice, typeof(VertexPositionColor), this.vertices.Length, BufferUsage.None);
        }
        private void UpdateVertices()
        {
            this.vertices = new VertexPositionColor[8]
            {
                new VertexPositionColor(new Vector3(this.min.X, this.min.Y, this.min.Z), this.color),
                new VertexPositionColor(new Vector3(this.max.X, this.min.Y, this.min.Z), this.color),
                new VertexPositionColor(new Vector3(this.min.X, this.max.Y, this.min.Z), this.color),
                new VertexPositionColor(new Vector3(this.max.X, this.max.Y, this.min.Z), this.color),
                new VertexPositionColor(new Vector3(this.min.X, this.min.Y, this.max.Z), this.color),
                new VertexPositionColor(new Vector3(this.max.X, this.min.Y, this.max.Z), this.color),
                new VertexPositionColor(new Vector3(this.min.X, this.max.Y, this.max.Z), this.color),
                new VertexPositionColor(new Vector3(this.max.X, this.max.Y, this.max.Z), this.color)
            };
        }

        /* Protected methods */



        /* Public methods */

        public void Draw(Camera pCamera)
        {
            RasterizerState oldRasterizerState = this.game.GraphicsDevice.RasterizerState;
            this.game.GraphicsDevice.RasterizerState = RasterizerState.CullNone;

            this.effect.Parameters["World"].SetValue(Matrix.Identity);
            this.effect.Parameters["View"].SetValue(pCamera.View);
            this.effect.Parameters["Projection"].SetValue(pCamera.Projection);

            this.game.GraphicsDevice.SetVertexBuffer(vertexBuffer);
            this.effect.CurrentTechnique.Passes[0].Apply();
            this.game.GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionColor>(
                PrimitiveType.LineList,
                this.vertices,
                0,
                this.vertices.Length,
                this.indices,
                0,
                12);

            this.game.GraphicsDevice.RasterizerState = oldRasterizerState;
        }

        // // //

        public void SetBounds(BoundingBox pBounds)
        {
            this.min = pBounds.Min;
            this.max = pBounds.Max;
            this.UpdateVertices();
        }

    }
}