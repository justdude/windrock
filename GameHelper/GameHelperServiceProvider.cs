﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper
{
    public class GameHelperServiceProvider : IServiceProvider
    {
        /* Fields */

        private Dictionary<Type, object> services;

        /* Properties */



        /* Constructors */

        public GameHelperServiceProvider()
        {
            this.services = new Dictionary<Type, object>();
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public void AddService<T>(T pService)
        {
            this.services.Add(typeof(T), pService);
        }

        /* Interfaces */

        #region IServiceProvider

        /* Public methods */

        public object GetService(Type pServiceType)
        {
            object service = null;

            this.services.TryGetValue(pServiceType, out service);

            return service;
        }

        #endregion

    }
}