﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

namespace GameHelper
{
    public class PerformanceDispecher : DrawableGameComponent
    {
        // Fields

        private int drawRate;
        private int drawCount;
        private int updateRate;
        private int updateCount;
        private int tickRate;
        private int tickCount;
        private TimeSpan elapsedTime;

        // Properties

        public float DrawRate
        {
            get
            {
                return this.drawRate;
            }
        }
        public float UpdateRate
        {
            get
            {
                return this.updateRate;
            }
        }
        public float TickRate
        {
            get
            {
                return this.tickRate;
            }
        }

        // Constructors

        public PerformanceDispecher(Game pGame)
            : base(pGame)
        {
        }

        // Private methods




        // Protected methods

        protected override void UnloadContent()
        {
            this.Game.Content.Unload();
        }

        // Public Methods

        public override void Update(GameTime pGameTime)
        {
            this.elapsedTime += pGameTime.ElapsedGameTime;
            this.updateCount++;

            if (this.elapsedTime > TimeSpan.FromSeconds(1))
            {
                this.elapsedTime -= TimeSpan.FromSeconds(1);
                this.drawRate = this.drawCount;
                this.drawCount = 0;
                this.tickRate = this.tickCount;
                this.tickCount = 0;
                this.updateRate = this.updateCount;
                this.updateCount = 0;
            }
        }

        public void NoteDraw()
        {
            this.drawCount++;
        }
        public void NoteTick()
        {
            this.tickCount++;
        }

    }
}