﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper
{
    [Flags]
    public enum Direction : byte
    {
        None = 0,
        IncreaseX = 1,
        East = 1,
        DecreaseX = 2,
        West = 2,
        IncreaseY = 4,
        Top = 4,
        DecreaseY = 8,
        Bottom = 8,
        IncreaseZ = 16,
        North = 16,
        DecreaseZ = 32,
        South = 32,
        All = 63
    }
}