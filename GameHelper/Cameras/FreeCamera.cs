﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using GameHelper.Input;
using Microsoft.Xna.Framework.Input;

namespace GameHelper.Cameras
{
    public class FreeCamera : Camera
    {
        /* Fields */

        private static float yawSpeed = MathHelper.PiOver4 * 0.25f;
        private static float pinchSpeed = MathHelper.PiOver4 * 0.25f;
        private static float rollSpeed = MathHelper.PiOver2;

        private Game game;
        private CustomKeyInputManager keyManager;
        private KeyInputInfo moveForwardKeyInfo;
        private KeyInputInfo moveBackwardKeyInfo;
        private KeyInputInfo moveLeftKeyInfo;
        private KeyInputInfo moveRightKeyInfo;
        private KeyInputInfo moveUpKeyInfo;
        private KeyInputInfo moveDownKeyInfo;
        private KeyInputInfo rollClockwiseKeyInfo;
        private KeyInputInfo rollCounterClockwiseKeyInfo;

        private float speed;

        private MouseState centericMouseState;

        private float yaw;
        private float pinch;
        private Vector3 moveDirection;

        /* Properties */



        /* Constructors */

        public FreeCamera(Game pGame, Vector3 pPosition, Vector3 pTarget, Vector3 pUpVector, float pFieldOfView, float pAspectRation, float pNearDistance, float pFarDistance, float pSpeed)
            : base(pPosition, pTarget, pUpVector, pFieldOfView, pAspectRation, pNearDistance, pFarDistance)
        {
            this.game = pGame;
            this.speed = pSpeed;
        }

        /* Private methods */

        private void InitializeKeyManager()
        {
            this.keyManager = new CustomKeyInputManager();
            this.moveForwardKeyInfo = this.keyManager.RegisterKey("MoveForward", Keys.W);
            this.moveBackwardKeyInfo = this.keyManager.RegisterKey("MoveBackward", Keys.S);
            this.moveLeftKeyInfo = this.keyManager.RegisterKey("MoveLeft", Keys.A);
            this.moveRightKeyInfo = this.keyManager.RegisterKey("MoveRight", Keys.D);
            this.moveUpKeyInfo = this.keyManager.RegisterKey("MoveUp", Keys.Space);
            this.moveDownKeyInfo = this.keyManager.RegisterKey("MoveDown", Keys.LeftShift);
            this.rollClockwiseKeyInfo = this.keyManager.RegisterKey("RollClockwise", Keys.E);
            this.rollCounterClockwiseKeyInfo = this.keyManager.RegisterKey("RollCounterClockwise", Keys.Q);
        }
        private void CenterMousePosition()
        {
            Mouse.SetPosition(
                (int) (this.game.GraphicsDevice.Viewport.Width * 0.5f),
                (int) (this.game.GraphicsDevice.Viewport.Height * 0.5f));
        }

        /* Protected methods */

        protected void UpdatePosition(GameTime pGameTime)
        {
            float time = (float) pGameTime.ElapsedGameTime.TotalSeconds;

            // Движение вперед или назад.
            if (this.moveForwardKeyInfo.IsPressed)
            {
                this.position += this.moveDirection * this.speed * time;
            }
            else if (this.moveBackwardKeyInfo.IsPressed)
            {
                this.position -= this.moveDirection * this.speed * time;
            }

            // Движение вправо или влево.
            if (this.moveRightKeyInfo.IsPressed)
            {
                this.position += Vector3.Cross(this.moveDirection, this.upVector) * this.speed * time;
            }
            else if (this.moveLeftKeyInfo.IsPressed)
            {
                this.position -= Vector3.Cross(this.moveDirection, this.upVector) * this.speed * time;
            }

            // Движение вверх или вниз.
            if (this.moveUpKeyInfo.IsPressed)
            {
                this.position += this.upVector * this.speed * time;
            }
            else if (this.moveDownKeyInfo.IsPressed)
            {
                this.position -= this.upVector * this.speed * time;
            }
        }
        protected void UpdateDirection(GameTime pGameTime)
        {
            float time = (float) pGameTime.ElapsedGameTime.TotalSeconds;

            // Yaw: поворот вокруг вектора, указывающего направление вверх.
            MouseState mouseState = Mouse.GetState();
            if (Math.Abs(this.centericMouseState.X - mouseState.X) > 0.1f)
            {
                this.yaw += (this.centericMouseState.X - mouseState.X) * yawSpeed * time;
                this.pinch += (this.centericMouseState.Y - mouseState.Y) * pinchSpeed * time;
                this.pinch = MathHelper.Clamp(this.pinch, -MathHelper.PiOver2 + 0.001f, MathHelper.PiOver2 - 0.001f);
                this.lookDirection = Vector3.Transform(Vector3.Forward, Matrix.CreateFromYawPitchRoll(this.yaw, this.pinch, 0));
                this.moveDirection = Vector3.Transform(Vector3.Forward, Matrix.CreateFromYawPitchRoll(this.yaw, 0, 0));
            }

            //// Roll: Поворот вокруг вектора направления.
            //if (this.rollClockwiseKeyInfo.IsPressed)
            //{
            //    this.upVector = Vector3.Transform(
            //        this.upVector,
            //        Matrix.CreateFromAxisAngle(
            //            this.direction,
            //            rollSpeed * time));
            //}
            //else if (this.rollCounterClockwiseKeyInfo.IsPressed)
            //{
            //    this.upVector = Vector3.Transform(
            //        this.upVector,
            //        Matrix.CreateFromAxisAngle(
            //            this.direction,
            //            -rollSpeed * time));
            //}

            this.CenterMousePosition();
        }

        /* Public methods */

        public override void Initialize()
        {
            this.InitializeKeyManager();
            this.CenterMousePosition();
            this.centericMouseState = Mouse.GetState();

            //this.lookDirection = Vector3.Forward;
            this.moveDirection = this.lookDirection;
        }
        public override void Update(GameTime pGameTime)
        {
            // Обновить состояния кнопок.
            this.keyManager.Update(pGameTime);

            this.UpdatePosition(pGameTime);
            this.UpdateDirection(pGameTime);
            this.UpdateView();
        }

    }
}