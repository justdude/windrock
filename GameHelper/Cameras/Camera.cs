﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using GameHelper.Input;
using Microsoft.Xna.Framework.Input;

namespace GameHelper.Cameras
{
    public class Camera
    {
        /* Fields */

        protected Matrix view;
        protected Matrix projection;

        protected Vector3 position;
        protected Vector3 lookDirection;
        protected Vector3 upVector;

        /* Properties */

        public Vector3 Position
        {
            set
            {
                this.position = value;
            }
            get
            {
                return this.position;
            }
        }
        public Matrix View
        {
            set
            {
                this.view = value;
            }
            get
            {
                return this.view;
            }
        }
        public Matrix Projection
        {
            set
            {
                this.projection = value;
            }
            get
            {
                return this.projection;
            }
        }
        public Vector3 UpVector
        {
            get
            {
                return this.upVector;
            }
        }

        /* Constructors */

        public Camera(Vector3 pPosition, Vector3 pTarget, Vector3 pUpVector, float pFieldOfView, float pAspectRation, float pNearDistance, float pFarDistance)
        {
            this.position = pPosition;
            this.lookDirection = pTarget - pPosition;
            this.lookDirection.Normalize();
            this.upVector = pUpVector;

            this.view = Matrix.CreateLookAt(pPosition, pTarget, pUpVector);
            this.projection = Matrix.CreatePerspectiveFieldOfView(pFieldOfView, pAspectRation, pNearDistance, pFarDistance);
        }

        /* Private methods */


        protected void UpdateView()
        {
            this.view = Matrix.CreateLookAt(this.position, this.position + this.lookDirection, this.upVector);
        }

        /* Protected methods */



        /* Public methods */

        public virtual void Initialize()
        {
        }
        public virtual void Update(GameTime pGameTime)
        {
        }
        public virtual void Draw()
        {
        }

        // // //

        public void SetLayout(Vector3 pPosition, Vector3 pDirection)
        {
            this.position = pPosition;
            this.lookDirection = pDirection;
            this.UpdateView();
        }

    }
}