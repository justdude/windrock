﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameHelper.GUI.General.Elements;
using GUITest.GUI.Standard.TextureManagers;

namespace GameHelper.GUI.Standard
{
    public class TextBox : UITextBox
    {
        /* Fields */

        private StretchedTextBoxTextureManager textureManager;

        /* Properties */



        /* Constructors */

        public TextBox(StretchedTextBoxTextureManager pTextureManager, Vector2 pPosition, Vector2 pSize, string pText, SpriteFont pFont, Color pForeground, Color pBackground)
            : base(pPosition, pSize, pText, pFont, pForeground, pBackground)
        {
            if (pTextureManager == null)
            {
                textureManager = pTextureManager;
            }

            this.textureManager = pTextureManager;

            this.UpdateVisibleTextLength();
        }

        /* Private methods */

        protected override void UpdateVisibleTextLength()
        {
            this.charWidth = this.font.MeasureString(" ").X;
            float availableWidth = this.size.X - textureManager.LeftBorderWidth - textureManager.RightBorderWidth;
            this.visibleTextLength = (int) Math.Floor(availableWidth / this.charWidth);
        }

        /* Protected methods */

        protected override void UpdateRenderTarget(SpriteBatch pSpriteBatch)
        {
            Rectangle topRectangle = new Rectangle(
                0, 0,
                (int) (this.size.X - textureManager.LeftBorderWidth - textureManager.RightBorderWidth),
                (int) textureManager.TopBorderHeight);
            Rectangle bottomRectangle = new Rectangle(
                0, 0,
                (int) (this.size.X - textureManager.LeftBorderWidth - textureManager.RightBorderWidth),
                (int) textureManager.BottomBorderHeight);
            Rectangle leftRectangle = new Rectangle(
                0, 0,
                (int) textureManager.LeftBorderWidth,
                (int) (this.size.Y - textureManager.TopBorderHeight - textureManager.BottomBorderHeight));
            Rectangle rightRectangle = new Rectangle(
                0, 0,
                (int) textureManager.RightBorderWidth,
                (int) (this.size.Y - textureManager.TopBorderHeight - textureManager.BottomBorderHeight));
            Rectangle fillRectangle = new Rectangle(
                0, 0,
                (int) (this.size.X - textureManager.LeftBorderWidth - textureManager.RightBorderWidth),
                (int) (this.size.Y - textureManager.TopBorderHeight - textureManager.BottomBorderHeight));

            Vector2 leftTopPosition = new Vector2(0, 0);
            Vector2 leftPosition = new Vector2(0, textureManager.TopBorderHeight);
            Vector2 leftBottomPosition = new Vector2(0, this.size.Y - textureManager.BottomBorderHeight);
            Vector2 topPosition = new Vector2(textureManager.LeftBorderWidth, 0);
            Vector2 fillPosition = new Vector2(textureManager.LeftBorderWidth, textureManager.TopBorderHeight);
            Vector2 bottomPosition = new Vector2(textureManager.LeftBorderWidth, this.size.Y - textureManager.BottomBorderHeight);
            Vector2 rightTopPosition = new Vector2(this.size.X - textureManager.RightBorderWidth, 0);
            Vector2 rightPosition = new Vector2(this.size.X - textureManager.RightBorderWidth, textureManager.TopBorderHeight);
            Vector2 rightBottomPosition = new Vector2(this.size.X - textureManager.RightBorderWidth, this.size.Y - textureManager.BottomBorderHeight);

            // // //

            this.formRenderTarget = new RenderTarget2D(pSpriteBatch.GraphicsDevice, (int) this.size.X, (int) this.size.Y);

            pSpriteBatch.GraphicsDevice.SetRenderTarget(this.formRenderTarget);

            pSpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.PointWrap, DepthStencilState.Default, null);

            // // // TextBox

            StretchTexture stretchTexture = null;
            if (this.IsFocused)
            {
                stretchTexture = textureManager.FocusedTexture;
            }
            else
            {
                stretchTexture = textureManager.UnfocusedTexture;
            }

            // Left top
            pSpriteBatch.Draw(
                stretchTexture.LeftTopTexture,
                leftTopPosition,
                null,
                Color.White);

            // Left
            pSpriteBatch.Draw(
                stretchTexture.LeftTexture,
                leftPosition,
                leftRectangle,
                Color.White);

            // Left bottom
            pSpriteBatch.Draw(
                stretchTexture.LeftBottomTexture,
                leftBottomPosition,
                null,
                Color.White);

            // Top
            pSpriteBatch.Draw(
                stretchTexture.TopTexture,
                topPosition,
                topRectangle,
                Color.White);

            // Fill
            pSpriteBatch.Draw(
                stretchTexture.BackgroundTexture,
                fillPosition,
                fillRectangle,
                Color.White);

            // Bottom
            pSpriteBatch.Draw(
                stretchTexture.BottomTexture,
                bottomPosition,
                bottomRectangle,
                Color.White);

            // Right top
            pSpriteBatch.Draw(
                stretchTexture.RightTopTexture,
                rightTopPosition,
                null,
                Color.White);

            // Right
            pSpriteBatch.Draw(
                stretchTexture.RightTexture,
                rightPosition,
                rightRectangle,
                Color.White);

            // Right bottom
            pSpriteBatch.Draw(
                stretchTexture.RightBottomTexture,
                rightBottomPosition,
                null,
                Color.White);

            // // // Text

            if (!string.IsNullOrEmpty(this.Text))
            {
                int availableVisibleTextWidth = (int) Math.Min(this.visibleTextLength, this.Text.Length - this.visibleTextStartIndex);
                string visibleText = this.Text.Substring(this.visibleTextStartIndex, availableVisibleTextWidth);
                Vector2 textPosition = new Vector2(
                    textureManager.LeftBorderWidth,
                    textureManager.TopBorderHeight);
                pSpriteBatch.DrawString(
                    this.font,
                    visibleText,
                    textPosition,
                    this.foreground);

                if (this.isCursorVisible)
                {
                    Vector2 cursorPosition = new Vector2(
                        textureManager.LeftBorderWidth + (this.cursorIndex - this.visibleTextStartIndex) * this.charWidth,
                        textureManager.TopBorderHeight);
                    pSpriteBatch.DrawString(
                        this.font,
                        Cursor,
                        cursorPosition,
                        this.foreground);
                }
            }

            pSpriteBatch.End();

            pSpriteBatch.GraphicsDevice.SetRenderTarget(null);
        }

        /* Public methods */



    }
}