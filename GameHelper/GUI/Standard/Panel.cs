﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.Data;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameHelper.GUI.General.Elements;
using GUITest.GUI.Standard.TextureManagers;

namespace GameHelper.GUI.Standard
{
    public class Panel : UIPanel
    {
        /* Fields */

        private StretchedTextureManager textureManager;

        /* Properties */

        public override Vector2 InnerOffset
        {
            get
            {
                return new Vector2(
                    textureManager.LeftBorderWidth,
                    textureManager.TopBorderHeight);
            }
        }
        public override Vector2 InnerSize
        {
            get
            {
                return new Vector2(
                    this.size.X - textureManager.LeftBorderWidth - textureManager.RightBorderWidth,
                    this.size.Y - textureManager.TopBorderHeight - textureManager.BottomBorderHeight);
            }
        }

        /* Constructors */

        public Panel(StretchedTextureManager pTextureManager, Vector2 pPosition, Vector2 pSize)
            : base(pPosition, pSize)
        {
            if (pTextureManager == null)
            {
                textureManager = pTextureManager;
            }

            this.textureManager = pTextureManager;
        }

        /* Private methods */



        /* Protected methods */

        protected override void UpdateRenderTarget(SpriteBatch pSpriteBatch)
        {
            Rectangle topRectangle = new Rectangle(
                0, 0,
                (int) (this.size.X - textureManager.LeftBorderWidth - textureManager.RightBorderWidth),
                (int) textureManager.TopBorderHeight);
            Rectangle bottomRectangle = new Rectangle(
                0, 0,
                (int) (this.size.X - textureManager.LeftBorderWidth - textureManager.RightBorderWidth),
                (int) textureManager.BottomBorderHeight);
            Rectangle leftRectangle = new Rectangle(
                0, 0,
                (int) textureManager.LeftBorderWidth,
                (int) (this.size.Y - textureManager.TopBorderHeight - textureManager.BottomBorderHeight));
            Rectangle rightRectangle = new Rectangle(
                0, 0,
                (int) textureManager.RightBorderWidth,
                (int) (this.size.Y - textureManager.TopBorderHeight - textureManager.BottomBorderHeight));
            Rectangle fillRectangle = new Rectangle(
                0, 0,
                (int) (this.size.X - textureManager.LeftBorderWidth - textureManager.RightBorderWidth),
                (int) (this.size.Y - textureManager.TopBorderHeight - textureManager.BottomBorderHeight));

            Vector2 leftTopPosition = new Vector2(0, 0);
            Vector2 leftPosition = new Vector2(0, textureManager.TopBorderHeight);
            Vector2 leftBottomPosition = new Vector2(0, this.size.Y - textureManager.BottomBorderHeight);
            Vector2 topPosition = new Vector2(textureManager.LeftBorderWidth, 0);
            Vector2 fillPosition = new Vector2(textureManager.LeftBorderWidth, textureManager.TopBorderHeight);
            Vector2 bottomPosition = new Vector2(textureManager.LeftBorderWidth, this.size.Y - textureManager.BottomBorderHeight);
            Vector2 rightTopPosition = new Vector2(this.size.X - textureManager.RightBorderWidth, 0);
            Vector2 rightPosition = new Vector2(this.size.X - textureManager.RightBorderWidth, textureManager.TopBorderHeight);
            Vector2 rightBottomPosition = new Vector2(this.size.X - textureManager.RightBorderWidth, this.size.Y - textureManager.BottomBorderHeight);

            // // //

            foreach (UIElement element in this.controls.Values)
            {
                element.Refresh(pSpriteBatch);
            }

            this.formRenderTarget = new RenderTarget2D(pSpriteBatch.GraphicsDevice, (int) this.size.X, (int) this.size.Y);

            pSpriteBatch.GraphicsDevice.SetRenderTarget(this.formRenderTarget);
            pSpriteBatch.GraphicsDevice.Clear(Color.Transparent);
            pSpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.PointWrap, DepthStencilState.Default, null);

            // // // Panel

            StretchTexture stretchTexture = textureManager.StretchTexture;

            // Left top
            pSpriteBatch.Draw(
                stretchTexture.LeftTopTexture,
                leftTopPosition,
                null,
                Color.White);

            // Left
            pSpriteBatch.Draw(
                stretchTexture.LeftTexture,
                leftPosition,
                leftRectangle,
                Color.White);

            // Left bottom
            pSpriteBatch.Draw(
                stretchTexture.LeftBottomTexture,
                leftBottomPosition,
                null,
                Color.White);

            // Top
            pSpriteBatch.Draw(
                stretchTexture.TopTexture,
                topPosition,
                topRectangle,
                Color.White);

            // Fill
            pSpriteBatch.Draw(
                stretchTexture.BackgroundTexture,
                fillPosition,
                fillRectangle,
                Color.White);

            // Bottom
            pSpriteBatch.Draw(
                stretchTexture.BottomTexture,
                bottomPosition,
                bottomRectangle,
                Color.White);

            // Right top
            pSpriteBatch.Draw(
                stretchTexture.RightTopTexture,
                rightTopPosition,
                null,
                Color.White);

            // Right
            pSpriteBatch.Draw(
                stretchTexture.RightTexture,
                rightPosition,
                rightRectangle,
                Color.White);

            // Right bottom
            pSpriteBatch.Draw(
                stretchTexture.RightBottomTexture,
                rightBottomPosition,
                null,
                Color.White);

            // Controls
            foreach (UIControl control in this.controls.Values)
            {
                control.Draw(pSpriteBatch);
            }

            pSpriteBatch.End();

            pSpriteBatch.GraphicsDevice.SetRenderTarget(null);
        }

        /* Public methods */



    }
}