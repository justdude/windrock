﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.Data;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameHelper.GUI.General.Elements;
using Microsoft.Xna.Framework.Content;
using GUITest.GUI.Standard.TextureManagers;

namespace GameHelper.GUI.Standard
{
    public class Form : UIForm
    {
        /* Fields */

        private StretchedTextureManager textureManager;

        /* Properties */

        public override Vector2 InnerOffset
        {
            get
            {
                return new Vector2(
                    this.textureManager.LeftBorderWidth,
                    this.textureManager.TopBorderHeight);
            }
        }
        public override Vector2 InnerSize
        {
            get
            {
                return new Vector2(
                    this.size.X - this.textureManager.LeftBorderWidth - this.textureManager.RightBorderWidth,
                    this.size.Y - this.textureManager.TopBorderHeight - this.textureManager.BottomBorderHeight);
            }
        }

        /* Constructors */

        public Form(StretchedTextureManager pTextureManager, Vector2 pPosition, Vector2 pSize, float pScale)
            : base(pPosition, pSize, pScale)
        {
            if (pTextureManager == null)
            {
                throw new NullReferenceException();
            }

            this.textureManager = pTextureManager;
            // ((content.ServiceProvider as GameHelperServiceProvider).GetService(typeof(GameHelperGraphicsDeviceService)) as GameHelperGraphicsDeviceService).GraphicsDevice
        }
        public Form(StretchedTextureManager pTextureManager, Vector2 pPosition, Vector2 pSize)
            : this(pTextureManager, pPosition, pSize, 1)
        {
        }

        /* Private methods */



        /* Protected methods */

        protected override void UpdateRenderTarget(SpriteBatch pSpriteBatch)
        {
            Rectangle topRectangle = new Rectangle(
                0, 0,
                (int) (this.size.X - this.textureManager.LeftBorderWidth - this.textureManager.RightBorderWidth),
                (int) this.textureManager.TopBorderHeight);
            Rectangle bottomRectangle = new Rectangle(
                0, 0,
                (int) (this.size.X - this.textureManager.LeftBorderWidth - this.textureManager.RightBorderWidth),
                (int) this.textureManager.BottomBorderHeight);
            Rectangle leftRectangle = new Rectangle(
                0, 0,
                (int) this.textureManager.LeftBorderWidth,
                (int) (this.size.Y - this.textureManager.TopBorderHeight - this.textureManager.BottomBorderHeight));
            Rectangle rightRectangle = new Rectangle(
                0, 0,
                (int) this.textureManager.RightBorderWidth,
                (int) (this.size.Y - this.textureManager.TopBorderHeight - this.textureManager.BottomBorderHeight));
            Rectangle fillRectangle = new Rectangle(
                0, 0,
                (int) (this.size.X - this.textureManager.LeftBorderWidth - this.textureManager.RightBorderWidth),
                (int) (this.size.Y - this.textureManager.TopBorderHeight - this.textureManager.BottomBorderHeight));

            Vector2 leftTopPosition = new Vector2(0, 0);
            Vector2 leftPosition = new Vector2(0, this.textureManager.TopBorderHeight);
            Vector2 leftBottomPosition = new Vector2(0, this.size.Y - this.textureManager.BottomBorderHeight);
            Vector2 topPosition = new Vector2(this.textureManager.LeftBorderWidth, 0);
            Vector2 fillPosition = new Vector2(this.textureManager.LeftBorderWidth, this.textureManager.TopBorderHeight);
            Vector2 bottomPosition = new Vector2(this.textureManager.LeftBorderWidth, this.size.Y - this.textureManager.BottomBorderHeight);
            Vector2 rightTopPosition = new Vector2(this.size.X - this.textureManager.RightBorderWidth, 0);
            Vector2 rightPosition = new Vector2(this.size.X - this.textureManager.RightBorderWidth, this.textureManager.TopBorderHeight);
            Vector2 rightBottomPosition = new Vector2(this.size.X - this.textureManager.RightBorderWidth, this.size.Y - this.textureManager.BottomBorderHeight);

            // // //

            foreach (UIElement element in this.controls.Values)
            {
                element.Refresh(pSpriteBatch);
            }

            this.formRenderTarget = new RenderTarget2D(pSpriteBatch.GraphicsDevice, (int) this.size.X, (int) this.size.Y);

            pSpriteBatch.GraphicsDevice.SetRenderTarget(this.formRenderTarget);
            pSpriteBatch.GraphicsDevice.Clear(Color.Transparent);
            pSpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.PointWrap, DepthStencilState.Default, null);

            StretchTexture stretchTexture = this.textureManager.StretchTexture;

            // Left top
            pSpriteBatch.Draw(
                stretchTexture.LeftTopTexture,
                leftTopPosition,
                null,
                Color.White);

            // Left
            pSpriteBatch.Draw(
                stretchTexture.LeftTexture,
                leftPosition,
                leftRectangle,
                Color.White);

            // Left bottom
            pSpriteBatch.Draw(
                stretchTexture.LeftBottomTexture,
                leftBottomPosition,
                null,
                Color.White);

            // Top
            pSpriteBatch.Draw(
                stretchTexture.TopTexture,
                topPosition,
                topRectangle,
                Color.White);

            // Fill
            pSpriteBatch.Draw(
                stretchTexture.BackgroundTexture,
                fillPosition,
                fillRectangle,
                Color.White);

            // Bottom
            pSpriteBatch.Draw(
                stretchTexture.BottomTexture,
                bottomPosition,
                bottomRectangle,
                Color.White);

            // Right top
            pSpriteBatch.Draw(
                stretchTexture.RightTopTexture,
                rightTopPosition,
                null,
                Color.White);

            // Right
            pSpriteBatch.Draw(
                stretchTexture.RightTexture,
                rightPosition,
                rightRectangle,
                Color.White);

            // Right bottom
            pSpriteBatch.Draw(
                stretchTexture.RightBottomTexture,
                rightBottomPosition,
                null,
                Color.White);

            // Elements
            foreach (UIElement element in this.controls.Values)
            {
                element.Draw(pSpriteBatch);
            }

            pSpriteBatch.End();

            pSpriteBatch.GraphicsDevice.SetRenderTarget(null);
        }

        /* Public methods */



    }
}