﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace GUITest.GUI.Standard.TextureManagers
{
    public class StretchTexture
    {
        /* Fields */

        private Texture2D backgroundTexture;
        private Texture2D leftTopTexture;
        private Texture2D leftBottomTexture;
        private Texture2D rightTopTexture;
        private Texture2D rightBottomTexture;
        private Texture2D leftTexture;
        private Texture2D rightTexture;
        private Texture2D topTexture;
        private Texture2D bottomTexture;

        /* Properties */

        public Texture2D BackgroundTexture
        {
            get
            {
                return this.backgroundTexture;
            }
        }
        public Texture2D LeftTopTexture
        {
            get
            {
                return this.leftTopTexture;
            }
        }
        public Texture2D LeftBottomTexture
        {
            get
            {
                return this.leftBottomTexture;
            }
        }
        public Texture2D RightTopTexture
        {
            get
            {
                return this.rightTopTexture;
            }
        }
        public Texture2D RightBottomTexture
        {
            get
            {
                return this.rightBottomTexture;
            }
        }
        public Texture2D LeftTexture
        {
            get
            {
                return this.leftTexture;
            }
        }
        public Texture2D RightTexture
        {
            get
            {
                return this.rightTexture;
            }
        }
        public Texture2D TopTexture
        {
            get
            {
                return this.topTexture;
            }
        }
        public Texture2D BottomTexture
        {
            get
            {
                return this.bottomTexture;
            }
        }

        /* Constructors */

        public StretchTexture(Texture2D pTexture, Rectangle? pRectangle, int pLeftBorderWidth, int pRightBorderWidth, int pTopBorderHeight, int pBottomBorderHeight)
        {
            if (pRectangle == null)
            {
                pRectangle = new Rectangle(0, 0, pTexture.Width, pTexture.Height);
            }

            int backgroundWidth = pRectangle.Value.Width - pLeftBorderWidth - pRightBorderWidth;
            int backgroundHeight = pRectangle.Value.Height - pTopBorderHeight - pBottomBorderHeight;

            this.leftTopTexture = this.CopyTextureFrom(
                pTexture,
                new Rectangle(
                    pRectangle.Value.Left,
                    pRectangle.Value.Top,
                    pLeftBorderWidth,
                    pTopBorderHeight));
            this.leftTexture = this.CopyTextureFrom(
                pTexture,
                new Rectangle(
                    pRectangle.Value.Left,
                    pRectangle.Value.Top + pTopBorderHeight,
                    pLeftBorderWidth,
                    backgroundHeight));
            this.leftBottomTexture = this.CopyTextureFrom(
                pTexture,
                new Rectangle(
                    pRectangle.Value.Left,
                    pRectangle.Value.Bottom - pBottomBorderHeight,
                    pLeftBorderWidth,
                    pBottomBorderHeight));

            this.topTexture = this.CopyTextureFrom(
                pTexture,
                new Rectangle(
                    pRectangle.Value.Left + pLeftBorderWidth,
                    pRectangle.Value.Top,
                    backgroundWidth,
                    pTopBorderHeight));
            this.backgroundTexture = this.CopyTextureFrom(
                pTexture,
                new Rectangle(
                    pRectangle.Value.Left + pLeftBorderWidth,
                    pRectangle.Value.Top + pTopBorderHeight,
                    backgroundWidth,
                    backgroundHeight));
            this.bottomTexture = this.CopyTextureFrom(
                pTexture,
                new Rectangle(
                    pRectangle.Value.Left + pLeftBorderWidth,
                    pRectangle.Value.Bottom - pBottomBorderHeight,
                    backgroundWidth,
                    pBottomBorderHeight));

            this.rightTopTexture = this.CopyTextureFrom(
                pTexture,
                new Rectangle(
                    pRectangle.Value.Right - pRightBorderWidth,
                    pRectangle.Value.Top,
                    pRightBorderWidth,
                    pTopBorderHeight));
            this.rightTexture = this.CopyTextureFrom(
                pTexture,
                new Rectangle(
                    pRectangle.Value.Right - pRightBorderWidth,
                    pRectangle.Value.Top + pTopBorderHeight,
                    pRightBorderWidth,
                    backgroundHeight));
            this.rightBottomTexture = this.CopyTextureFrom(
                pTexture,
                new Rectangle(
                    pRectangle.Value.Right - pRightBorderWidth,
                    pRectangle.Value.Bottom - pBottomBorderHeight,
                    pRightBorderWidth,
                    pBottomBorderHeight));

            //this.leftTopTexture = pGame.Content.Load<Texture2D>(@"Textures/GUI/Form/left_top");
            //this.leftTexture = pGame.Content.Load<Texture2D>(@"Textures/GUI/Form/left");
            //this.leftBottomTexture = pGame.Content.Load<Texture2D>(@"Textures/GUI/Form/left_bottom");
            //this.rightTopTexture = pGame.Content.Load<Texture2D>(@"Textures/GUI/Form/right_top");
            //this.rightTexture = pGame.Content.Load<Texture2D>(@"Textures/GUI/Form/right");
            //this.rightBottomTexture = pGame.Content.Load<Texture2D>(@"Textures/GUI/Form/right_bottom");
            //this.topTexture = pGame.Content.Load<Texture2D>(@"Textures/GUI/Form/top");
            //this.fillTexture = pGame.Content.Load<Texture2D>(@"Textures/GUI/Form/fill");
            //this.bottomTexture = pGame.Content.Load<Texture2D>(@"Textures/GUI/Form/bottom");
        }

        /* Private methods */

        private Texture2D CopyTextureFrom(Texture2D pSourceTexture, Rectangle pSourceRectangle)
        {
            Texture2D texture = new Texture2D(pSourceTexture.GraphicsDevice, pSourceRectangle.Width, pSourceRectangle.Height);
            Color[] data = new Color[pSourceRectangle.Width * pSourceRectangle.Height];
            pSourceTexture.GetData<Color>(0, pSourceRectangle, data, 0, data.Length);
            texture.SetData<Color>(data);

            return texture;
        }

        /* Protected methods */



        /* Public methods */



    }
}