﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace GUITest.GUI.Standard.TextureManagers
{
    public class StretchedButtonTextureManager
    {
        /* Fields */

        private int leftBorderWidth;
        private int rightBorderWidth;
        private int topBorderHeight;
        private int bottomBorderHeight;
        private StretchTexture unhoveredUnpressedTexture;
        private StretchTexture unhoveredPressedTexture;
        private StretchTexture hoveredUnpressedTexture;
        private StretchTexture hoveredPressedTexture;

        /* Properties */

        public int LeftBorderWidth
        {
            get
            {
                return this.leftBorderWidth;
            }
        }
        public int RightBorderWidth
        {
            get
            {
                return this.rightBorderWidth;
            }
        }
        public int TopBorderHeight
        {
            get
            {
                return this.topBorderHeight;
            }
        }
        public int BottomBorderHeight
        {
            get
            {
                return this.bottomBorderHeight;
            }
        }
        public StretchTexture UnhoveredUnpressedTexture
        {
            get
            {
                return this.unhoveredUnpressedTexture;
            }
        }
        public StretchTexture UnhoveredPressedTexture
        {
            get
            {
                return this.unhoveredPressedTexture;
            }
        }
        public StretchTexture HoveredUnpressedTexture
        {
            get
            {
                return this.hoveredUnpressedTexture;
            }
        }
        public StretchTexture HoveredPressedTexture
        {
            get
            {
                return this.hoveredPressedTexture;
            }
        }

        /* Constructors */

        public StretchedButtonTextureManager(Texture2D pTexture, Rectangle pUnhoveredUnpressedRectangle, Rectangle pUnhoveredPressedRectangle, Rectangle pHoveredUnpressedRectangle, Rectangle pHoveredPressedRectangle, int pLeftBorderWidth, int pRightBorderWidth, int pTopBorderHeight, int pBottomBorderHeight)
        {
            this.leftBorderWidth = pLeftBorderWidth;
            this.rightBorderWidth = pRightBorderWidth;
            this.topBorderHeight = pTopBorderHeight;
            this.bottomBorderHeight = pBottomBorderHeight;

            this.unhoveredUnpressedTexture = new StretchTexture(pTexture, pUnhoveredUnpressedRectangle, pLeftBorderWidth, pRightBorderWidth, pTopBorderHeight, pBottomBorderHeight);
            this.unhoveredPressedTexture = new StretchTexture(pTexture, pUnhoveredPressedRectangle, pLeftBorderWidth, pRightBorderWidth, pTopBorderHeight, pBottomBorderHeight);
            this.hoveredUnpressedTexture = new StretchTexture(pTexture, pHoveredUnpressedRectangle, pLeftBorderWidth, pRightBorderWidth, pTopBorderHeight, pBottomBorderHeight);
            this.hoveredPressedTexture = new StretchTexture(pTexture, pHoveredPressedRectangle, pLeftBorderWidth, pRightBorderWidth, pTopBorderHeight, pBottomBorderHeight);
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}