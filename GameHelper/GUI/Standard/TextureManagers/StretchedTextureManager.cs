﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace GUITest.GUI.Standard.TextureManagers
{
    public class StretchedTextureManager
    {
        /* Fields */

        private int leftBorderWidth;
        private int rightBorderWidth;
        private int topBorderHeight;
        private int bottomBorderHeight;
        private StretchTexture stretchTexture;

        /* Properties */

        public int LeftBorderWidth
        {
            get
            {
                return this.leftBorderWidth;
            }
        }
        public int RightBorderWidth
        {
            get
            {
                return this.rightBorderWidth;
            }
        }
        public int TopBorderHeight
        {
            get
            {
                return this.topBorderHeight;
            }
        }
        public int BottomBorderHeight
        {
            get
            {
                return this.bottomBorderHeight;
            }
        }
        public StretchTexture StretchTexture
        {
            get
            {
                return this.stretchTexture;
            }
        }

        /* Constructors */

        public StretchedTextureManager(Texture2D pTexture, Rectangle pRectangle, int pLeftBorderWidth, int pRightBorderWidth, int pTopBorderHeight, int pBottomBorderHeight)
        {
            this.leftBorderWidth = pLeftBorderWidth;
            this.rightBorderWidth = pRightBorderWidth;
            this.topBorderHeight = pTopBorderHeight;
            this.bottomBorderHeight = pBottomBorderHeight;

            this.stretchTexture = new StretchTexture(pTexture, pRectangle, pLeftBorderWidth, pRightBorderWidth, pTopBorderHeight, pBottomBorderHeight);
        }

        /* Private methods */

        

        /* Protected methods */

        

        /* Public methods */

        

    }
}