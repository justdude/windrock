﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace GUITest.GUI.Standard.TextureManagers
{
    public class StretchedTextBoxTextureManager
    {
        /* Fields */

        private int leftBorderWidth;
        private int rightBorderWidth;
        private int topBorderHeight;
        private int bottomBorderHeight;
        private StretchTexture unfocusedTexture;
        private StretchTexture focusedTexture;

        /* Properties */

        public int LeftBorderWidth
        {
            get
            {
                return this.leftBorderWidth;
            }
        }
        public int RightBorderWidth
        {
            get
            {
                return this.rightBorderWidth;
            }
        }
        public int TopBorderHeight
        {
            get
            {
                return this.topBorderHeight;
            }
        }
        public int BottomBorderHeight
        {
            get
            {
                return this.bottomBorderHeight;
            }
        }
        public StretchTexture UnfocusedTexture
        {
            get
            {
                return this.unfocusedTexture;
            }
        }
        public StretchTexture FocusedTexture
        {
            get
            {
                return this.focusedTexture;
            }
        }

        /* Constructors */

        public StretchedTextBoxTextureManager(Texture2D pTexture, Rectangle pUnfocusedRectangle, Rectangle pFocusedRectangle, int pLeftBorderWidth, int pRightBorderWidth, int pTopBorderHeight, int pBottomBorderHeight)
        {
            this.leftBorderWidth = pLeftBorderWidth;
            this.rightBorderWidth = pRightBorderWidth;
            this.topBorderHeight = pTopBorderHeight;
            this.bottomBorderHeight = pBottomBorderHeight;

            this.unfocusedTexture = new StretchTexture(pTexture, pUnfocusedRectangle, pLeftBorderWidth, pRightBorderWidth, pTopBorderHeight, pBottomBorderHeight);
            this.focusedTexture = new StretchTexture(pTexture, pFocusedRectangle, pLeftBorderWidth, pRightBorderWidth, pTopBorderHeight, pBottomBorderHeight);
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}