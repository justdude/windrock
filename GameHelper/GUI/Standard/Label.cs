﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameHelper.GUI.General.Elements;

namespace GameHelper.GUI.Standard
{
    public class Label : UILabel
    {
        /* Fields */



        /* Properties */



        /* Constructors */

        public Label(Vector2 pPosition, string pText, SpriteFont pFont, Color pForeground)
            : base(pPosition, pText, pFont, pForeground)
        {
        }

        /* Private methods */

        

        /* Protected methods */

        protected override void UpdateRenderTarget(SpriteBatch pSpriteBatch)
        {
            if (string.IsNullOrEmpty(this.text))
            {
                this.formRenderTarget = new RenderTarget2D(pSpriteBatch.GraphicsDevice, 0, 0);
            }
            else
            {
                this.formRenderTarget = new RenderTarget2D(pSpriteBatch.GraphicsDevice, (int) this.size.X, (int) this.size.Y);

                pSpriteBatch.GraphicsDevice.SetRenderTarget(this.formRenderTarget);
                pSpriteBatch.GraphicsDevice.Clear(Color.Transparent);

                pSpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.PointWrap, DepthStencilState.Default, null);

                pSpriteBatch.DrawString(
                    this.font,
                    this.text,
                    Vector2.Zero,
                    this.foreground);

                pSpriteBatch.End();
            }

            pSpriteBatch.GraphicsDevice.SetRenderTarget(null);
        }

        /* Public methods */



    }
}