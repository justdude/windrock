﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using GameHelper.Input;

namespace GameHelper.GUI.General
{
    public class TextInput
    {
        /* Constants */

        private static readonly Vector2 CursorLocalOffset = new Vector2(0, -1);

        /* Fields */

        private TextOutput output;
        private bool isEnterEnabled;
        private bool isEnterSplitText;
        private int cursorIndex;
        private TimeSpan cursorBlinkTimeout;
        private TimeSpan cursorBlinkInterval;
        private bool isCursorVisible;
        private Texture2D pixel;
        private Vector2 cursorSize;
        private Vector2 cursorOffset;
        private bool isFocused;

        /* Properties */

        public bool IsEnterEnabled
        {
            get
            {
                return this.isEnterEnabled;
            }
            set
            {
                this.isEnterEnabled = value;
            }
        }
        public bool IsEnterSplitText
        {
            get
            {
                return this.isEnterSplitText;
            }
            set
            {
                this.isEnterSplitText = value;
            }
        }
        public bool IsFocused
        {
            get
            {
                return this.isFocused;
            }
            set
            {
                this.isFocused = value;

                if (this.isFocused)
                {
                    this.cursorBlinkTimeout = this.cursorBlinkInterval;
                    this.isCursorVisible = true;
                }
                else
                {
                    this.cursorBlinkTimeout = TimeSpan.Zero;
                    this.isCursorVisible = false;
                }
            }
        }

        /* Constructors */

        public TextInput(Game pGame, TextOutput pOutput)
        {
            this.output = pOutput;

            this.cursorBlinkInterval = TimeSpan.FromSeconds(0.5f);
            this.cursorBlinkTimeout = this.cursorBlinkInterval;
            this.isCursorVisible = true;

            this.pixel = pGame.Content.Load<Texture2D>(@"Textures\pixel");
            this.cursorSize = new Vector2(1, this.output.Font.LineSpacing + 2);

            this.cursorIndex = -1;
        }

        /* Private methods */

        private void ResetCursor()
        {
            this.cursorOffset = Vector2.Zero;
        }
        private void MoveCursor(float pOffset)
        {
            if (this.cursorOffset.X + pOffset > this.output.IntendedSize.X)
            {
                this.cursorOffset.X = pOffset;
                this.cursorOffset.Y += this.output.Font.LineSpacing;
            }
            else if (this.cursorOffset.X + pOffset < 0)
            {
                this.cursorOffset.X = this.output.IntendedSize.X + pOffset;
                this.cursorOffset.Y += this.output.Font.LineSpacing;
            }
            else
            {
                this.cursorOffset.X += pOffset;
            }
        }
        private float GetCharWidth(int pIndex)
        {
            return this.output.Font.MeasureString(this.output.Text[pIndex].ToString()).X;
        }

        /* Protected methods */



        /* Public methods */

        public string Update(GameTime pGameTime, KeyInputManager pKeyManager)
        {
            string line = null;

            if (this.isFocused)
            {
                this.cursorBlinkTimeout -= pGameTime.ElapsedGameTime;
                if (this.cursorBlinkTimeout <= TimeSpan.Zero)
                {
                    this.cursorBlinkTimeout = this.cursorBlinkInterval;
                    this.isCursorVisible = !this.isCursorVisible;
                }
            }

            if (pKeyManager.IsKeyPressedAndNotHandled)
            {
                Keys pressedKey = pKeyManager.PressedKey;
                if (pressedKey != Keys.None)
                {
                    if (pressedKey == Keys.Back)
                    {
                        if (this.cursorIndex > -1)
                        {
                            this.MoveCursor(-this.GetCharWidth(this.cursorIndex));
                            this.output.Remove(this.cursorIndex, 1);
                            this.cursorIndex--;
                        }
                    }
                    else if (pressedKey == Keys.Delete)
                    {
                        if (this.cursorIndex < this.output.Text.Length - 1)
                        {
                            //this.MoveCursorBackward();
                            this.output.Remove(this.cursorIndex + 1, 1);
                        }
                    }
                    else if (pressedKey == Keys.Enter)
                    {
                        if (this.isEnterEnabled)
                        {
                            if (this.isEnterSplitText)
                            {
                                line = this.output.Text;
                                this.output.Clear();
                                this.cursorIndex = -1;
                                this.ResetCursor();
                            }
                            else
                            {
                                this.cursorIndex++;
                                this.output.Insert(this.cursorIndex, pKeyManager.GetPressedChar());
                                this.MoveCursor(this.GetCharWidth(this.cursorIndex));
                            }
                        }
                    }
                    else if (pressedKey == Keys.Left)
                    {
                        if (this.cursorIndex > -1)
                        {
                            this.MoveCursor(-this.GetCharWidth(this.cursorIndex));
                            this.cursorIndex--;
                        }
                    }
                    else if (pressedKey == Keys.Right)
                    {
                        if (this.cursorIndex < this.output.Text.Length - 1)
                        {
                            this.cursorIndex++;
                            this.MoveCursor(this.GetCharWidth(this.cursorIndex));
                        }
                    }
                    else
                    {
                        this.cursorIndex++;
                        if (pKeyManager.IsPressedCharKey())
                        {
                            this.output.Insert(this.cursorIndex, pKeyManager.GetPressedChar());
                            this.MoveCursor(this.GetCharWidth(this.cursorIndex));
                        }
                    }

                    pKeyManager.SetPressedKeyHandled();
                }
            }

            this.output.Update();

            return line;
        }
        public void Draw(SpriteBatch pSpriteBatch)
        {
            this.output.Draw(pSpriteBatch);
            if (this.isFocused && this.isCursorVisible)
            {
                Vector2 cursorPosition = this.output.IntendedPosition + this.cursorOffset + TextInput.CursorLocalOffset;
                pSpriteBatch.Draw(this.pixel, cursorPosition, null, Color.Black, 0, Vector2.Zero, this.cursorSize, SpriteEffects.None, 0);
            }
        }

        // // //

        public void SetText(string pText)
        {
            this.output.Text = pText ?? string.Empty;
            this.ResetCursor();
        }

    }
}