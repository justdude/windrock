﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameHelper.GUI.General
{
    public class LinesOutput
    {
        /* Constants */

        private const int ScrollBarWidth = 4;

        /* Fields */

        private List<string> lines;
        private int maxLinesCount;
        private List<string> history;
        private int maxInputHistoryCount;
        private string formattedText;
        private Vector2 position;
        private Vector2 size;
        private SpriteFont font;
        private Color foreground;
        private Color background;
        private bool isUpdateFormattedTextRequired;
        private bool isMultiLine;
        private bool isDrawLast;
        private Texture2D pixel;
        private Vector4 padding;
        private Vector2 intendedSize;

        private int scrollIndex;
        private Vector2 scrollBarPosition;
        private float scrollbarSliderHeight;
        private Vector2 scrollbarSliderPosition;

        /* Properties */

        public Vector2 Position
        {
            get
            {
                return this.position;
            }
            set
            {
                this.position = value;
            }
        }
        public Vector2 Size
        {
            get
            {
                return this.size;
            }
            set
            {
                this.isUpdateFormattedTextRequired = this.size != value;
                this.size = value;

                this.intendedSize = new Vector2(
                    this.size.X - this.padding.X - this.padding.Z - LinesOutput.ScrollBarWidth,
                    this.size.Y - this.padding.Y - this.padding.W);
                this.scrollBarPosition = new Vector2(
                    this.position.X + this.padding.X + this.intendedSize.X,
                    this.position.Y + this.padding.Y);
            }
        }
        public bool IsMultiLine
        {
            get
            {
                return this.isMultiLine;
            }
            set
            {
                this.isUpdateFormattedTextRequired = this.isMultiLine != value;
                this.isMultiLine = value;
            }
        }
        public bool IsDrawLast
        {
            get
            {
                return this.isDrawLast;
            }
            set
            {
                this.isUpdateFormattedTextRequired = this.isDrawLast != value;
                this.isDrawLast = value;
            }
        }
        public Color Foreground
        {
            get
            {
                return this.foreground;
            }
            set
            {
                this.foreground = value;
            }
        }
        public Color Background
        {
            get
            {
                return this.background;
            }
            set
            {
                this.background = value;
            }
        }
        public Vector4 Padding
        {
            get
            {
                return this.padding;
            }
            set
            {
                this.isUpdateFormattedTextRequired = this.padding != value;
                this.padding = value;

                this.intendedSize = new Vector2(
                    this.size.X - this.padding.X - this.padding.Z - LinesOutput.ScrollBarWidth,
                    this.size.Y - this.padding.Y - this.padding.W);
                this.scrollBarPosition = new Vector2(
                    this.position.X + this.padding.X + this.intendedSize.X,
                    this.position.Y + this.padding.Y);
            }
        }
        public SpriteFont Font
        {
            get
            {
                return this.font;
            }
        }
        public int LinesCount
        {
            get
            {
                return this.lines.Count;
            }
        }
        public int HistoryCount
        {
            get
            {
                return this.history.Count;
            }
        }

        public Vector2 IntendedPosition
        {
            get
            {
                return new Vector2(
                    this.position.X + this.padding.X,
                    this.position.Y + this.padding.Y);
            }
        }
        public Vector2 IntendedSize
        {
            get
            {
                return this.intendedSize;
            }
            set
            {
                this.intendedSize = value;

                this.size = new Vector2(
                    this.intendedSize.X + this.padding.X + this.padding.Z + LinesOutput.ScrollBarWidth,
                    this.intendedSize.Y + this.padding.Y + this.padding.W);
                this.scrollBarPosition = new Vector2(
                    this.position.X + this.padding.X + this.intendedSize.X,
                    this.position.Y + this.padding.Y);
            }
        }
        public int VisibleLineCount
        {
            get
            {
                return (int) Math.Floor((float) this.intendedSize.Y / this.font.LineSpacing);
            }
        }

        /* Constructors */

        public LinesOutput(Game pGame, Vector2 pPosition, Vector2 pSize, SpriteFont pFont, Color pForeground)
        {
            this.Position = pPosition;
            this.size = pSize;
            this.font = pFont;
            this.foreground = pForeground;

            this.lines = new List<string>();
            this.history = new List<string>();
            this.maxLinesCount = 100;
            this.pixel = pGame.Content.Load<Texture2D>(@"Textures\pixel");
        }

        /* Private methods */

        private void UpdateFormattedText()
        {
            StringBuilder formattedTextBuilder = new StringBuilder();
            for (int i = this.scrollIndex; i < this.lines.Count; i++)
            {
                formattedTextBuilder.Append(this.lines[i]);
                if (i < this.scrollIndex + this.VisibleLineCount)
                {
                    formattedTextBuilder.AppendLine();
                }
                else
                {
                    break;
                }
            }

            this.formattedText = formattedTextBuilder.ToString();
        }

        /* Protected methods */



        /* Public methods */

        public void Update()
        {
            if (this.isUpdateFormattedTextRequired)
            {
                this.UpdateFormattedText();
            }
        }
        public void Draw(SpriteBatch pSpriteBatch)
        {
            // Задний фон.
            pSpriteBatch.Draw(this.pixel, this.position, null, this.background, 0, Vector2.Zero, this.size, SpriteEffects.None, 0);
            if (!string.IsNullOrEmpty(this.formattedText))
            {
                pSpriteBatch.DrawString(this.font, this.formattedText, this.IntendedPosition, this.foreground);
            }

            // Полоса прокрутки.
            if (this.lines.Count > this.VisibleLineCount)
            {
                this.scrollbarSliderPosition = new Vector2(
                    this.scrollBarPosition.X,
                    this.scrollBarPosition.Y + (1f / (this.lines.Count - this.VisibleLineCount)) * (this.intendedSize.Y - this.scrollbarSliderHeight));

                pSpriteBatch.Draw(
                    this.pixel,
                    this.scrollbarSliderPosition,
                    null,
                    this.foreground,
                    0,
                    Vector2.Zero,
                    new Vector2(
                        ScrollBarWidth,
                        this.scrollbarSliderHeight),
                    SpriteEffects.None,
                    0);
            }
        }

        // // //

        public void AddToLines(string pText)
        {
            foreach (string line in pText.Split('\n'))
            {
                if (this.font.MeasureString(line).X <= this.intendedSize.X)
                {
                    this.lines.Add(line);
                }
                else
                {
                    int startIndex = 0;
                    float charWidth = 0;
                    float lineWidth = 0;
                    int index = 0;
                    for (; index < line.Length; index++)
                    {
                        charWidth = this.font.MeasureString(line[index].ToString()).X;
                        if (lineWidth + charWidth > this.intendedSize.X)
                        {
                            this.lines.Add(line.Substring(startIndex, index - startIndex));
                            startIndex = index;
                            lineWidth = charWidth;
                        }
                        else
                        {
                            lineWidth += charWidth;
                        }
                    }

                    if (startIndex < line.Length - 1)
                    {
                        this.lines.Add(line.Substring(startIndex, line.Length - 1 - startIndex));
                    }
                }

                if (this.lines.Count > this.maxLinesCount)
                {
                    this.lines.RemoveRange(0, this.lines.Count - this.maxLinesCount);
                }
            }

            this.isUpdateFormattedTextRequired = true;
            this.scrollbarSliderHeight = MathHelper.Clamp(
                (float) this.VisibleLineCount / (float) this.lines.Count * this.intendedSize.Y,
                4,
                this.intendedSize.Y);
        }
        public void AddToHistory(string pText)
        {
            this.history.Add(pText);
        }
        public void AddToLinesAndHistory(string pLinesText, string pHistoryText)
        {
            this.AddToLines(pLinesText);
            this.AddToHistory(pHistoryText);
        }
        public void ScrollTo(int pIndex)
        {
            this.scrollIndex = (int) MathHelper.Clamp(pIndex, 0, this.lines.Count - 1);
            this.isUpdateFormattedTextRequired = true;
        }
        public void ScrollToStart()
        {
            this.scrollIndex = 0;
            this.isUpdateFormattedTextRequired = true;
        }
        public void ScrollToEnd()
        {
            this.scrollIndex = (int) MathHelper.Clamp(this.lines.Count - 1 - this.VisibleLineCount, 0, this.lines.Count - 1);
            this.isUpdateFormattedTextRequired = true;
        }
        public void ScrollAt(int pOffset)
        {
            this.scrollIndex = (int) MathHelper.Clamp(this.scrollIndex + pOffset, 0, this.lines.Count - 1);
            this.isUpdateFormattedTextRequired = true;
        }
        public string GetLine(int pIndex)
        {
            return this.lines[pIndex];
        }
        public string GetHistory(int pIndex)
        {
            return this.history[pIndex];
        }

    }
}