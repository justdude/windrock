﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameHelper.Input
{
    public class TextOutput
    {
        /* Fields */

        private string text;
        private string formattedText;
        private Vector2 position;
        private Vector2 size;
        private SpriteFont font;
        private Color foreground;
        private Color background;
        private bool isUpdateFormattedTextRequired;
        private bool isMultiLine;
        private bool isDrawLast;
        private Texture2D pixel;
        private Vector4 padding;

        /* Properties */

        public string Text
        {
            get
            {
                return this.text;
            }
            set
            {
                this.isUpdateFormattedTextRequired = true;
                this.text = value;
            }
        }
        public Vector2 Position
        {
            get
            {
                return this.position;
            }
            set
            {
                this.position = value;
            }
        }
        public Vector2 Size
        {
            get
            {
                return this.size;
            }
            set
            {
                this.isUpdateFormattedTextRequired = this.size != value;
                this.size = value;
            }
        }
        public bool IsMultiLine
        {
            get
            {
                return this.isMultiLine;
            }
            set
            {
                this.isUpdateFormattedTextRequired = this.isMultiLine != value;
                this.isMultiLine = value;
            }
        }
        public bool IsDrawLast
        {
            get
            {
                return this.isDrawLast;
            }
            set
            {
                this.isUpdateFormattedTextRequired = this.isDrawLast != value;
                this.isDrawLast = value;
            }
        }
        public float Lines
        {
            get
            {
                return this.IntendedSize.Y / this.font.LineSpacing;
            }
            set
            {
                this.size.Y = this.font.LineSpacing * value + this.padding.Y + this.padding.W;
            }
        }
        public Color Foreground
        {
            get
            {
                return this.foreground;
            }
            set
            {
                this.foreground = value;
            }
        }
        public Color Background
        {
            get
            {
                return this.background;
            }
            set
            {
                this.background = value;
            }
        }
        public Vector4 Padding
        {
            get
            {
                return this.padding;
            }
            set
            {
                this.isUpdateFormattedTextRequired = this.padding != value;
                this.padding = value;
            }
        }
        public SpriteFont Font
        {
            get
            {
                return this.font;
            }
        }

        public Vector2 IntendedPosition
        {
            get
            {
                return new Vector2(
                    this.position.X + this.padding.X,
                    this.position.Y + this.padding.Y);
            }
        }
        public Vector2 IntendedSize
        {
            get
            {
                return new Vector2(
                    this.size.X - this.padding.X - this.padding.Z,
                    this.size.Y - this.padding.Y - this.padding.W);
            }
        }

        /* Constructors */

        public TextOutput(Game pGame, Vector2 pPosition, Vector2 pSize, SpriteFont pFont, Color pForeground)
        {
            this.Position = pPosition;
            this.size = pSize;
            this.font = pFont;
            this.foreground = pForeground;

            this.text = null;//string.Empty;
            this.pixel = pGame.Content.Load<Texture2D>(@"Textures\pixel");
        }

        /* Private methods */

        private void UpdateFormattedText()
        {
            if (this.text == null)
            {
                this.formattedText = null;
            }
            else
            {
                StringBuilder formattedTextBuilder = new StringBuilder();
                float formattedTextLineWidth = 0;
                int formattedTextLineIndex = 0;
                int formattedTextMaxLinesCount = (int) Math.Floor(this.IntendedSize.Y / this.font.LineSpacing);

                for (int i = 0; i < this.text.Length; i++)
                {
                    if (this.text[i] == '\n')
                    {
                        formattedTextLineIndex++;
                        if (formattedTextLineIndex >= formattedTextMaxLinesCount)
                        {
                            break;
                        }
                        formattedTextLineWidth = 0;
                        formattedTextBuilder.AppendLine();
                    }
                    else
                    {
                        formattedTextLineWidth += this.font.MeasureString(this.text[i].ToString()).X;

                        if (formattedTextLineWidth > this.IntendedSize.X)
                        {
                            formattedTextLineIndex++;
                            if (formattedTextLineIndex >= formattedTextMaxLinesCount)
                            {
                                break;
                            }
                            formattedTextLineWidth = this.font.MeasureString(this.text[i].ToString()).X;
                            formattedTextBuilder.AppendLine();
                        }
                        formattedTextBuilder.Append(this.text[i]);
                    }

                }

                this.formattedText = formattedTextBuilder.ToString();

                return;

                Vector2 textSize = this.font.MeasureString(this.text);
                if (textSize.X > this.IntendedSize.X
                    || textSize.Y > this.IntendedSize.Y)
                {
                    if (this.isMultiLine)
                    {
                        if (this.isDrawLast)
                        {

                        }
                        else //if (!this.isDrawLast)
                        {

                        }
                    }
                    else //if (!this.isMultiLine)
                    {
                        if (this.isDrawLast)
                        {

                        }
                        else //if (!this.isDrawLast)
                        {

                        }
                    }
                }
                else
                {
                    this.formattedText = this.text;
                }
            }
        }

        /* Protected methods */



        /* Public methods */

        public void Update()
        {
            if (this.isUpdateFormattedTextRequired)
            {
                this.UpdateFormattedText();
            }
        }
        public void Update(string pText)
        {
            this.Text = pText;
            this.Update();
        }
        public void Draw(SpriteBatch pSpriteBatch)
        {
            pSpriteBatch.Draw(this.pixel, this.position, null, this.background, 0, Vector2.Zero, this.size, SpriteEffects.None, 0);
            if (!string.IsNullOrEmpty(this.formattedText))
            {
                pSpriteBatch.DrawString(this.font, this.formattedText, this.IntendedPosition, this.foreground);
            }
        }

        // // //

        public void AppendLine()
        {
            this.Text = string.Concat(this.text, '\n');
        }
        public void AppendLine(string pText)
        {
            if (string.IsNullOrEmpty(this.text))
            {
                this.Text = pText;
            }
            else
            {
                this.Text = string.Concat(this.text, '\n', pText);
            }
        }
        public void Append(string pText)
        {
            this.Text = string.Concat(this.text, pText);
        }
        public void Append(char pChar)
        {
            this.Text = string.Concat(this.text, pChar);
        }
        public void Insert(int pIndex, char pChar)
        {
            if (string.IsNullOrEmpty(this.text))
            {
                this.text = pChar.ToString();
            }
            else
            {
                this.Text = this.text.Insert(pIndex, pChar.ToString());
            }
        }
        public void Remove(int pIndex, int pCount)
        {
            this.Text = this.text.Remove(pIndex, pCount);
        }
        public void RemoveFirst(int pCount)
        {
            this.Text = this.text.Remove(0, pCount);
        }
        public void RemoveLast(int pCount)
        {
            int count = Math.Min(pCount, this.text.Length);
            if (count > 0)
            {
                int index = Math.Max(this.text.Length - pCount, 0);
                this.Text = this.text.Remove(index, count);
            }
        }
        public void Clear()
        {
            this.Text = string.Empty;
        }

    }
}