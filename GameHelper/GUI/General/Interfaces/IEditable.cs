﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.GUI.General.Events;

namespace GameHelper.GUI.General.Interfaces
{
    public interface IEditable
    {
        /* Properties */

        string Text
        {
            get;
            set;
        }

        /* Events */

        event ValueEventHandler<string> Edited;
        event ValueEventHandler<string> Editing;

        /* Methods */



    }
}