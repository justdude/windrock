﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.GUI.General.Events;

namespace GameHelper.GUI.General.Interfaces
{
    public interface IClickable
    {
        /* Properties */

        

        /* Events */

        event MouseEventHandler MouseDown;
        event MouseEventHandler MouseUp;
        event MouseEventHandler MouseClick;
        
        /* Methods */

        void UpdateClickable(MouseEventArgument pMouseArgument);

    }
}
