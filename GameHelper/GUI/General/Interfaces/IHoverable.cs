﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.GUI.General.Events;

namespace GameHelper.GUI.General.Interfaces
{
    interface IHoverable
    {
        /* Properties */

        bool IsHovered
        {
            get;
        }

        /* Events */

        event MouseEventHandler MouseMove;
        event MouseEventHandler MouseEnter;
        event MouseEventHandler MouseLeave;

        /* Methods */

        void UpdateHoverable(MouseEventArgument pMouseArgument);

    }
}
