﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.GUI.General.Events;

namespace GameHelper.GUI.General.Interfaces
{
    public interface IFocusable
    {
        /* Properties */

        bool IsFocused
        {
            get;
        }

        /* Events */

        event UIEventHandler Focused;
        event UIEventHandler Unfocused;
        event KeyEventHandler KeyDown;
        event KeyEventHandler KeyUp;
        event KeyEventHandler KeyHold;

        /* Methods */

        void UpdateFocusable(bool pIsFocused);
        void ProcessKeyInput(KeyEventArgument pKeyArgument);
        void Focus();

    }
}