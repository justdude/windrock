﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.GUI.General.Events;

namespace GameHelper.GUI.General.Interfaces
{
    public interface IPressable
    {
        /* Properties */

        bool IsPressed
        {
            get;
        }

        /* Events */

        event UIEventHandler Pressed;
        event UIEventHandler Released;

        /* Methods */



    }
}