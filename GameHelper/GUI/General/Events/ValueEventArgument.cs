﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.GUI.General.Events
{
    public class ValueEventArgument<T>
    {
        /* Fields */

        private T value;

        /* Properties */

        public T Value
        {
            get
            {
                return this.value;
            }
        }

        /* Constructors */

        public ValueEventArgument(T pValue)
        {
            this.value = pValue;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}