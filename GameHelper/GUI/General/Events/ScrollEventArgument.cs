﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.GUI.General.Events
{
    public class ScrollEventArgument 
    {
        /* Fields */

        private int notchesScrolled;

        /* Properties */

        public int Notches
        {
            get
            {
                return this.notchesScrolled;
            }
        }

        /* Constructors */

        public ScrollEventArgument(int pNotchesScrolled)
        {
            this.notchesScrolled = pNotchesScrolled;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}