﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using GameHelper.Data;
using GameHelper.Input;

namespace GameHelper.GUI.General.Events
{
    public class MouseEventArgument
    {
        /* Fields */

        private Vector2 position;
        private Vector2 positionOnScreen;
        private MouseButtons heldButtons;
        private MouseButtons pressedButtons;
        private MouseButtons releasedButtons;
        private int notchesScrolled;
        private bool isHandled;

        /* Properties */

        public Vector2 Position
        {
            get
            {
                return this.position;
            }
        }
        public Vector2 PositionOnScreen
        {
            get
            {
                return this.positionOnScreen;
            }
        }
        public MouseButtons HeldButtons
        {
            get
            {
                return this.heldButtons;
            }
        }
        public MouseButtons PressedButtons
        {
            get
            {
                return this.pressedButtons;
            }
        }
        public MouseButtons ReleasedButtons
        {
            get
            {
                return this.releasedButtons;
            }
        }
        public int WheelNotches
        {
            get
            {
                return this.notchesScrolled;
            }
        }
        public bool IsHandled
        {
            get
            {
                return this.isHandled;
            }
            set
            {
                this.isHandled = value;
            }
        }

        /* Constructors */

        public MouseEventArgument(Vector2 pPositionOnScreen, Vector2 pPosition, MouseButtons pHeldButtons, MouseButtons pPressedButtons, MouseButtons pReleasedButtons, int pNotchesScrolled)
        {
            this.positionOnScreen = pPositionOnScreen;
            this.position = pPosition;
            this.heldButtons = pHeldButtons;
            this.pressedButtons = pPressedButtons;
            this.releasedButtons = pReleasedButtons;
            this.notchesScrolled = pNotchesScrolled;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        

    }
}