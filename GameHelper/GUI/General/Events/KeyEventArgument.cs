﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using GameHelper.Input.Data;

namespace GameHelper.GUI.General.Events
{
    public class KeyEventArgument
    {
        /* Fields */

        private Keys pressedKey;
        private Keys heldKey;
        private char _char;
        //private bool isUpperCase;
        private SpecialKeys specialKeys;

        /* Properties */

        public Keys PressedKey
        {
            get
            {
                return this.pressedKey;
            }
        }
        public Keys HeldKey
        {
            get
            {
                return this.heldKey;
            }
        }
        public char Char
        {
            get
            {
                return this._char;
            }
        }
        //public bool IsUpperCase
        //{
        //    get
        //    {
        //        return this.isUpperCase;
        //    }
        //}
        public SpecialKeys SpecialKeys
        {
            get
            {
                return this.specialKeys;
            }
        }

        /* Constructors */

        public KeyEventArgument(Keys pPressedKey, Keys pHeldKey, char pChar, SpecialKeys pSpecialKeys)
        {
            this.pressedKey = pPressedKey;
            this.heldKey = pHeldKey;
            this._char = pChar;
            this.specialKeys = pSpecialKeys;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}