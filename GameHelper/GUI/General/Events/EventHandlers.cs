﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.GUI.General.Elements;

namespace GameHelper.GUI.General.Events
{
    /* Delegates */

    public delegate void UIEventHandler(UIElement pSender);
    public delegate void MouseEventHandler(UIElement pSender, MouseEventArgument pMouseArgument);
    public delegate void KeyEventHandler(UIElement pSender, KeyEventArgument pKeyArgument);
    public delegate void ScrollEventHandler(UIElement pSender, ScrollEventArgument pScrollArgument);
    public delegate void ValueEventHandler<T>(UIElement pSender, ValueEventArgument<T> pValueArgument);
}