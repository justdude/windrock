﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.GUI.General.Events
{
    public class CheckEventArgument
    {
        /* Fields */

        private bool isChecked;

        /* Properties */

        public bool IsChecked
        {
            get
            {
                return this.isChecked;
            }
        }

        /* Constructors */

        public CheckEventArgument(bool pIsChecked)
        {
            this.isChecked = pIsChecked;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}