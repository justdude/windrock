﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using GameHelper.GUI.General.Interfaces;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using GameHelper.Input;
using GameHelper.Input.Data;
using GameHelper.GUI.General.Events;

namespace GameHelper.GUI.General.Elements
{
    public abstract class UITextBox : UIControl, IFocusable, IEditable
    {
        /* Constants */

        protected string Cursor = "_";

        /* Fields */

        protected SpriteFont font;
        protected Color foreground;
        protected Color background;
        protected int maxTextLength;

        //protected string textToDraw;
        protected int visibleTextStartIndex;
        protected int visibleTextLength;
        protected float charWidth;

        protected int cursorIndex;
        protected TimeSpan cursorBlinkTimeout;
        protected TimeSpan cursorBlinkInterval;
        protected bool isCursorVisible;

        /* Properties */

        public SpriteFont Font
        {
            get
            {
                return this.font;
            }
            set
            {
                if (value == null)
                {
                    throw new NullReferenceException("Font cannot be null.");
                }

                this.font = value;
                //this.UpdateSize();
                this.ResetRenderTarget();
            }
        }
        public Color Foreground
        {
            get
            {
                return this.foreground;
            }
            set
            {
                this.foreground = value;
                this.ResetRenderTarget();
            }
        }
        public Color Background
        {
            get
            {
                return this.background;
            }
            set
            {
                this.background = value;
                this.ResetRenderTarget();
            }
        }
        public int MaxTextLength
        {
            get
            {
                return this.maxTextLength;
            }
            set
            {
                int newMaxTextLength = Math.Max(0, value);

                if (this.maxTextLength != newMaxTextLength
                    && newMaxTextLength > 0)
                {
                    string textString = this.textBuilder.ToString();
                    int length = Math.Min(newMaxTextLength, textString.Length);
                    this.textBuilder = new StringBuilder(this.textBuilder.ToString().Substring(0, length));
                    this.isTextRefreshRequired = true;
                }

                this.maxTextLength = newMaxTextLength;
            }
        }

        /* Constructors */

        public UITextBox(Vector2 pPosition, Vector2 pSize, string pText, SpriteFont pFont, Color pForeground, Color pBackground)
            : base(pPosition, pSize)
        {
            this.font = pFont;
            this.text = pText;
            this.foreground = pForeground;
            this.background = pBackground;

            this.cursorBlinkInterval = TimeSpan.FromSeconds(0.5f);
            this.cursorBlinkTimeout = this.cursorBlinkInterval;
            this.isCursorVisible = false;
            this.cursorIndex = 0;

            this.textBuilder = new StringBuilder(pText);
        }

        /* Private methods */



        /* Protected methods */

        protected abstract void UpdateVisibleTextLength();
        protected void SetCursorIndex(int pIndex)
        {
            this.cursorIndex = (int) MathHelper.Clamp(pIndex, 0, this.textBuilder.Length);
            if (this.cursorIndex < this.visibleTextStartIndex)
            {
                this.visibleTextStartIndex = this.cursorIndex;
            }
            else if (this.cursorIndex > this.visibleTextStartIndex + this.visibleTextLength)
            {
                this.visibleTextStartIndex = this.cursorIndex - this.visibleTextLength;
            }
        }
        protected void OffsetCursorIndex(int pOffset)
        {
            this.SetCursorIndex(this.cursorIndex + pOffset);
        }
        protected void AddChar(char pChar)
        {
            if (this.textBuilder.Length == 0
                || this.cursorIndex == this.textBuilder.Length)
            {
                this.textBuilder.Append(pChar);
            }
            else
            {
                this.textBuilder.Insert(this.cursorIndex, pChar);
            }

            isTextRefreshRequired = true;
        }
        protected void RemoveChar(int pIndex)
        {
            this.textBuilder.Remove(pIndex, 1);
            isTextRefreshRequired = true;
        }

        /* Public methods */

        public override void Update(GameTime pGameTime)
        {
            if (this.isFocused)
            {
                this.cursorBlinkTimeout -= pGameTime.ElapsedGameTime;
                if (this.cursorBlinkTimeout <= TimeSpan.Zero)
                {
                    this.cursorBlinkTimeout += this.cursorBlinkInterval;
                    this.isCursorVisible = !this.isCursorVisible;

                    this.ResetRenderTarget();
                }
            }
        }

        /* Interfaces */

        #region IFocusable

        /* Fields */

        private bool isFocused;

        /* Properties */

        public bool IsFocused
        {
            get
            {
                return this.isFocused;
            }
        }

        /* Events */

        public event UIEventHandler Focused;
        public event UIEventHandler Unfocused;
        public event KeyEventHandler KeyDown;
        public event KeyEventHandler KeyUp;
        public event KeyEventHandler KeyHold;

        /* Private methods */

        private void NotifyFocused()
        {
            if (this.Focused != null)
            {
                this.Focused(this);
            }
        }
        private void NotifyUnfocused()
        {
            if (this.Unfocused != null)
            {
                this.Unfocused(this);
            }
        }
        private void NotifyKeyDown(KeyEventArgument pKeyArgument)
        {
            if (this.KeyDown != null)
            {
                this.KeyDown(this, pKeyArgument);
            }
        }
        private void NotifyKeyUp(KeyEventArgument pKeyArgument)
        {
            if (this.KeyUp != null)
            {
                this.KeyUp(this, pKeyArgument);
            }
        }
        private void NotifyKeyHold(KeyEventArgument pKeyArgument)
        {
            if (this.KeyHold != null)
            {
                this.KeyHold(this, pKeyArgument);
            }
        }

        /* Public methods */

        public void UpdateFocusable(bool pIsFocused)
        {
            if (pIsFocused)
            {
                if (!this.isFocused)
                {
                    this.cursorBlinkTimeout = this.cursorBlinkInterval;
                    this.isCursorVisible = true;
                    this.ResetRenderTarget();
                    this.NotifyFocused();
                }
            }
            else
            {
                if (this.isFocused)
                {
                    this.cursorBlinkTimeout = TimeSpan.Zero;
                    this.isCursorVisible = false;
                    this.ResetRenderTarget();
                    this.NotifyUnfocused();
                }
            }

            this.isFocused = pIsFocused;
        }
        public void ProcessKeyInput(KeyEventArgument pKeyArgument)
        {
            //if (pKeyArgument.PressedKey == Keys.C
            //    && (pKeyArgument.SpecialKeys.HasFlag(SpecialKeys.LeftControl)
            //        || pKeyArgument.SpecialKeys.HasFlag(SpecialKeys.LeftControl)))
            //{
            //}
            //else
            //{
            switch (pKeyArgument.PressedKey)
            {
                case Keys.Back:
                    if (this.cursorIndex > 0
                        && this.textBuilder.Length > 0)
                    {
                        this.RemoveChar(this.cursorIndex - 1);
                        this.OffsetCursorIndex(-1);
                        this.ResetRenderTarget();
                        this.NotifyEditing(new ValueEventArgument<string>(this.Text));
                    }
                    break;

                case Keys.Delete:
                    if (this.cursorIndex < this.textBuilder.Length
                        && this.textBuilder.Length > 0)
                    {
                        this.RemoveChar(this.cursorIndex);
                        this.OffsetCursorIndex(0);
                        this.ResetRenderTarget();
                        this.NotifyEditing(new ValueEventArgument<string>(this.Text));
                    }
                    break;

                case Keys.Left:
                    this.OffsetCursorIndex(-1);
                    this.ResetRenderTarget();
                    break;

                case Keys.Right:
                    this.OffsetCursorIndex(1);
                    this.ResetRenderTarget();
                    break;

                case Keys.Home:
                    this.SetCursorIndex(0);
                    this.ResetRenderTarget();
                    break;

                case Keys.End:
                    this.SetCursorIndex(this.textBuilder.Length);
                    this.ResetRenderTarget();
                    break;

                default:
                    if (pKeyArgument.Char != default(char))
                    {
                        if (this.maxTextLength == 0
                            || this.textBuilder.Length < this.maxTextLength)
                        {
                            this.AddChar(pKeyArgument.Char);
                            this.OffsetCursorIndex(1);
                            this.ResetRenderTarget();
                            this.NotifyEditing(new ValueEventArgument<string>(this.Text));
                        }
                    }
                    break;
            }
            //}
        }
        public void Focus()
        {
            if (this.parent is UIPanel)
            {
                (this.parent as UIPanel).SetFocus(this);
            }
            else if (this.parent is UIForm)
            {
                (this.parent as UIForm).SetFocus(this);
            }
        }

        #endregion

        #region IEditable

        /* Fields */

        protected StringBuilder textBuilder;
        protected bool isTextRefreshRequired;
        protected string text;
        private string oldText;

        /* Properties */

        public string Text
        {
            get
            {
                if (this.isTextRefreshRequired)
                {
                    this.text = this.textBuilder.ToString();
                    this.isTextRefreshRequired = false;
                }

                return this.text;
            }
            set
            {
                if (this.text != value)
                {
                    this.textBuilder = new StringBuilder(value);
                    this.isTextRefreshRequired = true;
                    this.oldText = value;
                    this.SetCursorIndex(this.textBuilder.Length);

                    this.ResetRenderTarget();
                    this.NotifyEdited(new ValueEventArgument<string>(this.Text));
                }
            }
        }

        /* Events */

        public event ValueEventHandler<string> Editing;
        public event ValueEventHandler<string> Edited;

        /* Private methods */

        private void NotifyEditing(ValueEventArgument<string> pStringValueArgument)
        {
            if (this.Editing != null)
            {
                this.Editing(this, pStringValueArgument);
            }
        }
        private void NotifyEdited(ValueEventArgument<string> pStringValueArgument)
        {
            if (this.Edited != null)
            {
                this.Edited(this, pStringValueArgument);
            }
        }

        /* Public methods */

        public void UpdateEditable(object pValue)
        {
            this.Text = pValue.ToString();
        }

        #endregion

        #region IClickable

        protected override void ProcessClickable(MouseEventArgument pMouseArgument)
        {
            Rectangle elementRectangle = this.GetElementRectangle();

            if (elementRectangle.Contains((int) pMouseArgument.Position.X, (int) pMouseArgument.Position.Y))
            {
                if (pMouseArgument.PressedButtons.HasFlag(MouseButtons.LeftButton))
                {
                    this.Focus();
                    this.ResetRenderTarget();
                    this.NotifyMouseDown(pMouseArgument);
                }
            }
        }

        #endregion

    }
}