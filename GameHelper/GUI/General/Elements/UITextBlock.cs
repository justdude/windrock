﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using GameHelper.GUI.Events;
using GameHelper.GUI.Interfaces;

namespace GameHelper.GUI.Elements
{
    public abstract class UITextBlock : UIControl, IFocusable
    {
        /* Fields */



        /* Properties */



        /* Constructors */

        public UITextBlock(Vector2 pPosition, Vector2 pSize)
            : base(pPosition, pSize)
        {
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



        /* Interfaces */

        #region IFocusable

        /* Fields */

        private bool isFocused;

        /* Properties */

        public bool IsFocused
        {
            get
            {
                return this.isFocused;
            }
        }

        /* Events */

        public event UIEventHandler Focused;
        public event UIEventHandler Unfocused;
        public event KeyEventHandler KeyDown;
        public event KeyEventHandler KeyUp;
        public event KeyEventHandler KeyHold;

        /* Private methods */

        private void DoFocused()
        {
            if (this.Focused != null)
            {
                this.Focused(this);
            }
        }
        private void DoUnfocused()
        {
            if (this.Unfocused != null)
            {
                this.Unfocused(this);
            }
        }
        private void DoKeyDown(KeyEventArgument pKeyArgument)
        {
            if (this.KeyDown != null)
            {
                this.KeyDown(this, pKeyArgument);
            }
        }
        private void DoKeyUp(KeyEventArgument pKeyArgument)
        {
            if (this.KeyUp != null)
            {
                this.KeyUp(this, pKeyArgument);
            }
        }
        private void DoKeyHold(KeyEventArgument pKeyArgument)
        {
            if (this.KeyHold != null)
            {
                this.KeyHold(this, pKeyArgument);
            }
        }

        /* Public methods */

        public void UpdateFocusable(bool pIsFocused)
        {
            if (pIsFocused)
            {
                if (!this.isFocused)
                {
                    this.ResetRenderTarget();
                    this.DoFocused();
                }
            }
            else
            {
                if (this.isFocused)
                {
                    this.ResetRenderTarget();
                    this.DoUnfocused();
                }
            }

            this.isFocused = pIsFocused;
        }
        public void ProcessInput(KeyEventArgument pKeyArgument)
        {
        }

        #endregion

    }
}