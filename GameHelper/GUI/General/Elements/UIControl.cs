﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.GUI.General.Interfaces;
using GameHelper.GUI.General.Events;
using Microsoft.Xna.Framework;

namespace GameHelper.GUI.General.Elements
{
    public abstract class UIControl : UIElement
    {
        /* Fields */

        protected UIElement parent;

        /* Properties */

        public UIElement Parent
        {
            get
            {
                return this.parent;
            }
            set
            {
                this.parent = value;
            }
        }

        /* Constructors */

        public UIControl(Vector2 pPosition, Vector2 pSize)
            : base(pPosition, pSize)
        {
        }

        /* Private methods */



        /* Protected methods */

        protected override Vector2 GetElementPosition()
        {
            Vector2 position =
                this.parent is UIForm ? this.position + (this.parent as UIForm).InnerOffset :
                this.parent is UIPanel ? this.position + (this.parent as UIPanel).InnerOffset :
                this.position;

            return position;
        }
        protected override Rectangle GetElementRectangle()
        {
            Vector2 elementPosition = this.GetElementPosition();
            Rectangle rectangle = new Rectangle(
                (int) elementPosition.X,
                (int) elementPosition.Y,
                (int) this.size.X,
                (int) this.size.Y);

            return rectangle;
        }

        /* Public methods */

        public override void ResetRenderTarget()
        {
            base.ResetRenderTarget();

            if (this.parent != null)
            {
                this.Parent.ResetRenderTarget();
            }
        }
        //public override Vector2 GetScreenPosition()
        //{
        //    Vector2 screenPosition = Vector2.Zero;

        //    if (this.parent is UIForm)
        //    {
        //        UIForm form = this.parent as UIForm;
        //        screenPosition = form.GetScreenPosition() + form.ContentOffset + this.position;
        //    }
        //    else if (this.parent is UIPanel)
        //    {
        //        UIPanel panel = this.parent as UIPanel;
        //        screenPosition = panel.GetScreenPosition() + panel.ContentOffset + this.position;
        //    }

        //    return screenPosition;
        //}

    }
}