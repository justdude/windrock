﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using GameHelper.Input;
using GameHelper.Data;
using GameHelper.GUI.General.Events;
using Microsoft.Xna.Framework.Input;
using GameHelper.GUI.General.Interfaces;
using Microsoft.Xna.Framework.Graphics;
using GameHelper.Input.Data;

namespace GameHelper.GUI.General.Elements
{
    public abstract class UIForm : UIElement
    {
        /* Fields */

        //protected Game game;
        protected Dictionary<int, UIControl> controls;
        protected int focusedControlKey;
        protected int controlAutokey;
        protected float scale;
        //protected Vector2 contentOffset;

        /* Properties */

        public List<UIControl> Control
        {
            get
            {
                return this.controls.Select(x => x.Value).ToList<UIControl>();
            }
        }
        public float Scale
        {
            get
            {
                return this.scale;
            }
            set
            {
                this.scale = Math.Max(0, value);
                this.ResetRenderTarget();
            }
        }
        public virtual Vector2 InnerOffset
        {
            get
            {
                return Vector2.Zero;
            }
        }
        public virtual Vector2 InnerSize
        {
            get
            {
                return this.size;
            }
        }

        /* Constructors */

        public UIForm(Vector2 pPosition, Vector2 pSize, float pScale)
            : base(pPosition, pSize)
        {
            this.scale = pScale;

            this.controls = new Dictionary<int, UIControl>();
            this.controlAutokey = -1;
        }

        /* Events */

        public event UIEventHandler Show;
        public event UIEventHandler Hide;

        /* Private methods */

        private MouseEventArgument GetMouseArgument(MouseInputManager pMouseInputManager)
        {
            Vector2 positionOnScreen = new Vector2(
               pMouseInputManager.Position.X,
               pMouseInputManager.Position.Y);
            Vector2 positionOnForm = new Vector2(
               pMouseInputManager.Position.X - this.position.X - this.InnerOffset.X,
               pMouseInputManager.Position.Y - this.position.Y - this.InnerOffset.Y) / this.scale;

            MouseEventArgument mouseArgument = new MouseEventArgument(
                positionOnScreen,
                positionOnForm,
                pMouseInputManager.HeldButtons,
                pMouseInputManager.PressedButtons,
                pMouseInputManager.ReleasedButtons,
                (int) Math.Floor(pMouseInputManager.GetWheelNotches()));

            return mouseArgument;
        }
        private KeyEventArgument GetKeyArgument(KeyInputManager pKeyInputManager)
        {
            Keys pressedKey = Keys.None;
            char charOfKey = default(char);
            if (pKeyInputManager.IsKeyPressedAndNotHandled)
            {
                pressedKey = pKeyInputManager.PressedKey;
                charOfKey = pKeyInputManager.GetPressedChar();
                pKeyInputManager.SetPressedKeyHandled();
            }

            KeyEventArgument keyArgumen = new KeyEventArgument(
                pressedKey,
                pKeyInputManager.HeldKey,
                charOfKey,
                pKeyInputManager.SpecialKeys);

            return keyArgumen;
        }
        private void FocusNextControl()
        {
            this.UpdateFocusedControl(false);

            int i = this.focusedControlKey + 1;
            this.focusedControlKey = -1;

            for (; i != this.focusedControlKey; i++)
            {
                if (i == this.controls.Count)
                {
                    i = 0;
                }

                if (this.controls[i] is IFocusable)
                {
                    this.focusedControlKey = i;
                    (this.controls[this.focusedControlKey] as IFocusable).UpdateFocusable(true);
                    break;
                }
                else if (this.controls[i] is UIPanel)
                {
                    if ((this.controls[i] as UIPanel).TryFocusNextControl())
                    {
                        this.focusedControlKey = i;
                        break;
                    }
                }
            }
        }
        private void FocusPreviousControl()
        {
            this.UpdateFocusedControl(false);

            this.focusedControlKey = -1;

            for (int i = this.focusedControlKey - 1; i != this.focusedControlKey; i--)
            {
                if (i < 0)
                {
                    i = this.controls.Count - 1;
                }

                if (this.controls[i] is IFocusable)
                {
                    this.focusedControlKey = i;
                    (this.controls[this.focusedControlKey] as IFocusable).UpdateFocusable(true);
                    break;
                }
                else if (this.controls[i] is UIPanel)
                {
                    if ((this.controls[i] as UIPanel).TryFocusPreviousControl())
                    {
                        this.focusedControlKey = i;
                        break;
                    }
                }
            }
        }
        private bool ProcessFormKeyInput(KeyEventArgument pKeyEventArgument)
        {
            bool result = false;

            switch (pKeyEventArgument.PressedKey)
            {
                case Keys.Tab:
                    bool isShiftPressed =
                        pKeyEventArgument.SpecialKeys.HasFlag(SpecialKeys.LeftShift)
                        || pKeyEventArgument.SpecialKeys.HasFlag(SpecialKeys.LeftShift);

                    if (isShiftPressed)
                    {
                        this.FocusPreviousControl();
                    }
                    else
                    {
                        this.FocusNextControl();
                    }

                    result = true;
                    break;

                case Keys.Escape:
                    this.ResetFocus();
                    result = true;
                    break;
            }

            return result;
        }
        private void ProcessMouseInputForHoverable(MouseEventArgument pMouseArgument)
        {
            foreach (UIControl control in this.controls.Values)
            {
                if (control is UIPanel)
                {
                    (control as UIPanel).ProcessHoverable(pMouseArgument);
                }
                else
                {
                    control.UpdateHoverable(pMouseArgument);
                }

                if (pMouseArgument.IsHandled)
                {
                    break;
                }
            }

            if (!pMouseArgument.IsHandled)
            {
                this.UpdateHoverable(pMouseArgument);
                pMouseArgument.IsHandled = true;
            }
        }
        private void ProcessMouseInputForClickable(MouseEventArgument pMouseArgument)
        {
            foreach (UIControl control in this.controls.Values)
            {
                if (control is UIPanel)
                {
                    (control as UIPanel).ProcessClickable(pMouseArgument);
                }
                else
                {
                    control.UpdateClickable(pMouseArgument);
                }

                if (pMouseArgument.IsHandled)
                {
                    break;
                }
            }

            if (!pMouseArgument.IsHandled)
            {
                this.UpdateClickable(pMouseArgument);
                pMouseArgument.IsHandled = true;
            }
        }

        /* Protected methods */

        protected void UpdateFocusedControl(bool pIsFocused)
        {
            if (this.focusedControlKey >= 0)
            {
                if (this.controls[this.focusedControlKey] is IFocusable)
                {
                    (this.controls[this.focusedControlKey] as IFocusable).UpdateFocusable(pIsFocused);
                }
                else if (this.controls[this.focusedControlKey] is UIPanel)
                {
                    (this.controls[this.focusedControlKey] as UIPanel).UpdateFocusedControl(pIsFocused);
                }
            }
        }
        //protected void ProcessMouseInput(MouseEventArgument pMouseArgument)
        //{
        //    Rectangle formRectangle = this.GetElementRectangle();
        //    if (formRectangle.Contains((int) pMouseArgument.Position.X, (int) pMouseArgument.Position.Y))
        //    {
        //        foreach (UIControl control in this.controls.Values)
        //        {
        //            if (control is UIPanel)
        //            {
        //                (control as UIPanel).ProcessMouseInput(pMouseArgument);
        //            }
        //            else
        //            {
        //                control.UpdateHoverable(pMouseArgument);
        //                control.UpdateClickable(pMouseArgument);
        //            }

        //            if (pMouseArgument.IsHandled)
        //            {
        //                break;
        //            }
        //        }
        //    }
        //}
        protected void ProcessKeyInput(KeyEventArgument pKeyArgument)
        {
            if (!this.ProcessFormKeyInput(pKeyArgument))
            {
                if (this.focusedControlKey >= 0)
                {
                    if (this.controls[this.focusedControlKey] is IFocusable)
                    {
                        (this.controls[this.focusedControlKey] as IFocusable).ProcessKeyInput(pKeyArgument);
                    }
                    else if (this.controls[this.focusedControlKey] is UIPanel)
                    {
                        (this.controls[this.focusedControlKey] as UIPanel).ProcessKeyInput(pKeyArgument);
                    }
                }
            }
        }

        /* Public methods */

        public virtual void ProcessInput(MouseInputManager pMouseInputManager, KeyInputManager pKeyInputManager)
        {
            KeyEventArgument keyArgument = this.GetKeyArgument(pKeyInputManager);
            this.ProcessKeyInput(keyArgument);

            Rectangle formRectangle = this.GetElementRectangle();

            MouseEventArgument mouseArgument = this.GetMouseArgument(pMouseInputManager);
            this.ProcessMouseInputForHoverable(mouseArgument);

            mouseArgument.IsHandled = false;
            this.ProcessMouseInputForClickable(mouseArgument);

        }
        public override void Update(GameTime pGameTime)
        {
            foreach (UIControl control in this.controls.Values)
            {
                control.Update(pGameTime);
            }
        }
        public override void Draw(SpriteBatch pSpriteBatch)
        {
            pSpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.PointWrap, null, null);
            pSpriteBatch.Draw(
                this.formRenderTarget,
                this.GetElementPosition(),
                null,
                Color.White,
                0,
                Vector2.Zero,
                this.scale,
                SpriteEffects.None,
                0);
            pSpriteBatch.End();
        }

        // // //

        public void Add(UIControl pControl)
        {
            if (pControl.Parent != null)
            {
                throw new Exception("This control already has parent.");
            }

            this.controls.Add(
                ++this.controlAutokey,
                pControl);
            pControl.Parent = this;
        }
        public void Remove(UIControl pControl)
        {
            int keyOfControl = -1;
            foreach (int key in this.controls.Keys)
            {
                if (this.controls[key] == pControl)
                {
                    keyOfControl = key;
                    break;
                }
            }

            if (keyOfControl > 0)
            {
                this.controls.Remove(keyOfControl);
                pControl.Parent = null;
            }
        }
        public void ResetFocus()
        {
            if (this.focusedControlKey != 0)
            {
                IFocusable control = this.controls[this.focusedControlKey] as IFocusable;
                control.UpdateFocusable(false);
            }

            this.focusedControlKey = 0;
        }
        //public override Vector2 GetScreenPosition()
        //{
        //    return this.position * this.scale;
        //}
        public void SetFocus(UIControl pControl)
        {
            this.UpdateFocusedControl(false);
            this.focusedControlKey = -1;

            if (pControl != null
                && this.controls.Values.Contains(pControl))
            {
                this.focusedControlKey = this.controls.First(
                    x =>
                        x.Value == pControl).Key;

                this.UpdateFocusedControl(true);
            }
        }

        // // //

        public void NotifyOpen()
        {
            if (this.Show != null)
            {
                this.Show(this);
            }
        }
        public void NotifyHide()
        {
            if (this.Hide != null)
            {
                this.Hide(this);
            }
        }

    }
}