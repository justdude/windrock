﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.Input;
using GameHelper.GUI.General.Events;
using GameHelper.GUI.General.Interfaces;
using Microsoft.Xna.Framework;
using GameHelper.Data;
using Microsoft.Xna.Framework.Graphics;

namespace GameHelper.GUI.General.Elements
{
    public abstract class UIElement : IHoverable, IClickable
    {
        /* Fields */

        protected Vector2 position;
        protected Vector2 size;
        protected RenderTarget2D formRenderTarget;

        /* Properties */

        public Vector2 Position
        {
            get
            {
                return this.position;
            }
        }
        public Vector2 Size
        {
            get
            {
                return this.size;
            }
        }

        /* Constructors */

        public UIElement(Vector2 pPosition, Vector2 pSize)
        {
            this.position = pPosition;
            this.size = pSize;
        }

        /* Private methods */



        /* Protected methods */

        protected abstract void UpdateRenderTarget(SpriteBatch pSpriteBatch);
        protected virtual Rectangle GetElementRectangle()
        {
            Rectangle rectangle = new Rectangle(
                (int) this.position.X,
                (int) this.position.Y,
                (int) this.size.X,
                (int) this.size.Y);

            return rectangle;
        }
        protected virtual Vector2 GetElementPosition()
        {
            Vector2 position = new Vector2(
                (int) this.position.X,
                (int) this.position.Y);

            return position;
        }

        /* Public methods */

        public virtual void Refresh(SpriteBatch pSpriteBatch)
        {
            if (this.formRenderTarget == null)
            {
                this.UpdateRenderTarget(pSpriteBatch);
            }
        }
        public virtual void Draw(SpriteBatch pSpriteBatch)
        {
            pSpriteBatch.Draw(
                this.formRenderTarget,
                this.GetElementPosition(),
                null,
                Color.White);
        }

        // // //

        public virtual void ResetRenderTarget()
        {
            this.formRenderTarget = null;
        }
        //public abstract Vector2 GetScreenPosition();

        // Interfaces

        #region IHoverable

        /* Fields */

        protected bool isHovered;

        /* Properties */

        public bool IsHovered
        {
            get
            {
                return this.isHovered;
            }
        }

        /* Events */

        public event MouseEventHandler MouseMove;
        public event MouseEventHandler MouseEnter;
        public event MouseEventHandler MouseLeave;

        /* Private methods */

        private void NotifyMouseMove(MouseEventArgument pMouseArgument)
        {
            if (this.MouseMove != null)
            {
                this.MouseMove(this, pMouseArgument);
            }
        }
        private void NotifyMouseEnter(MouseEventArgument pMouseArgument)
        {
            if (this.MouseEnter != null)
            {
                this.MouseEnter(this, pMouseArgument);
            }
        }
        private void NotifyMouseLeave(MouseEventArgument pMouseArgument)
        {
            if (this.MouseLeave != null)
            {
                this.MouseLeave(this, pMouseArgument);
            }
        }

        /* Public methods */

        public void UpdateHoverable(MouseEventArgument pMouseArgument)
        {
            // MouseEnter => MouseMove * N => MouseLeave.

            Rectangle elementRectangle = this.GetElementRectangle();

            if (elementRectangle.Contains((int) pMouseArgument.Position.X, (int) pMouseArgument.Position.Y))
            {
                if (!this.isHovered)
                {
                    this.isHovered = true;
                    this.ResetRenderTarget();
                    this.NotifyMouseEnter(pMouseArgument);
                }

                this.NotifyMouseMove(pMouseArgument);
            }
            else
            {
                if (this.isHovered)
                {
                    this.isHovered = false;
                    this.ResetRenderTarget();
                    this.NotifyMouseLeave(pMouseArgument);
                }
            }
        }

        #endregion

        #region IClickable

        /* Events */

        public event MouseEventHandler MouseDown;
        public event MouseEventHandler MouseUp;
        public event MouseEventHandler MouseClick;

        /* Protected methods */

        protected void NotifyMouseDown(MouseEventArgument pMouseArgument)
        {
            if (this.MouseDown != null)
            {
                this.MouseDown(this, pMouseArgument);
            }
        }
        protected void NotifyMouseUp(MouseEventArgument pMouseArgument)
        {
            if (this.MouseUp != null)
            {
                this.MouseUp(this, pMouseArgument);
            }
        }
        protected void NotifyMouseClick(MouseEventArgument pMouseArgument)
        {
            if (this.MouseClick != null)
            {
                this.MouseClick(this, pMouseArgument);
            }
        }

        protected virtual void ProcessClickable(MouseEventArgument pMouseArgument)
        {
            Rectangle elementRectangle = this.GetElementRectangle();

            if (elementRectangle.Contains((int) pMouseArgument.Position.X, (int) pMouseArgument.Position.Y))
            {
                if (pMouseArgument.PressedButtons != MouseButtons.None)
                {
                    this.ResetRenderTarget();
                    this.NotifyMouseDown(pMouseArgument);
                }

                if (pMouseArgument.ReleasedButtons != MouseButtons.None)
                {
                    this.ResetRenderTarget();
                    this.NotifyMouseUp(pMouseArgument);
                }
            }
        }

        /* Public methods */

        public void UpdateClickable(MouseEventArgument pMouseArgument)
        {
            this.ProcessClickable(pMouseArgument);
        }

        #endregion

        public virtual void Update(GameTime pGameTime)
        {
        }

    }
}