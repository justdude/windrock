﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.GUI.Interfaces;
using GameHelper.GUI.Events;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using GameHelper.Input;

namespace GameHelper.GUI.Elements
{
    public abstract class UICheckBox : UIControl, IFocusable, ICheckable, IClickable
    {
        /* Fields */



        /* Properties */



        /* Constructors */

        public UICheckBox(Vector2 pPosition, Vector2 pSize)
            : base(pPosition, pSize)
        {
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



        /* Interfaces */

        #region IFocusable

        /* Fields */

        private bool isFocused;

        /* Properties */

        public bool IsFocused
        {
            get
            {
                return this.isFocused;
            }
        }

        /* Events */

        public event UIEventHandler Focused;
        public event UIEventHandler Unfocused;
        public event KeyEventHandler KeyDown;
        public event KeyEventHandler KeyUp;
        public event KeyEventHandler KeyHold;

        /* Private methods */

        private void DoFocused()
        {
            if (this.Focused != null)
            {
                this.Focused(this);
            }
        }
        private void DoUnfocused()
        {
            if (this.Unfocused != null)
            {
                this.Unfocused(this);
            }
        }
        private void DoKeyDown(KeyEventArgument pKeyArgument)
        {
            if (this.KeyDown != null)
            {
                this.KeyDown(this, pKeyArgument);
            }
        }
        private void DoKeyUp(KeyEventArgument pKeyArgument)
        {
            if (this.KeyUp != null)
            {
                this.KeyUp(this, pKeyArgument);
            }
        }
        private void DoKeyHold(KeyEventArgument pKeyArgument)
        {
            if (this.KeyHold != null)
            {
                this.KeyHold(this, pKeyArgument);
            }
        }

        //private void OnFocusGet(UIElement pSender)
        //{
        //    this.isFocused = true;
        //    this.Refresh();
        //}
        //public void OnFocusLost(UIElement pSender)
        //{
        //    this.isFocused = false;
        //    this.Refresh();
        //}

        /* Public methods */

        public void UpdateFocusable(bool pIsFocused)
        {
            if (pIsFocused)
            {
                if (!this.isFocused)
                {
                    this.ResetRenderTarget();
                    this.DoFocused();
                }
            }
            else
            {
                if (this.isFocused)
                {
                    this.ResetRenderTarget();
                    this.DoUnfocused();
                }
            }

            this.isFocused = pIsFocused;
        }
        public void ProcessInput(KeyEventArgument pKeyArgument)
        {
            switch (pKeyArgument.PressedKey)
            {
                case Keys.Space:
                case Keys.Enter:
                    this.ToggleCheckable();
                    break;
            }
        }

        #endregion

        #region ICheckable

        /* Fields */

        private bool isChecked;

        /* Properties */

        public bool IsChecked
        {
            get
            {
                return this.isChecked;
            }
            set
            {
                this.UpdateCheckable(value);
            }
        }

        /* Events */

        public event UIEventHandler Checked;
        public event UIEventHandler Unchecked;

        /* Private methods */

        private void DoChecked()
        {
            if (this.Checked != null)
            {
                this.Checked(this);
            }
        }
        private void DoUnchecked()
        {
            if (this.Unchecked != null)
            {
                this.Unchecked(this);
            }
        }

        /* Public methods */

        public void ToggleCheckable()
        {
            if (this.isChecked)
            {
                this.ResetRenderTarget();
                this.DoUnchecked();
            }
            else
            {
                this.ResetRenderTarget();
                this.DoChecked();
            }

            this.isChecked = !this.isChecked;
        }
        public void UpdateCheckable(bool pIsChecked)
        {
            if (pIsChecked)
            {
                if (!this.isChecked)
                {
                    this.ResetRenderTarget();
                    this.DoChecked();
                }
            }
            else
            {
                if (this.isChecked)
                {
                    this.ResetRenderTarget();
                    this.DoUnchecked();
                }
            }

            this.isChecked = pIsChecked;
        }

        #endregion

        #region IClickable

        /* Fields */



        /* Properties */



        /* Events */

        //public event MouseEventHandler ButtonPressed;
        //public event MouseEventHandler ButtonReleased;
        //public event MouseEventHandler ButtonClick;

        /* Private methods */

        //private void DoMousePressed(MouseEventArgument pMouseArgument)
        //{
        //    if (this.ButtonPressed != null)
        //    {
        //        this.ButtonPressed(this, pMouseArgument);
        //    }
        //}
        //private void DoMouseReleased(MouseEventArgument pMouseArgument)
        //{
        //    if (this.ButtonReleased != null)
        //    {
        //        this.ButtonReleased(this, pMouseArgument);
        //    }
        //}
        //private void DoButtonClick(MouseEventArgument pMouseArgument)
        //{
        //    if (this.ButtonClick != null)
        //    {
        //        this.ButtonClick(this, pMouseArgument);
        //    }
        //}

        /* Public methods */

        public void UpdateClickable(MouseEventArgument pMouseArgument)
        {
            Rectangle rectangle = this.GetElementRectangle();

            if (rectangle.Contains((int) pMouseArgument.Position.X, (int) pMouseArgument.Position.Y))
            {
                if (pMouseArgument.PressedButtons.HasFlag(MouseButtons.LeftButton))
                {
                    this.IsChecked = !this.isChecked;
                    pMouseArgument.SetHandled(); // Hmmm...?
                }
            }
        }

        #endregion

    }
}