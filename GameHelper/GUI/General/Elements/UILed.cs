﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.GUI.Interfaces;
using GameHelper.GUI.Events;
using Microsoft.Xna.Framework;

namespace GameHelper.GUI.Elements
{
    public abstract class UILed : UIControl, ICheckable
    {
        /* Fields */



        /* Properties */



        /* Constructors */

        public UILed(Vector2 pPosition, Vector2 pSize)
            : base(pPosition, pSize)
        {
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        #region ICheckable

        /* Fields */

        private bool isChecked;

        /* Properties */

        public bool IsChecked
        {
            get
            {
                return this.isChecked;
            }
            set
            {
                this.UpdateCheckable(value);
            }
        }

        /* Events */

        public event UIEventHandler Checked;
        public event UIEventHandler Unchecked;

        /* Private methods */

        private void DoChecked()
        {
            if (this.Checked != null)
            {
                this.Checked(this);
            }
        }
        private void DoUnchecked()
        {
            if (this.Unchecked != null)
            {
                this.Unchecked(this);
            }
        }

        /* Public methods */

        public void ToggleCheckable()
        {
            if (this.isChecked)
            {
                this.ResetRenderTarget();
                this.DoUnchecked();
            }
            else
            {
                this.ResetRenderTarget();
                this.DoChecked();
            }

            this.isChecked = !this.isChecked;
        }
        public void UpdateCheckable(bool pIsChecked)
        {
            if (pIsChecked)
            {
                if (!this.isChecked)
                {
                    this.ResetRenderTarget();
                    this.DoChecked();
                }
            }
            else
            {
                if (this.isChecked)
                {
                    this.ResetRenderTarget();
                    this.DoUnchecked();
                }
            }

            this.isChecked = pIsChecked;
        }

        #endregion

    }
}