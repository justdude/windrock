﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using GameHelper.GUI.General.Events;
using GameHelper.Input;
using GameHelper.Data;
using Microsoft.Xna.Framework.Input;
using GameHelper.GUI.General.Interfaces;
using Microsoft.Xna.Framework.Graphics;

namespace GameHelper.GUI.General.Elements
{
    public abstract class UIPanel : UIControl
    {
        /* Fields */

        protected Dictionary<int, UIControl> controls;
        protected int focusedControlKey;
        protected int controlAutokey;

        /* Properties */

        public List<UIControl> Controls
        {
            get
            {
                return this.controls.Select(x => x.Value).ToList<UIControl>();
            }
        }
        public abstract Vector2 InnerOffset
        {
            get;
        }
        public virtual Vector2 InnerSize
        {
            get
            {
                return this.size;
            }
        }

        /* Constructors */

        public UIPanel(Vector2 pPosition, Vector2 pSize)
            : base(pPosition, pSize)
        {
            this.controlAutokey = -1;
        }

        /* Privat methods*/

        private MouseEventArgument GetMouseArgument(MouseEventArgument pParentMouseArgument)
        {
            // Relative mouse position.
            Vector2 positionOnPanel = new Vector2(
                pParentMouseArgument.Position.X - this.position.X - this.InnerOffset.X,
                pParentMouseArgument.Position.Y - this.position.Y - this.InnerOffset.Y);

            MouseEventArgument mouseArgument = new MouseEventArgument(
                pParentMouseArgument.PositionOnScreen,
                positionOnPanel,
                pParentMouseArgument.HeldButtons,
                pParentMouseArgument.PressedButtons,
                pParentMouseArgument.ReleasedButtons,
                pParentMouseArgument.WheelNotches);

            if (pParentMouseArgument.IsHandled)
            {
                mouseArgument.IsHandled = true;
            }

            return mouseArgument;
        }

        /* Protected methods */

        protected void Focus()
        {
            if (this.parent is UIPanel)
            {
                (this.parent as UIPanel).SetFocus(this);
            }
            else if (this.parent is UIForm)
            {
                (this.parent as UIForm).SetFocus(this);
            }
        }

        /* Public methods */

        public override void Update(GameTime pGameTime)
        {
            foreach (UIControl control in this.controls.Values)
            {
                control.Update(pGameTime);
            }
        }

        public void Add(UIControl pControl)
        {
            if (pControl.Parent != null)
            {
                throw new Exception("This control already has parent.");
            }

            if (this.controls == null)
            {
                this.controls = new Dictionary<int, UIControl>();
            }

            this.controls.Add(
                ++this.controlAutokey,
                pControl);
            pControl.Parent = this;
        }
        public void Remove(UIControl pControl)
        {
            int keyOfControl = -1;
            foreach (int key in this.controls.Keys)
            {
                if (this.controls[key] == pControl)
                {
                    keyOfControl = key;
                    break;
                }
            }

            if (keyOfControl > 0)
            {
                this.controls.Remove(keyOfControl);
                pControl.Parent = null;

                if (this.controls.Count == 0)
                {
                    this.controls = null;
                }
            }
        }
        public bool TryFocusNextControl()
        {
            bool result = false;

            this.UpdateFocusedControl(false);

            this.focusedControlKey = -1;

            for (int i = this.focusedControlKey + 1; i < this.focusedControlKey; i++)
            {
                if (i > this.controls.Count - 1)
                {
                    i = 0;
                }

                if (this.controls[i] is IFocusable)
                {
                    this.focusedControlKey = i;
                    (this.controls[this.focusedControlKey] as IFocusable).UpdateFocusable(true);
                    break;
                }
                else if (this.controls[i] is UIPanel)
                {
                    if ((this.controls[i] as UIPanel).TryFocusNextControl())
                    {
                        this.focusedControlKey = i;
                        break;
                    }
                }
            }

            return result;
        }
        public bool TryFocusPreviousControl()
        {
            bool result = false;

            this.UpdateFocusedControl(false);

            this.focusedControlKey = -1;

            for (int i = this.focusedControlKey - 1; i > this.focusedControlKey; i--)
            {
                if (i < 0)
                {
                    i = this.controls.Count - 1;
                }

                if (this.controls[i] is IFocusable)
                {
                    this.focusedControlKey = i;
                    (this.controls[this.focusedControlKey] as IFocusable).UpdateFocusable(true);
                    break;
                }
                else if (this.controls[i] is UIPanel)
                {
                    if ((this.controls[i] as UIPanel).TryFocusPreviousControl())
                    {
                        this.focusedControlKey = i;
                        break;
                    }
                }
            }

            return result;
        }
        //public void ProcessMouseInput(MouseEventArgument pMouseArgument)
        //{
        //    Rectangle panelRectangle = this.GetElementRectangle();
        //    if (panelRectangle.Contains((int) pMouseArgument.Position.X, (int) pMouseArgument.Position.Y))
        //    {
        //        MouseEventArgument mouseArgument = this.GetMouseArgument(pMouseArgument);

        //        foreach (UIControl control in this.controls.Values)
        //        {
        //            if (control is UIPanel)
        //            {
        //                (control as UIPanel).ProcessMouseInput(mouseArgument);
        //            }
        //            else
        //            {
        //                control.UpdateHoverable(mouseArgument);
        //                control.UpdateClickable(mouseArgument);
        //            }

        //            if (mouseArgument.IsHandled)
        //            {
        //                break;
        //            }
        //        }

        //        if (!mouseArgument.IsHandled)
        //        {
        //            this.UpdateHoverable(mouseArgument);
        //            this.UpdateClickable(mouseArgument);
        //        }

        //        if (mouseArgument.IsHandled)
        //        {
        //            pMouseArgument.SetHandled();
        //        }
        //    }
        //}
        public void UpdateFocusedControl(bool pIsFocused)
        {
            if (this.focusedControlKey >= 0)
            {
                if (this.controls[this.focusedControlKey] is IFocusable)
                {
                    (this.controls[this.focusedControlKey] as IFocusable).UpdateFocusable(pIsFocused);
                }
                else if (this.controls[this.focusedControlKey] is UIPanel)
                {
                    (this.controls[this.focusedControlKey] as UIPanel).UpdateFocusedControl(pIsFocused);
                }
            }
        }
        public void SetFocus(UIControl pControl)
        {
            this.UpdateFocusedControl(false);
            this.focusedControlKey = -1;

            if (pControl != null
                && this.controls.Values.Contains(pControl))
            {
                this.focusedControlKey = this.controls.First(
                    x =>
                        x.Value == pControl).Key;

                this.Focus();
            }
        }

        public void ProcessHoverable(MouseEventArgument pMouseArgument)
        {
            MouseEventArgument mouseArgument = this.GetMouseArgument(pMouseArgument);

            foreach (UIControl control in this.controls.Values)
            {
                if (control is UIPanel)
                {
                    (control as UIPanel).ProcessHoverable(mouseArgument);
                }
                else
                {
                    control.UpdateHoverable(mouseArgument);
                }

                if (mouseArgument.IsHandled)
                {
                    break;
                }
            }

            if (mouseArgument.IsHandled)
            {
                pMouseArgument.IsHandled = true;
            }

            if (!pMouseArgument.IsHandled)
            {
                this.UpdateHoverable(pMouseArgument);
            }
        }
        public void ProcessClickable(MouseEventArgument pMouseArgument)
        {
            MouseEventArgument mouseArgument = this.GetMouseArgument(pMouseArgument);

            foreach (UIControl control in this.controls.Values)
            {
                if (control is UIPanel)
                {
                    (control as UIPanel).ProcessClickable(mouseArgument);
                }
                else
                {
                    control.UpdateClickable(mouseArgument);
                }

                if (mouseArgument.IsHandled)
                {
                    break;
                }
            }

            if (mouseArgument.IsHandled)
            {
                pMouseArgument.IsHandled = true;
            }

            if (!pMouseArgument.IsHandled)
            {
                this.UpdateClickable(pMouseArgument);
            }
        }
        public void ProcessKeyInput(KeyEventArgument pKeyArgument)
        {
            if (this.focusedControlKey >= 0)
            {
                if (this.controls[this.focusedControlKey] is IFocusable)
                {
                    (this.controls[this.focusedControlKey] as IFocusable).ProcessKeyInput(pKeyArgument);
                }
                else if (this.controls[this.focusedControlKey] is UIPanel)
                {
                    (this.controls[this.focusedControlKey] as UIPanel).ProcessKeyInput(pKeyArgument);
                }
            }
        }

    }
}