﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.GUI.General.Interfaces;
using GameHelper.GUI.General.Events;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using GameHelper.Input;
using Microsoft.Xna.Framework.Graphics;

namespace GameHelper.GUI.General.Elements
{
    public abstract class UIButton : UIControl, IPressable
    {
        /* Fields */

        protected string text;
        protected SpriteFont font;
        protected Color foreground;
        protected Color background;

        /* Properties */

        public string Text
        {
            get
            {
                return this.text;
            }
            set
            {
                this.text = value;
                //this.UpdateSize();
                this.ResetRenderTarget();
            }
        }
        public SpriteFont Font
        {
            get
            {
                return this.font;
            }
            set
            {
                if (value == null)
                {
                    throw new NullReferenceException("Font cannot be null.");
                }

                this.font = value;
                //this.UpdateSize();
                this.ResetRenderTarget();
            }
        }
        public Color Foreground
        {
            get
            {
                return this.foreground;
            }
            set
            {
                this.foreground = value;
                this.ResetRenderTarget();
            }
        }
        public Color Background
        {
            get
            {
                return this.background;
            }
            set
            {
                this.background = value;
                this.ResetRenderTarget();
            }
        }

        /* Constructors */

        public UIButton(Vector2 pPosition, Vector2 pSize, string pText, SpriteFont pFont, Color pForeground, Color pBackground)
            : base(pPosition, pSize)
        {
            this.font = pFont;
            this.text = pText;
            this.foreground = pForeground;
            this.background = pBackground;

            //this.UpdateSize();
        }

        /* Private methods */



        /* Protected methods */

        //protected virtual void UpdateSize()
        //{
        //    Vector2 textSize = this.font.MeasureString(this.text);
        //}

        /* Public methods */


        /* Interfaces */

        #region IClickable

        /* Protected methods */

        protected override void ProcessClickable(MouseEventArgument pMouseArgument)
        {
            Rectangle elementRectangle = this.GetElementRectangle();

            if (elementRectangle.Contains((int) pMouseArgument.Position.X, (int) pMouseArgument.Position.Y))
            {
                if (pMouseArgument.PressedButtons.HasFlag(MouseButtons.LeftButton))
                {
                    this.isPressed = true;
                    ResetRenderTarget();
                    this.NotifyPressed();
                }
            }

            if (pMouseArgument.ReleasedButtons.HasFlag(MouseButtons.LeftButton))
            {
                if (this.isPressed)
                {
                    this.isPressed = false;
                    ResetRenderTarget();

                    if (this.isHovered)
                    {
                        this.NotifyReleased();
                    }
                }
            }
        }

        #endregion

        #region IPressable

        /* Fields */

        protected bool isPressed;

        /* Properies */

        public bool IsPressed
        {
            get
            {
                return this.isPressed;
            }
        }

        /* Events */

        public event UIEventHandler Pressed;
        public event UIEventHandler Released;

        /* Protected methods */

        protected void NotifyPressed()
        {
            if (this.Pressed != null)
            {
                this.Pressed(this);
            }
        }
        protected void NotifyReleased()
        {
            if (this.Released != null)
            {
                this.Released(this);
            }
        }

        #endregion

    }
}