﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameHelper.GUI.General.Elements
{
    /// <summary>
    /// Описывает текствую метку.
    /// </summary>
    /// <remarks>
    /// Текстовая метка предназначена дла отображения простого текста.
    /// </remarks>
    public abstract class UILabel : UIControl
    {
        /* Fields */

        protected string text;
        protected SpriteFont font;
        protected Color foreground;

        /* Properties */

        public string Text
        {
            get
            {
                return this.text;
            }
            set
            {
                this.text = value;
                this.UpdateSize();
                this.ResetRenderTarget();
            }
        }
        public SpriteFont Font
        {
            get
            {
                return this.font;
            }
            set
            {
                if (value == null)
                {
                    throw new NullReferenceException("Font cannot be null.");
                }

                this.font = value;
                this.UpdateSize();
                this.ResetRenderTarget();
            }
        }
        public Color Foreground
        {
            get
            {
                return this.foreground;
            }
            set
            {
                this.foreground = value;
                this.ResetRenderTarget();
            }
        }


        /* Constructors */

        public UILabel(Vector2 pPosition, string pText, SpriteFont pFont, Color pForeground)
            : base(pPosition, Vector2.Zero)
        {
            this.font = pFont;
            this.text = pText;
            this.foreground = pForeground;

            this.UpdateSize();
        }


        /* Private methods */



        /* Protected methods */

        protected virtual void UpdateSize()
        {
            this.size = this.font.MeasureString(this.text);
        }

        /* Public methods */



    }
}