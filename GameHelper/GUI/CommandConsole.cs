﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameHelper.Scripting;

namespace GameHelper.GUI.General
{
    public class CommandConsole<TContext, TCommand> : TextConsole
        where TContext : CommandContext
        where TCommand : Command<TContext>
    {
        /* Fields */

        private CommandRegister<TContext, TCommand> commandRegister;
        private TContext context;

        /* Properties */

        public CommandRegister<TContext, TCommand> CommandRegister
        {
            get
            {
                return this.commandRegister;
            }
        }

        /* Constructors */

        public CommandConsole(Game pGame, Vector2 pPosition, Vector2 pSize, SpriteFont pFont, Color pOutputForeground, Color pInputForeground, CommandRegister<TContext, TCommand> pCommandRegister)
            : base(pGame, pPosition, pSize, pFont, pOutputForeground, pInputForeground)
        {
            this.commandRegister = pCommandRegister;
        }

        /* Private methods */

        protected override void ProcessLine(string pLine, GameTime pGameTime, Input.KeyInputManager pKeyManager)
        {
            if (pLine.Length > 0
                && pLine[0] == '/')
            {
                TCommand command = this.commandRegister.Parse(pLine.Substring(1, pLine.Length - 1), this.context);
                if (command != null)
                {
                    command.Execute(this.context);
                    this.lineOutput.AddToHistory(pLine);
                }
                else
                {
                    this.WriteLine("Invalid command.");
                }
            }
            else
            {
                base.ProcessLine(pLine, pGameTime, pKeyManager);
            }
        }

        /* Protected methods */



        /* Public methods */

        public void SetContext(TContext pContext)
        {
            this.context = pContext;
        }

    }
}