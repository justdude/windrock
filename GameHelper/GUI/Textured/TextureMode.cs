﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHelper.GUI.Tiled
{
    public enum TextureMode : byte
    {
        Stretch = 0,
        Tile = 1,
        Uniform = 2,
        UniformToFill = 3
    }
}