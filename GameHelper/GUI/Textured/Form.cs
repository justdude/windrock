﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameHelper.Data;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameHelper.GUI.General.Elements;
using Microsoft.Xna.Framework.Content;

namespace GameHelper.GUI.Tiled
{
    public class Form : UIForm
    {
        /* Fields */

        private Texture2D backgroundTexture;
        private TextureMode textureMode;

        /* Properties */

        public Texture2D BackgroundTexture
        {
            get
            {
                return this.backgroundTexture;
            }
        }
        public TextureMode TextureMode
        {
            get
            {
                return this.TextureMode;
            }
            set
            {
                this.TextureMode = value;
            }
        }

        /* Constructors */

        public Form(Texture2D BackgroundTexture, Vector2 pPosition, Vector2 pSize, float pScale)
            : base(pPosition, pSize, pScale)
        {
            if (BackgroundTexture == null)
            {
                throw new NullReferenceException();
            }

            this.backgroundTexture = BackgroundTexture;

            this.textureMode = TextureMode.Stretch;
        }
        public Form(Texture2D BackgroundTexture, Vector2 pPosition, Vector2 pSize)
            : this(BackgroundTexture, pPosition, pSize, 1)
        {
        }

        /* Private methods */



        /* Protected methods */

        protected override void UpdateRenderTarget(SpriteBatch pSpriteBatch)
        {
            foreach (UIElement element in this.controls.Values)
            {
                element.Refresh(pSpriteBatch);
            }

            this.formRenderTarget = new RenderTarget2D(pSpriteBatch.GraphicsDevice, (int) this.size.X, (int) this.size.Y);

            pSpriteBatch.GraphicsDevice.SetRenderTarget(this.formRenderTarget);
            pSpriteBatch.GraphicsDevice.Clear(Color.Transparent);
            pSpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.PointWrap, DepthStencilState.Default, null);

            // Background texture

            switch (this.textureMode)
            {
                case TextureMode.Stretch:
                    pSpriteBatch.Draw(
                        this.backgroundTexture,
                        new Rectangle(
                            (int) this.position.X,
                            (int) this.position.Y,
                            (int) this.size.X,
                            (int) this.size.Y),
                        Color.White);
                    break;

                case TextureMode.Tile:
                    pSpriteBatch.Draw(
                        this.backgroundTexture,
                        this.position,
                        new Rectangle(
                            (int) this.position.X,
                            (int) this.position.Y,
                            (int) this.size.X,
                            (int) this.size.Y),
                        Color.White);
                    break;

                case TextureMode.Uniform:
                    throw new NotImplementedException();
                    break;

                case TextureMode.UniformToFill:
                    throw new NotImplementedException();
                    break;
            }

            // Elements
            foreach (UIElement element in this.controls.Values)
            {
                element.Draw(pSpriteBatch);
            }

            pSpriteBatch.End();

            pSpriteBatch.GraphicsDevice.SetRenderTarget(null);
        }

        /* Public methods */



    }
}