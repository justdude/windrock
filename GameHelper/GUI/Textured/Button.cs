﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameHelper.GUI.General.Elements;
using GameHelper.GUI.Textured;

namespace GameHelper.GUI.Tiled
{
    public class Button : UIButton
    {
        /* Fields */

        private TiledButtonTextureManager textureManager;

        /* Properties */



        /* Constructors */

        public Button(TiledButtonTextureManager pTextureManager, Vector2 pPosition, Vector2 pSize, string pText, SpriteFont pFont, Color pForeground, Color pBackground)
            : base(pPosition, pSize, pText, pFont, pForeground, pBackground)
        {
            if (pTextureManager == null)
            {
                throw new NullReferenceException();
            }

            this.textureManager = pTextureManager;
        }

        /* Private methods */



        /* Protected methods */

        protected override void UpdateRenderTarget(SpriteBatch pSpriteBatch)
        {
            this.formRenderTarget = new RenderTarget2D(pSpriteBatch.GraphicsDevice, (int) this.size.X, (int) this.size.Y);

            pSpriteBatch.GraphicsDevice.SetRenderTarget(this.formRenderTarget);
            pSpriteBatch.GraphicsDevice.Clear(Color.Transparent);
            pSpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.PointWrap, DepthStencilState.Default, null);

            // // //

            Rectangle topRectangle = new Rectangle(
                0, 0,
                (int) (this.size.X - this.textureManager.LeftBorderWidth - this.textureManager.RightBorderWidth),
                (int) this.textureManager.TopBorderHeight);
            Rectangle bottomRectangle = new Rectangle(
                0, 0,
                (int) (this.size.X - this.textureManager.LeftBorderWidth - this.textureManager.RightBorderWidth),
                (int) this.textureManager.BottomBorderHeight);
            Rectangle leftRectangle = new Rectangle(
                0, 0,
                (int) this.textureManager.LeftBorderWidth,
                (int) (this.size.Y - this.textureManager.TopBorderHeight - this.textureManager.BottomBorderHeight));
            Rectangle rightRectangle = new Rectangle(
                0, 0,
                (int) this.textureManager.RightBorderWidth,
                (int) (this.size.Y - this.textureManager.TopBorderHeight - this.textureManager.BottomBorderHeight));
            Rectangle fillRectangle = new Rectangle(
                0, 0,
                (int) (this.size.X - this.textureManager.LeftBorderWidth - this.textureManager.RightBorderWidth),
                (int) (this.size.Y - this.textureManager.TopBorderHeight - this.textureManager.BottomBorderHeight));

            Vector2 leftTopPosition = new Vector2(0, 0);
            Vector2 leftPosition = new Vector2(0, this.textureManager.TopBorderHeight);
            Vector2 leftBottomPosition = new Vector2(0, this.size.Y - this.textureManager.BottomBorderHeight);
            Vector2 topPosition = new Vector2(this.textureManager.LeftBorderWidth, 0);
            Vector2 fillPosition = new Vector2(this.textureManager.LeftBorderWidth, this.textureManager.TopBorderHeight);
            Vector2 bottomPosition = new Vector2(this.textureManager.LeftBorderWidth, this.size.Y - this.textureManager.BottomBorderHeight);
            Vector2 rightTopPosition = new Vector2(this.size.X - this.textureManager.RightBorderWidth, 0);
            Vector2 rightPosition = new Vector2(this.size.X - this.textureManager.RightBorderWidth, this.textureManager.TopBorderHeight);
            Vector2 rightBottomPosition = new Vector2(this.size.X - this.textureManager.RightBorderWidth, this.size.Y - this.textureManager.BottomBorderHeight);

            // // // Button

            StretchTexture stretchTexture = null;
            if (this.isHovered)
            {
                if (this.isPressed)
                {
                    stretchTexture = this.textureManager.HoveredPressedTexture;
                }
                else
                {
                    stretchTexture = this.textureManager.HoveredUnpressedTexture;
                }
            }
            else
            {
                if (this.isPressed)
                {
                    stretchTexture = this.textureManager.UnhoveredPressedTexture;
                }
                else
                {
                    stretchTexture = this.textureManager.UnhoveredUnpressedTexture;
                }
            }

            // Left top
            pSpriteBatch.Draw(
                stretchTexture.LeftTopTexture,
                leftTopPosition,
                null,
                Color.White);

            // Left
            pSpriteBatch.Draw(
                stretchTexture.LeftTexture,
                leftPosition,
                leftRectangle,
                Color.White);

            // Left bottom
            pSpriteBatch.Draw(
                stretchTexture.LeftBottomTexture,
                leftBottomPosition,
                null,
                Color.White);

            // Top
            pSpriteBatch.Draw(
                stretchTexture.TopTexture,
                topPosition,
                topRectangle,
                Color.White);

            // Fill
            pSpriteBatch.Draw(
                stretchTexture.BackgroundTexture,
                fillPosition,
                fillRectangle,
                Color.White);

            // Bottom
            pSpriteBatch.Draw(
                stretchTexture.BottomTexture,
                bottomPosition,
                bottomRectangle,
                Color.White);

            // Right top
            pSpriteBatch.Draw(
                stretchTexture.RightTopTexture,
                rightTopPosition,
                null,
                Color.White);

            // Right
            pSpriteBatch.Draw(
                stretchTexture.RightTexture,
                rightPosition,
                rightRectangle,
                Color.White);

            // Right bottom
            pSpriteBatch.Draw(
                stretchTexture.RightBottomTexture,
                rightBottomPosition,
                null,
                Color.White);

            // // // Text

            if (!string.IsNullOrEmpty(this.text))
            {
                Vector2 textSize = this.font.MeasureString(this.text);
                Vector2 textPosition = new Vector2(
                    this.textureManager.LeftBorderWidth + (this.size.X - this.textureManager.LeftBorderWidth - this.textureManager.RightBorderWidth - textSize.X) * 0.5f,
                    this.textureManager.TopBorderHeight + (this.size.Y - this.textureManager.TopBorderHeight - this.textureManager.BottomBorderHeight - textSize.Y) * 0.5f);

                if (this.isPressed)
                {
                    textPosition.Y++;
                }

                pSpriteBatch.DrawString(
                    this.font,
                    this.text,
                    textPosition,
                    this.foreground);
            }

            pSpriteBatch.End();

            pSpriteBatch.GraphicsDevice.SetRenderTarget(null);
        }

        /* Public methods */



    }
}