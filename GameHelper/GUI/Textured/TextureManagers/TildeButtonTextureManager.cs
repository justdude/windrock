﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using GameHelper.GUI.Tiled;

namespace GUITest.GUI.Tilde.TextureManagers
{
    public class ButtonTextureManager
    {
        /* Fields */

        private Texture2D unhoveredUnpressedTexture;
        private Texture2D unhoveredPressedTexture;
        private Texture2D hoveredUnpressedTexture;
        private Texture2D hoveredPressedTexture;
        private TextureMode textureMode;

        /* Properties */

        public Texture2D UnhoveredUnpressedTexture
        {
            get
            {
                return this.unhoveredUnpressedTexture;
            }
        }
        public Texture2D UnhoveredPressedTexture
        {
            get
            {
                return this.unhoveredPressedTexture;
            }
        }
        public Texture2D HoveredUnpressedTexture
        {
            get
            {
                return this.hoveredUnpressedTexture;
            }
        }
        public Texture2D HoveredPressedTexture
        {
            get
            {
                return this.hoveredPressedTexture;
            }
        }
        public TextureMode TextureMode
        {
            get
            {
                return this.textureMode;
            }
            set
            {
                this.textureMode = value;
            }
        }

        /* Constructors */

        public ButtonTextureManager(Texture2D pUnhoveredUnpressedTexture, Texture2D pUnhoveredPressedTexture, Texture2D pHoveredUnpressedTexture, Texture2D pHoveredPressedTexture)
        {
            this.unhoveredUnpressedTexture = pUnhoveredUnpressedTexture;
            this.unhoveredPressedTexture = pUnhoveredPressedTexture;
            this.hoveredUnpressedTexture = pHoveredUnpressedTexture;
            this.hoveredPressedTexture = pHoveredPressedTexture;

            this.textureMode = TextureMode.Stretch;
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}