﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GameHelper.Input;
using Microsoft.Xna.Framework.Input;

namespace GameHelper.GUI.General
{
    public class TextConsole
    {
        /* Fields */

        protected LinesOutput lineOutput;
        protected TextInput input;
        protected bool isFocused;
        protected bool isInputVisible;
        protected bool isOutputVisible;
        protected bool isLostFocusByEnter;
        protected int selectedHistoryIndex;

        /* Properties */

        public bool IsFocused
        {
            get
            {
                return this.isFocused;
            }
            set
            {
                this.isFocused = value;
                this.input.IsFocused = this.isFocused;
            }
        }
        public bool IsInputVisible
        {
            get
            {
                return this.isInputVisible;
            }
            set
            {
                this.isInputVisible = value;
            }
        }
        public bool IsOutputVisible
        {
            get
            {
                return this.isOutputVisible;
            }
            set
            {
                this.isOutputVisible = value;
            }
        }
        public bool IsVisible
        {
            get
            {
                return this.isInputVisible || this.isOutputVisible;
            }
            set
            {
                this.isInputVisible = value;
                this.isOutputVisible = value;
            }
        }
        public bool IsLostFocusByEnter
        {
            get
            {
                return this.isLostFocusByEnter;
            }
            set
            {
                this.isLostFocusByEnter = value;
            }
        }

        /* Constructors */

        public TextConsole(Game pGame, Vector2 pPosition, Vector2 pSize, SpriteFont pFont, Color pOutputForeground, Color pInputForeground)
        {
            Vector2 inputSize = new Vector2(pSize.X, pFont.LineSpacing + 8);

            this.lineOutput = new LinesOutput(pGame, pPosition, pSize, pFont, pOutputForeground)
            {
                Background = new Color(pOutputForeground.ToVector4() * new Vector4(1, 1, 1, 0.05f)),
                Padding = new Vector4(4)
            };

            this.input = new TextInput(
                pGame,
                new TextOutput(pGame, pPosition + new Vector2(0, this.lineOutput.Size.Y) + Vector2.UnitY, inputSize, pFont, pInputForeground)
                {
                    Background = new Color(pOutputForeground.ToVector4() * new Vector4(1, 1, 1, 0.1f)),
                    Padding = new Vector4(4)
                })
            {
                IsEnterEnabled = true,
                IsEnterSplitText = true
            };

            this.isInputVisible = true;
            this.isOutputVisible = true;
        }

        /* Private methods */



        /* Protected methods */

        protected virtual void ProcessLine(string pLine, GameTime pGameTime, KeyInputManager pKeyManager)
        {
            this.lineOutput.AddToLinesAndHistory(pLine, pLine);
            if (this.isLostFocusByEnter)
            {
                this.isFocused = false;
            }
            this.lineOutput.ScrollToEnd();
        }

        /* Public methods */

        public void Update(GameTime pGameTime, KeyInputManager pKeyManager)
        {
            if (this.isFocused)
            {
                if (pKeyManager.IsKeyPressedAndNotHandled)
                {
                    if (pKeyManager.PressedKey == Keys.Up)
                    {
                        if (this.selectedHistoryIndex < this.lineOutput.HistoryCount - 1)
                        {
                            this.selectedHistoryIndex++;
                            this.input.SetText(this.lineOutput.GetHistory(this.selectedHistoryIndex));
                        }
                    }
                    else if (pKeyManager.PressedKey == Keys.Down)
                    {
                        if (this.selectedHistoryIndex > 0)
                        {
                            this.selectedHistoryIndex--;
                            this.input.SetText(this.lineOutput.GetHistory(this.selectedHistoryIndex));
                        }
                        else if (this.selectedHistoryIndex == 0)
                        {
                            this.selectedHistoryIndex = -1;
                            this.input.SetText(null);
                        }
                    }
                    else if (pKeyManager.PressedKey == Keys.PageUp)
                    {
                        this.lineOutput.ScrollAt(-1);
                        pKeyManager.SetPressedKeyHandled();
                    }
                    else if (pKeyManager.PressedKey == Keys.PageDown)
                    {
                        this.lineOutput.ScrollAt(1);
                        pKeyManager.SetPressedKeyHandled();
                    }
                }


                string line = this.input.Update(pGameTime, pKeyManager);
                if (!string.IsNullOrEmpty(line))
                {
                    line.Replace(@"\", @"\\");
                    this.ProcessLine(line, pGameTime, pKeyManager);
                }


                this.lineOutput.Update();
            }
        }
        public void Draw(SpriteBatch pSpriteBatch)
        {
            if (this.isOutputVisible)
            {
                this.lineOutput.Draw(pSpriteBatch);
            }
            if (this.isInputVisible)
            {
                this.input.Draw(pSpriteBatch);
            }
        }

        public void WriteLine(string pText)
        {
            this.lineOutput.AddToLines(pText);
        }
        public void WriteLine()
        {
            this.lineOutput.AddToLines(null);
        }
        // // //

        //public void ScrollOutputTo(int pIndex)
        //{
        //    this.output.ScrollTo(pIndex);
        //}
        //public void ScrollOutputAt(int pOffset)
        //{
        //    this.output.ScrollAt(pOffset);
        //}

    }
}