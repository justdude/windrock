﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using ZImportHelper.Contents;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Graphics.PackedVector;

namespace ZImportHelper.Processors
{
    [ContentProcessor(DisplayName = "Z Model Processor")]
    public class ZModelProcessor : ContentProcessor<NodeContent, ZModelContent>
    {
        /* Fields */

        private ContentProcessorContext context;
        private Dictionary<MaterialContent, MaterialContent> materials;
        private ZModelContent model;

        /* Properties */



        /* Constructors */

        public ZModelProcessor()
        {
            this.materials = new Dictionary<MaterialContent, MaterialContent>();
        }

        /* Private methods */

        private void ProcessNode(NodeContent pNode)
        {
            MeshHelper.TransformScene(pNode, pNode.Transform);
            pNode.Transform = Matrix.Identity;
            int meshIndex = -1;

            if (pNode is MeshContent)
            {
                ZModelMeshContent mesh = this.ProcessMesh(pNode as MeshContent);
                this.model.Meshes.Add(mesh);
                this.model.MeshIndices.Add(pNode.Name, ++meshIndex);
            }

            foreach (NodeContent childNode in pNode.Children)
            {
                this.ProcessNode(childNode);
            }
        }
        private ZModelMeshContent ProcessMesh(MeshContent pMesh)
        {
            ZModelMeshContent mesh = new ZModelMeshContent()
            {
                Transform = pMesh.Transform
            };

            MeshHelper.OptimizeForCache(pMesh);
            foreach (GeometryContent geometry in pMesh.Geometry)
            {
                ZModelMeshPartContent part = this.ProcessGeometry(geometry);
                mesh.Parts.Add(part);
            }

            return mesh;
        }
        private ZModelMeshPartContent ProcessGeometry(GeometryContent pGeometry)
        {
            int triangleCount = pGeometry.Indices.Count / 3;
            int vertexCount = pGeometry.Vertices.VertexCount;
            VertexBufferContent vertexBufferContent = pGeometry.Vertices.CreateVertexBuffer();
            IndexCollection indexCollection = pGeometry.Indices;
            MaterialContent materialContent = this.ProcessMaterial(pGeometry.Material);

            ZModelMeshPartContent part = new ZModelMeshPartContent()
            {
                MaterialContent = materialContent,
                VertexBufferContent = vertexBufferContent,
                IndexCollection = indexCollection,
                VerticesCount = vertexCount,
                TrianglesCount = triangleCount
            };

            return part;
        }
        private MaterialContent ProcessMaterial(MaterialContent pMaterial)
        {
            MaterialContent material = null;
            if (pMaterial != null && !this.materials.ContainsKey(pMaterial))
            {
                material = this.context.Convert<MaterialContent, MaterialContent>(pMaterial, "MaterialProcessor");
                this.materials[pMaterial] = material;
            }

            return material;
        }

        /* Protected methods */



        /* Public methods */

        public override ZModelContent Process(NodeContent pNode, ContentProcessorContext pContext)
        {
            //System.Diagnostics.Debugger.Launch();

            this.context = pContext;
            this.model = new ZModelContent();

            this.ProcessNode(pNode);

            return this.model;
        }

    }
}