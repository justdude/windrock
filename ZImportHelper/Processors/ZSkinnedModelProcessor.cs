﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using ZImportHelper.Contents;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Graphics.PackedVector;
using Microsoft.Xna.Framework.Graphics;

namespace ZImportHelper.Processors
{
    [ContentProcessor(DisplayName = "Z Skinned Model Processor")]
    public class ZSkinnedModelProcessor : ContentProcessor<NodeContent, ZSkinnedModelContent>
    {
        /* Fields */

        private ContentProcessorContext context;
        //private Dictionary<MaterialContent, MaterialContent> materials;
        private ZSkinnedModelContent skinnedModel;

        /* Properties */



        /* Constructors */

        public ZSkinnedModelProcessor()
        {
            //this.materials = new Dictionary<MaterialContent, MaterialContent>();
        }

        /* Private methods */

        private void ProcessNodeForSkeleton(NodeContent pNode)
        {
            BoneContent skeleton = MeshHelper.FindSkeleton(pNode);
            this.skinnedModel.Skeleton = this.ProcessSkeleton(skeleton);
        }
        private ZModelSkeletonContent ProcessSkeleton(BoneContent pSkeleton)
        {
            IList<BoneContent> bones = MeshHelper.FlattenSkeleton(pSkeleton);
            int bonesCount = bones.Count;

            ZModelSkeletonContent skeleton = new ZModelSkeletonContent()
            {
                BoneCount = bonesCount,
                BoneIndices = new Dictionary<string, int>(bonesCount),
                ParentBoneIndices = new int[bonesCount],
                RelativeBindPoses = new Matrix[bonesCount],
                InverseAbsoluteBindPoses = new Matrix[bonesCount]
            };

            BoneContent bone = null;
            for (int boneIndex = 0; boneIndex < bones.Count; boneIndex++)
            {
                bone = bones[boneIndex];
                skeleton.BoneIndices.Add(bone.Name, boneIndex);

                int parentBoneIndex = boneIndex == 0 ? -1 : skeleton.BoneIndices[bone.Parent.Name];
                skeleton.ParentBoneIndices[boneIndex] = parentBoneIndex;
                skeleton.RelativeBindPoses[boneIndex] = bone.Transform;
                skeleton.InverseAbsoluteBindPoses[boneIndex] = Matrix.Invert(bone.AbsoluteTransform);
            }

            return skeleton;
        }

        private void ProcessNodeForMeshes(NodeContent pNode)
        {
            MeshHelper.TransformScene(pNode, pNode.Transform);
            pNode.Transform = Matrix.Identity;

            if (pNode is MeshContent)
            {
                ZModelMeshContent mesh = this.ProcessMesh(pNode as MeshContent);
                this.skinnedModel.Meshes.Add(mesh);
            }

            foreach (NodeContent childNode in pNode.Children)
            {
                this.ProcessNodeForMeshes(childNode);
            }
        }
        private ZModelMeshContent ProcessMesh(MeshContent pMesh)
        {
            ZModelMeshContent mesh = new ZModelMeshContent()
            {
                Transform = pMesh.Transform
            };

            MeshHelper.OptimizeForCache(pMesh);
            foreach (GeometryContent geometry in pMesh.Geometry)
            {
                ZModelMeshPartContent part = this.ProcessGeometry(geometry);
                mesh.Parts.Add(part);
            }

            return mesh;
        }
        private ZModelMeshPartContent ProcessGeometry(GeometryContent pGeometry)
        {
            this.ProcessVertexWeightsChannels(ref pGeometry);

            int triangleCount = pGeometry.Indices.Count / 3;
            int vertexCount = pGeometry.Vertices.VertexCount;
            VertexBufferContent vertexBufferContent = pGeometry.Vertices.CreateVertexBuffer();
            IndexCollection indexCollection = pGeometry.Indices;
            //MaterialContent materialContent = this.ProcessMaterial(pGeometry.Material);

            ZModelMeshPartContent part = new ZModelMeshPartContent()
            {
                //MaterialContent = materialContent,
                VertexBufferContent = vertexBufferContent,
                IndexCollection = indexCollection,
                VerticesCount = vertexCount,
                TrianglesCount = triangleCount
            };

            return part;
        }
        //private MaterialContent ProcessMaterial(MaterialContent pMaterial)
        //{
        //    if (!this.materials.ContainsKey(pMaterial))
        //    {
        //        this.materials[pMaterial] = this.context.Convert<MaterialContent, MaterialContent>(pMaterial, "MaterialProcessor");
        //    }

        //    return this.materials[pMaterial];
        //}
        private void ProcessVertexWeightsChannels(ref GeometryContent pGeometry)
        {
            for (int channelIndex = 0; channelIndex < pGeometry.Vertices.Channels.Count; )
            {
                if (pGeometry.Vertices.Channels[channelIndex] is VertexChannel<BoneWeightCollection>)
                {
                    VertexChannel<BoneWeightCollection> weightsChannel = pGeometry.Vertices.Channels[channelIndex] as VertexChannel<BoneWeightCollection>;

                    Byte4[] channelBlendIndices = new Byte4[pGeometry.Vertices.VertexCount];
                    Vector4[] channelBlendWeights = new Vector4[pGeometry.Vertices.VertexCount];

                    for (int vertexIndex = 0; vertexIndex < pGeometry.Vertices.VertexCount; vertexIndex++)
                    {
                        BoneWeightCollection vertexWeights = weightsChannel[vertexIndex];
                        vertexWeights.NormalizeWeights(4);
                        int[] blendIndices = new int[4];
                        float[] blendWeights = new float[4];

                        BoneWeight weight = default(BoneWeight);
                        int weightIndex = 0;
                        for (; weightIndex < vertexWeights.Count; weightIndex++)
                        {
                            weight = vertexWeights[weightIndex];
                            blendIndices[weightIndex] = this.skinnedModel.Skeleton.BoneIndices[weight.BoneName];
                            blendWeights[weightIndex] = weight.Weight;
                        }
                        for (; weightIndex < 4; weightIndex++)
                        {
                            blendIndices[weightIndex] = 0;
                            blendWeights[weightIndex] = 0;
                        }

                        channelBlendIndices[vertexIndex] = new Byte4(blendIndices[0], blendIndices[1], blendIndices[2], blendIndices[3]);
                        channelBlendWeights[vertexIndex] = new Vector4(blendWeights[0], blendWeights[1], blendWeights[2], blendWeights[3]);
                    }

                    int usageIndex = VertexChannelNames.DecodeUsageIndex(weightsChannel.Name);
                    pGeometry.Vertices.Channels.Insert<Byte4>(
                        channelIndex++,
                        VertexChannelNames.EncodeName(VertexElementUsage.BlendIndices, usageIndex),
                        channelBlendIndices);
                    pGeometry.Vertices.Channels.Insert<Vector4>(
                        channelIndex++,
                        VertexChannelNames.EncodeName(VertexElementUsage.BlendWeight, usageIndex),
                        channelBlendWeights);
                    pGeometry.Vertices.Channels.RemoveAt(channelIndex);
                }
                else
                {
                    channelIndex++;
                }
            }
        }

        /* Protected methods */



        /* Public methods */

        public override ZSkinnedModelContent Process(NodeContent pNode, ContentProcessorContext pContext)
        {
            //System.Diagnostics.Debugger.Launch();

            this.context = pContext;
            this.skinnedModel = new ZSkinnedModelContent();

            this.ProcessNodeForSkeleton(pNode);
            this.ProcessNodeForMeshes(pNode);

            return this.skinnedModel;
        }

    }
}