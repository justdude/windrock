﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;

namespace AnimatedModelPipeline
{
    [ContentProcessor(DisplayName = "Z Animated Model Processor")]
    public class adasdas : ModelProcessor
    {
        /* Properties */

        public override MaterialProcessorDefaultEffect DefaultEffect
        {
            get
            {
                return MaterialProcessorDefaultEffect.SkinnedEffect;
            }
        }

        /* Private methods */

        private void ValidateMesh(NodeContent pNode, string pParentBoneName)
        {
            MeshContent mesh = pNode as MeshContent;

            string parentBoneName = pParentBoneName;
            bool isRemoved = false;
            if (mesh != null)
            {
                if (!this.IsMeshHasSkinning(mesh))
                {
                    mesh.Parent.Children.Remove(mesh);
                    isRemoved = true;
                }
            }
            else if (pNode is BoneContent)
            {
                parentBoneName = pNode.Name;
            }

            if (!isRemoved)
            {
                foreach (NodeContent childNodeContent in new List<NodeContent>(pNode.Children))
                {
                    this.ValidateMesh(childNodeContent, parentBoneName);
                }
            }
        }

        /// <summary>
        /// Определяет, содержит ли меш информацию о привязке вершин.
        /// </summary>
        private bool IsMeshHasSkinning(MeshContent pMesh)
        {
            bool result = true;
            foreach (GeometryContent geometryContent in pMesh.Geometry)
            {
                if (!geometryContent.Vertices.Channels.Contains(VertexChannelNames.Weights()))
                {
                    result = false;
                    break;
                }
            }

            return result;
        }

        /// <summary>
        /// Переводит все меши в одну систему координат.
        /// </summary>
        private void ConvertTransformsToAbsolute(NodeContent pNode, BoneContent pSkeleton)
        {
            foreach (NodeContent childNodeContent in pNode.Children)
            {
                if (childNodeContent != pSkeleton)
                {
                    MeshHelper.TransformScene(childNodeContent, childNodeContent.Transform);
                    childNodeContent.Transform = Matrix.Identity;
                    this.ConvertTransformsToAbsolute(childNodeContent, pSkeleton);
                }
            }
        }

        /* Public methods */

        public override ModelContent Process(NodeContent pNode, ContentProcessorContext pContext)
        {
            //MeshHelper.TransformScene(
            //    pNode,
            //    Matrix.CreateScale(1f / 16f)
            //        //* Matrix.CreateRotationX(MathHelper.PiOver2)
            //        //* Matrix.CreateRotationY(MathHelper.Pi)
            //        );

            this.ValidateMesh(pNode, null);

            BoneContent skeleton = MeshHelper.FindSkeleton(pNode);

            if (skeleton == null)
            {
                throw new Exception(
                    string.Format("Скелет не найден. [{0}]",
                    pNode.Animations.Count));
            }

            //this.ConvertTransformsToAbsolute(pNode, skeleton);
            IList<BoneContent> bones = MeshHelper.FlattenSkeleton(skeleton);

            if (bones.Count > SkinnedEffect.MaxBones)
            {
                throw new Exception(string.Format(
                    "Количество костей модели ({0}) больше максимально допустимого ({1})",
                    bones.Count,
                    SkinnedEffect.MaxBones));
            }

            List<Matrix> boneBindPoses = new List<Matrix>(bones.Count);
            List<Matrix> inverseBoneBindPoses = new List<Matrix>(bones.Count);
            List<int> parentBoneIndices = new List<int>(bones.Count);
            Dictionary<string, int> boneIndices = new Dictionary<string, int>();

            foreach (BoneContent bone in bones)
            {
                boneBindPoses.Add(bone.Transform);
                inverseBoneBindPoses.Add(Matrix.Invert(bone.AbsoluteTransform));
                parentBoneIndices.Add(bones.IndexOf(bone.Parent as BoneContent));
                boneIndices.Add(
                    string.IsNullOrEmpty(bone.Name) || boneIndices.ContainsKey(bone.Name) ? "Bone" + boneIndices.Count.ToString("D2") : bone.Name,
                    boneIndices.Count);
            }

            ModelContent model = base.Process(pNode, pContext);
            model.Tag = new ModelSkeleton(
                boneBindPoses.ToArray(),
                inverseBoneBindPoses.ToArray(),
                parentBoneIndices.ToArray(),
                boneIndices);

            return model;
        }
    }
}