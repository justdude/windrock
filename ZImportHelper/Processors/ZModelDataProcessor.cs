﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Graphics.PackedVector;
using ZImportHelper.Contents.Data;

namespace ZImportHelper.Processors
{
    [ContentProcessor(DisplayName = "ZModelDataProcessor")]
    public class ZModelDataProcessor : ContentProcessor<NodeContent, ZModelDataContent>
    {
        /* Fields */

        private ContentProcessorContext context;
        private ZModelDataContent modelData;

        /* Properties */



        /* Constructors */



        /* Private methods */

        private void ProcessNode(NodeContent pNode)
        {
            MeshHelper.TransformScene(pNode, pNode.Transform);
            pNode.Transform = Matrix.Identity;

            if (pNode is MeshContent)
            {
                MeshContent mesh = pNode as MeshContent;
                MeshHelper.OptimizeForCache(mesh);

                this.modelData.AddMesh(this.ProcessMesh(mesh), mesh.Name);
            }

            foreach (NodeContent childNode in pNode.Children)
            {
                this.ProcessNode(childNode);
            }
        }
        private ZModelMeshDataContent ProcessMesh(MeshContent pMesh)
        {
            //int triangleCount = pGeometry.Indices.Count / 3;
            //int vertexCount = pGeometry.Vertices.VertexCount;
            //VertexBufferContent vertexBufferContent = pGeometry.Vertices.CreateVertexBuffer();
            //IndexCollection indexCollection = pGeometry.Indices;

            ZModelMeshDataContent meshData = new ZModelMeshDataContent();
            for (int positionIndex = 0; positionIndex < pMesh.Positions.Count; positionIndex++)
            {
                meshData.AddPosition(pMesh.Positions[positionIndex]);
            }

            foreach (GeometryContent geometry in pMesh.Geometry)
            {
                meshData.AddGeometry(this.ProcessGeometry(geometry));
            }

            return meshData;
        }
        private ZModelMeshGeometryDataContent ProcessGeometry(GeometryContent pGeometry)
        {
            ZModelMeshGeometryDataContent geometryData = new ZModelMeshGeometryDataContent();

            for (int i = 0; i < pGeometry.Indices.Count; i++)
            {
                geometryData.AddVertex(pGeometry.Vertices.PositionIndices[pGeometry.Indices[i]]); // О как тут, блять, всё закрутили!
            }

            foreach (VertexChannel channel in pGeometry.Vertices.Channels)
            {
                object[] channelValues = new object[channel.Count];
                channel.CopyTo(channelValues, 0);
                ZChannelContent geometryChannel = geometryData.CreateChannel(channel.Name, channel.ElementType, channelValues.Length);
                for (int vertexIndex = 0; vertexIndex < channelValues.Length; vertexIndex++)
                {
                    geometryChannel.Add(channelValues[vertexIndex]);
                }
            }

            return geometryData;
        }
        /* Protected methods */



        /* Public methods */

        public override ZModelDataContent Process(NodeContent pNode, ContentProcessorContext pContext)
        {
            //System.Diagnostics.Debugger.Launch();

            this.context = pContext;
            this.modelData = new ZModelDataContent();

            this.ProcessNode(pNode);

            return this.modelData;
        }

    }
}