﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using ImportHelper.Contents;

namespace ImportHelper.Writers
{
    [ContentTypeWriter]
    public class BasicModelPartContentWriter : ContentTypeWriter<BasicModelPartContent>
    {
        protected override void Write(ContentWriter pWriter, BasicModelPartContent pBasicModelPartContent)
        {
            pWriter.Write(pBasicModelPartContent.VertexCount);
            pWriter.Write(pBasicModelPartContent.TriangleCount);
            pWriter.WriteObject<VertexBufferContent>(pBasicModelPartContent.VertexBufferContent);
            pWriter.WriteObject<int[]>(pBasicModelPartContent.IndexCollection.ToArray<int>());
            pWriter.WriteObject<MaterialContent>(pBasicModelPartContent.MaterialContent);
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            throw new NotImplementedException();

            return "AnimatedModelLibrary.AnimatedModelSkinReader, AnimatedModelLibrary";
        }
    }
}