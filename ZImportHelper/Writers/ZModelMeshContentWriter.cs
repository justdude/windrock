﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using ZImportHelper.Contents;

namespace ZImportHelper.Writers
{
    [ContentTypeWriter]
    public class ZModelMeshContentWriter : ContentTypeWriter<ZModelMeshContent>
    {
        protected override void Write(ContentWriter pWriter, ZModelMeshContent pModelMeshContent)
        {
            pWriter.Write(pModelMeshContent.Transform);
            pWriter.WriteObject<List<ZModelMeshPartContent>>(pModelMeshContent.Parts);
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "ZImportData.Readers.ZModelMeshReader, ZImportData";
        }

        public override string GetRuntimeType(TargetPlatform targetPlatform)
        {
            return "ZImportData.Objects.ZModelMesh, ZImportData";
        }
    }
}