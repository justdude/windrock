﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using ImportHelper.Contents;

namespace ImportHelper.Writers
{
    [ContentTypeWriter]
    public class BasicModelContentWriter : ContentTypeWriter<SimpleModelContent>
    {
        protected override void Write(ContentWriter pWriter, SimpleModelContent pBasicModelContent)
        {
            pWriter.WriteObject<BasicModelPartContent[]>(pBasicModelContent.Parts.ToArray());
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "ImportData.Readers.BasicModelReader, ImportData";
        }
    }
}