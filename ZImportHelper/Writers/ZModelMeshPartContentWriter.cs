﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using ZImportHelper.Contents;

namespace ZImportHelper.Writers
{
    [ContentTypeWriter]
    public class ZModelMeshPartContentWriter : ContentTypeWriter<ZModelMeshPartContent>
    {
        protected override void Write(ContentWriter pWriter, ZModelMeshPartContent pModelMeshPartContent)
        {
            pWriter.Write(pModelMeshPartContent.VerticesCount);
            pWriter.Write(pModelMeshPartContent.TrianglesCount);
            pWriter.WriteObject<VertexBufferContent>(pModelMeshPartContent.VertexBufferContent);
            pWriter.WriteObject<IndexCollection>(pModelMeshPartContent.IndexCollection);
            pWriter.WriteObject<MaterialContent >(pModelMeshPartContent.MaterialContent);
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "ZImportData.Readers.ZModelMeshPartReader, ZImportData";
        }

        public override string GetRuntimeType(TargetPlatform targetPlatform)
        {
            return "ZImportData.Objects.ZModelMeshPart, ZImportData";
        }
    }
}