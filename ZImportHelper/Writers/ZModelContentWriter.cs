﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using ZImportHelper.Contents;

namespace ZImportHelper.Writers
{
    [ContentTypeWriter]
    public class ZModelContentWriter : ContentTypeWriter<ZModelContent>
    {
        protected override void Write(ContentWriter pWriter, ZModelContent pModelContent)
        {
            pWriter.WriteObject<List<ZModelMeshContent>>(pModelContent.Meshes);
            pWriter.WriteObject<Dictionary<string, int>>(pModelContent.MeshIndices);
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "ZImportData.Readers.ZModelReader, ZImportData";
        }

        public override string GetRuntimeType(TargetPlatform targetPlatform)
        {
            return "ZImportData.Objects.ZModel, ZImportData";
        }
    }
}