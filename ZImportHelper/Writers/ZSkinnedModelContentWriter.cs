﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using ZImportHelper.Contents;

namespace ZImportHelper.Writers
{
    [ContentTypeWriter]
    public class ZSkinnedModelContentWriter : ContentTypeWriter<ZSkinnedModelContent>
    {
        protected override void Write(ContentWriter pWriter, ZSkinnedModelContent pSkinnedModelContent)
        {
            pWriter.WriteObject<List<ZModelMeshContent>>(pSkinnedModelContent.Meshes);
            pWriter.WriteObject<ZModelSkeletonContent>(pSkinnedModelContent.Skeleton);
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "ZImportData.Readers.ZSkinnedModelReader, ZImportData";
        }

        public override string GetRuntimeType(TargetPlatform targetPlatform)
        {
            return "ZImportData.Objects.ZSkinnedModel, ZImportData";
        }
    }
}