﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using ZImportHelper.Contents.Data;

namespace ZImportHelper.DataContents.Writers.Data
{
    [ContentTypeWriter]
    public class ZModelMeshGeometryDataContentWriter : ContentTypeWriter<ZModelMeshGeometryDataContent>
    {
        protected override void Write(ContentWriter pWriter, ZModelMeshGeometryDataContent pGeometryContent)
        {
            pWriter.WriteObject<List<int>>(pGeometryContent.PositionIndices);
            pWriter.WriteObject<List<ZChannelContent>>(pGeometryContent.Channels);
            pWriter.WriteObject<Dictionary<string, int>>(pGeometryContent.ChannelIndices);
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "ZImportData.Readers.Data.ZModelMeshGeometryDataReader, ZImportData";
        }

        public override string GetRuntimeType(TargetPlatform targetPlatform)
        {
            return "ZImportData.Objects.Data.ZModelMeshGeometryData, ZImportData";
        }
    }
}