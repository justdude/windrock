﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using ZImportHelper.Contents.Data;

namespace ZImportHelper.DataContents.Writers.Data
{
    [ContentTypeWriter]
    public class ZModelMeshDataContentWriter : ContentTypeWriter<ZModelMeshDataContent>
    {
        protected override void Write(ContentWriter pWriter, ZModelMeshDataContent pModelMeshDataContent)
        {
            pWriter.WriteObject<List<Vector3>>(pModelMeshDataContent.Positions);
            pWriter.WriteObject<List<ZModelMeshGeometryDataContent>>(pModelMeshDataContent.Geometries);
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "ZImportData.Readers.Data.ZModelMeshDataReader, ZImportData";
        }

        public override string GetRuntimeType(TargetPlatform targetPlatform)
        {
            return "ZImportData.Objects.Data.ZModelMeshData, ZImportData";
        }
    }
}