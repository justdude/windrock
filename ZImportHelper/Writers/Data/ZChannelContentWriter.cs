﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using ZImportHelper.Contents.Data;

namespace ZImportHelper.DataContents.Writers.Data
{
    [ContentTypeWriter]
    public class ZChannelContentWriter : ContentTypeWriter<ZChannelContent>
    {
        protected override void Write(ContentWriter pWriter, ZChannelContent pChannelContent)
        {
            //pWriter.WriteObject<Type>(pChannelContent.ValueType);
            pWriter.WriteObject<List<object>>(pChannelContent.Values);
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "ZImportData.Readers.Data.ZChannelReader, ZImportData";
        }

        public override string GetRuntimeType(TargetPlatform targetPlatform)
        {
            return "ZImportData.Objects.Data.ZChannel, ZImportData";
        }
    }
}