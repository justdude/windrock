﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using ZImportHelper.Contents.Data;

namespace ZImportHelper.DataContents.Writers.Data
{
    [ContentTypeWriter]
    public class ZModelDataContentWriter : ContentTypeWriter<ZModelDataContent>
    {
        protected override void Write(ContentWriter pWriter, ZModelDataContent pMeshContent)
        {
            pWriter.WriteObject<List<ZModelMeshDataContent>>(pMeshContent.Meshes);
            pWriter.WriteObject<Dictionary<string, int>>(pMeshContent.MeshIndices);
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "ZImportData.Readers.Data.ZModelDataReader, ZImportData";
        }

        public override string GetRuntimeType(TargetPlatform targetPlatform)
        {
            return "ZImportData.Objects.Data.ZModelData, ZImportData";
        }
    }
}