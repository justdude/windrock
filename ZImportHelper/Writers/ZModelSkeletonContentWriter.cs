﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using ZImportHelper.Contents;

namespace ZImportHelper.Writers
{
    [ContentTypeWriter]
    public class ZModelSkeletonContentWriter : ContentTypeWriter<ZModelSkeletonContent>
    {
        protected override void Write(ContentWriter pWriter, ZModelSkeletonContent pModelSkeletonContent)
        {
            pWriter.Write(pModelSkeletonContent.BoneCount);
            pWriter.WriteObject<Dictionary<string, int>>(pModelSkeletonContent.BoneIndices);
            pWriter.WriteObject<int[]>(pModelSkeletonContent.ParentBoneIndices);
            pWriter.WriteObject<Matrix[]>(pModelSkeletonContent.RelativeBindPoses);
            pWriter.WriteObject<Matrix[]>(pModelSkeletonContent.InverseAbsoluteBindPoses);
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "ZImportData.Readers.ZModelSkeletonReader, ZImportData";
        }

        public override string GetRuntimeType(TargetPlatform targetPlatform)
        {
            return "ZImportData.Objects.ZModelSkeleton, ZImportData";
        }
    }
}