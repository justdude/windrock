﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using ImportData.Objects;

namespace AnimatedModelPipeline
{
    [ContentTypeWriter]
    public class AnimatedModelSkinWriter : ContentTypeWriter<ZModelSkeleton>
    {
        protected override void Write(ContentWriter pWriter, ZModelSkeleton pSkeleton)
        {
            pWriter.WriteObject<Matrix[]>(pSkeleton.RelativeBindPoses);
            pWriter.WriteObject<Matrix[]>(pSkeleton.InverseAbsoluteBindPoses);
            pWriter.WriteObject<int[]>(pSkeleton.ParentBoneIndices);
            pWriter.WriteObject<Dictionary<string, int>>(pSkeleton.BoneIndices);
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "AnimatedModelLibrary.AnimatedModelSkinReader, AnimatedModelLibrary";
        }
    }
}