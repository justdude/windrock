﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.Xna.Framework;

namespace ZImportHelper.Importers
{
    public class ZMaterialLoader
    {
        /* Constants */

        private const string NewMaterialMtlFileToken = "newmtl";
        private const string DiffuseColorMtlFileToken = "kd";
        private const string DiffuseTextureMtlFileToken = "map_kd";
        private const string AmbientColorMtlFileToken = "ka";
        private const string SpecularColorMtlFileToken = "ks";
        private const string SpecularPowerMtlFileToken = "ns";
        private const string EmissiveColorMtlFileToken = "ke";
        private const string AlphaMtlFileToken = "d";
        private const string IlluminationModeMtlFileToken = "illum";

        /* Fields */

        private Dictionary<string, MaterialContent> materials;
        ContentIdentity materialFileIdentity;
        BasicMaterialContent currentMaterial;

        /* Properties */



        /* Constructors */

        public ZMaterialLoader()
        {
        }

        /* Private methods */

        private void ProcessMtlFileTokens(string[] pTokens)
        {
            switch (pTokens[0].ToLower())
            {
                case NewMaterialMtlFileToken:
                    if (this.currentMaterial != null)
                    {
                        this.materials.Add(this.currentMaterial.Name, this.currentMaterial);
                    }
                    this.currentMaterial = new BasicMaterialContent();
                    this.currentMaterial.Identity = new ContentIdentity(this.materialFileIdentity.SourceFilename);
                    this.currentMaterial.Name = pTokens[1];
                    break;

                case DiffuseColorMtlFileToken:
                    this.currentMaterial.DiffuseColor = new Vector3(
                        Single.Parse(pTokens[1]),
                        Single.Parse(pTokens[2]),
                        Single.Parse(pTokens[3]));
                    break;

                case DiffuseTextureMtlFileToken:
                    this.currentMaterial.Texture = new ExternalReference<TextureContent>(
                        pTokens[1],
                        this.materialFileIdentity);
                    break;

                case AmbientColorMtlFileToken:
                    break;


                case SpecularColorMtlFileToken:
                    this.currentMaterial.SpecularColor = new Vector3(
                        Single.Parse(pTokens[1]),
                        Single.Parse(pTokens[2]),
                        Single.Parse(pTokens[3]));
                    break;

                case SpecularPowerMtlFileToken:
                    this.currentMaterial.SpecularPower = Single.Parse(pTokens[1]);
                    break;

                case EmissiveColorMtlFileToken:
                    this.currentMaterial.EmissiveColor = new Vector3(
                        Single.Parse(pTokens[1]),
                        Single.Parse(pTokens[2]),
                        Single.Parse(pTokens[3]));
                    break;

                case AlphaMtlFileToken:
                    this.currentMaterial.Alpha = Single.Parse(pTokens[1]);
                    break;

                case IlluminationModeMtlFileToken:
                    this.currentMaterial.OpaqueData.Add("Illumination mode", Int32.Parse(pTokens[1]));
                    break;

                case "ni":
                case "tf":
                case "tr":
                case "map_ka":
                    break;

                default:
                    string message = string.Format(
                        "Invalid material file token: \"{0}\"",
                        pTokens[0]);
                    throw new InvalidOperationException(message);
            }
        }
        private IEnumerable<string[]> GetNextFileTokens(string pFileName, ContentIdentity pIdentity)
        {
            using (StreamReader reader = new StreamReader(pFileName))
            {
                string[] tokens = null;
                string line = null;
                int lineNumber = 0;

                while (!reader.EndOfStream)
                {
                    lineNumber++;
                    pIdentity.FragmentIdentifier = lineNumber.ToString();

                    line = reader.ReadLine().Trim();
                    tokens = Regex.Split(line, @"\s+");

                    if (tokens.Length > 0
                        && !string.IsNullOrEmpty(tokens[0])
                        && tokens[0] != "#")
                    {
                        yield return tokens;
                    }
                }
            }
        }

        /* Protected methods */



        /* Public methods */

        public Dictionary<string, MaterialContent> GetMaterials(string pFileName)
        {
            try
            {
                this.materials = new Dictionary<string, MaterialContent>();
                this.materialFileIdentity = new ContentIdentity(pFileName);

                foreach (string[] tokens in this.GetNextFileTokens(pFileName, materialFileIdentity))
                {
                    this.ProcessMtlFileTokens(tokens);
                }

                if (this.currentMaterial != null)
                {
                    this.materials.Add(this.currentMaterial.Name, this.currentMaterial);
                }

                return this.materials;
            }
            finally
            {
                this.materials = null;
                this.currentMaterial = null;
                this.materialFileIdentity = null;
            }
        }

    }
}