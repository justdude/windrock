﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;
using ZImportHelper.Importers;

namespace ModelHelper.Importers
{
    [ContentImporter(".smd", CacheImportedData = true, DefaultProcessor = "ModelProcessor")]
    public class ZSmdReferenceImporter : ContentImporter<NodeContent>
    {
        /* Constants */

        protected const string NodesSmdFileToken = "nodes";
        protected const string AnimationSmdFileToken = "skeleton";
        private const string TrianglesSmdFileToken = "triangles";

        /* Fields */

        private ContentImporterContext context;
        private NodeContent root;
        private StreamReader smdFileReader;
        private Dictionary<int, NodeContent> nodes;
        private Dictionary<int, BoneContent> rootBones;
        private Dictionary<string, MaterialContent> materials;
        private MeshBuilder meshBuilder;

        /* Properties */



        /* Constructors */

        public ZSmdReferenceImporter()
        {
        }

        /* Private methods */

        /// <summary>
        /// Импортирует контент модели.
        /// </summary>
        /// <param name="pFileName">Имя файла модели.</param>
        private void ImportModelContent(string pFileName)
        {
            this.root = new NodeContent();
            this.root.Identity = new ContentIdentity(pFileName);

            this.smdFileReader = new StreamReader(File.OpenRead(pFileName));

            string line = this.smdFileReader.ReadLine();
            this.root.Name = line;

            while (!this.smdFileReader.EndOfStream)
            {
                line = this.smdFileReader.ReadLine();

                if (string.IsNullOrEmpty(line))
                {
                    break;
                }
                else
                {
                    switch (line.Trim().ToLower())
                    {
                        case ZSmdReferenceImporter.NodesSmdFileToken:
                            this.ProcessNodes();
                            break;

                        case ZSmdReferenceImporter.AnimationSmdFileToken:
                            this.ProcessAnimation();
                            break;

                        case ZSmdReferenceImporter.TrianglesSmdFileToken:
                            this.ProcessTriangles();
                            break;
                    }
                }
            }
        }
        /// <summary>
        /// Импортирует материаллы из файла с таким же названием из той же директории, но с расшрением .mtl.
        /// </summary>
        /// <param name="pFileName">Имя файла модели.</param>
        private void ImportMaterials(string pFileName)
        {
            string materialFileName = Path.ChangeExtension(pFileName, ".mtl");
            ZMaterialLoader materialLoader = new ZMaterialLoader();
            this.materials = materialLoader.GetMaterials(materialFileName);
        }
        private IEnumerable<string[]> GetNextFileTokens(string pFileName, ContentIdentity pIdentity)
        {
            using (StreamReader reader = new StreamReader(pFileName))
            {
                string[] tokens = null;
                string line = null;
                int lineNumber = 0;

                while (!reader.EndOfStream)
                {
                    lineNumber++;
                    pIdentity.FragmentIdentifier = lineNumber.ToString();

                    line = reader.ReadLine().Trim();
                    tokens = Regex.Split(line, @"\s+");

                    if (tokens.Length > 0
                        && !string.IsNullOrEmpty(tokens[0])
                        && tokens[0] != "#")
                    {
                        yield return tokens;
                    }
                }
            }
        }
        private void ProcessNodes()
        {
            this.nodes = new Dictionary<int, NodeContent>();
            int rootBoneIndex = -1;

            string line = null;
            string[] tokens = null;
            int nodeIndex = -1;
            int parentNodeIndex = -1;
            string nodeName = null;

            while (!this.smdFileReader.EndOfStream)
            {
                line = this.smdFileReader.ReadLine();
                if (line.Equals("end", StringComparison.InvariantCultureIgnoreCase))
                {
                    break;
                }

                tokens = line.Trim().Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                nodeIndex = Int32.Parse(tokens[0]);
                nodeName = tokens[1].Trim('\"');
                parentNodeIndex = Int32.Parse(tokens[2]);

                if (parentNodeIndex == -1)
                {
                    NodeContent node = new NodeContent()
                    {
                        Name = nodeName
                    };
                    this.nodes.Add(nodeIndex, node);
                    this.root.Children.Add(node);
                }
                else if (parentNodeIndex >= 0)
                {
                    NodeContent parentNode = null;
                    if (this.nodes.TryGetValue(parentNodeIndex, out parentNode))
                    {
                        if (parentNode.Parent == this.root)
                        {
                            if (rootBoneIndex == -1)
                            {
                                BoneContent rootBone = new BoneContent()
                                {
                                    Name = parentNode.Name
                                };
                                rootBoneIndex = parentNodeIndex;
                                this.nodes[rootBoneIndex] = rootBone;
                                this.root.Children.Remove(parentNode);
                                this.root.Children.Add(rootBone);
                                parentNode = rootBone;
                            }
                            else if (parentNodeIndex != rootBoneIndex)
                            {
                                throw new Exception("More then one node want to be a root bone.");
                            }
                        }

                        BoneContent bone = new BoneContent()
                        {
                            Name = nodeName
                        };
                        parentNode.Children.Add(bone);
                        this.nodes.Add(nodeIndex, bone);
                    }
                }
                else
                {
                    throw new Exception(
                        string.Format(
                            "Hey! Get off your \"{0}\" and use \"-1\" or greater number for parent index!);",
                            parentNodeIndex));
                }
            }

            if (rootBoneIndex == -1)
            {
                throw new Exception("Wait a moment! You are missed somethis very important!");
            }
        }
        private void ProcessAnimation()
        {
            string line = null;
            string[] tokens = null;
            int frameIndex = -1;
            int boneIndex = -1;
            Vector3 translation = Vector3.Zero;
            Vector3 rotation = Vector3.Zero;

            while (!this.smdFileReader.EndOfStream)
            {
                line = this.smdFileReader.ReadLine();
                if (line.Equals("end", StringComparison.InvariantCultureIgnoreCase))
                {
                    break;
                }

                if (line[0] == ' ')
                {
                    tokens = line.Trim().Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    boneIndex = Int32.Parse(tokens[0]);
                    translation = new Vector3(
                        Single.Parse(tokens[1]),
                        Single.Parse(tokens[3]),
                        -Single.Parse(tokens[2]));
                    rotation = new Vector3(
                        Single.Parse(tokens[4]),
                        Single.Parse(tokens[6]),
                        -Single.Parse(tokens[5]));

                    Matrix relativeBoneBindPose =
                        Matrix.CreateFromYawPitchRoll(rotation.Y, rotation.X, rotation.Z)
                        * Matrix.CreateTranslation(translation);
                    this.nodes[boneIndex].Transform = relativeBoneBindPose;
                }
                else
                {
                    tokens = line.Trim().Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    if (tokens[0] == "time")
                    {
                        frameIndex = Int32.Parse(tokens[1]);
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
            }
        }
        /// <summary>
        /// Импортирует контент модели.
        /// </summary>
        /// <param name="pFileName">Имя файла модели.</param>
        private void ProcessTriangles()
        {
            this.meshBuilder = MeshBuilder.StartMesh(string.Empty);
            this.meshBuilder.SwapWindingOrder = true;

            // Временные списки для свойств вершин.
            List<Vector2> verticesTexCoords = new List<Vector2>();
            List<Vector3> verticesNormals = new List<Vector3>();
            List<BoneWeightCollection> verticesWeights = new List<BoneWeightCollection>();
            List<int> verticesIndices = new List<int>();

            string line = null;
            string[] tokens = null;
            int boneIndex = -1;
            Vector3 position = Vector3.Zero;
            Vector3 normal = Vector3.Zero;
            Vector2 texCoord = Vector2.Zero;
            int linksCount = 0;
            int linkBoneIndex = -1;
            float linkBoneWeight = 0;
            int vertexIndex = 0;
            BoneWeightCollection weights = null;

            int lineNumber = -1;
            while (!this.smdFileReader.EndOfStream)
            {
                line = this.smdFileReader.ReadLine();
                if (line.Equals("end", StringComparison.InvariantCultureIgnoreCase))
                {
                    break;
                }

                lineNumber++;
                if (lineNumber % 4 == 0)
                {
                    // Material
                    string materialName = line.Trim();
                    MaterialContent material = null;
                    if (this.materials.TryGetValue(materialName, out material))
                    {
                        this.meshBuilder.SetMaterial(material);
                    }
                    else
                    {
                        throw new Exception("Material \"" + materialName + "\" was not found.");
                    }
                }
                else
                {
                    // Vertex
                    // <int|Parent bone> <float|PosX PosY PosZ> <normal|NormX NormY NormZ> <normal|U V> <int|links> <int|Bone ID> <normal|Weight>
                    tokens = line.Trim().Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    boneIndex = Int32.Parse(tokens[0]);
                    position = new Vector3(
                        Single.Parse(tokens[1]),
                        Single.Parse(tokens[3]),
                        -Single.Parse(tokens[2]));
                    Matrix inverceAbsoluteBoneTransform = Matrix.Invert(this.nodes[boneIndex].AbsoluteTransform);
                    //position = Vector3.Transform(position, inverceAbsoluteBoneTransform);
                    normal = new Vector3(
                        Single.Parse(tokens[4]),
                        Single.Parse(tokens[6]),
                        -Single.Parse(tokens[5]));
                    texCoord = new Vector2(
                        Single.Parse(tokens[7]),
                        1 - Single.Parse(tokens[8])); // Какого-то Печкина этот 3DMax'овский smd-експортер флипает v-координату!

                    // Нельзя поочередно добавлять позиции и вершины.
                    vertexIndex = this.meshBuilder.CreatePosition(position);

                    // Позици добавляются сразу в meshBuilder, а данные вершин сохраняются во временные списки.
                    verticesTexCoords.Add(texCoord);
                    verticesNormals.Add(normal);
                    verticesIndices.Add(vertexIndex);

                    // Привязки вершины к костям.
                    if (tokens.Length > 9)
                    {
                        linksCount = Int32.Parse(tokens[9]);
                        if (linksCount > 0)
                        {
                            weights = new BoneWeightCollection();
                            for (int i = 0; i < linksCount; i++)
                            {
                                linkBoneIndex = Int32.Parse(tokens[10 + i * 2]);
                                linkBoneWeight = Single.Parse(tokens[11 + i * 2]);
                                weights.Add(new BoneWeight(this.nodes[linkBoneIndex].Name, linkBoneWeight));
                            }

                            verticesWeights.Add(weights);
                        }
                    }
                    else
                    {
                        weights = new BoneWeightCollection();
                        weights.Add(new BoneWeight(this.nodes[boneIndex].Name, 1.0f));
                        verticesWeights.Add(weights);
                    }
                }
            }

            // Данные вершин из временных списков заносятся в meshBuilder.
            int texCoordChannelIndex = this.meshBuilder.CreateVertexChannel<Vector2>(VertexChannelNames.TextureCoordinate(0));
            int normalChannelIndex = this.meshBuilder.CreateVertexChannel<Vector3>(VertexChannelNames.Normal());
            int weightsChannelIndex = this.meshBuilder.CreateVertexChannel<BoneWeightCollection>(VertexChannelNames.Weights());
            for (int i = 0; i < verticesIndices.Count; i++)
            {
                texCoord = verticesTexCoords[i];
                normal = verticesNormals[i];
                weights = verticesWeights[i];
                vertexIndex = verticesIndices[i];

                this.meshBuilder.SetVertexChannelData(texCoordChannelIndex, texCoord);
                this.meshBuilder.SetVertexChannelData(normalChannelIndex, normal);
                this.meshBuilder.SetVertexChannelData(weightsChannelIndex, weights);
                this.meshBuilder.AddTriangleVertex(vertexIndex);
            }

            // Завершение меша и добавление его в корневой узел.
            MeshContent meshContent = this.meshBuilder.FinishMesh();
            if (meshContent.Geometry.Count > 0)
            {
                this.root.Children.Add(meshContent);
            }
        }

        /* Protected methods */



        /* Public methods */

        public override NodeContent Import(string pFileName, ContentImporterContext pContext)
        {
            //System.Diagnostics.Debugger.Launch();

            this.context = pContext;

            this.ImportMaterials(pFileName);
            this.ImportModelContent(pFileName);

            return this.root;
        }

    }
}