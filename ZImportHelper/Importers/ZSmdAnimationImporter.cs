﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;
using ZImportHelper.Importers;

namespace ZImportHelper.Importers
{
    [ContentImporter(".smd", CacheImportedData = true, DefaultProcessor = "ModelProcessor")]
    public class ZSmdAnimationImporter : ContentImporter<NodeContent>
    {
        /* Constants */

        protected const string NodesSmdFileToken = "nodes";
        protected const string AnimationSmdFileToken = "skeleton";

        /* Fields */

        protected ContentImporterContext context;
        protected NodeContent root;
        protected AnimationContent animation;
        protected Dictionary<int, NodeContent> nodes;
        protected StreamReader smdFileReader;
        protected Dictionary<int, BoneContent> rootBones;

        /* Properties */



        /* Constructors */

        public ZSmdAnimationImporter()
        {
        }

        /* Private methods */


        /* Protected methods */

        protected IEnumerable<string[]> GetNextFileTokens(string pFileName, ContentIdentity pIdentity)
        {
            using (StreamReader reader = new StreamReader(pFileName))
            {
                string[] tokens = null;
                string line = null;
                int lineNumber = 0;

                while (!reader.EndOfStream)
                {
                    lineNumber++;
                    pIdentity.FragmentIdentifier = lineNumber.ToString();

                    line = reader.ReadLine().Trim();
                    tokens = Regex.Split(line, @"\s+");

                    if (tokens.Length > 0
                        && !string.IsNullOrEmpty(tokens[0])
                        && tokens[0] != "#")
                    {
                        yield return tokens;
                    }
                }
            }
        }
        protected void ProcessNodes()
        {
            this.nodes = new Dictionary<int, NodeContent>();
            int rootBoneIndex = -1;

            string line = null;
            string[] tokens = null;
            int nodeIndex = -1;
            int parentNodeIndex = -1;
            string nodeName = null;

            while (!this.smdFileReader.EndOfStream)
            {
                line = this.smdFileReader.ReadLine();
                if (line.Equals("end", StringComparison.InvariantCultureIgnoreCase))
                {
                    break;
                }

                tokens = line.Trim().Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                nodeIndex = Int32.Parse(tokens[0]);
                nodeName = tokens[1].Trim('\"');
                parentNodeIndex = Int32.Parse(tokens[2]);

                if (parentNodeIndex == -1)
                {
                    NodeContent node = new NodeContent()
                    {
                        Name = nodeName
                    };
                    this.nodes.Add(nodeIndex, node);
                    this.root.Children.Add(node);
                }
                else if (parentNodeIndex >= 0)
                {
                    NodeContent parentNode = null;
                    if (this.nodes.TryGetValue(parentNodeIndex, out parentNode))
                    {
                        if (parentNode.Parent == this.root)
                        {
                            if (rootBoneIndex == -1)
                            {
                                BoneContent rootBone = new BoneContent()
                                {
                                    Name = parentNode.Name
                                };
                                rootBoneIndex = parentNodeIndex;
                                this.nodes[rootBoneIndex] = rootBone;
                                this.root.Children.Remove(parentNode);
                                this.root.Children.Add(rootBone);
                                parentNode = rootBone;
                            }
                            else if (parentNodeIndex != rootBoneIndex)
                            {
                                throw new Exception("More then one node want to be a root bone.");
                            }
                        }

                        BoneContent bone = new BoneContent()
                        {
                            Name = nodeName
                        };
                        parentNode.Children.Add(bone);
                        this.nodes.Add(nodeIndex, bone);
                    }
                }
                else
                {
                    throw new Exception(
                        string.Format(
                            "Hey! Get off your \"{0}\" and use \"-1\" or greater number for parent index!);",
                            parentNodeIndex));
                }
            }

            if (rootBoneIndex == -1)
            {
                throw new Exception("Wait a moment! You are missed somethis very important!");
            }
        }
        protected void ProcessAnimation()
        {
            this.animation = new AnimationContent();
            this.root.Animations.Add(this.root.Name, this.animation);

            string line = null;
            string[] tokens = null;
            int frameIndex = -1;
            int boneIndex = -1;
            Vector3 translation = Vector3.Zero;
            Vector3 rotation = Vector3.Zero;

            while (!this.smdFileReader.EndOfStream)
            {
                line = this.smdFileReader.ReadLine();
                if (line.Equals("end", StringComparison.InvariantCultureIgnoreCase))
                {
                    break;
                }

                if (line[0] == ' ')
                {
                    tokens = line.Trim().Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    boneIndex = Int32.Parse(tokens[0]);
                    translation = new Vector3(
                        Single.Parse(tokens[1]),
                        Single.Parse(tokens[2]),
                        Single.Parse(tokens[3]));
                    rotation = new Vector3(
                        Single.Parse(tokens[4]),
                        Single.Parse(tokens[5]),
                        Single.Parse(tokens[6]));

                    AnimationChannel channel = null;
                    if (frameIndex == 0)
                    {
                        channel = new AnimationChannel();
                        this.animation.Channels.Add(boneIndex.ToString(), channel);
                    }
                    else
                    {
                        channel = this.animation.Channels[boneIndex.ToString()];
                    }

                    Matrix relativeTranform =
                        Matrix.CreateFromYawPitchRoll(rotation.Y, rotation.X, rotation.Z)
                        * Matrix.CreateTranslation(translation);
                    AnimationKeyframe keyframe = new AnimationKeyframe(TimeSpan.FromTicks(frameIndex), relativeTranform);
                    channel.Add(keyframe);
                }
                else
                {
                    tokens = line.Trim().Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    if (tokens[0] == "time")
                    {
                        frameIndex = Int32.Parse(tokens[1]);
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
            }
        }
        /// <summary>
        /// Импортирует контент модели.
        /// </summary>
        /// <param name="pFileName">Имя файла модели.</param>
        protected virtual void ImportModelContent(string pFileName)
        {
            this.root = new NodeContent();
            this.root.Identity = new ContentIdentity(pFileName);

            this.smdFileReader = new StreamReader(File.OpenRead(pFileName));

            string line = this.smdFileReader.ReadLine();
            this.root.Name = line;

            while (!this.smdFileReader.EndOfStream)
            {
                line = this.smdFileReader.ReadLine();

                if (string.IsNullOrEmpty(line))
                {
                    break;
                }
                else
                {
                    switch (line.Trim().ToLower())
                    {
                        case ZSmdAnimationImporter.NodesSmdFileToken:
                            ProcessNodes();
                            break;

                        case ZSmdAnimationImporter.AnimationSmdFileToken:
                            this.ProcessAnimation();
                            break;
                    }
                }
            }
        }

        /* Public methods */

        public override NodeContent Import(string pFileName, ContentImporterContext pContext)
        {
            //System.Diagnostics.Debugger.Launch();

            this.context = pContext;

            this.ImportModelContent(pFileName);

            return this.root;
        }

    }
}