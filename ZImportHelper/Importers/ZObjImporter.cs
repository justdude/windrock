﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;

namespace ZImportHelper.Importers
{
    [ContentImporter(".obj", CacheImportedData = true, DefaultProcessor = "ModelProcessor")]
    public class ZObjImporter : ContentImporter<NodeContent>
    {
        /* Constants */

        private const string ObjectObjFileToken = "o";
        private const string VertexObjFileToken = "v";
        private const string TextureCoordObjFileToken = "vt";
        private const string NormalObjFileToken = "vn";
        private const string GroupObjFileToken = "g";
        private const string SmoothObjFileToken = "s";
        private const string FaceObjFileToken = "f";
        private const string ImportMaterialObjFileToken = "mtllib";
        private const string UseMaterialObjFileToken = "usemtl";

        private const string NewMaterialMtlFileToken = "newmtl";
        private const string DiffuseColorMtlFileToken = "kd";
        private const string DiffuseTextureMtlFileToken = "map_kd";
        private const string AmbientColorMtlFileToken = "ka";
        private const string SpecularColorMtlFileToken = "ks";
        private const string SpecularPowerMtlFileToken = "ns";
        private const string EmissiveColorMtlFileToken = "ke";
        private const string AlphaMtlFileToken = "d";
        private const string IlluminationModeMtlFileToken = "illum";

        /* Fields */

        private ContentImporterContext context;
        private NodeContent rootContent;
        private List<Vector3> positions;
        private List<Vector2> textureCoords;
        private List<Vector3> normals;
        private MeshBuilder meshBuilder;
        private int[] positionIndices;
        private int textureCoordsChannelIndex;
        private int normalsChannelIndex;

        private Dictionary<string, MaterialContent> materials;
        private ContentIdentity materialFileIdentity;
        private BasicMaterialContent materialContent;

        private int positionStartIndex;
        private int textureCoordStartIndex;
        private int normalStartIndex;
        private bool isFinishMeshRequired;

        /* Properties */



        /* Constructors */

        public ZObjImporter()
        {
            this.positionStartIndex = 0;
            this.textureCoordStartIndex = 0;
            this.normalStartIndex = 0;
        }

        /* Private methods */

        private void StartMesh(string pName)
        {
            this.meshBuilder = MeshBuilder.StartMesh(pName);
            this.textureCoordsChannelIndex = this.meshBuilder.CreateVertexChannel<Vector2>(VertexChannelNames.TextureCoordinate(0));
            this.normalsChannelIndex = this.meshBuilder.CreateVertexChannel<Vector3>(VertexChannelNames.Normal());
            this.positionIndices = new int[this.positions.Count];
            for (int i = 0; i < this.positions.Count; i++)
            {
                this.positionIndices[i] = this.meshBuilder.CreatePosition(this.positions[i]);
            }
        }
        private void FinishMesh()
        {
            this.meshBuilder.SwapWindingOrder = false;
            MeshContent meshContent = this.meshBuilder.FinishMesh();
            if (meshContent.Geometry.Count > 0)
            {
                this.rootContent.Children.Add(meshContent);
            }
            else
            {
                NodeContent nodeContent = new NodeContent();
                nodeContent.Name = meshContent.Name;
                this.rootContent.Children.Add(nodeContent);
            }

            this.meshBuilder = null;

            this.positionStartIndex += this.positionIndices.Length;
            this.positions.Clear();

            this.textureCoordStartIndex += this.textureCoords.Count;
            this.textureCoords.Clear();

            this.normalStartIndex += this.normals.Count;
            this.normals.Clear();
        }

        private IEnumerable<string[]> GetNextFileTokens(string pFileName, ContentIdentity pIdentity)
        {
            using (StreamReader reader = new StreamReader(pFileName))
            {
                string[] tokens = null;
                string line = null;
                int lineNumber = 0;

                while (!reader.EndOfStream)
                {
                    lineNumber++;
                    pIdentity.FragmentIdentifier = lineNumber.ToString();

                    line = reader.ReadLine().Trim();
                    tokens = Regex.Split(line, @"\s+");

                    if (tokens.Length > 0
                        && !string.IsNullOrEmpty(tokens[0])
                        && tokens[0] != "#")
                    {
                        yield return tokens;
                    }
                }
            }
        }
        private void ProcessObjFileTokens(string[] pTokens)
        {
            switch (pTokens[0].ToLower())
            {
                case ZObjImporter.ObjectObjFileToken:
                    this.rootContent.Name = pTokens[1];
                    break;

                case ZObjImporter.VertexObjFileToken:
                    if (this.isFinishMeshRequired)
                    {
                        this.FinishMesh();
                        this.isFinishMeshRequired = false;
                    }

                    this.positions.Add(
                        new Vector3(
                            Single.Parse(pTokens[1]),
                            Single.Parse(pTokens[2]),
                            Single.Parse(pTokens[3])));
                    break;

                case ZObjImporter.TextureCoordObjFileToken:
                    this.textureCoords.Add(
                        new Vector2(
                            Single.Parse(pTokens[1]),
                            Single.Parse(pTokens[2])));
                    break;

                case ZObjImporter.NormalObjFileToken:
                    this.normals.Add(
                        new Vector3(
                            Single.Parse(pTokens[1]),
                            Single.Parse(pTokens[2]),
                            Single.Parse(pTokens[3])));
                    break;

                case ZObjImporter.GroupObjFileToken:
                    if (this.meshBuilder != null)
                    {
                        this.FinishMesh();
                    }
                    string name = pTokens.Length > 1 ? pTokens[1] : null;
                    this.StartMesh(name);
                    break;

                case ZObjImporter.SmoothObjFileToken:
                    this.context.Logger.LogWarning(
                        null,
                        this.rootContent.Identity,
                        "Smooth are not supported. -> Ignored.");
                    break;

                case ZObjImporter.FaceObjFileToken:
                    if (pTokens.Length > 4)
                    {
                        this.context.Logger.LogWarning(
                            null,
                            this.rootContent.Identity,
                            "Polygons are not supported. -> Ignored.");
                    }
                    else
                    {
                        this.isFinishMeshRequired = true;

                        if (this.meshBuilder == null)
                        {
                            this.StartMesh(null);
                        }

                        for (int i = 1; i < 4; i++)
                        {
                            string[] indices = pTokens[i].Split('/');

                            // Vertex position.
                            int positionIndexIndex = Int32.Parse(indices[0]) - 1;

                            if (indices.Length > 1)
                            {
                                // Vertex texture coord.
                                int textureCoordIndex = 0;
                                Vector2 textureCoord =
                                    Int32.TryParse(indices[1], out textureCoordIndex) ?
                                    this.textureCoords[textureCoordIndex - this.textureCoordStartIndex - 1] :
                                    Vector2.Zero;
                                this.meshBuilder.SetVertexChannelData(this.textureCoordsChannelIndex, textureCoord);

                                if (indices.Length > 2)
                                {
                                    // Vertex normal.
                                    int normalIndex = 0;
                                    Vector3 normal =
                                        Int32.TryParse(indices[2], out normalIndex) ?
                                        this.normals[normalIndex - this.normalStartIndex - 1] :
                                        Vector3.Zero;
                                    this.meshBuilder.SetVertexChannelData(this.normalsChannelIndex, normal);
                                }
                            }

                            this.meshBuilder.AddTriangleVertex(this.positionIndices[positionIndexIndex - this.positionStartIndex]);
                        }
                    }
                    break;

                case ZObjImporter.ImportMaterialObjFileToken:
                    for (int i = 1; i < pTokens.Length; i++)
                    {
                        string materialFileName = pTokens[i];
                        if (!Path.IsPathRooted(materialFileName))
                        {
                            string sourceFileDirectory = Path.GetDirectoryName(this.rootContent.Identity.SourceFilename);
                            materialFileName = Path.GetFullPath(Path.Combine(sourceFileDirectory, materialFileName));
                        }

                        this.context.AddDependency(materialFileName);
                        this.ImportMaterials(materialFileName);
                    }
                    break;

                case ZObjImporter.UseMaterialObjFileToken:
                    if (this.meshBuilder == null)
                    {
                        this.StartMesh(null);
                    }
                    string materialName = pTokens[1];
                    MaterialContent material = null;
                    if (this.materials.TryGetValue(materialName, out material))
                    {
                        this.meshBuilder.SetMaterial(material);
                    }
                    else
                    {
                        throw new InvalidOperationException(
                            string.Format(
                              "Material \"{0}\" not found.",
                              materialName));
                    }
                    break;

                default:
                    string message = string.Format(
                        "Invalid object file token: \"{0}\"",
                        pTokens[0]);
                    throw new InvalidOperationException(message);
            }
        }
        private void ProcessMtlFileTokens(string[] pTokens)
        {
            switch (pTokens[0].ToLower())
            {
                case ZObjImporter.NewMaterialMtlFileToken:
                    if (this.materialContent != null)
                    {
                        this.materials.Add(this.materialContent.Name, this.materialContent);
                    }
                    this.materialContent = new BasicMaterialContent();
                    this.materialContent.Identity = new ContentIdentity(this.materialFileIdentity.SourceFilename);
                    this.materialContent.Name = pTokens[1];
                    break;

                case ZObjImporter.DiffuseColorMtlFileToken:
                    this.materialContent.DiffuseColor = new Vector3(
                        Single.Parse(pTokens[1]),
                        Single.Parse(pTokens[2]),
                        Single.Parse(pTokens[3]));
                    break;

                case ZObjImporter.DiffuseTextureMtlFileToken:
                    this.materialContent.Texture = new ExternalReference<TextureContent>(
                        pTokens[1],
                        this.materialFileIdentity);
                    break;

                case ZObjImporter.AmbientColorMtlFileToken:
                    break;


                case ZObjImporter.SpecularColorMtlFileToken:
                    this.materialContent.SpecularColor = new Vector3(
                        Single.Parse(pTokens[1]),
                        Single.Parse(pTokens[2]),
                        Single.Parse(pTokens[3]));
                    break;

                case ZObjImporter.SpecularPowerMtlFileToken:
                    this.materialContent.SpecularPower = Single.Parse(pTokens[1]);
                    break;

                case ZObjImporter.EmissiveColorMtlFileToken:
                    this.materialContent.EmissiveColor = new Vector3(
                        Single.Parse(pTokens[1]),
                        Single.Parse(pTokens[2]),
                        Single.Parse(pTokens[3]));
                    break;

                case ZObjImporter.AlphaMtlFileToken:
                    this.materialContent.Alpha = Single.Parse(pTokens[1]);
                    break;

                case ZObjImporter.IlluminationModeMtlFileToken:
                    this.materialContent.OpaqueData.Add("Illumination mode", Int32.Parse(pTokens[1]));
                    break;

                case "ni":
                case "tf":
                case "tr":
                case "map_ka":
                    break;

                default:
                    string message = string.Format(
                        "Invalid material file token: \"{0}\"",
                        pTokens[0]);
                    throw new InvalidOperationException(message);
            }
        }

        private void ImportMaterials(string pFileName)
        {
            this.materialFileIdentity = new ContentIdentity(pFileName);
            this.materialContent = null;

            foreach (string[] tokens in this.GetNextFileTokens(pFileName, this.materialFileIdentity))
            {
                this.ProcessMtlFileTokens(tokens);
            }

            if (this.materialContent != null)
            {
                this.materials.Add(this.materialContent.Name, this.materialContent);
            }
        }

        /* Protected methods */



        /* Public methods */

        public override NodeContent Import(string pFileName, ContentImporterContext pContext)
        {
            //System.Diagnostics.Debugger.Launch();

            this.context = pContext;
            this.rootContent = new NodeContent();
            this.rootContent.Identity = new ContentIdentity(pFileName);
            this.positions = new List<Vector3>();
            this.textureCoords = new List<Vector2>();
            this.normals = new List<Vector3>();
            this.meshBuilder = null;
            this.materials = new Dictionary<string, MaterialContent>();

            foreach (string[] tokens in this.GetNextFileTokens(pFileName, this.rootContent.Identity))
            {
                this.ProcessObjFileTokens(tokens);
            }

            if (string.IsNullOrEmpty(this.rootContent.Name))
            {
                this.rootContent.Name = Path.GetFileNameWithoutExtension(pFileName);
            }

            this.FinishMesh();

            return this.rootContent;
        }

    }
}