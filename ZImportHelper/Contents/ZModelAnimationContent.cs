﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace ZImportHelper.Contents
{
    [ContentSerializerRuntimeType("ZImportData.Objects.ZModelAnimation, ZImportData")]
    public class ZModelAnimationContent
    {
        /* Fields */

        private TimeSpan duration;
        private int length;
        private Dictionary<int, Matrix[]> boneAnimations;

        private float keyframesPerSecond;

        /* Properties */

        public TimeSpan Duration
        {
            get
            {
                return this.duration;
            }
            set
            {
                this.duration = value;
                this.keyframesPerSecond = (float) this.boneAnimations.Count / (float) this.duration.TotalSeconds;
            }
        }
        public int Length
        {
            get
            {
                return this.length;
            }
        }
        public Dictionary<int, Matrix[]> BoneAnimations
        {
            get
            {
                return this.boneAnimations;
            }
            set
            {
                this.boneAnimations = value;
            }
        }

        /* Constructors */

        public ZModelAnimationContent()
        {
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        

    }
}