﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ZImportHelper.Contents.Data
{
    public class ZModelMeshGeometryDataContent
    {
        /* Fields */

        private List<int> positionIndices;
        private List<ZChannelContent> channels;
        private Dictionary<string, int> channelIndices;

        /* Properties */

        public List<int> PositionIndices
        {
            get
            {
                return this.positionIndices;
            }
        }
        public List<ZChannelContent> Channels
        {
            get
            {
                return this.channels;
            }
        }
        public Dictionary<string, int> ChannelIndices
        {
            get
            {
                return this.channelIndices;
            }
        }

        /* Constructors */

        public ZModelMeshGeometryDataContent()
        {
            this.positionIndices = new List<int>();
            this.channels = new List<ZChannelContent>();
            this.channelIndices = new Dictionary<string, int>();
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public ZChannelContent CreateChannel(string pName, Type pValueType, int pValueCount = -1)
        {
            if (this.channelIndices.ContainsKey(pName))
            {
                throw new Exception();
            }

            ZChannelContent channel = pValueCount > -1 ? new ZChannelContent(pValueType, pValueCount) : new ZChannelContent(pValueType);
            this.channels.Add(channel);
            this.channelIndices.Add(pName, this.channels.Count - 1);

            return channel;
        }
        public void AddVertex(int pPositionIndex)
        {
            this.positionIndices.Add(pPositionIndex);
            foreach (ZChannelContent channel in this.channels)
            {
                channel.Add(null);
            }
        }
        public ZChannelContent GetChannel(int pIndex)
        {
            ZChannelContent channel = null;
            if (pIndex >= 0 && pIndex < this.channels.Count)
            {
                channel = this.channels[pIndex];
            }

            return channel;
        }
        public ZChannelContent GetChannel(string pName)
        {
            ZChannelContent channel = null;
            int channelIndex = -1;
            if (this.channelIndices.TryGetValue(pName, out channelIndex))
            {
                channel = this.channels[channelIndex];
            }

            return channel;
        }

    }
}