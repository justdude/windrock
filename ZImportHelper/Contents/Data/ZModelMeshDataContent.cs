﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ZImportHelper.Contents.Data
{
    public class ZModelMeshDataContent
    {
        /* Fields */

        private List<Vector3> positions;
        private List<ZModelMeshGeometryDataContent> geometries;

        /* Properties */

        public List<Vector3> Positions
        {
            get
            {
                return this.positions;
            }
        }
        public List<ZModelMeshGeometryDataContent> Geometries
        {
            get
            {
                return this.geometries;
            }
        }
        public int GeometriesCount
        {
            get
            {
                return this.geometries.Count;
            }
        }

        /* Constructors */

        public ZModelMeshDataContent()
        {
            this.positions = new List<Vector3>();
            this.geometries = new List<ZModelMeshGeometryDataContent>();
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public void AddPosition(Vector3 pPosition)
        {
            this.positions.Add(pPosition);
        }
        public void AddGeometry(ZModelMeshGeometryDataContent pGeometry)
        {
            this.geometries.Add(pGeometry);
        }

    }
}