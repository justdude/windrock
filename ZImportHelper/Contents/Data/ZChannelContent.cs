﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZImportHelper.Contents.Data
{
    public class ZChannelContent
    {
        /* Fields */

        private Type valueType;
        private List<object> values;

        /* Properties */

        public Type ValueType
        {
            get
            {
                return this.valueType;
            }
        }
        public List<object> Values
        {
            get
            {
                return this.values;
            }
        }

        /* Constructors */

        public ZChannelContent(Type pValueType)
        {
            this.valueType = pValueType;
            this.values = new List<object>();
        }
        public ZChannelContent(Type pValueType, int pValueCount)
        {
            this.valueType = pValueType;
            this.values = new List<object>(pValueCount);
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public void Add(object pValue)
        {
            if (pValue != null
                && pValue.GetType() != this.valueType)
            {
                throw new Exception("Invalid value type.");
            }

            this.values.Add(pValue);
        }
        public void Set(int pIndex, object pValue)
        {
            if (pIndex < 0 || pIndex >= this.values.Count)
            {
                throw new IndexOutOfRangeException();
            }

            if (pValue.GetType() != this.valueType)
            {
                throw new Exception("Invalid value type.");
            }

            this.values[pIndex] = pValue;
        }

    }
}