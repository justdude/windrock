﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ZImportHelper.Contents.Data
{
    [ContentSerializerRuntimeType("ZImportData.Objects.ZModelData, ZImportData")]
    public class ZModelDataContent
    {
        /* Fields */

        protected List<ZModelMeshDataContent> meshes;
        protected Dictionary<string, int> meshIndices;

        /* Properties */

        [ContentSerializer]
        public List<ZModelMeshDataContent> Meshes
        {
            get
            {
                return this.meshes;
            }
        }
        public Dictionary<string, int> MeshIndices
        {
            get
            {
                return this.meshIndices;
            }
            set
            {
                this.meshIndices = value;
            }
        }
        public int MeshCount
        {
            get
            {
                return this.meshes.Count;
            }
        }

        /* Constructors */

        public ZModelDataContent()
        {
            this.meshes = new List<ZModelMeshDataContent>();
            this.meshIndices = new Dictionary<string, int>();
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */

        public void AddMesh(ZModelMeshDataContent pMesh, string pName)
        {
            if (this.meshIndices.ContainsKey(pName))
            {
                throw new Exception();
            }

            this.meshes.Add(pMesh);
            this.meshIndices.Add(pName, this.meshes.Count - 1);
        }

    }
}