﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace ZImportHelper.Contents
{
    [ContentSerializerRuntimeType("ZImportData.Objects.ZModelSkeleton, ZImportData")]
    public class ZModelSkeletonContent
    {
        /* Fields */

        private Dictionary<string, int> boneIndices;
        private int[] parentBoneIndices;
        private int boneCount;
        private Matrix[] relativeBindPoses;
        private Matrix[] inverseAbsoluteBindPoses;

        /* Properties */

        public Dictionary<string, int> BoneIndices
        {
            get
            {
                return this.boneIndices;
            }
            set
            {
                this.boneIndices = value;
            }
        }
        public int[] ParentBoneIndices
        {
            get
            {
                return this.parentBoneIndices;
            }
            set
            {
                this.parentBoneIndices = value;
            }
        }
        public int BoneCount
        {
            get
            {
                return this.boneCount;
            }
            set
            {
                this.boneCount = value;
            }
        }
        public Matrix[] RelativeBindPoses
        {
            get
            {
                return this.relativeBindPoses;
            }
            set
            {
                this.relativeBindPoses = value;
            }
        }
        public Matrix[] InverseAbsoluteBindPoses
        {
            get
            {
                return this.inverseAbsoluteBindPoses;
            }
            set
            {
                this.inverseAbsoluteBindPoses = value;
            }
        }

        /* Constructors */

        public ZModelSkeletonContent()
        {
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}