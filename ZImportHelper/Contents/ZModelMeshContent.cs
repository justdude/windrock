﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace ZImportHelper.Contents
{
    [ContentSerializerRuntimeType("ZImportData.Objects.ZModelMesh, ZImportData")]
    public class ZModelMeshContent
    {
        /* Fields */

        private Matrix transform;
        private List<ZModelMeshPartContent> parts;

        /* Properties */

        public Matrix Transform
        {
            get
            {
                return this.transform;
            }
            set
            {
                this.transform = value;
            }
        }
        [ContentSerializer]
        public List<ZModelMeshPartContent> Parts
        {
            get
            {
                return this.parts;
            }
            set
            {
                this.parts = value;
            }
        }

        /* Constructors */

        public ZModelMeshContent()
        {
            this.parts = new List<ZModelMeshPartContent>();
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}