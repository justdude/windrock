﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace ZImportHelper.Contents
{
    [ContentSerializerRuntimeType("ZImportData.Objects.ZModel, ZImportData")]
    public class ZModelContent
    {
        /* Fields */

        protected List<ZModelMeshContent> meshes;
        protected Dictionary<string, int> meshIndices;


        /* Properties */

        [ContentSerializer]
        public List<ZModelMeshContent> Meshes
        {
            get
            {
                return this.meshes;
            }
            set
            {
                this.meshes = value;
            }
        }
        public Dictionary<string, int> MeshIndices
        {
            get
            {
                return this.meshIndices;
            }
            set
            {
                this.meshIndices = value;
            }
        }

        /* Constructors */

        public ZModelContent()
        {
            this.meshes = new List<ZModelMeshContent>();
            this.meshIndices = new Dictionary<string, int>();
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}