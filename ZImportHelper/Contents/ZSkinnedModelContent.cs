﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using ZImportHelper.Contents;

namespace ZImportHelper.Contents
{
    public class ZSkinnedModelContent
    {
        /* Fields */

        protected List<ZModelMeshContent> meshes;
        private ZModelSkeletonContent skeleton;

        /* Properties */

        [ContentSerializer]
        public List<ZModelMeshContent> Meshes
        {
            get
            {
                return this.meshes;
            }
            set
            {
                this.meshes = value;
            }
        }
        public ZModelSkeletonContent Skeleton
        {
            get
            {
                return this.skeleton;
            }
            set
            {
                this.skeleton = value;
            }
        }

        /* Constructors */

        public ZSkinnedModelContent()
        {
            this.meshes = new List<ZModelMeshContent>();
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}