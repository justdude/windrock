﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content;

namespace ZImportHelper.Contents
{
    [ContentSerializerRuntimeType("ZImportData.Objects.ZModelMeshPart, ZImportData")]
    public class ZModelMeshPartContent
    {
        /* Fields */

        private int verticesCount;
        private int trianglesCount;
        private VertexBufferContent vertexBufferContent;
        private IndexCollection indexCollection;
        private MaterialContent materialContent;

        /* Properties */

        public int VerticesCount
        {
            get
            {
                return this.verticesCount;
            }
            set
            {
                this.verticesCount = value;
            }
        }
        public int TrianglesCount
        {
            get
            {
                return this.trianglesCount;
            }
            set
            {
                this.trianglesCount = value;
            }
        }
        public VertexBufferContent VertexBufferContent
        {
            get
            {
                return this.vertexBufferContent;
            }
            set
            {
                this.vertexBufferContent = value;
            }
        }
        public IndexCollection IndexCollection
        {
            get
            {
                return this.indexCollection;
            }
            set
            {
                this.indexCollection = value;
            }
        }
        [ContentSerializer(SharedResource = true)]
        public MaterialContent MaterialContent
        {
            get
            {
                return this.materialContent;
            }
            set
            {
                this.materialContent = value;
            }
        }

        /* Constructors */

        public ZModelMeshPartContent()
        {
        }

        /* Private methods */



        /* Protected methods */



        /* Public methods */



    }
}