float4x4 WorldViewProjection;

Texture2D TextureMap;
sampler2D TextureMapSampler = sampler_state
{
	Texture = <TextureMap>;
	MinFilter = point;
	MagFilter = point;
	MipFilter = point;
};

// // //

struct VertexShaderInput
{
	float4 Position : position;
	float4 Color : color0;
	float2 TexCoord : texcoord0;
};

struct VertexShaderOutput
{
	float4 Position : position;
	float4 Color : color0;
	float2 TexCoord : texcoord0;
};

VertexShaderOutput VertexShader1(VertexShaderInput pInput)
{
  VertexShaderOutput output;

  output.Position = mul(pInput.Position, WorldViewProjection);
  output.Color = pInput.Color;
  output.TexCoord = pInput.TexCoord;
  
  return output;
}

float4 PixelShader1(VertexShaderOutput pInput) : color0
{
	float4 texColor = tex2D(TextureMapSampler, pInput.TexCoord);

	clip(texColor.a);

    return texColor;
}

technique Technique1
{
    pass Pass1
    {
        VertexShader = compile vs_2_0 VertexShader1();
        PixelShader = compile ps_2_0 PixelShader1();
    }
}
