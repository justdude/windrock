float4x4 WorldViewProjection;
float4 AmbientColor;
float AmbientIntencity;

struct Input
{
	float4 Position : POSITION0;
	float2 TexCoord : TEXCOORD0;
};

struct Output
{
	float4 Position : POSITION0;
	float2 TexCoord : TEXCOORD0;
};

Texture2D Texture;
sampler2D TextureSampler = sampler_state
{
	Texture = <Texture>;
	MinFilter = point;
	MagFilter = point;
	MipFilter = point;
};

Output VertexShaderFunction( Input pInput )
{
	Output output;
	output.Position = mul(pInput.Position, WorldViewProjection);
	output.TexCoord = pInput.TexCoord;
	return output;
}

float4 PixelShaderFunction( Output pOutput ) : color0
{
	float4 texColor = tex2D( TextureSampler, pOutput.TexCoord );
	//return ( texColor * ( 1 - AmbientIntencity ) ) + ( AmbientColor * AmbientIntencity );
	return texColor;
}

technique Technique1
{
	pass Pass1
	{
		VertexShader = compile vs_2_0 VertexShaderFunction();
		PixelShader = compile ps_2_0 PixelShaderFunction();
	}
}