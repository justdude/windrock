float4x4 WorldViewProjection;
//float4 AmbientColor;
//float AmbientIntencity;
float4 FogColor;
float NearFogDistance;
float FarFogDistance;
float MinFogFactor;
float MaxFogFactor;
float3 CameraPosition;

struct Input
{
	float4 Position : position0;
	float2 TexCoord : texcoord0;
};

struct Output
{
	float4 Position : position0;
	float2 TexCoord : texcoord0;
};

Texture2D Texture;
sampler2D TextureSampler = sampler_state
{
	Texture = <Texture>;
	MinFilter = point;
	MagFilter = point;
	MipFilter = point;
};

Output MainVertexShader( Input pInput )
{
	Output output;
	output.Position = mul(pInput.Position, WorldViewProjection);
	output.TexCoord = pInput.TexCoord;
	return output;
}

float4 MainPixelShader( Output pInput ) : color0
{
	float4 texColor = tex2D( TextureSampler, pInput.TexCoord );
	float fogDistance = length(pInput.Position - CameraPosition);
	float fogFactor = 0.9f;
		//fogDistance > FarFogDistance ? MaxFogFactor :
		//MinFogFactor + (MaxFogFactor - MinFogFactor) * (fogDistance - NearFogDistance) / (MaxFogFactor - MinFogFactor);
    
	return texColor * (1 - fogFactor) + FogColor * fogFactor;
}

technique Technique1
{
	pass Pass1
	{
		VertexShader = compile vs_2_0 MainVertexShader();
		PixelShader = compile ps_2_0 MainPixelShader();
	}
}