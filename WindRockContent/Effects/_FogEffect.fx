float4x4 World;
float4x4 View;
float4x4 Projection;
float CameraPosition;

float4 FogColor;
float NearFogDistance;
float FarFogDistance;
float MinFogFactor;
float MaxFogFactor;

 //float OneDivFogDistanceRange = 1.0f / (MaxFogFactor - MinFogFactor);
 //float FogFactorRange = MaxFogfactor - MinFogFactor;

struct VertexShaderInput
{
	float4 Position : position0;
	float4 Color : color0;
};

struct VertexShaderOutput
{
	float4 Position : position0;
	float4 Color : color0;
};

VertexShaderOutput MainVertexShader(VertexShaderInput pInput)
{
  VertexShaderOutput output;

  float4 worldPosition = mul(pInput.Position, World);
  float4 viewPosition = mul(worldPosition, View);
  output.Position = mul(viewPosition, Projection);
  output.Color = pInput.Color;
  //float4(0.5, 0.5, 0.5, 1); //
  
  return output;
}

float4 MainPixelShader(VertexShaderOutput pInput) : COLOR0
{
	float fogDistance = length(pInput.Position - CameraPosition);
	float fogFactor = 0.5f;
		//fogDistance < NearFogDistance ? MinFogFactor:
		//fogDistance > FarFogDistance ? MaxFogFactor:
		////MinFogFactor + (MaxFogfactor - MinFogFactor) * (fogDistance - MinFogDistance) / (MaxFogDistace - MinFogDistance)
		//MinFogFactor + (MaxFogFactor - MinFogFactor) * (fogDistance - NearFogDistance) * (1.0f / (MaxFogFactor - MinFogFactor));
    
	return pInput.Color * (1 - fogFactor) + FogColor * fogFactor;
}

technique Technique1
{
    pass Pass1
    {
        // TODO: set renderstates here.

        VertexShader = compile vs_2_0 MainVertexShader();
        PixelShader = compile ps_2_0 MainPixelShader();
    }
}
