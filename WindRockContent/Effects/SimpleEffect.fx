float4x4 World;
float4x4 View;
float4x4 Projection;

struct VertexShaderInput
{
	float4 Position : position0;
	float4 Color : color0;
};

struct VertexShaderOutput
{
	float4 Position : position0;
	float4 Color : color0;
};

VertexShaderOutput MainVertexShader(VertexShaderInput pInput)
{
  VertexShaderOutput output;

  float4 worldPosition = mul(pInput.Position, World);
  float4 viewPosition = mul(worldPosition, View);
  output.Position = mul(viewPosition, Projection);
  output.Color = pInput.Color;
  //float4(0.5, 0.5, 0.5, 1); //
  
  return output;
}

float4 MainPixelShader(VertexShaderOutput pInput) : COLOR0
{
    return pInput.Color;
}

technique Technique1
{
    pass Pass1
    {
        // TODO: set renderstates here.

        VertexShader = compile vs_2_0 MainVertexShader();
        PixelShader = compile ps_2_0 MainPixelShader();
    }
}
